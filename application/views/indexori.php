   
<!--banner top-->
<div class="da-da BannerSearchTopIndex">
    <div class="da-da wh100"    >
        <div class="da-da wh100">

            <!--position relative-->
            <div class="da-da BannerSearchTopIndexRel">
                <div class="da-da wh100">
                    <div class="da-da wh100">


                        <!--banner image-->
                        <div class="da-da BannerSearchTopIndexRel-banner">
                            <div class="da-da wh100">
                                <div class="da-da wh100">
                                    <!--banner starts-->
                                    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                        <div class="carousel-inner">
                                            <?php $i = 0 ?>
                                            <?php foreach ($slider

                                            as $slid): ?>
                                            <?php if ($i == 0){ ?>
                                            <div class="carousel-item active">
                                                <?php }else{ ?>
                                                <div class="carousel-item ">
                                                    <?php } ?>
                                                    <img class="d-block w-100"
                                                         src="<?php echo base_url() ?>upload/slider/<?php echo $slid['image']; ?>"
                                                         alt="First slide">

                                                </div>
                                                <?php $i++; ?>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>
                                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                    <!--banner starts-->
                                </div>
                            </div>
                        </div>
                        <!--banner image-->

                        <!--search option menu-->
                        <div class="da-da SearchConfigurationIndex">
                            <div class="da-da wh100">
                                <div class="da-da wh100 SearchConfigurationIndexBg">
                                    <!--container-->
                                    <div class="da-da container h100">
                                        <div class="da-da wh100">
                                            <form action="<?php echo base_url('home/results')?>" method="POST">
                                            <div class="da-da wh100 SearchConfigurationIndexSpanvhmdDv">
                                                <div class="da-da wh100 SearchConfigurationIndexSpanvhmd maxwidth780">

                                                    <!--title-->
                                                    <div class="da-da DefaultTitleOnindexbanner">
                                                        <div class="da-da wh100">
                                                            <div class="da-da wh100">
                                                                <h3>Buying a car?</h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--title-->

                                                    <!--search bar-->
                                                    <div class="da-da searchBarIndex">
                                                        <div class="da-da wh100">
                                                            <div class="da-da wh100 displayFlex">
                                                                <input type="text" id="txtContent" name="searchbarinput" placeholder="I am looking for.." class="da-da InputFieldAppIndex isseachInputFieldAppIndex">
                                                                <input type="submit" class="da-da InputFieldAppIndexBtn IssearchInputFieldAppIndexBtn" value="Search">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--search bar-->

                                                    <!--title-->
                                                    <div class="da-da DefaultTitleOnindexbanner DefaultTitleOnindexbanner2nd">
                                                        <div class="da-da wh100">
                                                            <div class="da-da wh100">
                                                                <h3>Browse by</h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--title-->

                                                    <!--filter options-->
                                                    <div class="da-da searchformFilerIndex">
                                                        <div class="da-da wh100">
                                                            <div class="da-da wh100">
                                                                <ul class="da-da inputFieldIsonFormUlMain">
                                                                    <li>
                                                                        <select class="da-da wh100 inputFieldIsonForm" name="ddlContent" id="ddlContent"  onchange="SelectedTextValue(this)">
                                                                            <option selected="selected">Type</option>
                                                                            <?php foreach($vehicletype as $type): ?>
                                                                                <option value="<?php echo $type['vehicle_type_name']; ?>"><?php echo $type['vehicle_type_name']; ?></option>
                                                                            <?php endforeach; ?>
                                                                        </select>
                                                                    </li>
                                                                    <li>
                                                                        <select class="da-da wh100 inputFieldIsonForm" data-id="vehicle_brand" id="vehicle_brand" name="vehicle_brand"  id="ddlContent">
                                                                            <option>Make</option>
                                                                            <?php foreach($vehiclebrand as $brand): ?>
                                                                                <option value="<?php echo $brand['vehicle_brand_name']; ?>" data-id="<?php echo $brand['vehicle_brand_id']; ?>"><?php echo $brand['vehicle_brand_name']; ?> </option>
                                                                            <?php endforeach; ?>
                                                                        </select>
                                                                    </li>
                                                                    <li>
                                                                        <select class="da-da wh100 inputFieldIsonForm" name="select2" data-id="select2" id="select2" id="ddlContent"  disabled>
                                                                            <option>Model</option>
                                                                            <?php foreach($vehiclemodel as $vehicles): ?>
                                                                                <option value="<?php echo $vehicles['vehicle_model_name']; ?>" class="<?php echo $vehicles['vehicle_brand_id']; ?>"><?php echo $vehicles['vehicle_model_name']; ?></option>
                                                                            <?php endforeach; ?>
                                                                        </select>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--filter options-->


                                                    <!--title-->
                                                    <div class="da-da DefaultTitleOnindexbanner">
                                                        <div class="da-da wh100">
                                                            <div class="da-da wh100">
                                                                <h3>Selling a car? <a href="#" class="da-da BtnIntheIndexSelACar" id="popup1sellingopen">Sell a car</a> </h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--title-->

                                                </div>
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                    <!--container-->
                                </div>
                            </div>
                        </div>
                        <!--search option menu-->

                    </div>
                </div>
            </div>
            <!--position relative-->

        </div>
    </div>
</div>
<!--banner top-->


<!--category 5 types-->
<div class="da-da container">
    <div class="da-da wh100"    >
        <div class="da-da wh00 positionrelative">
            <div class="da-da categoryList5OnIndex">
                <div class="da-da wh100"    >
                    <div class="da-da wh100">
                        <!--content-->
                        <div class="da-da maxwidth980 categoryList5OnIndexConTnt">
                            <div class="da-da wh100">

                                <!--box 5-->
                                <ul class="da-da categoryList5OnIndexConTntUl">

                                    <?php foreach ($grid as $data): ?>
                                        <!--one category-->
                                        <li>
                                            <div class="paddingadjustedforCagry">
                                                <!--image-->
                                                <div class="da-da categoryList5OnIndexConTntImg">
                                                    <img src="<?php echo base_url() ?>upload/grid/<?php echo $data['image']; ?>">
                                                </div>
                                                <!--image-->
                                                <!--title-->
                                                <div class="da-da categoryList5OnIndexConTntTitle">
                                                    <h4>
                                                        <?php echo $data['title']; ?>
                                                    </h4>
                                                </div>
                                                <!--title-->
                                                <!--description-->
                                                <div class="da-da categoryList5OnIndexConTntdescrptn">
                                                    <p>
                                                        <?php echo $data['description']; ?>
                                                    </p>
                                                </div>
                                                <!--description-->

                                                <!--button-->
                                                <div class="da-da categoryList5OnIndexConTntdescrptn">
                                                    <a href="<?php echo $data['link']; ?>"><?php echo $data['button'];?>
                                                        <i
                                                                class="fas fa-caret-right"></i></a>
                                                </div>
                                                <!--button-->
                                            </div>
                                        </li>
                                        <!--one category-->
                                    <?php endforeach; ?>




                                </ul>
                                <!--box 5-->

                            </div>
                        </div>
                        <!--content-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--category 5 types-->


<!--container-->
<div class="da-da container">



    <!--header-->
    <div class="da-da DifFontHeader maxwidth980">
        <div class="da-da wh100">
            <div class="da-da wh100">
                <h3 class="da-da DifFontHeaderH3">Where your dream come true</h3>
                <p class="da-da DifFontHeaderP">
                Autosure is backed by Alliance Finance Co, PLC, Sri Lanka’s 3rd oldest financial institution providing customers, with 60 years of Financial & Motoring expertise to make the correct choice in purchasing a vehicle.
                    <!--The first online automobile mall in Sri-Lanka. Best choice for buying and selling cars, vans, bikes, trucks, SUVs, vehicle parts and to be updated with the latest news in Sri-Lanka's Car market.-->
                </p>
                <a href="<?php echo base_url('home/about_us') ?>" class="da-da DifFontHeaderBtn">About Us</a>
            </div>
        </div>
    </div>
    <!--header-->


</div>
<!--container-->



<!--products list-->
<div class="da-da container">
    <div class="da-da wh100"    >
        <div class="da-da wh100" style="
                overflow: auto;
            ">

            <!--products starts-->
            <div class="da-da IndexProductsListsSatsrs maxwidth980">
                <div class="da-da wh100">
                    <div class="da-da wh100 displayFlex" style="
                            overflow: auto;
                            padding-top: 10px;    background: #f1f1f1;
                        ">

                        <?php foreach ($vehicle as $value): ?>

                        <?php

                        $year_of_manufacture = $value['year_of_manufacture'];
                        $vehicle_transmission = $value['vehicle_transmission'];
                        $vehicle_mileage_range = $value['vehicle_mileage_range'];
                        $vehicle_total_value = $value['vehicle_total_value'];
                        $vehicle_total_value1 = (int)$vehicle_total_value;
                        $vehicle_status =$value['vehicle_status'];
                        
                        ?>

                        <!--one product-->
                        
                        <?php if($vehicle_status == 1) { ?>
                        <div class="da-da one-products-homepage brandNewSign">
                            <?php }else{ ?>
                            <div class="da-da one-products-homepage">
                                <?php } ?>
                                <div class="da-da wh100">
                                    <div class="da-da wh100">
                                   
                                        <!--image--><a href="<?php echo base_url('home/viewvehicle/'.$value['vehicle_id'].'/1');?>">
                                       <div class="da-da one-products-homepageImage product-image" style="background-image: url(<?php echo base_url() ?>upload/selling/<?php echo $value['inside_images_name']; ?>)">
                                       <div class="watermakerd">
                                        </div>
					
                                        </div></a>
                                        <!--image-->

                                        <!--!info tag-->
                                        <div class="da-da one-products-homepageinftag">
                                            <span><img src="<?php echo base_url('assets/frontend/') ?>assets/index-products-ico/calender.png"> </span>
                                            <?php echo $year_of_manufacture; ?>
                                        </div>
                                        <!--!info tag-->

                                        <!--!info tag-->
                                        <div class="da-da one-products-homepageinftag">
                                            <span><img src="<?php echo base_url('assets/frontend/') ?>assets/index-products-ico/dashboard.png"> </span>
                                            <?php echo $vehicle_mileage_range; ?> 
                                        </div>
                                        <!--!info tag-->

                                        <!--!info tag-->
                                        <div class="da-da one-products-homepageinftag">
                                            <span><img src="<?php echo base_url('assets/frontend/') ?>assets/index-products-ico/auto.png"> </span>
                                            <?php echo $vehicle_transmission; ?>
                                        </div>
                                        <!--!info tag-->

                                        <!--!info tag-->
                                        <div class="da-da one-products-homepageinftag Isprice334">
                                            <span><img src="<?php echo base_url('assets/frontend/') ?>assets/index-products-ico/label.png"> </span>
                                            <?php echo number_format($vehicle_total_value1)."";?>
                                        </div>
                                        <!--!info tag-->

                                    </div>
                                </div>
                            </div>
			<?php endforeach; ?>
                            
                        </div>
                        
                        

                    </div>
                </div>
            </div>
            <!--products starts-->

        </div>
    </div>
</div>
<!--products list-->


<!--sign up banner-->
<div class="da-da SignUpGreen">
    <div class="da-da wh100"    >
        <div class="da-da container">
            <div class="da-da  maxwidth980">
                <div class="row">

                    <div class="col-md-8 displayFlex">
                        <div class="da-da SignUpGreenLeftSide">
                            <img src="<?php echo base_url('assets/frontend/');?>assets/sign-up-area/sign-up.png">
                        </div>
                        <div class="da-da SignUpGreenRightSide">
                            <h3>SIGN UP FOR UPATES</h3>
                            <p>Will update you with all our exciting new offers as we introduce them</p>
                        </div>
                    </div>
                    <div class="col-md-4 SignUpGreenTxtdtcr">
                        <a href="#" class="cta btn-green sign-up" id="popupisLoginPop1">SIGN UP</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!--sign up banner-->




</body>
</html>
