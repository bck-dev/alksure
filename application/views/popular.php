<style>
.btn-is-disabled {
  pointer-events: none; / Disables the button completely. Better than just cursor: default; /
  @include opacity(0.7);
}
</style>

<div class="page-content">
        <!-- search results -->
<section class="search-results floating-section">
    <div class="container">
        <div class="results-container">
            <div class="column filter-column">
                <a href="" class="refine-by">Refine by</a>

                <form action="" method="POST">
                    <select name="" id="vehicle_type1" class="vehitype" data-id="vehicle_type1">
                        <option value="featured">Selected Type</option>
                        <option value="" data-id="aa" id="all">All</option>
                        <?php foreach ($vehicletype as $type): ?>
                            <option value="<?php echo $type['vehicle_type_id']; ?>"
                                    data-id="<?php echo $type['vehicle_type_id']; ?>"><?php echo $type['vehicle_type_name']; ?></option>
                        <?php endforeach; ?>
                    </select>

                    <div id="vehiclew">
                        <select class="form-control brand" id="vehicle_brand1" name="vehicle_brand"
                                data-id="vehicle_brand1" disabled>
                            <option value=""> Select Brand</option>
                            <?php foreach ($vehiclebrand as $brand): ?>
                                <option value="<?php echo $brand['vehicle_brand_id']; ?>"
                                        class="<?php echo "aa".$brand['vehicle_type_id']."aa"; ?>"
                                        data-id="<?php echo $brand['vehicle_brand_id']; ?>"><?php echo $brand['vehicle_brand_name']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <select class="form-control brand" name="select1" id="select1" data-id="select1" disabled>
                        <option value=" ">Select Model</option>
                        <?php foreach ($vehiclemodel as $vehiclemodel): ?>
                            <option value="<?php echo $vehiclemodel['vehicle_model_id']; ?>"
                                    class="<?php echo "aa".$vehiclemodel['vehicle_brand_id']."aa"; ?>" data-id="<?php echo $vehiclemodel['vehicle_model_id'];?>"><?php echo $vehiclemodel['vehicle_model_name']; ?></option>
                        <?php endforeach; ?>
                    </select>                   
                </form>

                  <div class="compare-wrapper  aa" id="com" cursor: not-allowed;>
                        <div class="compare-vehicles">Vehicles Added: <span class="selected"></span>/5</div>
                        <a href="<?php echo base_url('home/comparison');?>" class="cta btn-green compare-btn btn-is-disabled" style=" background-color: gray !important; border-radius:0px !important;  
 display: block !important; padding: 5px 15px !important; margin-top:0px !important; cursor: not-allowed;">compare</a>
                    </div> 
                
            </div>
            <div class="column results-column">
        
                <div class="results-wrap" id="results-wrap">
                    <?php $count = 0; ?>
                        <?php foreach($results as $value):?>
                            <?php $count++; ?>
                        <?php endforeach;?>

                        <p class="results-count"><?php if ($count == 0) {
                            echo 'No';
                        } else {
                            echo $count;
                        } ?>&nbsp;Vehicles</p>

                        <?php foreach ($results as $value): ?>

                            <?php

                            $year_of_manufacture = $value['year_of_manufacture'];
                            $vehicle_id = $value['vehicle_id'];
                            $vehicle_total_value = $value['vehicle_total_value'];
                            $vehicle_total_value1 = (int)$vehicle_total_value;
                            $vehicle_transmission = $value['vehicle_transmission'];
                            $vehicle_model = $value['vehicle_model_name'];
                            $vehicle_mileage_range = $value['vehicle_mileage_range'];
                            $vehicle_brand_name = $value['vehicle_brand_name'];
                            $fuel_type = $value['fuel_type'];
                            $id = $value['vehicle_id'];
                            $publish_status = $value['publish_status'];
                            $reject_status = $value['reject_status'];
                            $autosure_certified = $value['autosure_certified'];

                            ?>

                        <?php if ($publish_status == 1 && $reject_status == 0) { ?>
                           
                    <div class="result">
                        <figure>
                            <div class="watermakerd">
                                <img class="waterimg" src="<?php echo base_url(); ?>upload/selling/Front/<?php echo $value['inside_360_images']; ?>"style="width: 400px;height: auto; margin: 0 auto; display: flex;">
                            </div>
                        </figure>
                        <div class="right-side">
                            <p class="product-name"><?php echo $vehicle_brand_name; ?> 
                                <?php echo $vehicle_model; ?> 
                                <?php if($autosure_certified==1): ?>
                                    <img src="<?php echo base_url('assets/')?>certified.png" class="certified" alt=""/>
                                <?php endif; ?>
                            </p>
                            <div class="price">
                                <img src="<?php echo base_url();?>assets/frontendone/images/misc/price-tag.png" alt=""/>
                                <p>Rs. <?php echo number_format($vehicle_total_value1) . ""; ?></p>
                                <img src="<?php echo base_url('assets/')?>drive-away.png" class="drive-away" alt=""/>
                            </div>

                            <div class="main-features">
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url();?>assets/frontendone/images/misc/calender.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Year</p>
                                        <p><?php echo $year_of_manufacture; ?></p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url();?>assets/frontendone/images/misc/dashboard.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Milage</p>
                                        <p><?php if($value['millage_range_unit']==1){
                                                    echo $value['vehicle_mileage_range']. "Km";
                                                }
                                                else{
                                                    echo $value['vehicle_mileage_range']. "mil";
                                                }
                                        ?></p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url();?>assets/frontendone/images/misc/auto.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Transmission</p>
                                        <p><?php if($vehicle_transmission==1){
                                                echo "Automatic";
                                            } elseif($vehicle_transmission==2){
                                                echo "Manual";
                                            }
                                            else{
                                                echo "Triptonic";
                                            } ?></p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url();?>assets/frontendone/images/misc/auto.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Fuel Type</p>
                                        <p><?php if($fuel_type==1){
                                                echo "Petrol";
                                            }
                                            elseif($fuel_type==2){
                                                echo "Diesel";
                                            }
                                            elseif($fuel_type==3){
                                                echo "Hybrid";
                                            }
                                            else{
                                                echo "Electric";
                                            }  ?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="cta-group">
                                <a href="<?php echo base_url('home/viewvehicle/' . $value['vehicle_id']).'/3' ?>" class="cta btn-green">VIEW</a>
                                <a href="" class="cta enquire checkresult open-popup" data-id="<?php echo $value['vehicle_id']; ?>">INQUIRE</a>
                                <a href="" class="add-to-compare compare" value="<?php echo $value['vehicle_id']; ?>" data-hotel-name='<?php echo $value['vehicle_id']; ?>'>
                                    <span class="plus-icon"><i class="fa fa-plus"></i></span>
                                    <span class="text">compare</span>
                                </a>
                            </div>
                        </div>
                    </div> 
                    
            <?php } ?>
        <?php endforeach ?>       
    </div>
    </div>
            <div class="column ads-column">
                <?php foreach ($ad as $a){ ?>
                <a href="<?php echo $a['link'] ?>" class="ads-block" target="_blank">
                    <img src="<?php echo base_url(); ?>upload/advertisement/<?php echo $a['ad_image_name']; ?>" >
                </a>  
                <?php } ?>
            </div>
        </div>        
    </div>
</section>
<!-- end of search results -->
    </div>

<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">

    var base_url ='http://test.autosure.lk/';
var hotelName = []; 
 var i =0;
 $(".compare").click(function(e) {

     e.preventDefault(); 
      hotelName.push( $(this).data('hotel-name') );  
       
       var x =hotelName.toString();

       if(i>4){
        alert('only five vehicles can be selected');
       }else{
       i++;
  
       Cookies.set('yasas', x);
     }
        
   $.ajax({
            type: "POST",
            url: base_url + "home/divresult",
            //dataType: "JSON",
            data: {i:i},
            success: function(result) {
              //alert(results);
              $('#com').html(result);
           
                
            }

        });

   }); 

</script>

<script src="<?php echo base_url('assets/frontend');?>/assets/js/inquery.js"></script>
<!--<script src="<?php echo base_url('assets/frontendone/');?>js/lib.js"></script>-->
<!--<script src="<?php echo base_url('assets/frontendone/');?>js/main.js"></script>-->
    
<!-- enquire popup -->

<div class="popup enquire-popup" id="enquire-popup">
    
    <div class="popup-container">
        <div class="left-side">
            <form action="<?php echo base_url('home/insertiquery')?>" method="POST">
                <input type="text" name="fullname" placeholder="Full Name">
                <input type="email" name="address" placeholder="Email Address">
                <input type="text" name="phone" placeholder="Phone">
                <textarea name="comments" id="" placeholder="Comments"></textarea>
                <input type="hidden" id="resyltInqyry" class="resyltInqyry" name="vehicle_id" value="">
                <button type="submit">send inquiry</button>
            </form>
        </div>
        <div class="right-side">
            <h5>Call directly</h5>
            <p>Call one of our agents directly to reserve this vehicle or obtain more information.</p>

            <a href="tel:0766377666">(+94)766 377 666</a>
            <p>Contact between 9.00am - 5.00pm on weekdays and except for poyadays.</p>
        </div>
        <div class="popup-close"><i class="fa fa-close"></i></div>
    </div>
</div>


<!-- end of enquire popup -->

<!-- signup popup -->

<!-- end of signup popup -->

<!-- signin popup -->

<!-- end of signin popup -->

<!-- seller popup -->

<!-- end of seller popup -->

<!-- register popup -->

<!-- end of lease popup -->

<!-- </body>
</html> -->