<body>

<div class="da-da AppMainBody">
    <div class="da-da wh100">
        <div class="da-da wh100">

            <!--body starts-->
            <div class="da-da AppMainBodyHl">
                <div class="da-da wh100">
                    <div class="da-da wh100">








                    </div>
                </div>
            </div>
            <!--bottom menu-->







            <!--container-->
            <div class="da-da container" style="
    width: 918px;
">



                <!--header-->
                <div class="da-da DifFontHeader maxwidth980">
                    <div class="da-da wh100">
                        <div class="da-da wh100">
                            <h1 class="da-da DifFontHeaderH3" style="text-align: center;">About Us</h1>
                                <p class="da-da DifFontHeaderP" >
                                    Autosure is backed by Alliance Finance Co, PLC, Sri Lanka’s 3rd oldest financial institution providing customers, with 60 years of Financial & Motoring expertise to make the correct choice in purchasing a vehicle.
                                </p>

                            <p class="da-da DifFontHeaderP" >
                                Delivering Trust, Backed by 6 Years of Motoring Heritage, Autosure is a SBU of AFC launched as a new business vertical during the year, with the objective of leveraging on the synergies presented by the Company’s vehicle leasing business. With the necessary foundation now in place, we are well positioned to capture growth opportunities in this segment in which we see strong medium to long-term growth potential.
                            </p>

                            <p class="da-da DifFontHeaderP" >
                                The division intends to focus on three main areas of operations; vehicle trading, rental service (Easy drive) and a fully-fledged service center. A growing middle class and social aspirations towards owning a vehicle is expected to drive demand growth, particularly in the 1000CC vehicles which have relatively favorable tax structures. In order to drive this business, we have an industry specialist with over 18 years’ experience in vehicle trading who is supported by a team of dedicated and experienced employees. The division has also successfully partnered with several leading automobile retailers to facilitate vehicle exchange. We also hope to launch a fully-fledged vehicle service center and showroom.
                            </p>
                        </div>
                    </div>
                </div>
                <!--header-->


            </div>
            <!--container-->








        </div>
    </div>
</div>
<!--body starts-->

</div>
</div>
</div>


</body>
</html>