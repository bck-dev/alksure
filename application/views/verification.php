<!DOCTYPE html>
<html>

<!-- Mirrored from webapplayers.com/luna_admin-v1.3/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 27 Dec 2017 09:47:41 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900' rel='stylesheet' type='text/css'>

    <!-- Page title -->
    <title>LUNA | Responsive Admin Theme</title>

    <!-- Vendor styles -->
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/dist/vendor/fontawesome/css/')?>font-awesome.css"/>
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/dist/vendor/animate.css/')?>animate.css"/>
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/dist/vendor/bootstrap/css/')?>bootstrap.css"/>

    <!-- App styles -->
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/dist/css/')?>pe-icons/pe-icon-7-stroke.css"/>
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/dist/css/')?>pe-icons/helper.css"/>
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/dist/css/')?>stroke-icons/style.css"/>
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/dist/css/')?>style.css">
</head>
<body class="blank">

<!-- Wrapper-->
<div class="wrapper" >


    <!-- Main content-->
    <section class="content">
        <div class="back-link" >
            <!-- <a href="index-2.html" class="btn btn-accent">Back to Dashboard</a> -->
        </div>

        <div class="container-center animated slideInDown" style="background-image:  url('ccc.jpg');" >


            <div class="view-header">
                <div class="header-icon">
                    <i class="pe page-header-icon pe-7s-unlock"></i>
                </div>
                <div class="header-title">
                    <h3>Verification</h3>
                   
                </div>
            </div>

            <div class="panel panel-filled">
                 <div class="panel-body">

                    <form action='' method='post' name='process'>
        <?php echo $this->session->flashdata('msg'); ?>
        <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
          <div class="row">
            <div class="col-xs-8">
            </div><!-- /.col -->
            <div class="col-xs-4">
            </div><!-- /.col -->
          </div>
        </form>
         <a href="<?php echo site_url('home'); ?>">Go to Autosure WebSite</a><br>
                </div> 
                
            </div>

        </div>
    </section>
    <!-- End main content-->

</div>
<!-- End wrapper-->

<!-- Vendor scripts -->
<script src="<?php echo base_url('assets/admin/dist/vendor/')?>pacejs/pace.min.js"></script>
<script src="<?php echo base_url('assets/admin/dist/vendor/')?>jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url('assets/admin/dist/vendor/')?>bootstrap/js/bootstrap.min.js"></script>

<!-- App scripts -->
<script src="<?php echo base_url('assets/admin/dist/js/')?>luna.js"></script>

</body>


<!-- Mirrored from webapplayers.com/luna_admin-v1.3/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 27 Dec 2017 09:47:41 GMT -->
</html>