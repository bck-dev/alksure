<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<div class="page-content">

    <?php //var_dump($vehicle);?>
    </pre>
    <?php if (isset($_COOKIE["yasas"])){?>

    <?php $a= $_COOKIE['yasas'];
    $classes=explode(",",$a);
    // print_r(sizeof($classes));
    ?>
    <!-- vehicle comparison -->

    <section class="comparison">

        <div class="container">
            <div class="comparison-wrapper">

                <!-- <h3 class="section-title">Vehicle Comparison</h3> -->
                <a href="<?php echo base_url('home/operating');?>" class="" style="color: blue;">Back to Results</a>
                <table class="vehicle-compare tablesaw" data-tablesaw-mode="swipe" data-tablesaw-minimap>
                    <thead>
                    <th class="info-head" data-tablesaw-priority="persist"></th>
                    <th>
                        Vehicle 01
                    </th>
                    <th>
                        Vehicle 02
                    </th>
                    <th>
                        Vehicle 03
                    </th>
                    <th>
                        Vehicle 04
                    </th>
                    <th>
                        Vehicle 05
                    </th>
                    </thead>
                    <tbody>
                    <tr>
                        <td></td>
                        <?php
                        $i = 0;
                        foreach($classes as $b){
                            foreach ($vehicle as $key => $new_vehicle) {
                                if($b==$new_vehicle['vehicle_id']){
                                    if ($i >= 5) {
                                        break;
                                    }
                                    ?>
                                    <td>
                                        <img src="<?php echo base_url('upload/selling/Front/').$new_vehicle['inside_360_images'];?>" alt="">
                                    </td>
                                    <?php
                                    $i++;
                                }
                            }}
                        ?>
                        <!-- <td>
                            <img src="<?php echo base_url('assets/frontend/')?>images/compare/bmw-x1.jpg" alt="">
                        </td>
                        <td>
                            <img src="<?php echo base_url('assets/frontend/')?>images/compare/audi-q2.jpg" alt="">
                        </td>
                        <td>
                            <img src="<?php echo base_url('assets/frontend/')?>images/compare/volvo-xc60.jpg" alt="">
                        </td>
                        <td>
                            <img src="<?php echo base_url('assets/frontend/')?>images/compare/peugeot-3008.jpg" alt="">
                        </td>
                        <td>
                            <img src="<?php echo base_url('assets/frontend/')?>images/compare/peugeot-3008.jpg" alt="">
                        </td> -->
                        <!-- <td>
                            <a href="{{ site.pageurl }}/results.html" class="add-vehicle"><img src="<?php echo base_url('assets/frontend/')?>images/misc/add.svg" alt=""></a>
                        </td> -->
                    </tr>
                    <tr>
                        <td>Make</td>
                        <?php
                        $i = 0;
                        foreach($classes as $b){
                            foreach ($vehicle as $key => $new_vehicle) {
                                if($b==$new_vehicle['vehicle_id']){
                                    if ($i >= 5) {
                                        break;
                                    }
                                    ?>
                                    <td><?=$new_vehicle['vehicle_brand_name'];?></td>
                                    <?php

                                    $i++;
                                }
                            }}
                        ?>

                        <!-- <td>Audi</td>
                        <td>Volvo</td>
                        <td>Peugeot</td>
                        <td>Toyota</td> -->
                        <td></td>
                    </tr>
                    <tr>
                        <td>Mode</td>
                        <?php
                        $i = 0;
                        foreach($classes as $b){
                            foreach ($vehicle as $key => $new_vehicle) {
                                if($b==$new_vehicle['vehicle_id']){
                                    if ($i >= 5) {
                                        break;
                                    }
                                    ?>
                                    <td><?=$new_vehicle['vehicle_model_name'];?></td>
                                    <?php

                                    $i++;
                                }
                            }}
                        ?>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Transmission</td>
                        <?php
                        $i = 0;
                        foreach($classes as $b){
                            foreach ($vehicle as $key => $new_vehicle) {
                                if($b==$new_vehicle['vehicle_id']){
                                    if ($i >= 5) {
                                        break;
                                    }
                                    ?>
                                    <td><?php if($new_vehicle['vehicle_transmission']==1){
                                    echo "Automatic";
                                    
                                    }elseif($new_vehicle['vehicle_transmission']==2){
                                    echo "Manual";
                                    
                                    }else{
                                    echo "Triptonic";
                                    
                                    }?></td>
                                    <?php

                                    $i++;
                                }
                            }}
                        ?>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Milage</td>
                        <?php
                        $i = 0;
                        foreach($classes as $b){
                            foreach ($vehicle as $key => $new_vehicle) {
                                if($b==$new_vehicle['vehicle_id']){
                                    if ($i >= 5) {
                                        break;
                                    }
                                    ?>
                                    <td><?=$new_vehicle['vehicle_mileage_range'];?></td>
                                    <?php

                                    $i++;
                                }
                            }}
                        ?>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Engine</td>
                        <?php
                        $i = 0;
                        foreach($classes as $b){
                            foreach ($vehicle as $key => $new_vehicle) {
                                if($b==$new_vehicle['vehicle_id']){
                                    if ($i >= 5) {
                                        break;
                                    }
                                    ?>
                                    <td><?=$new_vehicle['vehicle_engine_capacity'];?></td>
                                    <?php

                                    $i++;
                                }
                            }}
                        ?>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Seats</td>
                        <?php
                        $i = 0;
                        foreach($classes as $b){
                            foreach ($vehicle as $key => $new_vehicle) {
                                if($b==$new_vehicle['vehicle_id']){
                                    if ($i >= 5) {
                                        break;
                                    }
                                    ?>
                                    <td><?=$new_vehicle['vehicle_seating_capacity'];?></td>
                                    <?php

                                    $i++;
                                }
                            }}
                        ?>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Price</td>
                        <?php
                        $i = 0;
                        foreach($classes as $b){
                            foreach ($vehicle as $key => $new_vehicle) {
                                if($b==$new_vehicle['vehicle_id']){
                                    if ($i >= 5) {
                                        break;
                                    }
                                    ?>
                                    <td>Rs. <?=number_format($new_vehicle['vehicle_total_value'], 2);?></td>
                                    <?php

                                    $i++;
                                }
                            }}
                        ?>
                        <td></td>
                    </tr>
                    <tr>
                       <td>
                            <div class="button-group">
                                <a href="" class="cta btn-green abc">Clear all</a>
                            </div>
                        </td>

                        <?php
                        $i = 0;
                        foreach($classes as $b){
                            foreach ($vehicle as $key => $new_vehicle) {
                                if($b==$new_vehicle['vehicle_id']){
                                    if ($i >= 5) {
                                        break;
                                    }
                                    ?>
                                    <td><a href="" class="cta enquire checkresult open-popup" data-id="<?php echo $new_vehicle['vehicle_id']; ?>">Inquiry</a></td>
                                    <?php

                                    $i++;
                                }
                            }}
                        ?>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
                <?php }else{?>

                    <?php echo 'no cars';?>
                <?php }?>

               
            </div>
        </div>
    </section>
    <!-- end of vehicle comparison -->
</div>




    
<!-- enquire popup -->

<script src="<?php echo base_url('assets/frontend');?>/assets/js/inquery.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/lib.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/main.js"></script>

<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>

<!-- enquire popup -->

<div class="popup enquire-popup" id="enquire-popup">
    <div class="popup-container">
        <div class="left-side">
            <form action="<?php echo base_url('home/insertiquery')?>" method="POST">
                <input type="text" name="fullname" placeholder="Full Name">
                <input type="email" name="address" placeholder="Email Address">
                <input type="text" name="phone" placeholder="Phone">
                <textarea name="comments" id="" placeholder="Comments"></textarea>
                <input type="hidden" id="resyltInqyry" class="resyltInqyry" name="vehicle_id" value="">
                <button type="submit">send inquiry</button>
            </form>
        </div>
        <div class="right-side">
            <h5>Call directly</h5>
            <p>Call one of our agents directly to reserve this vehicle or obtain more information.</p>

            <a href="tel:0766377666">(+94) 766 377 666</a>
            <p>Contact between 9.00am - 5.00pm on weekdays and except for poyadays.</p>
        </div>
        <div class="popup-close"><i class="fa fa-close"></i></div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
<script type="text/javascript">

    $(".abc").click(function(e) {
        e.preventDefault();
        // alert('hi');
        Cookies.set("yasas",null);


        location.reload();


    });

</script>