<script https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js></script>

<!DOCTYPE html>
<html lang="en">
	<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Autosure</title>

    <meta name="description" content="">
    <link rel="shortcut icon" href="">

    <link rel="stylesheet" href="<?php echo base_url('assets/frontend/'); ?>assets/css/lib.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/frontend/'); ?>assets/css/jquery-ui.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/frontend/'); ?>assets/css/main.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/frontend/'); ?>assets/css/style.css">
</head>
<body>
<body>
	

	<!-- search results -->
<section class="search-results view-vehicle import-lease">
    <div class="container">
        <div class="results-container">
            <div class="column test-column">
                <div class="test-drive">
                    <h4>Book a Test Drive</h4>

                    <div class="test-drive-info">
                        <div class="reservation-block">
                            <a href="tel:01123456263">(+94) 766 377 666</a>
                            <p>Call for reservations</p>
                        </div>

                        <p>We bring the vehicle to your door-step for inspections and for a test drive.</p>

                        <p class="note">*Conditions Apply</p>
                    </div>
                </div>
            </div>
            <div class="column results-column">
                <a href="results.html" class="back-to"><i class="fa fa-angle-left"></i> Back to results</a>
  <?php foreach($vehicle as $vehi): ?>

                    <?php foreach($type as $ty): 
                        if($ty['vehical_type_id']==$vehi['import_vehicle_type']):
                            $vtype = $ty['vehical_type_name'];
                        endif;
                    endforeach;
                       
                    foreach($brand as $br): 
                        if($br['vehicle_brand_id']==$vehi['import_brand_id']):
                            $vbrand = $br['vehicle_brand_name'];
                        endif;
                    endforeach;

                    foreach($model as $mod): 
                        if($mod['vehicle_model_id']==$vehi['import_model_id']):
                            $vmodal = $mod['vehicle_model_name'];
                        endif;
                    endforeach;

                    $id = $vehi['vehicle_id'];
                    $trans = $vehi['vehicle_transmission'];
                    $millge = $vehi['vehicle_mileage_range'];
                    // $p_d_price = $vehicle['per_day_price'];
                    $fuel_type = $vehi['fuel_type'];
                    // $disc = $vehicle['discount'];
                    $reg_year = $vehi['year_of_registration'];
                    $man_year = $vehi['year_of_manufacture'];
                    $capacity = $vehi['vehicle_engine_capacity'];
                    $total = $vehi['vehicle_total_value'];
                    $seating = $vehi['vehicle_seating_capacity'];
                    $fuel_con = $vehi['average_of_fuel_consuption'];
                    $downpay = $vehi['vehicle_minimum_downpayment'];
                    $engine_cap = $vehi['engine_capacity_unit'];
                    $fuel_con_unit = $vehi['fuel_consumption_unit'];
                    $millage_unit = $vehi['millage_range_unit'];
                    $sellers_description = $vehi['sellers_description'];
                    $owners = $vehi['number_of_owners'];
                    $downpay = $vehi['vehicle_minimum_downpayment'];

                    ?>
                  
                <div class="single-vehicle">
                    <p class="product-name"><?php echo $vbrand; ?> <?php echo $vmodal;?></p>
                    <p class="ref">Veh/Im/00<?php echo $id;?></p>

                    <div class="details">
                        <div class="single-vehicle-group">
                            <div class="owl-carousel single-vehicle-slider" data-slider-id="1">
                                <div class="item" style="background-image: url('<?php echo base_url(); ?>upload/import/inside/<?php echo $vehi['import_inside_image']; ?>')">
                                </div>
                             <?php foreach($outside_image as $imgs): 
                                if($imgs['vehicle_vehicle_id'] == $vehi['vehicle_id'] ):
                                    ?>    
                                <div class="item" style="background-image: url('<?php echo base_url(); ?>upload/import/outside_multi/<?php echo $imgs['out_side_image_name']; ?>')">
                                    
                                </div>
                               
                                <?php

                                    endif;
                                endforeach;
                                ?>
                            </div>
                            
                        <div class="owl-thumbs" data-slider-id="1">
                            <button class="owl-thumb-item" style="background-image: url('<?php echo base_url(); ?>upload/import/inside/<?php echo $vehi['import_inside_image']; ?>')"></button>
                            <?php foreach($outside_image as $imgs): 
                                if($imgs['vehicle_vehicle_id'] == $vehi['vehicle_id'] ):
                            ?>
                            <button class="owl-thumb-item" style="background-image: url('<?php echo base_url(); ?>upload/import/outside_multi/<?php echo $imgs['out_side_image_name']; ?>')"></button>
                            <?php endif;
                            endforeach; ?>
                        </div>                            
                    </div>  

                        <div class="right-side">

                            <div class="main-features">
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url();?>assets/frontendone/images/misc/calender.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Year</p>
                                        <p><?php echo $reg_year;?></p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url();?>assets/frontendone/images/misc/dashboard.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Milage</p>
                                        <p><?php echo $millge;?> <?php if($millage_unit==1){
                                        	echo "Km";
                                        }
                                        else{
                                        	echo "mil";
                                        }
                                        
                                        ?></p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url();?>assets/frontendone/images/misc/auto.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Transmission</p>
                                        <p><?php if($trans==1){
                                        	echo "Automatic";
                                        }
                                        else if($trans==2){
                                        	echo "Manual";
                                        }
                                        else{
                                        	echo "Triponic";
                                        }
                                        ?></p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url(); ?>assets/fuel.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Fuel Type</p>
                                        <p><?php if($fuel_type==1){
                                        	echo"Petrol";
                                        }
                                        else if($fuel_type ==2){
                                        	echo "Diesel";
                                        }
                                        else if($fuel_type==3)
                                        {
                                        echo "Hybrid";
                                        }
                                        else{
                                        	echo"Electric";
                                        }
                                         ?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="price">
                                <img src="<?php echo base_url();?>assets/frontendone/images/misc/price-tag.png" alt="">
                                <p>Rs. <?php echo number_format($total); ?></p>
                            </div>

                            <p class="note">Drive Away Price</p>

                            <div class="cta-group">
                                <a href="" class="cta enquire checkresult open-popup" data-id="<?php echo $vehi['vehicle_id']; ?>">INQUIRE</a>
                                <!--<a href="" class="add-to-compare">
                                    <span class="plus-icon"><i class="fa fa-plus"></i></span>
                                    <span class="text">compare</span>-->
                                </a>
                            </div>
                        </div>
                    </div>                    
                </div>

               <?php if($sellers_description!=NULL): ?>
                <div class="description-wrap">
                  <h5>Seller Description</h5> 

                   <p><?php echo $sellers_description; ?></p>
                </div>
            <?php endif; ?>
                

                <div class="more-details">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#details" data-toggle="tab">Details</a></li>
                        <li><a href="#features" data-toggle="tab">Features</a></li>
                    </ul>
                    
                    <div class="tab-content">
                        <div id="details" class="tab-pane fade in active">
                            <div class="detail">
                                <div class="detail-name">Make</div>
                                <div class="detail-description"><?php echo $vbrand; ?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Model</div>
                                <div class="detail-description"><?php echo $vmodal; ?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Transmission</div>
                                <div class="detail-description">
                                <?php if($trans==1){
                                        	echo "Automatic";
                                        }
                                        else if($trans==2){
                                        	echo "Manual";
                                        }
                                        else{
                                        	echo "Triponic";
                                        }
                                        ?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Mileage Range</div>
                                <div class="detail-description">
                                	<?php echo $millge;?> <?php if($millage_unit==1){
                                        	echo "Km";
                                        }
                                        else{
                                        	echo "mil";
                                        }
                                        
                                        ?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Engine Capacity</div>
                                <div class="detail-description"><?php echo $capacity; ?> 
                                	<?php if($engine_cap==1){
                                		echo "CC";
                                	
                                	}
                                	else{
                                		echo "kW";
                                	}  ?>
                                </div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Seating Capacity</div>
                                <div class="detail-description"><?php echo $seating; ?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Year of manufacture</div>
                                <div class="detail-description"><?php echo $man_year;?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Year of registration</div>
                                <div class="detail-description"><?php echo $reg_year;?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Fuel type</div>
                                <div class="detail-description"><?php if($fuel_type==1){
                                        	echo"Petrol";
                                        }
                                        else if($fuel_type ==2){
                                        	echo "Diesel";
                                        }
                                        else if($fuel_type==3)
                                        {
                                        echo "Hybrid";
                                        }
                                        else{
                                        	echo"Electric";
                                        }
                                         ?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Average fuel consumption</div>
                                <div class="detail-description"><?php echo $fuel_con;?> 
                                	<?php if($fuel_con_unit==1){
                                		echo "Km/L";
                                	}
                                	else{
                                		echo"mil/L";
                                	}
                                	 ?>
                                </div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Number of Owners</div>
                                <div class="detail-description"><?php echo $owners; ?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Total Value</div>
                                <div class="detail-description">Rs. <?php echo number_format($total); ?></div>
                            </div>
                          
                            
                            <div class="detail">
                                <div class="detail-name">Downpayment</div>
                                <div class="detail-description">Rs. <?php echo number_format($downpay); ?></div>
                            </div>
                           
                        </div>
                        <div id="features" class="tab-pane fade">
                          <?php $count=0;?>
                        <?php $varr; ?>
                        <?php foreach($vehicle as $vehi): ?>
                            <?php 
                           $vehicle_features1 = $vehi['vehicle_features'];
                       
                             ?>
                        

                        <?php
                        
                         $varr = explode(",",$vehicle_features1);
                        ?>

                        
                            <ul class="feature">
                                <?php foreach ($varr as $var): ?>   
                            
                                <?php if($count%4==0){?> 
                                </ul><ul class="feature">
                                 <?php }?>
                                <li><a href=""><?php print_r($var); ?></a></li><br>
                                
                               
				<?php $count++; ?>
                                <?php endforeach ?>
                            </ul>
                            <?php break; ?>
                             <?php endforeach ?>
                        </div>
                        </div>
                        <?php break; ?>
                          <?php endforeach;?>             
                </div>

                <!-- lease calculator -->
                <div class="lease-calculator-wrap">
                    <div class="bars">
                        <div class="group">
                            <h4>Lease amount</h4>
                            <div class="bar">
                                <input 
                                    type="range" 
                                    class="lease-amount-bar" 
                                    min="100000"                    
                                    max="10000000"                  
                                    step="50000"                   
                                    value="100000">
                                <div class="min-value">min</div>
                                <div class="selected-value">Rs. <span class="amount">0</span></div>
                                <div class="max-value">max</div>                            
                            </div>
                        </div>
                        <div class="group">
                            <h4>Duration</h4>
                            <div class="bar">
                                <input
                                    type="range" 
                                    class="duration-bar" 
                                    min="6"                    
                                    max="60"                  
                                    step="6"                   
                                    value="6">
                                <div class="min-value">min</div>
                                <div class="selected-value"><span class="months"></span> Months</div>
                                <div class="max-value">max</div>
                            </div>  
                        </div>
                    </div>

                    <p class="monthly-instalment">Monthly Instalment: Rs. <span class="instalment">0</span></p>
                </div>
                <!-- end of lease calculator -->

            </div>
            <div class="column ads-column">
                <a href="" class="ads-block">
                    <img src="<?php echo base_url('assets/frontendone/images/misc/')?>ad.jpg" alt="">
                </a>                                
            </div>
        </div>        
    </div>
</section>
<!-- end of search results -->

	

<script src="<?php echo base_url('assets/frontend/'); ?>assets/js/inquery.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/lib.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/main.js"></script>


<!-- enquire popup -->
<div class="popup enquire-popup" id="enquire-popup">
    <?php echo $vehi['vehicle_id'];?>
    <div class="popup-container">
        <div class="left-side">
            <form action="<?php echo base_url('home/importInquiry')?>" method="POST">
                <input type="text" name="name" placeholder="Full Name">
                <input type="email" name="address" placeholder="Email Address">
                <input type="text" name="phone" placeholder="Phone">
                <textarea name="comments" id="" placeholder="Comments"></textarea>
                <input type="hidden" id="resyltInqyry" class="resyltInqyry" name="im_vehicle_id" value="">
                <button type="submit">send inquiry</button>
            </form>
        </div>
        <div class="right-side">
            <h5>Call directly</h5>
            <p>Call one of our agents directly to reserve this vehicle or obtain more information.</p>

            <a href="tel:0766377666">(+94) 766 377 666</a>
            <p>Contact between 9.00am - 5.00pm on weekdays and except for poyadays.</p>
        </div>
        <div class="popup-close"><i class="fa fa-close"></i></div>
    </div>
</div>
<!-- end of enquire popup -->

<!-- enquire popup -->
<div class="popup signup-popup">
    <div class="popup-container">
        <div class="left-side">
            <form action="" method="POST">
                <input type="text" placeholder="Name">
                <input type="email" placeholder="Email Address">
                <button type="submit">sign up</button>
            </form>
        </div>
        <div class="popup-close"><i class="fa fa-close"></i></div>
    </div>
</div>
<!-- end of enquire popup -->