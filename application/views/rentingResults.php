<div class="page-content">
        <!-- search results -->
<section class="search-results floating-section">
    <div class="container">
        <div class="results-container">
            <div class="column filter-column">
                <a href="" class="refine-by">Refine by</a>
        
                <form action="" method="POST">
                    <select name="" id="vehicle_typeR" class="vehitype" data-id="vehicle_typeR">
                        <option value="featured">Select Type</option>
                        <?php foreach ($rentingtype as $type): ?>
                            <option value="<?php echo $type['vehical_type_id']; ?>"
                                    data-id="<?php echo $type['vehical_type_id']; ?>"><?php echo $type['vehical_type_name']; ?></option>
                        <?php endforeach; ?>
                    </select>

                        <select class="form-control trans" id="transmiss1" name="transmiss1"
                                data-id="transmiss1" disabled>
                            <option value=""> Select Vehicle Transmission</option>
                            <?php foreach ($trans as $tra): ?>
                                <option value="<?php echo $tra['trans_id']; ?>"
                                        class="<?php echo $tra['trans_id']; ?>"
                                        data-id="<?php echo $tra['trans_id']; ?>"><?php echo $tra['transmission']; ?></option>
                            <?php endforeach; ?>
                        </select>
                </form>
   
            </div>
            <div class="column results-column">
            <a href="<?php echo base_url('home/rent')?>" class="back-to"><i class="fa fa-angle-left"></i> Back to results</a>
        
                <div class="results-wrap" id="results-wrap">
                    <?php $count = 0; ?>
                    <?php foreach($Rvehicle as $vehicle1):?>
                        <?php $count++; ?>
                    <?php endforeach;?>

                        <p class="results-count"><?php if ($count == 0) {
                            echo 'No';
                        } else {
                            echo $count;
                        } ?>&nbsp;Vehicles</p>

                        <?php foreach($Rvehicle as $vehicle):?>
                <?php foreach($rentingtype as $type): 
                    if($type['vehical_type_id']==$vehicle['vehical_vehical_type_id']):
                        $vtype = $type['vehical_type_name'];
                    endif;
                endforeach;
                   
                foreach($rentingbrand as $brand): 
                    if($brand['vehical_brand_id']==$vehicle['vehical_vehical_brand_id']):
                        $vbrand = $brand['vehical_brand_name'];
                    endif;
                endforeach;

                foreach($rentingmodal as $modal): 
                    if($modal['vehical_model_id']==$vehicle['vehical_vehical_model_id']):
                        $vmodal = $modal['vehical_model_name'];
                    endif;
                endforeach;

                $id = $vehicle['renting_vehical_id'];
                $trans = $vehicle['transmission'];
                $millge = $vehicle['millage_range'];
                $p_d_price = $vehicle['per_day_price'];
                $fuel_type = $vehicle['fuel_type'];
                $disc = $vehicle['discount'];
                $reg_year = $vehicle['year_of_registration'];
                $man_year = $vehicle['year_of_manufactured'];
                $capacity = $vehicle['engine_capacity'];
                $image = $vehicle['renting_vehical_inside_image'];
                ?>

                
                    <div class="result">
                        <figure>
                            <div class="watermakerd">
                                <img src="<?php echo base_url(); ?>upload/renting/inside/<?php echo $image; ?>" style="width:230px; height:180px; ">
                            </div>
                        </figure>
                        <div class="right-side">
                            <p class="product-name"><?php echo $vbrand." ".$vmodal; ?> </p>
                            <div class="price">
                                <img src="<?php echo base_url();?>assets/frontendone/images/misc/price-tag.png" alt="">
                                <!-- =========================== -->
                                <?php if($difference!=0){ ?>
                                <div class="col-md-6">
                                    <div>Per day price</div>
                                    <p>Rs. <?php echo $p_d_price; ?></p>
                                </div>
                                <div class="col-md-6">
                                    <div>Total</div>
                                    <p>Rs. <?php 
                                        $total = $p_d_price*$difference;
                                        echo $total;
                                    ?></p>
                                </div>
                            <?php } else{ ?>
                                <div class="col-md-6">
                                    <div>Per day price</div>
                                    <p>Rs. <?php echo $p_d_price; ?></p>
                                </div>
                            <?php }?>
                            </div>

                            <div class="main-features">
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url();?>assets/frontendone/images/misc/calender.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Year</p>
                                        <p><?php echo $reg_year; ?></p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url();?>assets/frontendone/images/misc/dashboard.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Milage</p>
                                        <p><?php echo $millge;?> Km</p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url();?>assets/frontendone/images/misc/auto.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Transmission</p>
                                        <p><?php if($trans == 1){
                                                echo "Automatic";
                                            }else if($trans== 2){
                                                echo "Manual";
                                            }
                                            else{
                                                echo "Triptonic";
                                            }

                                         ?></p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url(); ?>assets/fuel.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Fuel Type</p>
                                        <p><?php if($fuel_type==1){
                                                echo "Petrol";
                                            }
                                            elseif($fuel_type==2){
                                                echo "Diesel";
                                            }
                                            elseif($fuel_type==3){
                                                echo "Hybrid";
                                            }
                                            else{
                                                echo "Electric";
                                            }  ?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="cta-group">
                                <a href="<?php echo base_url();?>home/rentingVehicle/<?php echo $id; ?>/<?php echo $difference ;?>" class="cta btn-green">VIEW</a>
                                <a href="" class="cta enquire checkresult open-popup" data-id="<?php echo $id; ?>">INQUIRE</a>
                                
                            </div>
                        </div>
                    </div> 
                   
                    <?php endforeach; ?>      
    </div>
    </div>
            <div class="column ads-column">
                <?php foreach ($ad as $a){ ?>
                    <a href="<?php echo $a['link'] ?>" class="ads-block" target="_blank">
                        <img src="<?php echo base_url(); ?>upload/advertisement/<?php echo $a['ad_image_name']; ?>" >
                    </a>
                <?php } ?>
            </div>
        </div>        
    </div>
</section>
<!-- end of search results -->
    </div>
<script src="<?php echo base_url('assets/frontend');?>/assets/js/inquery.js"></script>

<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>


    
<!-- enquire popup -->

<div class="popup enquire-popup" id="enquire-popup">
    <?php echo $id;?>
    <div class="popup-container">
        <div class="left-side">
            <form action="<?php echo base_url('home/rentingInquiry')?>" method="POST">
                <input type="text" name="fullname" placeholder="Full Name">
                <input type="email" name="address" placeholder="Email Address">
                <input type="text" name="phone" placeholder="Phone">
                <textarea name="comments" id="" placeholder="Comments"></textarea>
                <input type="hidden" id="resyltInqyry" class="resyltInqyry" name="vehicle_id" value="">
                <button type="submit">send inquiry</button>
            </form>
        </div>
        <div class="right-side">
            <h5>Call directly</h5>
            <p>Call one of our agents directly to reserve this vehicle or obtain more information.</p>

            <a href="tel:0766377666">(+94) 766 377 666</a>
            <p>Contact between 9.00am - 5.00pm on weekdays and except for poyadays.</p>
        </div>
        <div class="popup-close"><i class="fa fa-close"></i></div>
    </div>
</div>

<!-- end of enquire popup -->

<!-- signup popup -->

<!-- end of signup popup -->

<!-- signin popup -->

<!-- end of signin popup -->

<!-- seller popup -->

<!-- end of seller popup -->

<!-- register popup -->

<!-- end of lease popup -->

<!-- </body>
</html> -->