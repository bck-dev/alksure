<style>
    .btn-is-disabled {
        pointer-events: none; /* Disables the button completely. Better than just cursor: default; */
    @include opacity(0.7);
    }
</style>

<section class="banner" style="background-image: url('<?php echo base_url('assets/frontendone/'); ?>images/banner/vehicle-import-banner.jpg')"></section>

<section class="search-results floating-section vehicle-import">
    <div class="container">
        <div class="results-container">
            <div class="column filter-column">

                <a href="" class="refine-by">Refine by</a>

                <form action="" method="POST">
                    <select name="importType" id="importType" >
                        <option>Select Type</option>
                        <option value="" data-id="bb" id="all">All</option>
                        <?php foreach($type as $ty):?>
                            <option value="<?php echo $ty['vehical_type_id'];?>" data-id="<?php echo $ty['vehical_type_id'];?>"><?php echo $ty['vehical_type_name'];?></option>
                        <?php endforeach;?>
                    </select>

                    <select name="importBrand" id="importBrand" disabled>
                        <option>Select Make</option>
                        <?php foreach($brand as $br): ?>
                            <option value="<?php echo $br['vehicle_brand_id']; ?>" data-id="<?php echo $br['vehicle_brand_id'];?>"><?php echo $br['vehicle_brand_name'];?></option>
                        <?php endforeach;?>
                    </select>

                    <select name="importModel" id="importModel" disabled>
                        <option>Select Model </option>
                        <?php foreach($model as $mod):?>
                            <option value="<?php echo $mod['vehicle_model_id']?>" data-id="<?php echo $mod['vehicle_model_id'];?>" class="<?php echo $mod['vehicle_brand_id'];?>"><?php echo $mod['vehicle_model_name'];?></option>
                        <?php endforeach;?>
                    </select>
                </form>

                <div class="compare-wrapper  aa" id="import" cursor: not-allowed;>
                    <div class="compare-vehicles">Vehicles Added: <span class="selected"></span>/5</div>
                    <a href="<?php echo base_url('home/comparisonoperating');?>" class="cta btn-green compare-btn btn-is-disabled" style=" background-color: gray !important; border-radius:0px !important;
 display: block !important; padding: 5px 15px !important; margin-top:0px !important; cursor: not-allowed;">compare</a>
                </div>

                <!-- <div class="compare-wrapper disabled">
                    <div class="compare-vehicles">Vehicles Added: <span class="selected">2</span>/5</div>
                    <a href="" class="cta btn-green compare-btn" style="padding: 3px 76.5px 7px; margin-top: 0px;">compare</a>
                </div> -->
            </div>
            <div class="column results-column" id="results-wrap" >
                

                <div class="results-wrap" style="margin-left: 30px;" >
                <?php $count = 0; ?>
                        <?php foreach($vehicle as $vehi):?>
                            <?php $count++; ?>
                        <?php endforeach;?>

                        <p class="results-count"><?php if ($count == 0) {
                            echo 'No';
                        } else {
                            echo $count;
                        } ?>&nbsp;Vehicles</p>

                    <?php foreach($vehicle as $vehi): ?>

                        <?php foreach($type as $ty):
                            if($ty['vehical_type_id']==$vehi['import_vehicle_type']):
                                $vtype = $ty['vehical_type_name'];
                            endif;
                        endforeach;

                        foreach($brand as $br):
                            if($br['vehicle_brand_id']==$vehi['import_brand_id']):
                                $vbrand = $br['vehicle_brand_name'];
                            endif;
                        endforeach;

                        foreach($model as $mod):
                            if($mod['vehicle_model_id']==$vehi['import_model_id']):
                                $vmodal = $mod['vehicle_model_name'];
                            endif;
                        endforeach;

                        $id = $vehi['vehicle_id'];
                        $trans = $vehi['vehicle_transmission'];
                        $millge = $vehi['vehicle_mileage_range'];
                        // $p_d_price = $vehicle['per_day_price'];
                        $fuel_type = $vehi['fuel_type'];
                        // $disc = $vehicle['discount'];
                        $reg_year = $vehi['year_of_registration'];
                        $man_year = $vehi['year_of_manufacture'];
                        $capacity = $vehi['vehicle_seating_capacity'];
                        $total = $vehi['vehicle_total_value'];
                        $engine_cap = $vehi['engine_capacity_unit'];
                        $fuel_con_unit = $vehi['fuel_consumption_unit'];
                        $millage_unit = $vehi['millage_range_unit'];
                        


                        ?>
                        <div class="result">
                            <figure>
                                <div class="watermakerd">
                                <img src="<?php echo base_url(); ?>upload/import/inside/<?php echo $vehi['import_inside_image']; ?>" style="width:230px; height:180px; ">
                            </div>
                                

                            </figure>
                            <div class="right-side">
                                <p class="product-name"><?php echo $vbrand; ?> <?php echo $vmodal;?></p>
                                <div class="price">
                                    <img src="<?php echo base_url();?>assets/frontendone/images/misc/price-tag.png" alt="">
                                    <p>Rs. <?php echo number_format($total);?></p>
                                </div>

                                <div class="main-features">
                                    <div class="feature">
                                        <div class="icon">
                                            <img src="<?php echo base_url();?>assets/frontendone/images/misc/calender.png" alt="">
                                        </div>
                                        <div class="info">
                                            <p>Year</p>
                                            <p><?php echo $man_year; ?></p>
                                        </div>
                                    </div>
                                    <div class="feature">
                                        <div class="icon">
                                            <img src="<?php echo base_url();?>assets/frontendone/images/misc/dashboard.png" alt="">
                                        </div>
                                        <div class="info">
                                            <p>Milage</p>
                                            <p><?php echo $millge;?> <?php if($millage_unit==1){
                                                    echo "Km";
                                                }
                                                else{
                                                    echo "mil";
                                                }
                                                ?></p>
                                        </div>
                                    </div>
                                    <div class="feature">
                                        <div class="icon">
                                            <img src="<?php echo base_url();?>assets/frontendone/images/misc/auto.png" alt="">
                                        </div>
                                        <div class="info">
                                            <p>Transmission</p>
                                            <p><?php if($trans==1){
                                                    echo "Automatic";
                                                }
                                                elseif($trans==2){
                                                    echo "Manual";
                                                }
                                                else{
                                                    echo"Triptonic";
                                                }
                                                ?>

                                            </p>
                                        </div>
                                    </div>
                                    <div class="feature">
                                        <div class="icon">
                                            <img src="<?php echo base_url(); ?>assets/fuel.png" alt="">
                                        </div>
                                        <div class="info">
                                            <p>Fuel Type</p>
                                            <p><?php if($fuel_type==1){
                                                    echo "Petrol";
                                                }
                                                elseif($fuel_type==2){
                                                    echo 'Diesel';
                                                }
                                                elseif($fuel_type==3){
                                                    echo 'Hybrid';
                                                }
                                                else{
                                                    echo 'Electric';
                                                }
                                                ?></p>

                                        </div>
                                    </div>
                                </div>

                                <div class="cta-group">
                                    <a href="<?php echo base_url();?>home/importVehicle/<?php echo $vehi['vehicle_id']; ?>" class="cta btn-green">VIEW</a>
                                    <a href="" class="cta enquire checkresult open-popup" data-id="<?php echo $vehi['vehicle_id']; ?>">INQUIRE</a>
                                    <a href="" class="add-to-compare compare" value="<?php echo $vehi['vehicle_id']; ?>" data-hotel-name='<?php echo $vehi['vehicle_id']; ?>'>
                                        <span class="plus-icon"><i class="fa fa-plus"></i></span>
                                        <span class="text">compare</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <?php
                        // break;

                    endforeach;
                    // endforeach;
                    ?>
                </div>

            </div>
            <div class="column ads-column">
                <?php foreach ($ad as $a){ ?>
                    <a href="<?php echo $a['link'] ?>" class="ads-block" target="_blank">
                        <img src="<?php echo base_url(); ?>upload/advertisement/<?php echo $a['ad_image_name']; ?>" >
                    </a>
                <?php } ?>
            </div>
        </div>
    </div>

</section>

<script src="<?php echo base_url('assets/frontend/'); ?>assets/js/inquery.js"></script>
<!--<script src="<?php echo base_url('assets/frontendone/');?>js/lib.js"></script>-->
<!--<script src="<?php echo base_url('assets/frontendone/');?>js/main.js"></script>-->
<!-- end of vehicle import -->
<div class="popup enquire-popup" id="enquire-popup">
    <?php echo $vehi['vehicle_id'];?>
    <div class="popup-container">
        <div class="left-side">
            <form action="<?php echo base_url('home/importInquiry')?>" method="POST">
                <input type="text" name="name" placeholder="Full Name">
                <input type="email" name="address" placeholder="Email Address">
                <input type="text" name="phone" placeholder="Phone">
                <textarea name="comments" id="" placeholder="Comments"></textarea>
                <input type="hidden" id="resyltInqyry" class="resyltInqyry" name="im_vehicle_id" value="">
                <button type="submit">send inquiry</button>
            </form>
        </div>
        <div class="right-side">
            <h5>Call directly</h5>
            <p>Call one of our agents directly to reserve this vehicle or obtain more information.</p>

            <a href="tel:0766377666">(+94) 766 377 666</a>
            <p>Contact between 9.00am - 5.00pm on weekdays and except for poyadays.</p>
        </div>
        <div class="popup-close"><i class="fa fa-close"></i></div>
    </div>
</div>
<!-- end of enquire popup -->

<!-- enquire popup -->
<div class="popup signup-popup">
    <div class="popup-container">
        <div class="left-side">
            <form action="" method="POST">
                <input type="text" placeholder="Name">
                <input type="email" placeholder="Email Address">
                <button type="submit">sign up</button>
            </form>
        </div>
        <div class="popup-close"><i class="fa fa-close"></i></div>
    </div>
</div>


</div>

</div>
<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>

<script type="text/javascript">

    var base_url ='http://autosure.lk/'
    var hotelName = [];
    var i =0;
    $(".compare").click(function(e) {

        e.preventDefault();
        hotelName.push( $(this).data('hotel-name') );

        var x =hotelName.toString();
        if(i>4){
            alert('only five vehicles can be selected');
        }else{
            i++;

            Cookies.set('yasas', x);
        }

        $.ajax({
            type: "POST",
            url: base_url + "home/divresultimport",
            //dataType: "JSON",
            data: {i:i},
            success: function(result) {
                //alert(results);
                $('#import').html(result);


            }

        });



    });

</script>


</body>
</html>