
<div class="results-wrap">

                    <?php foreach ($results as $value): ?>

                        <?php 

                            $threewheel_bike_id = $value['threewheel_bike_id'];
                            $total_price = $value['total_price'];
                            $vehicle_total_value1 = (int)$total_price;
                            $year_of_manufacture = $value['Year_of_manufacture'];
                            $mileage_range = $value['mileage_range'];
                            $transmission = $value['transmission'];
                            $fuel_type = $value['fuel_type'];
                            $threewheel_bike_brand_name = $value['threewheel_bike_brand_name'];
                            $threewheel_bike_model_name = $value['threewheel_bike_model_name'];

                         ?>

                        
                    <div class="result">
                        <?php foreach ($image as $value1):  ?>
                           <?php   if ($value['threewheel_bike_id'] == $value1['threewheel_bike_id']): ?>
                        <figure>
                            <div class="watermakerd">
                                <img src="<?php echo base_url(); ?>upload/3wheel/<?php echo $value1['threewheel_bike_image_name']; ?>" alt="">
                            </div>
                        </figure>
                        <?php break; ?>
                        <?php endif ?>

                    <?php endforeach ?>

                        <div class="right-side">
                            <p class="product-name"><?php echo $threewheel_bike_brand_name; ?> <?php echo $threewheel_bike_model_name; ?> </p>
                            <div class="price">
                                <img src="<?php echo base_url();?>assets/images/misc/price-tag.png" alt="">
                                <p>Rs. <?php echo number_format($vehicle_total_value1) . ""; ?></p>
                            </div>

                            <div class="main-features">
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url();?>assets/frontendone/images/misc/calender.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Year</p>
                                        <p><?php echo $year_of_manufacture; ?></p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url();?>assets/frontendone/images/misc/dashboard.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Milage</p>
                                        <p><?php echo $mileage_range ?> Km</p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url();?>assets/frontendone/images/misc/auto.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Transmission</p>
                                        <p><?php if($transmission==1){
                                    echo "Automatic";
                                    } 
                                    else{
                                    echo "Manual";
                                    } ?></p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url();?>assets/frontendone/images/misc/auto.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Fuel Type</p>
                                        <p>
                                   
                                <?php if($fuel_type==1){
                                echo "Petrol";
                                }
                                elseif($fuel_type==2){
                                echo "Diesel";
                                }
                                else{
                                echo "Electric";
                                }  ?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="cta-group">
                                <a href="<?php echo base_url('home/bikeview/' . $value['threewheel_bike_id']) ?>" class="cta btn-green">VIEW</a>
                                <a href="" class="cta enquire checkresult open-popup" data-id="<?php echo $value['threewheel_bike_id']; ?>">INQUIRE</a>
                                <!-- <a href="" class="add-to-compare compare" value="<?php echo $value['vehicle_id']; ?>" data-hotel-name='<?php echo $value['vehicle_id']; ?>'>
                                    <span class="plus-icon"><i class="fa fa-plus"></i></span>
                                    <span class="text">compare</span>
                                </a> -->
                            </div>
                        </div>
                    </div>
                    <?php endforeach ?>                  
                </div>
                
                <!-- enquire popup -->

<div class="popup enquire-popup" id="enquire-popup">
    <?php echo $value['threewheel_bike_id'];?>
    <div class="popup-container">
        <div class="left-side">
            <form action="<?php echo base_url('home/inserttrewheeliquery')?>" method="POST">
                <input type="text" placeholder="Full Name" name="fulname">
                <input type="email" placeholder="Email Address" name="email">
                <input type="text" placeholder="Phone" name="phone">
                <textarea name="comments" id="" placeholder="Comments" name=""></textarea>
                <input type="hidden" id="resyltInqyry" class="resyltInqyry" name="vehicle_id" value="">
                <button type="submit">send enquiry</button>
            </form>
        </div>
        <div class="right-side">
            <h5>Call directly</h5>
            <p>Call one of our agents directly to reserve this vehicle or obtain more information.</p>

            <a href="tel:1123456263">112 3456 263</a>
            <p>Contact between 9.00am - 5.00pm on weekdays and except for poyadays.</p>
        </div>
        <div class="popup-close"><i class="fa fa-close"></i></div>
    </div>
</div>

<!-- end of enquire popup -->


<script src="<?php echo base_url('assets/frontend');?>/assets/js/inquery.js"></script>
<script src="<?php echo base_url('assets/frontend/');?>javascripts/script.js"></script>
<script src="<?php echo base_url('assets/frontend/');?>javascripts/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/lib.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/main.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/plugins/rangeslider.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/plugins/tablesaw.jquery.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/plugins/tablesaw-init.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/plugins/jquery-ui.min.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/plugins/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/plugins/wow.min.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/script.js"></script>

<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>

