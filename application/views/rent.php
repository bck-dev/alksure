    <div class="page-content">
        <!-- main search -->
<section class="main-search rent-search">
    <div class="search-container">
        <?php $i = 0 ?>
            <?php foreach ($slider as $slid): ?>
            <?php if ($i == 0){ ?>
        <div class="owl-carousel banner-slider">
            <?php }else{ ?>
                <?php } ?>

            <div class="item" style="background-image: url('<?php echo base_url() ?>upload/slider/<?php echo $slid['image']; ?>')"></div>
            <!-- <div class="item" style="background-image: url('<?php echo base_url();?>assets/frontendone/images/home/banner/banner-02.jpg')"></div>
            <div class="item" style="background-image: url('<?php echo base_url();?>assets/frontendone/images/home/banner/banner-03.jpg')"></div> -->
                <?php $i++; ?>
            <?php endforeach; ?>
        </div>
        <form action="<?php echo base_url('home/rentingResults')?>" method="POST">
            <p class="search-head">Looking to rent a vehicle?</p>
            <div class="group">
                <select name="selecttype" id="" required>
                    <option value="">Type</option>
                        <?php foreach($types as $type): ?>
                        <option value="<?php echo $type['vehical_type_id']; ?>"><?php echo strtoupper($type['vehical_type_name']); ?></option>
                        <?php endforeach; ?>
                </select>
                <input type="text" class="pickup-date" id="from" autocomplete="off" placeholder="Pickup Date" name="datepicker1">
                <input type="text" class="return-date" id="to" autocomplete="off" placeholder="Return Date" name="datepicker2">
                <button type="submit">Search</button>
            </div>

            <hr>

            <div class="group rent-group">
                <p class="search-head sell-head">Interested in operating lease?</p>
                <a href="<?php echo base_url('home/operating');?>" class="cta">Explore</a>
            </div>

        </form>                
    </div>            
</section>
<!-- end of main search -->

<!-- main services -->
<section class="main-services">
    <div class="container">
        <div class="inner-container">
            <div class="main-services-container">
                <?php foreach ($grid as $data): ?>
                <div class="block">
                    <div class="block-image" style="background-image: url('<?php echo base_url() ?>upload/grid/<?php echo $data['image']; ?>')"></div>
                    <h4 class="main-service-title"><?php echo $data['title']; ?></h4>
                    <p><?php echo $data['description']; ?></p>
                    <a href="<?php echo $data['link']; ?>" class="cta"><?php echo $data['button'];?> <i class="fa fa-caret-right"></i></a>
                </div>
                <?php endforeach;?>
            </div>
        </div>
    </div>
</section>
<!-- end of main services -->

<!-- about blurb section -->
<section class="about-blurb">
    <div class="container">
        <div class="inner-container">
            <h4>Where your dream come true</h4>

            <p>
                Autosure is backed by Alliance Finance Co, PLC, Sri Lanka’s 3rd oldest financial institution providing customers, with 60 years of Financial & Motoring expertise to make the correct choice in purchasing a vehicle.
            </p>

            <a href="" class="cta">ABOUT US</a>
        </div>
    </div>
</section>
<!-- end of about blurb section -->



</div>

<!-- featured product -->
    <section class="featured-product">
        <?php $x =0;?>
        <div class="container">
            <div class="inner-container">
                <div class="featured-container">
                    <?php foreach ($rentingVehi as $value): ?>

                    <?php

                    $year_of_manufacture = $value['year_of_manufactured'];
                    $vehicle_transmission = ucfirst($value['transmission']);
                    $vehicle_mileage_range_string = $value['millage_range'];
                    $vehicle_mileage_range = number_format(preg_replace('/[^0-9]/', '', $vehicle_mileage_range_string));
                    $vehicle_total_value = $value['per_day_price'];
                    $vehicle_total_value1 = (int)$vehicle_total_value;
                    $vehicle_status =$value['status'];
                    $images = $value['renting_vehical_inside_image'];
                    

                    ?>
                    <?php if($x<6){?>
                    
                    <a href="<?php echo base_url('home/rentingVehicle/'.$value['renting_vehical_id'].'/1');?>" class="product ">
                        <?php }else{ ?>
                        <a href="<?php echo base_url('home/viewvehicle/'.$value['renting_vehical_id'].'/1');?>" class="product">
                            <?php } ?>
                            
                        <div class="product-image" style="background-image: url('<?php echo base_url() ?>upload/renting/inside/<?php echo $images ?>'); width: 100%;"></div>
                        
                        <div class="info-wrap">
                            <div class="icon"><img src="<?php echo base_url('assets/frontendone/');?>images/misc/calender.png" alt=""></div>
                            <p><?php echo $year_of_manufacture; ?></p>
                        </div>
                        <div class="info-wrap">
                            <div class="icon"><img src="<?php echo base_url('assets/frontendone/');?>images/misc/dashboard.png" alt=""></div>
                            <p> <?php if($value['millage_range_unit']==1){
                                echo $value['millage_range']."Km";
                            }
                            else{
                                echo $value['millage_range']."mil";
                            }
                            ?></p>
                        </div>
                        <div class="info-wrap">
                            <div class="icon"><img src="<?php echo base_url('assets/frontendone/');?>images/misc/auto.png" alt=""></div>
                            <p>  <?php if($vehicle_transmission==1){
                                    echo "Automatic";
                                } elseif($vehicle_transmission==2){
                                    echo "Manual";
                                }
                                else{
                                    echo "Triptonic";
                                } ?></p>
                        </div>
                        <div class="info-wrap price-wrap">
                            <div class="icon"><img src="<?php echo base_url('assets/frontendone/');?>images/misc/label.png" alt=""></div>
                            <p> Rs.<?php echo number_format($vehicle_total_value1)."";?></p>
                        </div>
                    </a>
                        <?php $x=$x+1;?>
                        
                    <?php endforeach; ?>

                </div>
            </div>
        </div>
    </section>
    <!-- end of featured product -->

   





