<div class="page-content">
    <!-- main search -->
    <section class="main-search">
        <div class="search-container">
            <div class="owl-carousel banner-slider">
                <?php foreach ($slider

                as $slid): ?>
                <div class="item" style="background-image: url('<?php echo base_url() ?>upload/slider/<?php echo $slid['image']; ?>')"></div>
                <?php endforeach; ?>

            </div>
            <form action="<?php echo base_url('home/results')?>" method="POST">
                <p class="search-head">Buying a car?</p>
                <div class="group">
                    <input type="text" name="searchbarinput" placeholder="I am looking for...">
                    <button type="submit">Search</button>
                </div>

                <p class="search-head">Browse by</p>
                <div class="group">
                    <select name="vehicle_type" id="ddlContent">
                        <option value="">Type</option>
                        <?php foreach($vehicletype as $type): ?>
                            <option value="<?php echo $type['vehicle_type_name']; ?>" data-id="<?php echo $type['vehicle_type_id']; ?>"><?php echo strtoupper($type['vehicle_type_name']); ?></option>
                        <?php endforeach; ?>
                    </select>
                    <select data-id="vehicle_brand" id="vehicle_brand" name="vehicle_brand"   disabled class="make" >
                        <option value="">Make</option>
                        <?php foreach($vehiclebrand as $brand): ?>
                            <option value="<?php echo $brand['vehicle_brand_name']; ?>" class="<?php echo "so".$brand['vehicle_type_id']."so"; ?>" data-id="<?php echo $brand['vehicle_brand_id']; ?>"><?php echo strtoupper($brand['vehicle_brand_name']); ?> </option>
                        <?php endforeach; ?>
                    </select>
                    <select  name="vehicle_model" data-id="select2" id="select2" disabled class="model">
                        <option value="">Model</option>
                        <?php foreach($vehiclemodel as $vehicles): ?>
                            <option value="<?php echo $vehicles['vehicle_model_name']; ?>" class="<?php echo "so".$vehicles['vehicle_brand_id']."so"; ?>"><?php echo strtoupper($vehicles['vehicle_model_name']); ?></option>
                        <?php endforeach; ?>
                    </select>
                    <select  name="price_range" class="model">
                        <option value="" disabled selected>Price Range</option>
                        <option value="1000000-2000000">1000000-2000000</option>
                        <option value="2000000-3000000">2000000-3000000</option>
                        <option value="3000000-4000000">3000000-4000000</option>
                        <option value="4000000-5000000">4000000-5000000</option>
                        <option value="5000000-6000000">5000000-6000000</option>
                        <option value="6000000-7000000">6000000-7000000</option>
                        <option value="7000000-8000000">7000000-8000000</option>
                        <option value="8000000-9000000">8000000-9000000</option>
                        <option value="9000000-10000000">9000000-10000000</option>
                    </select>

                    <a href="<?php echo base_url('all')?>" ><button type="button" class="">All Vehicles</button></a>
                </div>
                <div class="group">
                    <input type="checkbox" name="autosure_certified" value="1" /><p class="search-head sell-head">&nbsp;&nbsp;Autosure Certified</p>
                </div>    
                

                <hr>

                <div class="group sell-group">
                    <p class="search-head sell-head">Selling a car?</p>
                    <button type="button" class="sell">Sell a car</button>
                </div>

            </form>
        </div>
    </section>
    <!-- end of main search -->

    <!-- main services -->
    <section class="main-services">
        <div class="container">
            <div class="inner-container">
                <div class="main-services-container">
                    <?php foreach ($grid as $data): ?>
                    <div class="block">
                        <div class="block-image" style="background-image: url('<?php echo base_url() ?>upload/grid/<?php echo $data['image']; ?>')"></div>
                        <h4 class="main-service-title"><?php echo $data['title']; ?></h4>
                        <p><?php echo $data['description']; ?></p>
                        <a href="<?php echo $data['link']; ?>" class="cta"><?php echo $data['button'];?> <i class="fa fa-caret-right"></i></a>
                    </div>
                    <?php endforeach; ?>

                </div>
            </div>
        </div>
    </section>
    <!-- end of main services -->

    <!-- about blurb section -->
    <section class="about-blurb">
        <div class="container">
            <div class="inner-container">
                <h4>Where your dream come true</h4>

                <p>
                    Autosure is backed by Alliance Finance Co, PLC, Sri Lanka’s 3rd oldest financial institution providing customers, with 60 years of Financial & Motoring expertise to make the correct choice in purchasing a vehicle.
                </p>

                <a href="<?php echo base_url('home/about_us') ?>" class="cta">ABOUT US</a>
            </div>
        </div>
    </section>
    <!-- end of about blurb section -->

    <!-- featured product -->
    <section class="featured-product">
        <?php $x =0; $y=0;?>
        <div class="container">
            <div class="inner-container">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="featured-tab" data-toggle="tab" href="#featured" role="tab" aria-controls="featured" aria-selected="true">Featured</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="popular-tab" data-toggle="tab" href="#popular" role="tab" aria-controls="popular" aria-selected="false">Popular</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <!-- featured -->
                    <div class="tab-pane show active" id="featured" role="tabpanel" aria-labelledby="featured-tab">
                        <div class="featured-container">
                        <?php foreach ($vehicle as $value): ?>

                        <?php

                        $year_of_manufacture = $value['year_of_manufacture'];
                        $vehicle_transmission = ucfirst($value['vehicle_transmission']);
                        $vehicle_mileage_range_string = $value['vehicle_mileage_range'];
                        $vehicle_mileage_range = number_format(preg_replace('/[^0-9]/', '', $vehicle_mileage_range_string));
                        $vehicle_total_value = $value['vehicle_total_value'];
                        $vehicle_total_value1 = (int)$vehicle_total_value;
                        $vehicle_status =$value['vehicle_status'];
                        $publish_status =$value['publish_status'];
                        $reject_status =$value['reject_status'];

                        ?>
                        <?php if($x<6){?>
                        <?php if($publish_status == 1 && $reject_status == 0) { ?>
                        <?php if($vehicle_status == 1) { ?>


                        <a href="<?php echo base_url('home/viewvehicle/'.$value['vehicle_id'].'/1');?>" class="product brand-new">
                            <?php }else{ ?>
                            <a href="<?php echo base_url('home/viewvehicle/'.$value['vehicle_id'].'/1');?>" class="product">
                                <?php } ?>
                                
                            <div class="product-image" style="background-image: url('<?php echo base_url(); ?>upload/selling/Front/<?php echo $value['inside_360_images']; ?>'); width: 100%;"></div>
                            
                            <div class="info-wrap">
                                <div class="icon"><img src="<?php echo base_url('assets/frontendone/');?>images/misc/calender.png" alt=""></div>
                                <p><?php echo $year_of_manufacture; ?></p>
                            </div>
                            <div class="info-wrap">
                                <div class="icon"><img src="<?php echo base_url('assets/frontendone/');?>images/misc/dashboard.png" alt=""></div>
                                <p> <?php if($value['millage_range_unit']==1){
                                    echo $value['vehicle_mileage_range']."Km";
                                }
                                else{
                                    echo $value['vehicle_mileage_range']."mil";
                                }
                                ?></p>
                            </div>
                            <div class="info-wrap">
                                <div class="icon"><img src="<?php echo base_url('assets/frontendone/');?>images/misc/auto.png" alt=""></div>
                                <p>  <?php if($vehicle_transmission==1){
                                        echo "Automatic";
                                    } elseif($vehicle_transmission==2){
                                        echo "Manual";
                                    }
                                    else{
                                        echo "Triptonic";
                                    } ?></p>
                            </div>
                            <div class="info-wrap price-wrap">
                                <div class="icon"><img src="<?php echo base_url('assets/frontendone/');?>images/misc/label.png" alt=""></div>
                                <p> Rs.<?php echo number_format($vehicle_total_value1)."";?></p>
                            </div>
                        </a>
                            <?php $x=$x+1;?>
                            <?php } ?>
                            <?php } ?>
                        <?php endforeach; ?>
                        </div>
                    </div>
                    <!-- popular -->
                    <div class="tab-pane" id="popular" role="tabpanel" aria-labelledby="popular-tab">
                        <div class="featured-container">
                        <?php foreach ($popularvehicles as $vehicle){
                            
                            $year_of_manufacture = $vehicle['year_of_manufacture'];
                            $vehicle_transmission = ucfirst($vehicle['vehicle_transmission']);
                            $vehicle_mileage_range_string = $vehicle['vehicle_mileage_range'];
                            $vehicle_mileage_range = number_format(preg_replace('/[^0-9]/', '', $vehicle_mileage_range_string));
                            $vehicle_total_value = $vehicle['vehicle_total_value'];
                            $vehicle_total_value1 = (int)$vehicle_total_value;
                            $vehicle_status =$vehicle['vehicle_status'];
                            $publish_status =$vehicle['publish_status'];
                            $reject_status =$vehicle['reject_status'];

                            if($y<6){
                                if($publish_status == 1 && $reject_status == 0) { 
                                    if($vehicle_status == 1) { ?>


                                        <a href="<?php echo base_url('home/viewvehicle/'.$vehicle['vehicle_id'].'/3');?>" class="product brand-new">
                                            <?php }else{ ?>
                                            <a href="<?php echo base_url('home/viewvehicle/'.$vehicle['vehicle_id'].'/3');?>" class="product">
                                                <?php } ?>
                                                
                                            <div class="product-image" style="background-image: url('<?php echo base_url(); ?>upload/selling/Front/<?php echo $vehicle['inside_360_images']; ?>'); width: 100%;"></div>
                                            
                                            <div class="info-wrap">
                                                <div class="icon"><img src="<?php echo base_url('assets/frontendone/');?>images/misc/calender.png" alt=""></div>
                                                <p><?php echo $year_of_manufacture; ?></p>
                                            </div>
                                            <div class="info-wrap">
                                                <div class="icon"><img src="<?php echo base_url('assets/frontendone/');?>images/misc/dashboard.png" alt=""></div>
                                                <p> <?php if($vehicle['millage_range_unit']==1){
                                                    echo $vehicle['vehicle_mileage_range']."Km";
                                                }
                                                else{
                                                    echo $vehicle['vehicle_mileage_range']."mil";
                                                }
                                                ?></p>
                                            </div>
                                            <div class="info-wrap">
                                                <div class="icon"><img src="<?php echo base_url('assets/frontendone/');?>images/misc/auto.png" alt=""></div>
                                                <p>  <?php if($vehicle_transmission==1){
                                                        echo "Automatic";
                                                    } elseif($vehicle_transmission==2){
                                                        echo "Manual";
                                                    }
                                                    else{
                                                        echo "Triptonic";
                                                    } ?></p>
                                            </div>
                                            <div class="info-wrap price-wrap">
                                                <div class="icon"><img src="<?php echo base_url('assets/frontendone/');?>images/misc/label.png" alt=""></div>
                                                <p> Rs.<?php echo number_format($vehicle_total_value1)."";?></p>
                                            </div>
                                        </a>
                                <?php $y=$y+1;
                                } 
                            } 
                        } ?>


                        </div>

                        <div class="cta-group mt-3">
                                <a href="<?php echo base_url('home/popular'); ?>" class="cta enquire">View All</a>
                                
                            </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!-- end of featured product -->
 <!-- newsletter -->
    <section class="newsletter">
        <div class="container">
            <div class="inner-container">
                <div class="left-side">
                    <div class="icon"><img src="<?php echo base_url('assets/frontendone/');?>images/misc/sign-up.png" alt=""></div>
                    <div class="text-content">
                        <h4>SIGN UP FOR UPATES</h4>
                        <p>Will update you with all our exciting new offers as we introduce them</p>
                    </div>
                </div>
                <div class="right-side">
                    <a href="" class="cta btn-green sign-up">SIGN UP</a>
                </div>
            </div>
        </div>
    </section>
    <!-- end of newsletter -->

</div>

</div>
    
<script>
    $(document).ready(function(){
        $("#myModal").modal('show');
    });
</script>
    
<div id="myModal" class="modal fade w-75">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Attention</h4>
            </div>
            <div class="modal-body">
                <p>Subscribe to our mailing list to get the latest updates straight in your inbox. Subscribe to our mailing list to get the latest updates straight in your inbox. Subscribe to our mailing list to get the latest updates straight in your inbox. Subscribe to our mailing list to get the latest updates straight in your inbox. Subscribe to our mailing list to get the latest updates straight in your inbox.</p>
                

            </div>

        </div>

    </div>

</div>