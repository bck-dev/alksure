<section class="content">
    <div class="container-fluid">
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-filled">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                        <a class="panel-close"><i class="fa fa-times"></i></a>
                    </div>
                    Selling Vehicle List
                </div>

                <div class="panel-body">
                    <?php echo $this->session->flashdata('msg'); ?>
                    <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
                    <div class="table-responsive">
                        <table id="tableExample3" class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Vehicle Id</th>
                                <th>Image</th>
                                <th>Brand</th>
                                <th>Model</th>
                                <th>Vehicle Transmission</th>
                                <th>Details</th>
                                <th>Option</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $count = 0;
                            foreach ($vehicle as $data):?>
                                    <?php foreach ($insideimage as $inside): ?>
                                        <?php if ($inside['vehicle_vehicle_id'] == $data['vehicle_id']): ?>
                                            <tr>
                                                <td><?php echo ++$count; ?>.</td>
                                                <td><?php echo 'veh/sel'.sprintf("%03d",$data['vehicle_id']); ?></td>
                                                <td><img src="<?php echo base_url(); ?>upload/selling/Front/<?php echo $data['inside_360_images']; ?>"
                                                         style="vertical-align:text-top;height: 61px;margin-top: 0px;margin-left: 3px;">

                                                </td>

                                                <td><?php echo $data['vehicle_brand_name']; ?></td>
                                                <td><?php echo $data['vehicle_model_name']; ?></td>
                                                <td>
                                                <?php if($data['vehicle_transmission']==1){
                                    echo "Automatic";
                                    } elseif($data['vehicle_transmission']==2){
                                    echo "Manual";
                                    }
                                    else{
                                    echo "Triptonic";
                                    } ?>
                                                </td>


                                                <td>
                                                    <a href="" class="label label-primary" data-toggle="modal"
                                                       data-target="#agenModal<?php echo $data['vehicle_id']; ?>">View</a></td>



                                                <td>

                                                    <a href="<?php echo base_url(); ?>vehicle/editvehiclecus/<?php echo $data['vehicle_id']; ?>" class="label label-success">Edit</a>
                                                    <a href="" data-vehiclesell-id="<?php echo $data['vehicle_id']; ?>" class="label label-danger vehicleselConfirm">Delete</a>
                                                </td>

                                            </tr>
                                        <?php break; ?>
                                        <?php endif ?>


                                    <?php endforeach ?>

                            <?php endforeach; ?>

                            </tbody>

                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Content Wrapper. Contains page content -->


<?php foreach ($vehicle as $value): {
?>
    <?php
    $vehicle_id = $value['vehicle_id'];
    $vehicle_type_name = $value['vehicle_type_name'];
    $vehicle_brand_name = $value['vehicle_brand_name'];
    $vehicle_transmission = $value['vehicle_transmission'];
    $vehicle_number = $value['vehicle_number'];
    $vehicle_engine_capacity = $value['vehicle_engine_capacity'];
    $year_of_manufacture = $value['year_of_manufacture'];
    $year_of_registration = $value['year_of_registration'];
    $fuel_type = $value['fuel_type'];
    $average_of_fuel_consuption = $value['average_of_fuel_consuption'];
    $vehicle_total_value = $value['vehicle_total_value'];
    $vehicle_mileage_range = $value['vehicle_mileage_range'];
    $vehicle_model_name = $value['vehicle_model_name'];
    $front_image = $data['inside_360_images'];

    ?>


    <div class="modal fade" id="agenModal<?php echo $value['vehicle_id']; ?>" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">

            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel"
                    >Vehicle Details</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body" style="overflow-y: auto; color: #ffffff; font-size: 15px;">

                    <div class="form-group">
                        <label>Vehicle Mileage Range  :</label> 
                        <?php echo $vehicle_mileage_range; ?>
                    </div>
                    <div class="form-group">
                        <label>Vehicle Engine Capacity :</label>&nbsp;
                        <?php echo $vehicle_engine_capacity; ?>cc
                    </div>
                  
                    <div class="form-group">
                        <label>Year of Manufacture :</label>&nbsp;
                        <?php echo $year_of_manufacture; ?>
                    </div>
                    <div class="form-group">
                        <label>Year of Registration :</label>&nbsp;
                        <?php echo $year_of_registration; ?>
                    </div>
                    <div class="form-group">
                        <label>Average Fuel Consuption :</label>&nbsp;
                        <?php echo $average_of_fuel_consuption; ?>km/l
                    </div>
                    <div class="form-group">
                        <label>Fuel Type :</label>&nbsp;
                        <?php if($fuel_type==1){
                                echo "Petrol";
                                }
                                elseif($fuel_type==2){
                                echo "Diesel";
                                }
                                elseif($fuel_type==3){
                                echo "Hybrid";
                                }
                                else{
                                echo "Electric";
                                }  ?>
                    </div>
                    <div class="form-group">
                        <label>Vehicle Total Value :</label>&nbsp;
                        Rs.<?php echo $vehicle_total_value; ?>
                    </div>
                    <div class="form-group">
                        <label>Vehicle Number :</label>&nbsp;
                        <?php echo $vehicle_number; ?>
                    </div>
                    <div class="form-group">
                        <label>Vehicle Images :</label>&nbsp;

                        <img src="<?php echo base_url(); ?>upload/selling/Front/<?php echo $front_image; ?>" style="vertical-align:text-top;height: 61px;margin-top: 44px;margin-left: 3px;">
                        <?php foreach ($insideimage as $inside): ?>
                            <?php if ($inside['vehicle_vehicle_id'] == $vehicle_id): ?>
                                <img src="<?php echo base_url(); ?>upload/selling/<?php echo $inside['inside_images_name']; ?>"
                                     style="vertical-align:text-top;height: 61px;margin-top: 44px;margin-left: 3px;">

                            <?php endif ?>


                        <?php endforeach ?>
                    </div>


                    <!--  <div class="form-group">
          <label>Outside Images :</label>&nbsp;
          <?php
                    foreach ($outside as $key1) {
                        ?>
            <?php if ($key1->vehicle_vehicle_id == $value->vehicle_id) { ?>
            
            <img src="<?php echo base_url(); ?>upload/vehicle/outside/<?php echo $key1->out_side_image_name; ?>" style="vertical-align:text-top;height: 61px;margin-top: 44px;margin-left: 3px;">
            <?php } ?>
            <?php } ?>
          </div> -->


                </div>

                <div class="modal-footer">

                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
<?php } ?>
<?php endforeach; ?>
