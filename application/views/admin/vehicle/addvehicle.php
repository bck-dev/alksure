<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>
  <style type="text/css">
    input[type=file]{
      display: inline;
    }
    #image_preview{
      border: 1px solid black;
      padding: 10px;
    }
    #image_preview img{
      width: 200px;
      padding: 5px;
    }
  </style>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
   <!-- <h1>
      <?php echo $pagetitle; ?>
    </h1> -->
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><?php echo $pagetitle; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><?php echo $pagetitle; ?></h3>
          </div><!-- /.box-header -->
          <!-- form start -->
           <form role="form" action="<?php echo base_url('selling/insertvehicleadmin'); ?>" method="POST" enctype='multipart/form-data'>
            <div class="box-body">
              <?php echo $this->session->flashdata('msg'); ?>
              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>

              <div class="form-group">
            <div class="row">         
              <div class="col-md-6">
                <label>Vehicle Type</label>
                <select class="form-control" class="typeSelect" id="typeSelect" name="vehicle_type" required>
                  <option value="">-- Select Vehicle Type --</option>
                   <?php foreach($vehicletype as $type): ?>
                   <option value="<?php echo $type['vehicle_type_id']; ?>"><?php echo $type['vehicle_type_name']; ?></option>
                   <?php endforeach; ?>
                </select>
              </div>
              <div class="col-md-6">
                <label>Vehicle Brand</label>
                 <select class="form-control" class="makeSelect" id="makeSelect" name="vehicle_brand" disabled required>
                  <option value="">-- Select Vehicle Brand --</option>
                   <?php foreach($vehiclebrand as $brand): ?>
                   <option value="<?php echo $brand['vehicle_brand_id']; ?>" class="<?php echo "aa".$brand['vehicle_type_id']."aa";?>"><?php echo $brand['vehicle_brand_name']; ?></option>
                   <?php endforeach; ?>
                </select>
              </div>
              <div class="col-md-6">
                <label>Vehicle Model</label>
                <select class="form-control" name="vehicle_model" class="modelSelect" required id="modelSelect" class="modelSelect" disabled>
                      <option value="">-- Select Vehicle Model --</option>
                       <?php foreach($vehiclemodel as $vehiclemodel): ?>
                      <option class="<?php echo "aa".$vehiclemodel['vehicle_brand_id']."aa";?>" value="<?php echo $vehiclemodel['vehicle_model_id']; ?>"><?php echo $vehiclemodel['vehicle_model_name']; ?></option>
                       <?php endforeach; ?>
                    </select>                 
              </div>

              

              <div class="col-md-6">
                <label>Vehicle Transmission</label>
                <select class="form-control" id="vehicle_transmission" name="vehicle_transmission" required >
	            <option>Transmission</option>
                    <option value="1">Automatic</option>
                    <option value="2">Manual</option>
                    <option value="3">Triptonic</option>
	        </select>
                <!-- <input type="text" class="form-control" id="vehicle_transmission" placeholder="Enter Vehicle Transmission" name="vehicle_transmission"> -->
              </div>
              <div class="col-md-4">
                <label>Vehicle Engine Capacity</label>
                <input type="number" class="form-control" id="vehicle_engine_capacity" placeholder="Enter Vehicle Engine Capacity" required name="vehicle_engine_capacity">
              </div>
              <div class="col-md-2">
                    <label> Capacity</label>
                    <select class="form-control" id="Capacity" name="capacity" required>
                        <option>--Select Capacity--</option>
                        <option value="1">CC</option>
                        <option value="2">kW</option>
                    </select>
                </div>
              
              <div class="col-md-6">
                <label>Vehicle Seating Capacity</label>
                <input type="number" class="form-control" id="vehicle_seating_capacity" placeholder="Enter Vehicle Seating Capacity" name="vehicle_seating_capacity" required >
              </div>
              <div class="col-md-6">
                <label>Year of Manufacture</label>
                <input type="number" class="form-control" id="year_of_manufacture" placeholder="Enter Year of Manufacture" name="year_of_manufacture" required>
              </div>
              <div class="col-md-6">
                <label>Year of Registration</label>
                <input type="text" class="form-control" id="year_of_registration" placeholder="Enter Year of Registration" name="year_of_registration" required>
              </div>
              <div class="col-md-6">
                <label>Vehicle Registration Number</label>
                <input type="text" class="form-control" id="vehicle_number" placeholder="Enter Vehicle Registration Number" name="vehicle_number" required>
              </div>
            <div class="col-md-6">
                <label>Fuel Type</label>
               <select class="form-control" id="fuel_type" name="fuel_type" required >
                  <option value="">-- Select Fuel Type --</option>
                   <option value="1">Petrol</option>
                   <option value="2">Diesel</option>
                   <option value="3">Hybrid</option>
                   <option value="4">Electric</option>
                </select>
              </div>
              <div class="col-md-4">
                <label>Vehicle Mileage Range</label>
                <input type="number" class="form-control" id="vehicle_milage_range" placeholder="Enter Vehicle Mileage Range (Km)" name="vehicle_mileage_range" required >
              </div>
              <div class="col-md-2">
                    <label> Range</label>
                    <select class="form-control" id="vehicle_range" name="vehicle_range" required >
                        <option>--Select Range--</option>
                        <option value="1" data-id="1">Km</option>
                        <option value="2" data-id="2">mi</option>
                    </select>
                </div>
              
              <div class="col-md-4">
                <label>Average of Fuel Consuption</label>
                <input type="number" class="form-control" id="average_of_fuel_consuption" placeholder="Enter Average of Fuel Consuption" name="average_of_fuel_consuption" required >
              </div>
              <div class="col-md-2">
                    <label> Unit</label>
                    <select class="form-control" id="fuel_con" name="letersPer" required >
                        <option>--Select Consumption type--</option>
                        <option value="1">Km/l</option>
                        <option value="2">mi/l</option>

                    </select>
                </div>
              
              <div class="col-md-6">
                <label>Number of Owners</label>
                <input type="number" class="form-control" id="number_of_owners" placeholder="Enter Number of Owners" name="number_of_owners" required >
              </div>
              <div class="col-md-6">
                <label>Vehicle Total Value</label>
                <input type="number" class="form-control" id="vehicle_total_value" placeholder="Enter Vehicle Total Value" name="vehicle_total_value" onkeypress="return 
                  isNumberKey(event)" required >
              </div>
              <div class="col-md-12" >
                <div><label>Vehicle Features</label></div>
                
                <div class="col-md-3">
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="A/C: Front">A/C: Front</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Power Locks">Power Locks</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Airbag: Driver">Airbag: Driver</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Anti-Lock Brakes">Anti-Lock Brakes</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Rear Window Wiper">Rear Window Wiper</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="USB">USB</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Tow Package">Tow Package</label>
                </div>
                <div class="col-md-3">
                  
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="A/C: Rear">A/C: Rear</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Multifunction Steering">Multifunction Steering</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Alarm">Alarm</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Rear Window Defroster">Rear Window Defroster</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="DVD">DVD</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Third Row Seats">Third Row Seats</label><br>
                </div>
                <div class="col-md-3">

                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Cruise Control">Cruise Control</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Power Steering">Power Steering</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Airbag: Passenger">Airbag: Passenger</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Fog Lights">Fog Lights</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Tinted Glass">Tinted Glass</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Alloy Wheels">Alloy Wheels</label><br>
                </div>
                <div class="col-md-3" style="margin-bottom: 26px;">
                
                <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Navigation System">Navigation System</label><br>
                <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Remote Keyless Entry">Remote Keyless Entry</label><br>
                <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Airbag: Side">Airbag: Side</label><br>
                <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Power Windows">Power Windows</label><br>
                <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="CD">CD</label><br>
                <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Sunroof/Moonroof">Sunroof/Moonroof</label><br>
                </div>
                <br>
                <div class="form-group">
                  <label for="comment">Additional Features:</label>
                  <textarea class="form-control" rows="5" id="comment" name="additional_features" required ></textarea>
                </div>
              </div>


              <div class="col-md-6">
                <label>Vehicle Minimum Downpayment</label>
                <input type="number" class="form-control" id="vehicle_minimum_downpayment" placeholder="Enter Vehicle Minimum Downpayment (Rs)" name="vehicle_minimum_downpayment" required >
              </div>
              
              
              
              <div class="col-md-6">
                <label>Vehicle Status</label>
                <select class="form-control" id="vehicle_status" name="vehicle_status" required>
                  <option value="">-- Select Vehicle Status --</option>
                   <?php foreach($vehiclestatus as $status): ?>
                   <option value="<?php echo $status['vehicle_status_id']; ?>"><?php echo $status['vehicle_status_name']; ?></option>
                   <?php endforeach; ?>
                </select>
              </div>
              
              <div class="col-md-6">
                <label>Sellers Description</label>
                <textarea class="form-control" id="description" placeholder="Enter Sellers Description" name="sellers_description" required ></textarea>
              </div>

              <div class="col-md-6">
                  <label>Warranty</label>
                  <select class="form-control" id="warranty" name="warranty" required>
                      <option value="">-- Select Warranty --</option>
                      <option value="0">None</option>
                      <option value="5">5 Years</option>
                  </select>
              </div>

              <div class="col-md-6">
                <label class="checkbox-inline"><input type="checkbox" name="autosure_certified" value="1">Autosure Certified</label><br>
              </div>
              
              <div class="col-md-12">
                <label>Front Image</label>
                <input type="file" name="front_image" id="uploadFile1" accept=".png,.jpg,.jpeg,.gif" required>
                <p class="help-block">
                </p>
              </div>

              <div class="col-md-6">
                <label>Vehicle Images</label>
                  <input type="file" id="uploadFile" name="userfile[]" multiple required/>
                <p class="help-block">Image size should be below then 3mb and JPG, JPEG,PNG, GIF can be uploaded.<br/>
              
                </p>
              </div>

              

              <!-- <div class="col-md-6">
                <label>Other Images</label>
                <input type="file" name="userfile1[]"  accept=".png,.jpg,.jpeg,.gif" multiple required>
                <p class="help-block">Image size should be below then 3mb and JPG, JPEG,PNG, GIF can be uploaded.<br/>
                Recomanded resolution for images is <strong>400 x 600</strong> pixels.
                </p>
              </div> -->

              <!-- <div class="col-md-6">
                <label>Outside 360 Images</label>
                <input type="file" name="outside_360_images" accept=".png,.jpg,.jpeg,.gif" >
                <p class="help-block">Image size should be below then 3mb and JPG, JPEG,PNG, GIF can be uploaded.<br/>
                Recomanded resolution for images is <strong>400 x 600</strong> pixels.
                </p>
              </div> -->

              <!-- <div class="col-md-6">
                <label>Vehicle Videos</label>
                <input type="file" name="userfile2[]"  id="image_file" accept=".png,.jpg,.jpeg,.gif" multiple>
                <p class="help-block">Image size should be below then 3mb and JPG, JPEG,PNG, GIF can be uploaded.<br/>
                Recomanded resolution for images is <strong>400 x 600</strong> pixels.
                </p>
              </div> -->
              

            </div>
                <div id="image_preview1"></div>
            </div>
                <div id="image_preview"></div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Add New Vehicle</button>
              <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
            </div>
          </form>
         
        </div><!-- /.box -->

      </div><!--/.col (full) -->
    </div>   <!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script type="text/javascript">
  
  $("#uploadFile").change(function(){
     $('#image_preview').html("");
     var total_file=document.getElementById("uploadFile").files.length;

     for(var i=0;i<total_file;i++)
     {
      $('#image_preview').append("<img src='"+URL.createObjectURL(event.target.files[i])+"'>");
     }

  });

  

</script>
<script type="text/javascript">
  
  $("#uploadFile1").change(function(){
     $('#image_preview1').html("");
     var total_file=document.getElementById("uploadFile1").files.length;

     for(var i=0;i<total_file;i++)
     {
      $('#image_preview1').append("<img src='"+URL.createObjectURL(event.target.files[i])+"'>");
     }

  });

  

</script>
