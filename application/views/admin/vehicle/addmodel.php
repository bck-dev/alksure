<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $pagetitle; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><?php echo $pagetitle; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><?php echo $pagetitle; ?></h3>
          </div><!-- /.box-header -->
          <!-- form start -->
          <form role="form" action="<?php echo base_url('Vehicle/insertmodel'); ?>" method="POST" enctype='multipart/form-data'>
            <div class="box-body">
              <?php echo $this->session->flashdata('msg'); ?>
              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>

              <div class="form-group">
                  
                 <div class="row">
                  <div class="col-md-6">
                    <label>Vehicle Type</label>
                    <select class="form-control"  id="vehicle_type_back" name="vehicle_type" required>
                      <option value="">-- Select Vehicle Type --</option>
                      <?php foreach($vehicletype as $type): ?>
                       <option value="<?php echo $type['vehicle_type_id']; ?>"><?php echo $type['vehicle_type_name']; ?></option>
                     <?php endforeach; ?>
                   </select>
                 </div>
               </div>

                <div class="row">
                  <div class="col-md-6">
                    <label>Vehicle Brand</label>
                    <select class="form-control" id="vehicle_brand_back" name="vehicle_brand" required>
                      <option value="">-- Select Vehicle Brand --</option>
                      <?php foreach($vehiclebrand as $brand): ?>
                       <option value="<?php echo $brand['vehicle_brand_id']; ?>" class="<?php echo "fuck".$brand['vehicle_type_id']."fuck"?>"><?php echo $brand['vehicle_brand_name']; ?></option>
                     <?php endforeach; ?>
                   </select>
                 </div>
               </div>

               <div class="row">
                 <div class="col-md-6">
                  <label>Vehicle Model</label>
                  <input type="text" class="form-control" id="vehicle_model_name" placeholder="Enter Vehicle Model" name="vehicle_model_name" required>
                </div>
              </div>
            </div>

          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Add New Model</button>
            <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
          </div>
        </form>
      </div><!-- /.box -->

    </div><!--/.col (full) -->
  </div>   <!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->
