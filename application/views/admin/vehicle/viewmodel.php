<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-filled">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                            <a class="panel-close"><i class="fa fa-times"></i></a>
                        </div>
                        List Model
                    </div>
                    <div class="panel-body">

                        <div class="table-responsive">

                            <table id="tableExample3" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Type</th>
                                    <th>Brand</th>
                                    <th>model</th>
                                    <th>Options</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $count = 0;
                                foreach($vehicle_model as $model):?>
                                    <tr>
                                        <td><?php echo ++$count; ?>.</td>
                                        <td>
                                            <?php foreach ($vehicle_type as $type) {
                                                if($model['vehicle_type_id']==$type['vehicle_type_id']){
                                                    echo $type['vehicle_type_name'];
                                                }
                                            } ?></td>
                                        <td>
                                            <?php foreach ($vehicle_brand as $brand) {
                                                if($model['vehicle_brand_id']==$brand['vehicle_brand_id']){
                                                    echo $brand['vehicle_brand_name'];
                                                }
                                            } ?>
                                        </td>
                                        <td><?php echo $model['vehicle_model_name']; ?></td>
                                        <td><a href="<?php echo base_url(); ?>vehicle/editmodel/<?php echo $model['vehicle_model_id']; ?>" class="label label-success">Edit</a>
                                            <a href="" data-model-id="<?php echo $model['vehicle_model_id']; ?>" class="label label-danger modelConfirm">Delete</a></td>
                                    </tr>
                                <?php endforeach; ?>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

        </div>


    </div>
</section>








