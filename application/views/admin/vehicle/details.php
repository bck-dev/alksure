<section class="content">
    <div class="container-fluid">
<?php foreach ($vehicle as $value): {
    ?>
    <?php
    $vehicle_id = $value['vehicle_id'];
    $vehicle_type_name = $value['vehicle_type_name'];
    $vehicle_brand_name = $value['vehicle_brand_name'];
    $vehicle_transmission = $value['vehicle_transmission'];
    $vehicle_number = $value['vehicle_number'];
    $vehicle_engine_capacity = $value['vehicle_engine_capacity'];
    $vehicle_seating_capacity = $value['vehicle_seating_capacity'];
    $year_of_manufacture = $value['year_of_manufacture'];
    $year_of_registration = $value['year_of_registration'];
    $fuel_type = $value['fuel_type'];
    $average_of_fuel_consuption = $value['average_of_fuel_consuption'];
    
    $vehicle_total_value = $value['vehicle_total_value'];
    $vehicle_mileage_range = $value['vehicle_mileage_range'];
    $vehicle_model_name = $value['vehicle_model_name'];
    $customer_name = $value['customer_name'];
    $phone_number = $value['phone_number'];
    $address = $value['address'];
    $email = $value['email'];
    $admin_customer = $value['admin_customer'];
    $inside_360_images = $value['inside_360_images'];

    ?>



    <div class="row">
        <div class="col-lg-12">
            <div class=" panel-filled">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                        <a class="panel-close"><i class="fa fa-times"></i></a>
                    </div>
                    <div style="color:#ffffff; font-size: 18px; font-weight: bold;">Vehicle Details</div>
                </div>
                <div class="panel-body">

                    <div class="row">
                        <div class="modal-body" style="max-height: 500px;  color: #ffffff">
                            <div>
                                <h4 class="modal-title" id="" style="color: #0f83c9; font-size: 16px">Vehicle Details</h4><br>
                            </div>
                            <div class="form-group">
                                <label>ID :</label>&nbsp;
                                <?php echo 'vehicle'.sprintf("%03d",$vehicle_id) ?>
                            </div>
                            <div class="form-group">
                                <label>Vehicle Type :</label>&nbsp;
                                <?php echo $vehicle_type_name; ?>
                            </div>
                            <div class="form-group">
                                <label>Vehicle Brand :</label>&nbsp;
                                <?php echo $vehicle_brand_name; ?>
                            </div>
                            <div class="form-group">
                                <label>Vehicle Model :</label>&nbsp;
                                <?php echo $vehicle_model_name; ?>
                            </div>
                            <div class="form-group">
                                <label>Vehicle Transmission :</label>&nbsp;
                                <?php if($vehicle_transmission==1){
                                echo "Automatic";
                                } elseif($vehicle_transmission==2){
                                echo "Manual";
                                }
                                else{
                                echo "Triptonic";
                                } ?>
                            </div>
                            <div class="form-group">
                                <label>Vehicle Mileage Range  :</label> 
                                <?php if($value['millage_range_unit']==1){
                                        echo $value['vehicle_mileage_range']. "Km";
                                    }
                                    else{
                                        echo $value['vehicle_mileage_range']. "mil";
                                    }
                                    ?>
                            </div>
                            <div class="form-group">
                                <label>Vehicle Engine Capacity :</label>&nbsp;
                                <?php if($value['engine_capacity_unit']==1){ 
                                        echo $value['vehicle_engine_capacity']."CC";
                                    }
                                    else{
                                        echo $value['vehicle_engine_capacity']."kW";
                                    }
                                    ?>
                            </div>
                            <div class="form-group">
                                <label>Vehicle Seating Capacity :</label>&nbsp;
                                <?php echo $vehicle_seating_capacity; ?>
                            </div>
                            <div class="form-group">
                                <label>Year of Manufacture :</label>&nbsp;
                                <?php echo $year_of_manufacture; ?>
                            </div>
                            <div class="form-group">
                                <label>Year of Registration :</label>&nbsp;
                                <?php echo $year_of_registration; ?>
                            </div>
                            <div class="form-group">
                                <label>Average Fuel Consuption :</label>&nbsp;
                                <?php if($value['avarage_fuel_consumption_unit']==1){
                                        echo $value['average_of_fuel_consuption']."Km/l";
                                    }
                                    else{
                                        echo $value['average_of_fuel_consuption']."mil/l";
                                    }

                                    ?>
                            </div>
                            <div class="form-group">
                                <label>Fuel Type :</label>&nbsp;
                                <?php if($fuel_type==1){
                                echo "Petrol";
                                }
                                elseif($fuel_type==2){
                                echo "Diesel";
                                }
                                elseif($fuel_type==3){
                                echo "Hybrid";
                                }
                                else{
                                echo "Electric";
                                }  ?>
                            </div>
                            <div class="form-group">
                                <label>Vehicle Total Value :</label>&nbsp;
                                Rs.<?php echo $vehicle_total_value; ?>
                            </div>
                            <div class="form-group">
                                <label>Vehicle Number :</label>&nbsp;
                                <?php echo $vehicle_number; ?>
                            </div>
                            <div class="form-group">
                                <label>Vehicle Images :</label>&nbsp;
                                <img src="<?php echo base_url(); ?>upload/selling/Front/<?php echo $inside_360_images; ?>" style="vertical-align:text-top;height: 61px;margin-top: 0px;margin-left: 3px;" ><br>
                                
                            
                                <?php foreach ($insideimage as $inside): ?>
                                    <?php if ($inside['vehicle_vehicle_id'] == $vehicle_id): ?>
                                        <img src="<?php echo base_url(); ?>upload/selling/<?php echo $inside['inside_images_name']; ?>"
                                             style="vertical-align:text-top;height: 61px;margin-top: 44px;margin-left: 3px;">

                                    <?php endif ?>


                                <?php endforeach ?>
                            </div>


                            <!--  <div class="form-group">
          <label>Outside Images :</label>&nbsp;
          
          
          
          <?php
                            foreach ($outside as $key1) {
                                ?>
            <?php if ($key1->vehicle_vehicle_id == $value->vehicle_id) { ?>
            <img src="<?php echo base_url(); ?>upload/vehicle/outside/<?php echo $key1->out_side_image_name; ?>" style="vertical-align:text-top;height: 61px;margin-top: 44px;margin-left: 3px;">
            <?php } ?>
            <?php } ?>
            
          </div> -->

                            <div><h4 class="modal-title" id="" style="color: #0f83c9; font-size: 16px">Client Information</h4></div>
                            <br>
                            <div class="form-group">
                                <label>Customer Name:</label>&nbsp;
                                <?php echo $customer_name; ?>
                            </div>
                            <div class="form-group">
                                <label>Phone Number :</label>&nbsp;
                                <?php echo $phone_number; ?>
                            </div>
                            <div class="form-group">
                                <label>Address :</label>&nbsp;
                                <?php echo $address; ?>
                            </div>
                            <div class="form-group">
                                <label>Email :</label>&nbsp;
                                <?php echo $email; ?>
                            </div>

                         <div class="form-group">
                            <button type="button" class="btn btn-success" <?php if($admin_customer == 1){ ?> disabled="disabled" <?php } ?> data-dismiss="modal" data-toggle="modal"
                                    data-target="#assignModal<?php echo $vehicle_id; ?>">Assign Agent</button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal" <?php if($admin_customer == 1){ ?> disabled="disabled" <?php } ?> data-toggle="modal"
                                    data-target="#rejectModal<?php echo $vehicle_id; ?>">Reject</button>
                            <a href="<?php echo base_url('vehicle/viewvehicle/' ) ?>" type="button" class="btn btn-danger">Close</a>

                        </div>
                        </div>
                    </div>
                </div>



                    </div>
                </div>
            </div>


<?php } ?><?php break; ?>
<?php endforeach; ?>

    </div>


</section>

<?php foreach ($vehicle as $vehi) {
    ; ?>

    <?php

    $vehicle_type_name = $vehi['vehicle_type_name'];
    $vehicle_number = $vehi['vehicle_number'];
    $vehicle_brand_name = $vehi['vehicle_brand_name'];
    $vehicle_transmission = $vehi['vehicle_transmission'];
    $vehicle_engine_capacity = $vehi['vehicle_engine_capacity'];
    $vehicle_seating_capacity = $vehi['vehicle_seating_capacity'];
    $year_of_manufacture = $vehi['year_of_manufacture'];
    $year_of_registration = $vehi['year_of_registration'];
    $fuel_type = $vehi['fuel_type'];
    ?>

    <div class="modal fade" id="assignModal<?php echo $vehi['vehicle_id']; ?>" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">

            <div class="modal-content">
                <form role="form" action="<?php echo base_url('Vehicle/insertagent/' . $vehi['vehicle_id']); ?>" method="post">
                    <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLabel"
                        >Agent Details</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body" style="overflow-y: auto; color: #ffffff; font-size: 15px;">

                        <div class="form-group">
                            <label>Vehicle Id :</label>&nbsp;
                            <?php echo 'vehicle'.sprintf("%03d",$vehi['vehicle_id']); ?>
                            <input type="hidden" id="vehicle_id" name="vehicle_id" required
                                   value="<?php echo $vehi['vehicle_id']; ?>">

                        </div>
                        <input type="hidden" id="vehicle_number" name="vehicle_number" required
                               value="<?php echo $vehicle_number; ?>">
                        <input type="hidden" id="vehicle_type_name" name="vehicle_type_name" required
                               value="<?php echo $vehicle_type_name; ?>">
                        <input type="hidden" id="vehicle_brand_name" name="vehicle_brand_name" required
                               value="<?php echo $vehicle_brand_name; ?>">
                        <input type="hidden" id="vehicle_transmission" name="vehicle_transmission" required
                               value="<?php echo $vehicle_transmission; ?>">
                        <input type="hidden" id="vehicle_engine_capacity" name="vehicle_engine_capacity" required
                               value="<?php echo $vehicle_engine_capacity; ?>">
                        <input type="hidden" id="vehicle_seating_capacity" name="vehicle_seating_capacity" required
                               value="<?php echo $vehicle_seating_capacity; ?>">


                        <div class="form-group">
                            <label>Select Agent :</label>&nbsp;
                            <select class="form-control agentt" id="agent" name="agent_id" required>
                                <option value="">-- Select Agent --</option>
                                <?php foreach($agent as $age): ?>
                                    <option value="<?php echo $age['agent_id'] ?>"><?php echo $age['agent_name'] ?></option>
                                <?php endforeach; ?>
                            </select>

                        </div>

                        <div class="agee" id="age">
                        <?php foreach($agentid as $val): ?>
        <input type="hidden" id="agent_email" name="agent_email" required
               value="<?php echo $val['adress']; ?>">
               <input type="hidden" id="agent_name" name="agent_name" required
               value="<?php echo $val['agent_name']; ?>">
        <?php endforeach; ?>
                         <?php foreach($agents as $agedata): ?>
    <div class="form-group">
        <label>agent Id :</label>&nbsp;
        <?php echo $agedata['agent_id']; ?>
        <input type="hidden" id="agent_id" name="agent_id" required
               value="<?php echo $agedata['agent_id']; ?>">
    </div>
        <?php foreach($agent as $age){ ?>
    <?php if($age['agent_id']==$agedata['agent_id']){ ?>
    <div class="form-group">
        <label>agent name :</label>&nbsp;
        <?php echo $age['agent_name']; ?>
        <input type="hidden" id="agent_name" name="agent_name" required
               value="<?php echo $age['agent_name']; ?>">
    </div>
    <?php }?>
    <?php }?>

    <div class="form-group">
        <label>ID :</label>&nbsp;
        <?php echo 'vehicle'.sprintf("%03d",$agedata['vehicle_id']) ?>
    </div>
        <?php foreach($type as $data){?>
        <?php if($data['vehicle_type_id']== $agedata['vehicle_type']){?>
    <div class="form-group">
        <label>Vehicle Type :</label>&nbsp;
        <?php echo $data['vehicle_type_name'] ; ?>
    </div>
        <?php }?>
        <?php }?>
    <?php foreach($brand as $val){?>
    <?php if($val['vehicle_brand_id']== $agedata['vehicle_brand']){?>
    <div class="form-group">
        <label>Vehicle Brand :</label>&nbsp;
        <?php echo $val['vehicle_brand_name']; ?>
    </div>
            <?php }?>
        <?php }?>
    <?php foreach($model as $dat){?>
    <?php if($dat['vehicle_model_id']== $agedata['vehicle_model']){?>
    <div class="form-group">
        <label>Vehicle Model :</label>&nbsp;
        <?php echo $dat['vehicle_model_name']; ?>
    </div>
        <?php }?>
    <?php }?>
    <div class="form-group">
        <label>Vehicle Number :</label>&nbsp;
        <?php echo $agedata['vehicle_number']; ?>
    </div>
        <hr>
    <?php endforeach;?>

                        </div>



                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Assign</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>




<?php foreach ($vehicle as $veh) {
    ; ?>


    <div class="modal fade" id="rejectModal<?php echo $veh['vehicle_id']; ?>" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">

            <div class="modal-content">
                <form role="form" action="<?php echo base_url('Vehicle/insertreject/' . $veh['vehicle_id']); ?>" method="post">
                    <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLabel"
                        >Reject Reason</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body" style="overflow-y: auto; color: #ffffff; font-size: 15px;">
                            <input type="hidden" id="vehicle_id" name="vehicle_id" required
                                   value="<?php echo $veh['vehicle_id']; ?>">
                        <input type="hidden" id="selling_customer_id" name="selling_customer_id" required
                               value="<?php echo $veh['selling_customer_id']; ?>">



                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Reject reason</label>
                                     <textarea class="col-md-12 col-xs-12" placeholder="Vehicle Reject Reason" style="color: black"
                                      rows="4" maxlength="300" name="reject_reason"
                                      id="regect_reason" required></textarea>
                                </div>
                            </div>
                        </div>


                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Submit</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>


