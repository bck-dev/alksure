<section class="content">
    <div class="container-fluid">
    </div>

    <!-- Main content -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-filled">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                        <a class="panel-close"><i class="fa fa-times"></i></a>
                    </div>
                    Edit Vehicle
                </div>
                <div class="panel-body">
                    <?php echo $this->session->flashdata('msg'); ?>
                    <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">

                                <?php foreach($vehicle1 as $vehi):



                                    $type_id = $vehi['vehicle_type'];
                                    $brand_id = $vehi['vehicle_brand'];
                                    $model_id = $vehi['vehicle_model'];
                                    $vehicle_id = $vehi['vehicle_id'];
                                    $transmission = $vehi['vehicle_transmission'];
                                    $millage_range = $vehi['vehicle_mileage_range'];
                                    $engine_capacity = $vehi['vehicle_engine_capacity'];
                                    $seating_capacity = $vehi['vehicle_seating_capacity'];
                                    $year_of_manufactured = $vehi['year_of_manufacture'];
                                    $year_of_registration = $vehi['year_of_registration'];
                                    $fuel_type = $vehi['fuel_type'];
                                    $number_of_owners = $vehi['number_of_owners'];
                                    $avarage_fuel_consumption = $vehi['average_of_fuel_consuption'];
                                    $features = $vehi['vehicle_features'];
                                    $total = $vehi['vehicle_total_value'];
                                    $downpay = $vehi['vehicle_minimum_downpayment'];
                                    $owners = $vehi['number_of_owners'];
                                    $seller_des = $vehi['sellers_description'];
                                    $vehicle_num = $vehi['vehicle_number'];
                                    $front_image = $vehi['inside_360_images'];

//                                    $images = $vehi['renting_vehical_inside_image'];

                                    ?>

                                    <!-- form start -->
                                    <?php ?>
                                    <form role="form" action="<?php echo base_url('vehicle/updatevehiclecus/'.$vehicle_id); ?>" method="POST" enctype='multipart/form-data'>
                                        <div class="box-body">
                                            <!-- <?php echo $this->session->flashdata('msg'); ?>
                                        <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?> -->

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <label>Vehicle Type</label>
                                                        <select class="form-control" id="vehicle_brand" name="vehicle_type">
                                                            <option value="">-- Select Vehicle Type --</option>
                                                            <?php foreach($types as $type): ?>
                                                                <option value="<?php echo $type['vehicle_type_id']; ?>" <?php echo ($type_id == $type['vehicle_type_id']) ? 'selected' : ''; ?>><?php echo $type['vehicle_type_name']; ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Vehicle Brand</label>
                                                        <select class="form-control" id="vehicle_brand" name="vehicle_brand">
                                                            <option value="">-- Select Vehicle Brand --</option>
                                                            <?php foreach($brands as $brand): ?>
                                                                <option value="<?php echo $brand['vehicle_brand_id']; ?>" <?php echo ($brand_id == $brand['vehicle_brand_id']) ? 'selected' : ''; ?>><?php echo $brand['vehicle_brand_name']; ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Vehicle Model</label>
                                                        <select class="form-control" name="vehicle_model" id="">
                                                            <option value="">-- Select Vehicle Modal --</option>
                                                            <?php foreach($modals as $vehiclemodel): ?>
                                                                <option value="<?php echo $vehiclemodel['vehicle_model_id']; ?>" <?php echo ($model_id == $vehiclemodel['vehicle_model_id']) ? 'selected' : ''; ?>><?php echo $vehiclemodel['vehicle_model_name'] ?> </option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label>Year of Registration</label>
                                                        <input type="text" class="form-control" id="year_of_registration" placeholder="Enter Year of Registration" name="year_of_registration" value="<?php echo $year_of_registration ?>">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Year of Manufacture</label>
                                                        <input type="text" class="form-control" id="year_of_manufacture" placeholder="Enter Year of Manufacture" name="year_of_manufacture" value="<?php echo $year_of_manufactured ?>">
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label>Transmission</label>
                                                        <select class="form-control" id="vehicle_transmission" name="vehicle_transmission">
                                                            <option value="">-- Select Transmission --</option>
                                                            <?php for($x=1; $x<=2; $x++) : ?>
                                                                <option value="<?php echo $x; ?>" <?php echo ($x == $transmission) ? 'selected' : ''; ?>><?php echo ($x == '2') ? 'Manual' :'Automatic' ; ?></option>
                                                            <?php endfor; ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Vehicle Mileage Range</label>
                                                        <input type="text" class="form-control" id="vehicle_milage_range" placeholder="Enter Vehicle Mileage Range (Km)" name="vehicle_mileage_range" value="<?php echo $millage_range ?>">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Fuel Type</label>
                                                        <select class="form-control" id="fuel_type" name="fuel_type">
                                                            <option value="">-- Select Fuel Type --</option>
                                                            <?php for($x=1; $x<=2; $x++) : ?>
                                                                <option value="<?php echo $x; ?>" <?php echo ($x == $fuel_type) ? 'selected' : ''; ?>><?php echo ($x == '2') ? 'Diesel' :'Petrol' ; ?></option>
                                                            <?php endfor; ?>
                                                        </select>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label>Average of Fuel Consuption</label>
                                                        <input type="text" class="form-control" id="average_of_fuel_consuption" placeholder="Enter Average of Fuel Consuption" name="average_of_fuel_consuption" value="<?php echo $avarage_fuel_consumption; ?>">
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label>Vehicle Engine Capacity</label>
                                                        <input type="text" class="form-control" id="vehicle_engine_capacity" placeholder="Enter Vehicle Engine Capacity (CC)" name="vehicle_engine_capacity" value="<?php echo  $engine_capacity; ?>">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Vehicle Seating Capacity</label>
                                                        <input type="text" class="form-control" id="vehicle_seating_capacity" placeholder="Enter Vehicle Seating Capacity" name="vehicle_seating_capacity" value="<?php echo $seating_capacity; ?>">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Vehicle Total Price</label>
                                                        <input type="text" class="form-control" id="vehicle_engine_capacity" placeholder="Enter Total Price" name="total" value="<?php echo  $total; ?>">
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label>Downpayment</label>
                                                        <input type="text" class="form-control" id="vehicle_engine_capacity" placeholder="Enter Downpayment" name="downpayment" value="<?php echo  $downpay; ?>">
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label>Number of Owners</label>
                                                        <input type="text" class="form-control" id="vehicle_engine_capacity" placeholder="Enter Number of Owners" name="owners" value="<?php echo  $owners; ?>">
                                                    </div>

                                                    <div class="col-md-12">
                                                        <label>Vehicle Number</label>
                                                        <input type="text" class="form-control" id="vehicle_number" placeholder="Enter Vehicle Number" name="vehi_number" value="<?php echo  $vehicle_num ; ?>">
                                                    </div>

                                                    <div class="col-md-12">
                                                        <label>Vehicle Features</label>
                                                        <textarea class="form-control" placeholder="Vehicle Features" name="vehicle_features" ><?php echo $features; ?></textarea>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <label>sellers Description</label>
                                                        <textarea class="form-control" placeholder="Sellers description" name="sellers_description" ><?php echo $seller_des; ?></textarea>
                                                    </div>

                                                    <br><br>
                                                    <div class="col-md-12">
                                                    <label>Front Image</label>
                           
                                                        <input type="hidden" id="inside_images" name="new_front_image"
                                                         value="<?php echo $front_image; ?>">
                                                        <div class="image-group" style="position: relative; margin-top: 20px;">
                                                        <div class="image-group" style="position: relative; margin-top: 20px;">
                                                        <img src="<?php echo base_url(); ?>upload/selling/Front/<?php echo $front_image; ?>" class="thumbnail" alt="<?php echo $front_image; ?>" style="width: 20%">
                                                        <span class="img-close">&#10006;</span>
                                                    </div>
                            
                                                    </div>
                                                    <input type="file" id="input-image" name="fileinput">
                        
                                                    <p class="help-block">Image size should be below then 3mb and JPG, JPEG,PNG, GIF can be
                                                        uploaded.</p>
                                                </div>
                                                    
                                                    <div class="form-group">
                                                        <?php
                                                        if (isset($vehicle_images) && is_array($vehicle1) && count($vehicle1)): $i = 1;
                                                            foreach ($vehicle_images as $key => $data) {
                                                                ?>
                                                                <div class="imagelocation<?php echo $data['inside_images_id'] ?>">
                                                                    <br/>
                                                                    <img src="<?php echo base_url(); ?>upload/selling/<?php echo $data['inside_images_name']; ?>"
                                                                         style="vertical-align:text-top;" width="200" height="200">
                                                                    <a href="" style="cursor:pointer;"
                                                                       data-imagesell-id="<?php echo $data['inside_images_id']; ?>"
                                                                       class="label label-danger imagesellConfirm">X</a>
                                                                </div>
                                                            <?php }endif; ?>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Outside Images</label>
                                                        <input type="file" name="userfile[]" id="image_file" accept=".png,.jpg,.jpeg,.gif"
                                                               multiple>
                                                        <p class="help-block">Image size should be below then 3mb and JPG, JPEG,PNG, GIF can be
                                                            uploaded.<br/>
                                                            Recomanded resolution for images is <strong>120 x 120</strong> pixels.
                                                        </p>
                                                    </div><br><br>
                                                </div>

                                            </div>
                                            <!-- /.box-body -->
                                            <div class="box-footer">
                                                <button type="submit" class="btn btn-primary">Update Vehicle</button>
                                                <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
                                            </div>

                                        </div> <!-- /.box -->
                                    </form>
                                <?php endforeach; ?>
                            </div><!--/.col (full) -->
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>