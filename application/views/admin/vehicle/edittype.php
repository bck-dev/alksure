<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>

        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><?php  ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit vehicle type</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <?php foreach ($vehicle_type as $types){
                        $type_id = $types['vehicle_type_id'];
                        $type = $types['vehicle_type_name'];
                    }?>
                    <form role="form" action="<?php echo base_url('Vehicle/updatetype/'.$type_id ); ?>" method="POST"
                          enctype='multipart/form-data'>
                        <div class="box-body">
                            <?php echo $this->session->flashdata('msg'); ?>
                            <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>


                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Vehicle Type</label>
                                        <input type="text" class="form-control" id="vehicle_type_name"
                                               placeholder="Enter Vehicle Type" name="vehicle_type_name" value="<?php echo $type; ?>">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Update Type</button>
                            <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
                        </div>
                    </form>
                </div><!-- /.box -->

            </div><!--/.col (full) -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
