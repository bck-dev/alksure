<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-filled">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                            <a class="panel-close"><i class="fa fa-times"></i></a>
                        </div>
                        List Type
                    </div>
                    <div class="panel-body">

                        <div class="table-responsive">

                            <table id="tableExample3" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Type</th>
                                    <th>Options</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $count = 0;
                                foreach($vehicle_type as $type):?>
                                    <tr>
                                        <td><?php echo ++$count; ?>.</td>
                                        <td><?php echo $type['vehicle_type_name']; ?></td>
                                        <td><a href="<?php echo base_url(); ?>vehicle/edittype/<?php echo $type['vehicle_type_id']; ?>" class="label label-success">Edit</a>
                                        <a href="" data-type-id="<?php echo $type['vehicle_type_id']; ?>" class="label label-danger typeConfirm">Delete</a></td>
                                    </tr>
                                <?php endforeach; ?>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

        </div>


    </div>
</section>








