<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-filled">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                            <a class="panel-close"><i class="fa fa-times"></i></a>
                        </div>
                        List Status
                    </div>
                    <div class="panel-body">

                        <div class="table-responsive">

                            <table id="tableExample3" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Status</th>
                                    <th>Options</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $count = 0;
                                foreach($vehicle_status as $status):?>
                                    <tr>
                                        <td><?php echo ++$count; ?>.</td>
                                        <td><?php echo $status['vehicle_status_name']; ?></td>
                                        <td> <a href="<?php echo base_url(); ?>vehicle/editstatus/<?php echo $status['vehicle_status_id']; ?>" class="label label-success">Edit</a>
                                            <a href="" data-status-id="<?php echo $status['vehicle_status_id']; ?>" class="label label-danger statusConfirm">Delete</td>
                                    </tr>
                                <?php endforeach; ?>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

        </div>


    </div>
</section>












