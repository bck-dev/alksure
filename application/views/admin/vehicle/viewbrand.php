<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-filled">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                            <a class="panel-close"><i class="fa fa-times"></i></a>
                        </div>
                        List Brand
                    </div>
                    <div class="panel-body">

                        <div class="table-responsive">

                            <table id="tableExample3" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Type</th>
                                    <th>Brand</th>
                                    <th>Options</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $count = 0;
                                foreach($Brand as $brand):?>
                                    <tr>
                                        <td><?php echo ++$count; ?>.</td>
                                        <td>
                                            <?php foreach ($vehicle_type as $type) {
                                                if($brand['vehicle_type_id']==$type['vehicle_type_id']){
                                                    echo $type['vehicle_type_name'];
                                                }
                                            } ?>
                                        </td>
                                        <td><?php echo $brand['vehicle_brand_name']; ?></td>
                                        <td><a href="<?php echo base_url(); ?>vehicle/editbrand/<?php echo $brand['vehicle_brand_id']; ?>" class="label label-success">Edit</a>
                                            <a href="" data-brand-id="<?php echo $brand['vehicle_brand_id']; ?>" class="label label-danger brandConfirm">Delete</a></td>

                                    </tr>
                                <?php endforeach; ?>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

        </div>


    </div>
</section>



