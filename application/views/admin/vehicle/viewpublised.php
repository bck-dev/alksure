<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-filled">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                            <a class="panel-close"><i class="fa fa-times"></i></a>
                        </div>
                        List Published Vehicle
                    </div>
                    <div class="panel-body">

                        <div class="table-responsive">

                            <table id="tableExample3" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Status</th>
                                    <th>Options</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $count = 0;
                                foreach($rats as $pub):?>
                                 
                                    <tr>
                                        <td><?php echo ++$count; ?>.</td>
<!--                                        --><?php //echo ''.sprintf("%03d",$pub->rating_id) ?>
<!--                                        <td>--><?php //echo $pub['vehicle_id']; ?><!--.</td>-->
                                        <td><input type="checkbox" <?php if ($pub->publish_status == 1) { ?> checked <?php } ?> id=" <?php echo $pub->publish_status; ?>"
                                                                                    data-pub-id="<?php echo $pub->vehical_vehical_id; ?>" class="published" name="<?php $pub->publish_status; ?>" value="1">Published</td>

                                        <td>
                                            <a href="" class="label label-primary viewmodal" data-id="<?php echo $pub->vehical_vehical_id;?>" data-toggle="modal"
                                               data-target="#pubModal<?php echo $pub->vehical_vehical_id;?>">View</a>
                                         <a href="" data-publish-id="<?php echo $pub->vehicle_id;?>" class="label label-danger publishConfirm">Remove</td>
                                    </tr>
                                    
                                <?php endforeach; ?>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

        </div>


    </div>
</section>






<?php foreach ($rating as $ratin) {
    ; ?>
    <?php
    $vehicle_id = $ratin->vehicle_id;
    $vehicle_type_name = $ratin->vehicle_type_name;
    $vehicle_brand_name = $ratin->vehicle_brand_name;
    $vehicle_transmission = $ratin->vehicle_transmission;
    $vehicle_engine_capacity = $ratin->vehicle_engine_capacity;
    $engine_capacity_unit= $ratin->engine_capacity_unit;
    $vehicle_seating_capacity = $ratin->vehicle_seating_capacity;
    $year_of_manufacture = $ratin->year_of_manufacture;
    $year_of_registration = $ratin->year_of_registration;
    $fuel_type = $ratin->fuel_type;
    $average_of_fuel_consuption = $ratin->average_of_fuel_consuption;
    $avarage_fuel_consumption_unit= $ratin->avarage_fuel_consumption_unit;
    $vehicle_total_value = $ratin->vehicle_total_value;
    $vehicle_mileage_range = $ratin->vehicle_mileage_range;
    $millage_range_unit = $ratin->millage_range_unit;
    $vehicle_model_name = $ratin->vehicle_model_name;
    $agent_name = $ratin->agent_name;
    $contact_number = $ratin->contact_number;
    $address = $ratin->adress;
    $selling_customer_id = $ratin->selling_customer_id;

    ?>

    <div class="modal fade" id="pubModal<?php echo $vehicle_id ?>" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">

            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel"
                    >Agent Details</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body" style="overflow-y: auto; color: #ffffff; font-size: 15px;">


<!--                    <div class="form-group">-->
<!--                        <label>Agent Name:</label>&nbsp;-->
<!--                        --><?php //echo $agg['agent_name']; ?>
<!---->
<!--                    </div>-->
<!--                    <div class="form-group">-->
<!--                        <label>Agent Address:</label>&nbsp;-->
<!--                        --><?php //echo $agg['adress']; ?>
<!--                    </div>-->
<!--                    <div class="form-group">-->
<!--                        <label>Agent NIC No:</label>&nbsp;-->
<!--                        --><?php //echo $agg['NIC_number']; ?>
<!--                    </div>-->
<!--                    <div class="form-group">-->
<!--                        <label>Agent Contact No:</label>&nbsp;-->
<!--                        --><?php //echo $agg['contact_number']; ?>
<!--                    </div>-->
<!---->
<!--                    <div class="form-group">-->
<!--                        <label>Agent Id :</label>&nbsp;-->
<!--                        --><?php //echo 'agent'.sprintf("%03d",$agg['emp_id']); ?>


                    <div class="form-group">
                        <label>ID :</label>&nbsp;
                        <?php echo 'vehicle'.sprintf("%03d",$vehicle_id) ?>
                    </div>
                    <div class="form-group">
                        <label>Vehicle Type :</label>&nbsp;
                        <?php echo $vehicle_type_name; ?>
                    </div>
                    <div class="form-group">
                        <label>Vehicle Brand :</label>&nbsp;
                        <?php echo $vehicle_brand_name; ?>
                    </div>
                    <div class="form-group">
                        <label>Vehicle Model :</label>&nbsp;
                        <?php echo $vehicle_model_name; ?>
                    </div>
                    <div class="form-group">
                        <label>Vehicle Transmission :</label>&nbsp;
                        
                         <?php if($vehicle_transmission==1){
                                echo "Automatic";
                                } elseif($vehicle_transmission==2){
                                echo "Manual";
                                }
                                else{
                                echo "Triptonic";
                                } ?>
                    </div>
                    <div class="form-group">
                        <label>Vehicle Mileage Range  :</label> 
                       <?php if($millage_range_unit ==1){
                                        echo $vehicle_mileage_range."km";
                                    }
                                    else{
                                        echo $vehicle_mileage_range."mil";
                                    }
                                    ?>
                    </div>
                    <div class="form-group">
                        <label>Vehicle Engine Capacity :</label>&nbsp;
                        <?php if($engine_capacity_unit ==1){ 
                                        echo $vehicle_engine_capacity."CC";
                                    }
                                    else{
                                        echo $vehicle_engine_capacity."kW";
                                    }
                                    ?>
                    </div>
                    <div class="form-group">
                        <label>Vehicle Seating Capacity :</label>&nbsp;
                        <?php echo $vehicle_seating_capacity; ?>
                    </div>
                    <div class="form-group">
                        <label>Year of Manufacture :</label>&nbsp;
                        <?php echo $year_of_manufacture; ?>
                    </div>
                    <div class="form-group">
                        <label>Year of Registration :</label>&nbsp;
                        <?php echo $year_of_registration; ?>
                    </div>
                    <div class="form-group">
                        <label>Average Fuel Consuption :</label>&nbsp;
                         <?php if($avarage_fuel_consumption_unit ==1){
                                        echo $average_of_fuel_consuption."Km/l";
                                    }
                                    else{
                                        echo $average_of_fuel_consuption."mil/l";
                                    }

                                    ?>
                    </div>
                    <div class="form-group">
                        <label>Fuel Type :</label>&nbsp;
                        
                         <?php if($fuel_type==1){
                        echo "Petrol";
                        }
                        elseif($fuel_type==2){
                        echo "Diesel";
                        }
                        elseif($fuel_type==3){
                        echo "Hybrid";
                        }
                        else{
                        echo "Electric";
                        }  ?>
                    </div>
                    <div class="form-group">
                        <label>Vehicle Total Value :</label>&nbsp;
                        Rs.<?php echo $vehicle_total_value; ?>
                    </div>
                    <div class="form-group">
                        <label>Vehicle Images :</label>&nbsp;


                        <?php foreach ($insideimage as $inside): ?>
                            <?php if ($inside['vehicle_vehicle_id'] == $vehicle_id): ?>
                                <img src="<?php echo base_url(); ?>upload/selling/<?php echo $inside['inside_images_name']; ?>"
                                     style="vertical-align:text-top;height: 61px;margin-top: 44px;margin-left: 3px;">

                            <?php endif ?>


                        <?php endforeach ?>
                    </div>


                    <!--  <div class="form-group">
          <label>Outside Images :</label>&nbsp;
          <?php
                    foreach ($outside as $key1) {
                        ?>
            <?php if ($key1->vehicle_vehicle_id == $ratin->vehicle_id) { ?>
            <img src="<?php echo base_url(); ?>upload/vehicle/outside/<?php echo $key1->out_side_image_name; ?>" style="vertical-align:text-top;height: 61px;margin-top: 44px;margin-left: 3px;">
            <?php } ?>
            <?php } ?>
          </div> -->
                    
                    <div><h4 class="modal-title" id="" style="color: #0f83c9; font-size: 16px">Contact details</h4></div>
                    <br>
                    <div class="form-group">
                        <label>Customer Name:</label>&nbsp;
                        <?php echo $agent_name; ?>
                    </div>
                    <div class="form-group">
                        <label>Phone Number :</label>&nbsp;
                        <?php echo $contact_number; ?>
                    </div>
                    <div class="form-group">
                        <label>Address :</label>&nbsp;
                        <?php echo $address; ?>
                    </div>

                    <hr>

                    <?php foreach($rates1 as $ss): ?>

                        <?php $aa=  $ss['extirier_condition']; ?>

                        <?php   $CI=&get_instance();?>


                   <?php if ($ss['vehical_vehical_id'] == $vehicle_id){?>

                    <div class="form-group">
                        <label>Extirier condition :</label>&nbsp;
                        <?php echo $CI->get_star($ss['extirier_condition']); ?>
                    </div>

                    <div class="form-group">
                        <label>Interior condition :</label>&nbsp;
                        <?php echo $CI->get_star($ss['interior_condition']); ?>
                    </div>

                    <div class="form-group">
                        <label>Engine condition :</label>&nbsp;
                        <?php echo $CI->get_star($ss['engine_condition']); ?>
                    </div>

                    
                    <?php if($ss['image']==null){
                    echo "No image";}
                    else{ ?>
                        <div class="form-group">
                        <img src="<?php echo base_url()?>upload/publish/<?php echo $ss['image']; ?>" height="83px" width="100px" >
                    </div>
                    <?php }?>
                    
                    <div class="form-group">
                        <label>Details :</label>&nbsp;
                        <?php echo $ss['details']; ?>
                    </div>
                    <?php }?>
                    <?php endforeach;?>


                </div>

                <div class="modal-footer">

                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
        </div>

    </div>

    </div>

<?php } ?>





