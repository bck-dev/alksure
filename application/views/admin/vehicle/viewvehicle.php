<section class="content">
    <div class="container-fluid">
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-filled">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                        <a class="panel-close"><i class="fa fa-times"></i></a>
                    </div>
                    Selling Vehicle List
                </div>
                <div>
                    <a href="<?php echo base_url(); ?>vehicle/viewvehicle" style="background-color: #204d74;"
                       class="btn btn-primary">Admin</a>
                    <a href="<?php echo base_url(); ?>vehicle/viewvehicles" class="btn btn-success">Customer</a>
                </div>
                <div class="panel-body">
                    <?php echo $this->session->flashdata('msg'); ?>
                    <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
                    <div class="table-responsive">
                        <table id="tableExample3" class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Vehicle Id</th>
                                <th>Image</th>
                                <th>Options</th>
                                <th>Option</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $count = 0;
                            foreach ($vehicle as $data):?>
                                <?php if ($data['admin_customer'] == 1) { ?>
                                    <?php foreach ($insideimage as $inside): ?>
                                        <?php if ($inside['vehicle_vehicle_id'] == $data['vehicle_id']): ?>
                                            <tr>
                                                <td><?php echo ++$count; ?>.</td>
                                                <td><?php echo 'veh/sel' . sprintf("%03d", $data['vehicle_id']); ?></td>
                                                <td>

                                           
                                                <img src="<?php echo base_url(); ?>upload/selling/Front/<?php echo $data['inside_360_images']; ?>" style="vertical-align:text-top;height: 61px;margin-top: 0px;margin-left: 3px;" >
                                                    
                                        </td>

                                                <td>
                                                    <a href="<?php echo base_url('vehicle/details/' . $data['vehicle_id']) ?>"
                                                       class="label label-primary">View</a></td>

                                                <td>

                                                    <a href="<?php echo base_url(); ?>vehicle/editvehicle1/<?php echo $data['vehicle_id']; ?>"
                                                       class="label label-success">Edit</a>
                                                    <?php if ($data['vehicle_status']!= 8): ?>
                                                        <a href="<?php echo base_url(); ?>vehicle/sold/<?php echo $data['vehicle_id']; ?>"
                                                        class="label label-warning">Sold</a>
                                                    <?php endif; ?>
                                                    <a href="" data-vehiclesell-id="<?php echo $data['vehicle_id']; ?>"
                                                       class="label label-danger vehicleselConfirm">Delete</a>
                                                </td>

                                            </tr>
                                            <?php break; ?>
                                        <?php endif ?>
                                    <?php endforeach ?>
                                <?php } ?>
                            <?php endforeach; ?>

                            </tbody>

                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Content Wrapper. Contains page content -->


<?php foreach ($vehicle as $agg) {
    ; ?>


    <div class="modal fade" id="agenModal<?php echo $agg['agent_id']; ?>" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">

            <div class="modal-content">

                    <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLabel"
                        >Agent Details</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body" style="overflow-y: auto; color: #ffffff; font-size: 15px;">



                        <div class="form-group">

                            <img src="<?php echo base_url()?>upload/agent/<?php echo $agg['image']; ?>" height="83px" width="100px" >
                        </div>

                        <div class="form-group">
                            <label>Agent Name:</label>&nbsp;
                            <?php echo $agg['agent_name']; ?>

                        </div>
                        <div class="form-group">
                            <label>Agent Address:</label>&nbsp;
                            <?php echo $agg['adress']; ?>
                        </div>
                        <div class="form-group">
                            <label>Agent NIC No:</label>&nbsp;
                            <?php echo $agg['NIC_number']; ?>
                        </div>
                        <div class="form-group">
                            <label>Agent Contact No:</label>&nbsp;
                            <?php echo $agg['contact_number']; ?>
                        </div>

                        <div class="form-group">
                            <label>Agent Id :</label>&nbsp;
                            <?php echo 'agent'.sprintf("%03d",$agg['emp_id']); ?>

                        </div>

                    </div>

                    <div class="modal-footer">

                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>

            </div>
        </div>
    </div>
<?php } ?>
