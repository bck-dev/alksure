<section class="content">
  <div class="container-fluid">
  </div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-filled">
            <div class="panel-heading">
                <div class="panel-tools">
                    <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                    <a class="panel-close"><i class="fa fa-times"></i></a>
                </div>
                Confirmed Renting Inquiry Table
            </div>
            <div class="panel-body">
              <?php echo $this->session->flashdata('msg'); ?>
              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
                <div class="table-responsive">
                  <!-- Reservation table -->
                    <table id="tableExample3" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Inquiry ID</th>
                            <th>Vehicle ID</th>
                            <th>Customer Name</th>
                            <th>Contact Number</th>
                            <th>Comment</th>
                            <th>Confirmed Date</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                                $count = 0;
                                foreach($inquiry as $data):
                                    if($data['status']==2):
                                    
                                  $id = $data['renting_inquiry_id'];
                            ?>

                              <tr>
                                <td><?php echo ++$count; ?>.</td>
                                <td><?php echo 'renting'.sprintf("%03d",$data['renting_inquiry_id']); ?></td>
                                <td>
                                  <?php echo 'vehicle'.sprintf("%03d",$data['renting_renting_vehical_id']); ?> <!-- Vehicle ID -->
                                  &nbsp;&nbsp;
                                  
                                  <button type="button" class="label label-info" data-toggle="modal" data-target="#vehicleModal<?php echo $data['renting_renting_vehical_id'];?>">info</button>
                               
                                </td>
                                
                                <td>
                                  <?php echo $data['customer_name']; ?> <!-- Customer Name -->
                                  &nbsp;&nbsp;
                                  <button type="button" class="label label-info" data-toggle="modal" data-target="#peopleModal<?php echo $data['renting_inquiry_id'];?>">info</button>
                                </td>
                                <td><?php echo $data['phone_number']; ?></td>
                                
                                <td><?php echo $data['comments']; ?></td>
                                
                                 <td>
                                  <?php echo $data['status_change_date']; ?>
                      
                                  <a href="" data-inquiry-id="<?php echo $data['renting_inquiry_id']; ?>" data-toggle="modal" data-target="#collectedModal<?php echo $data['renting_inquiry_id'];?>" data-vehicle-id="<?php echo $data['renting_renting_vehical_id'];?>" class="label label-success inquiryButton collected" id="collected">Book a vehicle</a>

                                </td>
                            </tr>

                        <?php
                          endif;
                        // endforeach;
                         endforeach;?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
</section>

<!-- Customer modal -->
<?php
  foreach($inquiry as $cus):  
?>
  <div class="modal fade" id="peopleModal<?php echo $cus['renting_inquiry_id'];?>" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Contact Person</h4>
        </div>
        <div class="modal-body">
          <table>
            <tr>
              <td><b>Customer Name</b></td>
              <td><b> : </b></td>
              <td><?php echo $cus['customer_name'];?></td>
            </tr>
            <tr>
              <td><b>Contact Number</b></td>
              <td><b> : </b></td>
              <td><?php echo $cus['phone_number'];?></td>
            </tr>
            
            <tr>
              <td><b>Email</b></td>
              <td><b> : </b></td>
              <td><?php echo $cus['email_address'];?></td>
            </tr>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<?php 

endforeach;
?>

<!-- Vehcle details modal -->
<?php
  foreach($vehicles as $vehicle):
?>
  <div class="modal fade" id="vehicleModal<?php echo $vehicle['renting_vehical_id'];?>" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Vehicle Details</h4>
        </div>
        
        <div class="modal-body">
          <table>
            
            <tr>
              <td><b>Vehicle Type</b></td>
              <td><b> : </b></td>
              <td>
                <?php foreach($types as $vehicleType){
                    if($vehicleType['vehical_type_id']==$vehicle['vehical_vehical_type_id']){
                      echo $vehicleType['vehical_type_name'];
                    }
                } ?>
              </td>
            </tr>
            <tr>
              <td><b>Make</b></td>
              <td><b> : </b></td>
              <td>
                <?php foreach($brands as $vehicleBrand){
                      if($vehicleBrand['vehical_brand_id']==$vehicle['vehical_vehical_brand_id']){
                        echo $vehicleBrand['vehical_brand_name'];
                      }
                  } ?>
              </td>
            </tr>
            <tr>
               <td><b>Model</b></td>
              <td><b> : </b></td>
              <td>
                <?php foreach($model as $vehicleModel){
                      if($vehicleModel['vehical_model_id']==$vehicle['vehical_vehical_model_id']){
                        echo $vehicleModel['vehical_model_name'];
                      }
                  } ?>
              </td>
            </tr>
            <tr>
              <td><b>Transmission</b></td>
              <td><b> : </b></td>
              <td><?php if($vehicle['transmission']==1){
              		echo "Automatic";
              }
              else if($vehicle['transmission']==2){
              		echo "Manual";
              }
              else{
              		echo "Triptonic";
              }
              
              ?></td>
            </tr>
            <tr>
              <td><b>Engine Capacity</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['engine_capacity'];?>
              	<?php if($vehicle['engine_capacity_unit']==1){
              		echo "CC";
              	}
              	else{
              		echo "kW";
              	}
              	 ?>
              </td>
            </tr>
            <tr>
              <td><b>Seating Capacity</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['seating_capacity'];?></td>
            </tr>
            <tr>
              <td><b>Year of Manufacture</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['year_of_manufactured'];?></td>
            </tr>
            <tr>
              <td><b>Year of Registration</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['year_of_registration'];?></td>
            </tr>
            <tr>
              <td><b>Average Fuel Consuption</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['avarage_fuel_consumption'];?>
              	<?php if($vehicle['avarage_fuel_consumption_unit']==1){
              		echo "Km/l";
              	}
              	else{
              		echo "mil/l";
              	}
              	 ?>
              </td>
            </tr>
            <tr>
              <td><b>Vehicle Number</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['vehical_number'];?></td>
            </tr>
            <tr>
              <td><b>Fuel Type</b></td>
              <td><b> : </b></td>
              <td><?php if($vehicle['fuel_type']==1){
              		echo "Petrol";
              }
              else if($vehicle['fuel_type']==2){
              		echo "Diesel";
              }
              else if($vehicle['fuel_type']==3){
              		echo "Hybrid";
              }
              
              else{
              		echo "Electric";
              }
              ?></td>
            </tr>
            <tr>
              <td><b>Per Day Price</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['per_day_price'];?></td>
            </tr>
            <tr>
              <td><b>Vehicle Features</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['vehicle_features'];?></td>
            </tr>
           
            <tr>
              <td><b>Images</b></td>
              <?php foreach($outside_images as $key):?>
              <?php if($key['renting_vehical_id']==$vehicle['renting_vehical_id']){?>
              <td></td>
              <td> <img src="<?php echo base_url(); ?>upload/renting/outside/<?php echo $key['renting_vehical_images']; ?>" style="vertical-align:text-top;height: 61px;margin-top: 0px;margin-left: 3px;" ></td>
              <?php }?>
              <?php endforeach;?>
            </tr>
            <tr>
              <td></td>
            </tr>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<?php 
endforeach;
?>

<!-- book a vehicle modal -->
<?php 
$countinquiry = 1;
foreach($inquiry as $data): 

$inq_id = $data['renting_inquiry_id'];
?>

<div class="modal fade" id="collectedModal<?php echo $data['renting_inquiry_id'];?>" role="dialog">
<div class="modal-dialog">
  <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">Book a vehicle</h4>
    </div>
      <!-- <?php print_r($inq_id);?> -->
      <div class="modal-body">
        <form role="form" action="<?php echo base_url('renting/bookvehicle');?>" method="POST" enctype='multipart/form-data' class="formpc">
        <input type="hidden" name="inq_id" id="inq_id" value="<?php echo $inq_id;?>">

        <?php 

        foreach($vehicles as $vehicle):
        if($vehicle['renting_vehical_id']==$data['renting_renting_vehical_id']):
          echo 'Vehicle'.sprintf("%03d",$data['renting_renting_vehical_id']);
        ?>

      <div class="form-group">
        <label for="home_page_grid_description">Duration<b></label>
        </div>
        <div class="form-group">
          <div class="row">
            <div class="col-md-6">
              <label>From</label>
              <input type="date" name="from_date" class="form-control from_date" id="from_date<?php echo $countinquiry;?>">
            </div>

            <div class="col-md-6">
              <label>To</label>
              <input type="date" name="to_date" class="form-control to_date" id="to_date<?php echo $countinquiry;?>">
            </div>

            <a href="#" data-id="<?php echo $countinquiry;?>" data-inquiry-id="<?php echo $data['renting_inquiry_id']; ?>" data-vehicle-id="<?php echo $data['renting_renting_vehical_id'];?>" class="label label-warning getTotalButton" style="margin-left:10px; margin-top:5px;">Get Total</a><br><br>
            
            <div class="col-md-12">
              <label><b>Cost Details</b></label><br>
              <label>Per Day Price(Rs.)</label>
              <input type="text" name="view_perday_price" class="form-control" id="view_perday_price<?php echo $countinquiry;?>" value="<?php echo $vehicle['per_day_price']; ?>">
            </div>

            <div class="col-md-12">
              <label>Discount</label>
              <input type="text" name="discount_vehicle" class="form-control" id="discount_vehicle<?php echo $countinquiry;?>" value="<?php echo $vehicle['discount']; ?>">
            </div>

            <div class="col-md-12">
              <label>Total</label>
              <input type="text" name="vehicle_total" class="form-control" id="vehicle_total<?php echo $countinquiry;?>">
            </div>

            <div class="col-md-12">
              <label>Deposit Amount(Rs.)</label>
              <input type="text" name="diposit_amount" class="form-control" id="diposit_amount" value="">
            </div>

            <div class="col-md-12">
              <label>Duration(Days)</label>
              <input type="text" name="duration" class="form-control" id="duration<?php echo $countinquiry;?>" value="">
            </div>
          </div>
        </div>
      <?php 
        endif;
        endforeach;
      ?>

    </div>
    <div class="modal-footer">
      <button type="submit" class="btn btn-success">Book a vehicle</button>
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>
  </form>
  
</div>
</div>
<?php 
$countinquiry++;
endforeach;
?>
