<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>
  <style type="text/css">
    input[type=file]{
      display: inline;
    }
    #image_preview{
      border: 1px solid black;
      padding: 10px;
    }
    #image_preview img{
      width: 200px;
      padding: 5px;
    }
  </style>
<section class="content">
            <div class="container-fluid">
            </div>

  <!-- Main content -->
  <div class="row">
    <div class="col-md-12">
        <div class="panel panel-filled">
            <div class="panel-heading">
                <div class="panel-tools">
                    <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                    <a class="panel-close"><i class="fa fa-times"></i></a>
                </div>
                Add New Renting Vehicle
            </div>
            <div class="panel-body">
              <?php echo $this->session->flashdata('msg'); ?>
              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
                <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <!-- <div class="box-header with-border">
            <h3 class="box-title"><?php echo $pagetitle; ?></h3>
          </div> --><!-- /.box-header -->
          <!-- form start -->
          <form role="form" action="<?php echo base_url('renting/insertvehicle'); ?>" method="POST" enctype='multipart/form-data'>
            <div class="box-body">
              <!-- <?php echo $this->session->flashdata('msg'); ?>
              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?> -->

              <div class="form-group">
            <div class="row">        
              <div class="col-md-6">
                <label>Vehicle Type</label>
                <select class="form-control" id="typeSelectRent" name="vehicle_type" required>
                  <option value="">-- Select Vehicle Type --</option>
                   <?php foreach($types as $type): ?>
                   <option value="<?php echo $type['vehical_type_id']; ?>"><?php echo $type['vehical_type_name']; ?></option>
                   <?php endforeach; ?>
                </select>
              </div>

              <div class="col-md-6">
                <label>Vehicle Brand</label>
                 <select class="form-control" id="makeSelectRent" name="vehical_brand" disabled>
                  <option value="">-- Select Vehicle Brand --</option>
                   <?php foreach($Brands as $brand): ?>
                   <option value="<?php echo $brand['vehical_brand_id']; ?>" class="<?php echo "fuck".$brand['vehical_type_id']."fuck";?>"><?php echo $brand['vehical_brand_name']; ?></option>
                   <?php endforeach; ?>
                </select>
              </div>
              
              <div class="col-md-6">
                <label>Vehicle Model</label>
                <select class="form-control" name="vehicle_model" required id="modelSelectRent" disabled>
                      <option value="">-- Select Vehicle Model --</option>
                       <?php foreach($modals as $vehiclemodel): ?>
                      <option class="<?php echo "fuck".$vehiclemodel['vehical_brand_id']."fuck"; ?>" value="<?php echo $vehiclemodel['vehical_model_id']; ?>"><?php echo $vehiclemodel['vehical_model_name']; ?></option>
                       <?php endforeach; ?>
                    </select>                 
              </div>

              <div class="col-md-6">
                <label>Year of Registration</label>
                <input type="number" class="form-control" id="year_of_registration" placeholder="Enter Year of Registration" name="year_of_registration">
              </div>
              <div class="col-md-6">
                <label>Year of Manufacture</label>
                <input type="number" class="form-control" id="year_of_manufacture" placeholder="Enter Year of Manufacture" name="year_of_manufacture">
              </div>
               <div class="col-md-6">
                <label>Vehicle Number</label>
                <input type="text" class="form-control" id="vehicle_num" placeholder="Enter vehicle number" name="vehical_number">
              </div>
              <div class="col-md-6">
                <label>Transmission</label>
               <select class="form-control" id="vehicle_transmission" name="vehicle_transmission">
                  <option value="">-- Select Transmission --</option>
                   <option value="1">Automatic</option>
                   <option value="2">Manual</option>
                   <option value="3">Triptonic</option>
                </select>
              </div>
              
              <div class="col-md-4">
                <label>Vehicle Mileage Range</label>
                <input type="number" class="form-control" id="vehicle_milage_range" placeholder="Enter Vehicle Mileage Range (Km)" name="vehicle_mileage_range" required>
              </div>
              <div class="col-md-2">
                    <label> Range</label>
                    <select class="form-control" id="vehicle_range1" name="vehicle_range"  required>
                        <option>--Select Range--</option>
                        <option value="1" data-id="1">Km</option>
                        <option value="2" data-id="2">mi</option>
                    </select>
                </div>
              
               <div class="col-md-6">
                <label>Fuel Type</label>
               <select class="form-control" id="fuel_type" name="fuel_type">
                  <option value="">-- Select Fuel Type --</option>
                   <option value="1">Petrol</option>
                   <option value="2">Diesel</option>
                   <option value="3">Hybrid</option>
                   <option value="4">Electric</option>
                </select>
              </div>

               <div class="col-md-4">
                <label>Average of Fuel Consuption</label>
                <input type="number" class="form-control" id="average_of_fuel_consuption" placeholder="Enter Average of Fuel Consuption" name="average_of_fuel_consuption">
              </div>
              <div class="col-md-2">
                    <label> Unit</label>
                    <select class="form-control" id="fuel_con1" name="letersPer" >
                        <option>--Select Consumption type--</option>
                        <option value="1">Km/l</option>
                        <option value="2">mi/l</option>

                    </select>
                </div>

              <div class="col-md-4">
                <label>Vehicle Engine Capacity</label>
                <input type="number" class="form-control" id="vehicle_engine_capacity" placeholder="Enter Vehicle Engine Capacity" name="vehicle_engine_capacity">
              </div>
              <div class="col-md-2">
                    <label> Capacity</label>
                    <select class="form-control" id="Capacity" name="capacity" >
                        <option>--Select Capacity--</option>
                        <option value="1">CC</option>
                        <option value="2">kW</option>
                    </select>
                </div>
                
              <div class="col-md-6">
                <label>Vehicle Seating Capacity</label>
                <input type="number" class="form-control" id="vehicle_seating_capacity" placeholder="Enter Vehicle Seating Capacity" name="vehicle_seating_capacity">
              </div>

              <div class="col-md-12" >
                <div><label>Vehicle Features</label></div>
                
                <div class="col-md-3">
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="A/C: Front">A/C: Front</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Power Locks">Power Locks</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Airbag: Driver">Airbag: Driver</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Anti-Lock Brakes">Anti-Lock Brakes</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Rear Window Wiper">Rear Window Wiper</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="USB">USB</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Tow Package">Tow Package</label>
                </div>
                <div class="col-md-3">
                  
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="A/C: Rear">A/C: Rear</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Multifunction Steering">Multifunction Steering</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Alarm">Alarm</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Rear Window Defroster">Rear Window Defroster</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="DVD">DVD</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Third Row Seats">Third Row Seats</label><br>
                </div>
                <div class="col-md-3">

                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Cruise Control">Cruise Control</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Power Steering">Power Steering</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Airbag: Passenger">Airbag: Passenger</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Fog Lights">Fog Lights</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Tinted Glass">Tinted Glass</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Alloy Wheels">Alloy Wheels</label><br>
                </div>
                <div class="col-md-3" style="margin-bottom: 26px;">
                
                <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Navigation System">Navigation System</label><br>
                <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Remote Keyless Entry">Remote Keyless Entry</label><br>
                <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Airbag: Side">Airbag: Side</label><br>
                <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Power Windows">Power Windows</label><br>
                <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="CD">CD</label><br>
                <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Sunroof/Moonroof">Sunroof/Moonroof</label><br>
                </div>
                <br>
                <div class="form-group">
                  <label for="comment">Additional Features:</label>
                  <textarea class="form-control" rows="5" id="comment" name="additional_features"></textarea>
                </div>
              </div>

               <div class="col-md-6">
                <label>Per day price</label>
                <input type="number" class="form-control" id="per_day_price" placeholder="Enter per day price" name="per_day_price" required>
              </div>

              <div class="col-md-6">
                <label>Discount</label>
                <input type="number" class="form-control" id="discount" placeholder="Enter Discount" name="discount">
              </div>
		
              <div class="col-md-6 style="margin-top: 25px;">
                <label>Front Image</label>
                <input type="file" name="inside_images" id="uploadFile1" accept=".png,.jpg,.jpeg,.gif">
              </div>
              <br><br>
              
               <div class="col-md-6" style="margin-top: 25px;" style="
    margin-top: 21px;
">
                <label>Outside Images</label>
                <input type="file" name="userfile[]" id="uploadFile" accept=".png,.jpg,.jpeg,.gif" multiple>
                <p class="help-block">Image size should be below then 3mb and JPG, JPEG,PNG, GIF can be uploaded.<br/>
                
                </p>
              </div> 

              <div id="details" class="details">
                <div class="form-group">
                  <input type="checkbox" name="checkbox" class="cov" id="cov" value="0">
                  <label>Comapany Owned Vehical </label><br>
                  <input type="checkbox" name="checkbox" class="Individual" id="Individual" value="1">
                  <label>Vehical Owned By Individual</label>
                  
                </div>
              <div id="details_form" class="details_form">
                <div class="col-md-6">
                  <label>Name</label>
                  <input type="text" name="name" id="name" class="form-control" placeholder="Name">
                </div>
                <div class="col-md-6">
                  <label>Phone Number</label>
                  <input type="text" name="phone_number" id="phone_number" class="form-control" placeholder="Phone Number">
                </div>
                <div class="col-md-6">
                  <label>Adress</label>
                  <input type="text" name="adress" id="adress" class="form-control" placeholder="Address">
                </div>
                <div class="col-md-6">
                  <label>Email</label>
                  <input type="text" name="Email" id="Email" class="form-control" placeholder="Email">
                </div>
              </div>    
            </div>

               
              

              

            </div>
             <div id="image_preview1"></div>
            </div>
              <div id="image_preview"> </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Add New Vehicle</button>
              <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
            </div>
          </form>
        </div><!-- /.box -->

      </div><!--/.col (full) -->
    </div>

            </div>
        </div>
    </div>
</div>
</section>

<script type="text/javascript">
  
  $("#uploadFile").change(function(){
     $('#image_preview').html("");
     var total_file=document.getElementById("uploadFile").files.length;

     for(var i=0;i<total_file;i++)
     {
      $('#image_preview').append("<img src='"+URL.createObjectURL(event.target.files[i])+"'>");
     }

  });

  

</script>

<script type="text/javascript">
  
  $("#uploadFile1").change(function(){
     $('#image_preview1').html("");
     var total_file=document.getElementById("uploadFile1").files.length;

     for(var i=0;i<total_file;i++)
     {
      $('#image_preview1').append("<img src='"+URL.createObjectURL(event.target.files[i])+"'>");
     }

  });

  

</script>