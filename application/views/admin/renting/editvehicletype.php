<section class="content">
    <div class="container-fluid">
    </div>

    <!-- Main content -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-filled">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                        <a class="panel-close"><i class="fa fa-times"></i></a>
                    </div>
                    Add Vehicle Type
                </div>
                <div class="panel-body">
                    <?php echo $this->session->flashdata('msg'); ?>
                    <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <!-- <div class="box-header with-border">
            <h3 class="box-title"></h3>
          </div> --><!-- /.box-header -->
                                <?php foreach ($types as $type){
                                    $type_id = $type['vehical_type_id'];
                                    $name = $type['vehical_type_name'];
                                }?>
                                <!-- form start -->
                                <form role="form" action="<?php echo base_url('renting/updatetype/'.$type_id); ?>" method="POST" enctype='multipart/form-data'>
                                    <div class="box-body">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Vehicle Type</label>
                                                    <input type="text" class="form-control" id="vehicle_type_name" placeholder="Enter Vehicle Type" name="vehicle_type_name" value="<?php echo $name; ?>">
                                                </div>
                                            </div><br>
                                            <div class="box-footer">
                                                <button type="submit" class="btn btn-primary">Update Type</button>
                                                <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- /.box-body -->
                                </form>
                            </div><!-- /.box -->

                        </div><!--/.col (full) -->
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>