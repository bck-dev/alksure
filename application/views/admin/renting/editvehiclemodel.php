<section class="content">
    <div class="container-fluid">
    </div>

    <!-- Main content -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-filled">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                        <a class="panel-close"><i class="fa fa-times"></i></a>
                    </div>
                    Edit Vehicle Modal
                </div>
                <div class="panel-body">
                    <?php echo $this->session->flashdata('msg'); ?>
                    <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <!-- <div class="box-header with-border">
            <h3 class="box-title"><?php echo $pagetitle; ?></h3>
          </div> --><!-- /.box-header -->
                                <?php foreach ($modals as $model){
                                    $model_id = $model['vehical_model_id'];
                                    $model_name = $model['vehical_model_name'];
                                    $brand_id = $model['vehical_brand_id'];
                                    $type_id = $model['vehical_type_id'];
                                    
                                }?>
                                <!-- form start -->
                                <form role="form" action="<?php echo base_url('renting/updatemodal/'.$model_id); ?>" method="POST" enctype='multipart/form-data'>
                                    <div class="box-body">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Vehicle Type</label>
                                                    <select class="form-control" id="vehicle_type_edit_rent" name="vehicle_type_rent_edit" required>
                                                        <option value="">-- Select Vehicle Type --</option>
                                                        <?php foreach($Types as $type): ?>
                                                            <option value="<?php echo $type['vehical_type_id']; ?>" <?php echo ($type_id == $type['vehical_type_id']) ? 'selected' : ''; ?> ><?php echo $type['vehical_type_name']; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Vehicle Brand</label>
                                                    <select class="form-control" id="vehicle_brand_edit_rent" name="vehicle_brand_rent_edit" required>
                                                        <option value="">-- Select Vehicle Brand --</option>
                                                        <?php foreach($brands as $brand): ?>
                                                            <option value="<?php echo $brand['vehical_brand_id']; ?>" class="<?php echo $brand['vehical_type_id']; ?>" <?php echo ($brand_id == $brand['vehical_brand_id']) ? 'selected' : ''; ?> ><?php echo $brand['vehical_brand_name']; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Vehicle Modal</label>
                                                    <input type="text" class="form-control" id="vehicle_modal_name" placeholder="Enter Vehicle Type" name="vehicle_modal_name" value="<?php echo $model_name; ?>">
                                                </div>
                                            </div><br>
                                            <div class="box-footer">
                                                <button type="submit" class="btn btn-primary">Update Modal</button>
                                                <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- /.box-body -->
                                </form>
                            </div><!-- /.box -->

                        </div><!--/.col (full) -->
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>