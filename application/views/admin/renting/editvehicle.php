<section class="content">
    <div class="container-fluid">
    </div>

    <!-- Main content -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-filled">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                        <a class="panel-close"><i class="fa fa-times"></i></a>
                    </div>
                    Edit Renting Vehicle
                </div>
                <div class="panel-body">
                    <?php echo $this->session->flashdata('msg'); ?>
                    <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <!-- <div class="box-header with-border">
            <h3 class="box-title"><?php echo $pagetitle; ?></h3>
          </div> --><!-- /.box-header -->
                                <?php foreach($renting as $vehi){
                                    $type_id = $vehi['vehical_vehical_type_id'];
                                    $brand_id = $vehi['vehical_vehical_brand_id'];
                                    $model_id = $vehi['vehical_vehical_model_id'];
                                    $vehicle_id = $vehi['renting_vehical_id'];
                                    $per_day_price = $vehi['per_day_price'];
                                    $vehical_number = $vehi['vehical_number'];
                                    $transmission = $vehi['transmission'];
                                    $millage_range = $vehi['millage_range'];;
                                    $engine_capacity = $vehi['engine_capacity'];
                                    $seating_capacity = $vehi['seating_capacity'];
                                    $year_of_manufactured = $vehi['year_of_manufactured'];
                                    $year_of_registration = $vehi['year_of_registration'];
                                    $fuel_type = $vehi['fuel_type'];
                                    $number_of_owners = $vehi['number_of_owners'];
                                    $avarage_fuel_consumption = $vehi['avarage_fuel_consumption'];
                                    $features = explode(',',$vehi['vehicle_features']);
                                    $additional_features = $vehi['additional_features'];
                                    $discount = $vehi['discount'];
                                    $per_day_price = $vehi['per_day_price'];
                                    $number_of_passenger = $vehi['number_of_passenger'];
                                    $number_of_doors = $vehi['number_of_doors'];
                                    $AC_status = $vehi['A/C status'];
                                    $owned_by = $vehi['owned_by'];
                                    $name = $vehi['name'];
                                    $address = $vehi['address'];
                                    $phone_number = $vehi['phone_number'];
                                    $email = $vehi['email'];
                                    $inside_images = $vehi['renting_vehical_inside_image'];

                                }?>

                                <!-- form start -->
                                <form role="form" action="<?php echo base_url('renting/updatevehicle/'.$vehicle_id); ?>" method="POST" enctype='multipart/form-data'>
                                    <div class="box-body">
                                        <!-- <?php echo $this->session->flashdata('msg'); ?>
                                        <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?> -->

                                        <div class="form-group">
                                                    <?php
                                                    if (isset($vehicle_images) && is_array($vehicle) && count($vehicle)): $i = 1;
                                                        foreach ($vehicle_images as $key => $data) {
                                                            ?>
                                                            <div class="imagelocation<?php echo $data['renting_image_id'] ?>">
                                                                <br/>
                                                                <img src="<?php echo base_url(); ?>upload/renting/outside/<?php echo $data['renting_vehical_images']; ?>"
                                                                     style="vertical-align:text-top;" width="200" height="200">
                                                                <a href="" style="cursor:pointer;"
                                                                   data-imagerent-id="<?php echo $data['renting_image_id']; ?>"
                                                                   class="label label-danger imagerentConfirm">X</a>
                                                            </div>
                                                        <?php }endif; ?>
                                                </div>
                                                <div class="form-group">
                                                    <label>Outside Images</label>
                                                    <input type="file" name="userfile[]" id="image_file" accept=".png,.jpg,.jpeg,.gif"
                                                           multiple>
                                                           <p style="color: red">Minimum Upload 2 or more images</p>
                                                    <p class="help-block">Image size should be below then 3mb and JPG, JPEG,PNG, GIF can be
                                                        uploaded.<br/>
                                                        Recomanded resolution for images is <strong>120 x 120</strong> pixels.
                                                    </p>
                                                </div><br><br>
                                        
                                        
                                         <div class="col-md-12">
                                                    <label>inside Image</label>
                                                    <input type="hidden" id="inside_images" name="inside_images"
                                                           value="<?php echo $inside_images; ?>">
                                                    <div class="image-group" style="position: relative; margin-top: 20px;">
                                                        <img src="<?php echo base_url('upload/renting/inside'); ?>/<?php echo $inside_images; ?>"
                                                             class="thumbnail" alt="<?php echo $inside_images; ?>" style="width: 20%">
                                                        <span class="img-close">&#10006;</span>
                                                    </div>
                                                    <input type="file" id="input-image" name="fileinput">

                                                    <p class="help-block">Image size should be below then 3mb and JPG, JPEG,PNG, GIF can be
                                                        uploaded.</p>
                                                </div>

                                                <br><br>
                                        
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Vehicle Type</label>
                                                    <select class="form-control" id="vehicle_type" name="vehicle_type">
                                                        <option value="">-- Select Vehicle Type --</option>
                                                        <?php foreach($types as $type): ?>
                                                            <option value="<?php echo $type['vehical_type_id']; ?>" <?php echo ($type_id == $type['vehical_type_id']) ? 'selected' : ''; ?>><?php echo $type['vehical_type_name']; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Vehicle Brand</label>
                                                    <select class="form-control" id="vehicle_brand" name="vehicle_brand">
                                                        <option value="">-- Select Vehicle Brand --</option>
                                                        <?php foreach($brands as $brand): ?>
                                                            <option value="<?php echo $brand['vehical_brand_id']; ?>" <?php echo ($brand_id == $brand['vehical_brand_id']) ? 'selected' : ''; ?>><?php echo $brand['vehical_brand_name']; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Vehicle Model</label>
                                                    <select class="form-control" name="vehicle_model" id="">
                                                        <option value="">-- Select Vehicle Modal --</option>
                                                        <?php foreach($modals as $vehiclemodel): ?>
                                                            <option value="<?php echo $vehiclemodel['vehical_model_id']; ?>" <?php echo ($model_id == $vehiclemodel['vehical_model_id']) ? 'selected' : ''; ?>><?php echo $vehiclemodel['vehical_model_name'] ?> </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>

                                                <div class="col-md-6">
                                                    <label>Year of Registration</label>
                                                    <input type="text" class="form-control" id="year_of_registration" placeholder="Enter Year of Registration" name="year_of_registration" value="<?php echo $year_of_registration ?>">
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Year of Manufacture</label>
                                                    <input type="text" class="form-control" id="year_of_manufacture" placeholder="Enter Year of Manufacture" name="year_of_manufacture" value="<?php echo $year_of_manufactured ?>">
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Vehicle Number</label>
                                                    <input type="text" class="form-control" id="vehicle_num" placeholder="Enter vehicle number" name="vehical_number" value="<?php echo $vehical_number?>">
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Transmission</label>
                                                    <select class="form-control" id="vehicle_transmission" name="vehicle_transmission">
                                                        <option value="">-- Select Transmission --</option>
                                                        <?php for($x=1; $x<=2; $x++) : ?>
                                                            <option value="<?php echo $x; ?>" <?php echo ($x == $transmission) ? 'selected' : ''; ?>><?php echo ($x == '2') ? 'Manual' :'Automatic' ; ?></option>
                                                        <?php endfor; ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Vehicle Mileage Range</label>
                                                    <input type="text" class="form-control" id="vehicle_milage_range" placeholder="Enter Vehicle Mileage Range (Km)" name="vehicle_mileage_range" value="<?php echo $millage_range ?>" required>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Fuel Type</label>
                                                    <select class="form-control" id="fuel_type" name="fuel_type">
                                                        <option value="">-- Select Fuel Type --</option>
                                                        <?php for($x=1; $x<=2; $x++) : ?>
                                                            <option value="<?php echo $x; ?>" <?php echo ($x == $fuel_type) ? 'selected' : ''; ?>><?php echo ($x == '2') ? 'Diesel' :'Petrol' ; ?></option>
                                                        <?php endfor; ?>
                                                    </select>
                                                </div>

                                                <div class="col-md-6">
                                                    <label>Average of Fuel Consuption</label>
                                                    <input type="text" class="form-control" id="average_of_fuel_consuption" placeholder="Enter Average of Fuel Consuption" name="average_of_fuel_consuption" value="<?php echo $avarage_fuel_consumption; ?>">
                                                </div>

                                                <div class="col-md-6">
                                                    <label>Vehicle Engine Capacity</label>
                                                    <input type="text" class="form-control" id="vehicle_engine_capacity" placeholder="Enter Vehicle Engine Capacity (CC)" name="vehicle_engine_capacity" value="<?php echo  $engine_capacity; ?>">
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Vehicle Seating Capacity</label>
                                                    <input type="text" class="form-control" id="vehicle_seating_capacity" placeholder="Enter Vehicle Seating Capacity" name="vehicle_seating_capacity" value="<?php echo $seating_capacity; ?>">
                                                </div>


                                                <div class="col-md-6">
                                                    <label>Per day price</label>
                                                    <input type="text" class="form-control" id="per_day_price" placeholder="Enter per day price" name="per_day_price" value="<?php echo $per_day_price; ?>" required>
                                                </div>

                                                <div class="col-md-6">
                                                    <label>Discount</label>
                                                    <input type="text" class="form-control" id="discount" placeholder="Enter Discount" name="discount" value="<?php echo $discount; ?>">
                                                </div>
                                                 <div class="col-md-12" >
                <div><label>Vehicle Features</label></div>
                
                <div class="col-md-3">
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="A/C: Front" <?php echo in_array('A/C: Front',$features) ? 'checked': '' ?>>A/C: Front</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Power Locks" <?php echo in_array('Power Locks',$features) ? 'checked': '' ?>>Power Locks</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Airbag: Driver" <?php echo in_array('Airbag: Driver',$features) ? 'checked': '' ?>>Airbag: Driver</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Anti-Lock Brakes" <?php echo in_array('Anti-Lock Brakes',$features) ? 'checked': '' ?>>Anti-Lock Brakes</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Rear Window Wiper" <?php echo in_array('Rear Window Wiper',$features) ? 'checked': '' ?>>Rear Window Wiper</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="USB" <?php echo in_array('USB',$features) ? 'checked': '' ?>>USB</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Tow Package" <?php echo in_array('Tow Package',$features) ? 'checked': '' ?>>Tow Package</label>
                </div>
                <div class="col-md-3">
                  
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="A/C: Rear" <?php echo in_array('A/C: Rear',$features) ? 'checked': '' ?>>A/C: Rear</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Multifunction Steering" <?php echo in_array('Multifunction Steering',$features) ? 'checked': '' ?>>Multifunction Steering</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Alarm" <?php echo in_array('Alarm',$features) ? 'checked': '' ?>>Alarm</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Rear Window Defroster" <?php echo in_array('Rear Window Defroster',$features) ? 'checked': '' ?>>Rear Window Defroster</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="DVD" <?php echo in_array('DVD',$features) ? 'checked': '' ?>>DVD</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Third Row Seats" <?php echo in_array('Third Row Seats',$features) ? 'checked': '' ?>>Third Row Seats</label><br>
                </div>
                <div class="col-md-3">

                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Cruise Control" <?php echo in_array('Cruise Control',$features) ? 'checked': '' ?>>Cruise Control</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Power Steering" <?php echo in_array('Power Steering',$features) ? 'checked': '' ?>>Power Steering</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Airbag: Passenger" <?php echo in_array('Airbag: Passenger',$features) ? 'checked': '' ?>>Airbag: Passenger</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Fog Lights" <?php echo in_array('Fog Lights',$features) ? 'checked': '' ?>>Fog Lights</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Tinted Glass" <?php echo in_array('Tinted Glass',$features) ? 'checked': '' ?>>Tinted Glass</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Alloy Wheels" <?php echo in_array('Alloy Wheels',$features) ? 'checked': '' ?>>Alloy Wheels</label><br>
                </div>
                <div class="col-md-3" style="margin-bottom: 26px;">
                
                <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Navigation System" <?php echo in_array('Navigation System',$features) ? 'checked': '' ?>>Navigation System</label><br>
                <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Remote Keyless Entry" <?php echo in_array('Remote Keyless Entry',$features) ? 'checked': '' ?>>Remote Keyless Entry</label><br>
                <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Airbag: Side" <?php echo in_array('Airbag: Side',$features) ? 'checked': '' ?>>Airbag: Side</label><br>
                <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Power Windows" <?php echo in_array('Power Windows',$features) ? 'checked': '' ?>>Power Windows</label><br>
                <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="CD" <?php echo in_array('CD',$features) ? 'checked': '' ?>>CD</label><br>
                <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Sunroof/Moonroof" <?php echo in_array('Sunroof/Moonroof',$features) ? 'checked': '' ?>>Sunroof/Moonroof</label><br>
                </div>
                <div class="form-group">
                  <label for="comment">Additional Features:</label>
                  <textarea class="form-control" rows="5" id="comment" name="additional_features"></textarea>
                </div>
              </div>

                                               
                                                


                                                <div id="details" class="details">
                                                    <div class="form-group">
                                                        <input type="checkbox" name="checkbox" class="cov" <?php if ($owned_by == 0){ ?>checked <?php }?> id="cov" value="0">
                                                        <label>Comapany Owned Vehical </label><br>
                                                        <input type="checkbox" name="checkbox" class="Individua" <?php if ($owned_by == 1){ ?>checked <?php }?> id="Individua" value="1">
                                                        <label>Vehical Owned By Individual</label>

                                                    </div>
                                                    <div id="detail_form" class="detail_form">
                                                        <div class="col-md-6">
                                                            <label>Name</label>
                                                            <input type="text" name="name" id="name" class="form-control" placeholder="Name" value="<?php echo $name ?>">
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label>Phone Number</label>
                                                            <input type="text" name="phone_number" id="phone_number" class="form-control" placeholder="Phone Number" value="<?php echo $phone_number; ?>">
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label>Adress</label>
                                                            <input type="text" name="adress" id="adress" class="form-control" placeholder="Address" value="<?php echo $address ?>">
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label>Email</label>
                                                            <input type="text" name="Email" id="Email" class="form-control" placeholder="Email" value="<?php echo $email ?>">
                                                        </div>
                                                    </div>
                                                </div>





                                            </div>

                                        </div>
                                        <!-- /.box-body -->
                                        <div class="box-footer">
                                            <button type="submit" class="btn btn-primary">Update Vehicle</button>
                                            <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
                                        </div>
                                </form>
                            </div><!-- /.box -->

                        </div><!--/.col (full) -->
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>