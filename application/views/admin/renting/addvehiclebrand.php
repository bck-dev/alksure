<section class="content">
            <div class="container-fluid">
            </div>

  <!-- Main content -->
  <div class="row">
    <div class="col-md-12">
        <div class="panel panel-filled">
            <div class="panel-heading">
                <div class="panel-tools">
                    <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                    <a class="panel-close"><i class="fa fa-times"></i></a>
                </div>
                Add Vehicle Type
            </div>
            <div class="panel-body">
              <?php echo $this->session->flashdata('msg'); ?>
              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
                <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <!-- <div class="box-header with-border">
            <h3 class="box-title"><?php echo $pagetitle; ?></h3>
          </div> --><!-- /.box-header -->
          <!-- form start -->
           <form role="form" action="<?php echo base_url('renting/insertbrand'); ?>" method="POST" enctype='multipart/form-data'>
            <div class="box-body">
              <div class="form-group">
                <div class="row">
                <div class="col-md-6">
                  <label>Vehicle Type</label>
                  <select class="form-control" id="vehicle_type_rent" name="vehicle_type" required>
                    <option value="">-- Select Vehicle Type --</option>
                    <?php foreach($type as $ty): ?>
                     <option value="<?php echo $ty['vehical_type_id']; ?>"><?php echo $ty['vehical_type_name']; ?></option>
                   <?php endforeach; ?>
                 </select>
               </div>
             </div>
                  
              <div class="row">
              <div class="col-md-6">
                <label>Vehicle Brand</label>
                <input type="text" class="form-control" id="vehicle_brand_name" placeholder="Enter Vehicle Brand" name="vehicle_brand_name">
              </div>
              </div><br>
              <div class="box-footer">
              <button type="submit" class="btn btn-primary">Add New Brand</button>
              <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
            </div>
            </div>
           
            </div>
            <!-- /.box-body -->
          </form>
        </div><!-- /.box -->

      </div><!--/.col (full) -->
    </div>

            </div>
        </div>
    </div>
</div>
</section>