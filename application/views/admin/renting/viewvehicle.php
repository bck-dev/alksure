<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-filled">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                            <a class="panel-close"><i class="fa fa-times"></i></a>
                        </div>
                        List Vehicle
                    </div>
                    <div class="panel-body">

                        <div class="table-responsive">

                            <table id="tableExample3" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Image</th>
                                    <th>Type</th>
                                    <th>Brand</th>
                                    <th>Model</th>
                                    <th>Per Day Price (RS)</th>
                                    <th>Options</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $count = 0;
                                foreach($vehicle as $data):?>

                                    <?php

                                    $vehicle_id = $data['renting_vehical_id'];


                                    ?>

                                    <tr>
                                        <td><?php echo ++$count; ?>.</td>
                                        <td>

                                            
                                                    <img src="<?php echo base_url(); ?>upload/renting/inside/<?php echo $data['renting_vehical_inside_image']; ?>" style="vertical-align:text-top;height: 61px;margin-top: 0px;margin-left: 3px;" >
                                                    

                                           
                                        </td>

                                        <td><?php echo $data['vehical_type_name']; ?></td>
                                        <td><?php echo $data['vehical_brand_name']; ?></td>
                                        <td><?php echo $data['vehical_model_name']; ?></td>
                                        <td><?php echo $data['per_day_price']; ?></td>
                                        <td>
                                            <a href="" class="label label-primary" data-toggle="modal" data-target="#exampleModal<?php echo $data['renting_vehical_id'];?>" data-whatever="@mdo">View</a>
                                            <a href="<?php echo base_url(); ?>renting/editvehicle/<?php echo $data['renting_vehical_id']; ?>" class="label label-success">Edit</a>
                                            <a href="" data-vehiclerent-id="<?php echo $data['renting_vehical_id']; ?>" class="label label-danger vehicleConfirm">Delete</a>
                                    </tr>

                                <?php endforeach; ?>



                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

        </div>


    </div>
</section>



<?php foreach($vehicle as $vehi):

    $vehicle_id = $vehi['renting_vehical_id'];
    ?>

    <div class="modal fade" id="exampleModal<?php echo $vehi['renting_vehical_id'];?>" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Book Vehicle Details</h4>
                </div>
                <!-- <?php print_r($inq_id);?> -->
                <div class="modal-body">

                        <label for="home_page_grid_description">Duration<b></label>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Outside Images</label>
                                    <?php foreach ($outside as $outs): ?>
                                        <?php if ($outs['renting_vehical_id'] == $vehicle_id ): ?>
                                            <img src="<?php echo base_url(); ?>upload/renting/outside/<?php echo $outs['renting_vehical_images']; ?>" style="vertical-align:text-top;height: 61px;margin-top: 0px;margin-left: 3px;" >

                                        <?php endif ?>
                                    <?php endforeach ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Inside image</label>
                                    <img src="<?php echo base_url(); ?>upload/renting/inside/<?php echo $vehi['renting_vehical_inside_image']; ?>" style="vertical-align:text-top;height: 61px;margin-top: 0px;margin-left: 3px;" >

                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Vehicle number</label>
                                    <?php echo $vehi['vehical_number'];?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Transmission</label>
                                    <?php if($vehi['transmission'] == 1){
                                        echo 'Automatic';}else{
                                        echo 'Manual';
                                    }?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Millage range</label>
                                    <?php echo $vehi['millage_range'];?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Engine capacity</label>
                                    <?php echo $vehi['engine_capacity'];?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Seating capacity </label>
                                    <?php echo $vehi['seating_capacity'];?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Year of manufactured</label>
                                    <?php echo $vehi['year_of_manufactured'];?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Year of registration</label>
                                    <?php echo $vehi['year_of_registration'];?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Fuel type</label>
                                    <?php if($vehi['fuel_type']==1){
                                        echo 'Petrol';}else{
                                        echo 'Diesel';
                                    }?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Number of owners</label>
                                    <?php echo $vehi['number_of_owners'];?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Avarage fuel consumption</label>
                                    <?php echo $vehi['avarage_fuel_consumption'];?>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Features</label>
                                    <?php echo $vehi['vehicle_features'];?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Discount</label>
                                    <?php echo $vehi['discount'];?>
                                </div>
                            </div>
                        </div>

                    <?php  if($vehi['owned_by']==0){?>
                    <div><h4 class="modal-title" id="" style="color: #0f83c9; font-size: 16px">Comapany Owned Vehical</h4></div><?php }else{?>
                    <div><h4 class="modal-title" id="" style="color: #0f83c9; font-size: 16px">Vehical Owned By Individual</h4></div><br>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Name</label>
                                    <?php echo $vehi['name'];?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>address</label>
                                    <?php echo $vehi['address'];?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Phone number</label>
                                    <?php echo $vehi['phone_number'];?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Email</label>
                                    <?php echo $vehi['email'];?>
                                </div>
                            </div>
                        </div>
                        <?php }?>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>


        </div>
    </div>
    <?php

endforeach;
?>




