<!DOCTYPE html>
<html>

<!-- Mirrored from webapplayers.com/luna_admin-v1.3/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 27 Dec 2017 09:47:41 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900' rel='stylesheet' type='text/css'>

    <!-- Page title -->
    <title>LUNA | Responsive Admin Theme</title>

    <!-- Vendor styles -->
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/dist/vendor/fontawesome/css/')?>font-awesome.css"/>
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/dist/vendor/animate.css/')?>animate.css"/>
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/dist/vendor/bootstrap/css/')?>bootstrap.css"/>

    <!-- App styles -->
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/dist/css/')?>pe-icons/pe-icon-7-stroke.css"/>
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/dist/css/')?>pe-icons/helper.css"/>
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/dist/css/')?>stroke-icons/style.css"/>
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/dist/css/')?>style.css">
</head>
<body class="blank">

<!-- Wrapper-->
<div class="wrapper" >


    <!-- Main content-->
    <section class="content">
        <div class="back-link" >
            <!-- <a href="index-2.html" class="btn btn-accent">Back to Dashboard</a> -->
        </div>

        <div class="container-center animated slideInDown" style="background-image:  url('ccc.jpg');" >


            <div class="view-header">
                <div class="header-icon">
                    <i class="pe page-header-icon pe-7s-unlock"></i>
                </div>
                <div class="header-title">
                    <h3>Reset Password</h3>
                    <small>
                        Please enter your credentials to Email.
                    </small>
                </div>
            </div>

            <div class="panel panel-filled">
                <!-- <div class="panel-body">

                    <form action='<?php echo site_url('reset') ?> method='post' name='process' id="loginForm" >
                         <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
                        <div class="form-group">
                            <label class="control-label" for="username">Email</label>
                            <input type="text" placeholder="example@gmail.com" title="Please enter you username" required="" value="" name="email" id="email" class="form-control">
                            <span class="help-block small">Your unique username to app</span>
                        </div>
                         <?php echo $this->session->flashdata('msg'); ?>

                       
                        <div>
                            <button class="btn btn-accent" type="submit">Reset</button>
                           
                        </div>
                    </form>
                </div> -->
                 <div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-50">
                <form class="login100-form validate-form" action='<?php echo site_url('reset') ?>' method='post' name='process'>
                     <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
                    <span class="login100-form-title p-b-33">
                        <strong style="font-weight: 9500">Auto Sure</strong>
                        <br>
                       Password Reset
                    </span>

                    <?php echo $this->session->flashdata('msg'); ?>

                    <div class="form-group wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
                        <input class="form-control input100" type="text" name="email" placeholder="Email" required>
                        <span class="focus-input100-1"></span>
                        <span class="focus-input100-2"></span>
                    </div>

                    <div class="form-group container-login100-form-btn m-t-20">
                        <button class="lbtn btn-accent" type="submit">
                           Reset Password
                        </button>
                    </div>

                

                    <!-- <div class="text-center">
                        <span class="txt1">
                            Create an account?
                        </span>

                        <a href="#" class="txt2 hov1">
                            Sign up
                        </a>
                    </div> -->
                </form>
            </div>
            </div>

        </div>
    </section>
    <!-- End main content-->

</div>
<!-- End wrapper-->

<!-- Vendor scripts -->
<script src="<?php echo base_url('assets/admin/dist/vendor/')?>pacejs/pace.min.js"></script>
<script src="<?php echo base_url('assets/admin/dist/vendor/')?>jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url('assets/admin/dist/vendor/')?>bootstrap/js/bootstrap.min.js"></script>

<!-- App scripts -->
<script src="<?php echo base_url('assets/admin/dist/js/')?>luna.js"></script>

</body>


<!-- Mirrored from webapplayers.com/luna_admin-v1.3/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 27 Dec 2017 09:47:41 GMT -->
</html>