
<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"></h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?php foreach ($agent as $age){
                    $id = $age['agent_id'];
                    $name = $age['agent_name'];
                    $no = $age['contact_number'];
                    $nic_no = $age['NIC_number'];
                    $address = $age['adress'];
                    $emp_id = $age['emp_id'];
                    $images = $age['image'];

                }?>
                <form role="form" action="<?php echo base_url('Agent/updateagent/'.$id); ?>" method="POST" enctype='multipart/form-data'>
                    <div class="box-body">
                        <?php echo $this->session->flashdata('msg'); ?>
                        <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>

                        <div class="col-md-12">
                            <label>Agent Image</label>
                            <input type="hidden" id="agent_image" name="agent_image"
                                   value="<?php echo $images; ?>">
                            <div class="image-group" style="position: relative; margin-top: 20px;">
                                <img src="<?php echo base_url('upload/agent'); ?>/<?php echo $images; ?>"
                                     class="thumbnail" alt="<?php echo $images; ?>" style="width: 20%">
                                <span class="img-close">&#10006;</span>
                            </div>
                            <input type="file" id="input-image" name="fileinput">

                            <p class="help-block">Image size should be below then 3mb and JPG, JPEG,PNG, GIF can be
                                uploaded.</p>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Agent Name</label>
                                    <input type="text" class="form-control" id="agent_name" placeholder="Enter agent_name" name="agent_name" value="<?php echo $name; ?>">
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>NIC No</label>
                                    <input type="text" class="form-control" id="agent_nic_no" placeholder="Enter Agent NIC No" name="agent_nic_no" value="<?php echo $nic_no; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Address</label>
                                    <input type="email" class="form-control" id="agent_address" placeholder="Enter Agent Address" name="agent_address" value="<?php echo $address; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Contact Number</label>
                                    <input type="text" class="form-control" id="agent_con_no" placeholder="Enter Agent Contact No" name="agent_con_no" value="<?php echo $no; ?>">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Employee ID</label>
                                    <input type="text" class="form-control" id="emp_id" placeholder="Enter Employee ID" name="emp_id" value="<?php echo  $emp_id; ?>">
                                </div>
                            </div>
                        </div>


                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Update Agent</button>
                        <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
                    </div>
                </form>
            </div><!-- /.box -->

        </div><!--/.col (full) -->
    </div>   <!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->
