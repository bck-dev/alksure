    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo $pagetitle; ?></h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" action="<?php echo base_url('Agent/insertagent'); ?>" method="POST" enctype='multipart/form-data'>
                        <div class="box-body">
                            <?php echo $this->session->flashdata('msg'); ?>
                            <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>


                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Agent Name</label>
                                        <input type="text" class="form-control" id="agent_name" placeholder="Enter agent_name" name="agent_name" required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="main_banner_image">Agent Image</label>
                                        <input type="file" class="form-control" id="agent_image" name="agent_image" required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>NIC No</label>
                                        <input type="text" class="form-control" id="agent_nic_no" placeholder="Enter Agent NIC No" name="agent_nic_no" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Address</label>
                                        <input type="email" class="form-control" id="agent_address" placeholder="Enter Agent Address" name="agent_address" required>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Contact Number</label>
                                        <input type="text" class="form-control" id="agent_con_no" placeholder="Enter Agent Contact No" name="agent_con_no" required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Employee ID</label>
                                        <input type="text" class="form-control" id="emp_id" placeholder="Enter Employee ID" name="emp_id" required>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Add New Admin</button>
                            <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
                        </div>
                    </form>
                </div><!-- /.box -->

            </div><!--/.col (full) -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
