<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-filled">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                            <a class="panel-close"><i class="fa fa-times"></i></a>
                        </div>
                        List Agent
                    </div>
                    <div class="panel-body">

                        <div class="table-responsive">

                            <table id="tableExample3" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Agent No</th>
                                    <th>Agent Name</th>
                                    <th>Contact No</th>
                                    <th>Option</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $count = 0;
                                foreach ($agent as $agen):?>
                                <tr>
                                    <td><?php echo ++$count; ?>.</td>
                                    <td><?php echo 'agent'.sprintf("%03d",$agen['emp_id']); ?></td>
                                    <td><?php echo $agen['agent_name']; ?></td>

                                    <td><?php echo $agen['contact_number']; ?></td>
                                    <td>
                                        <a href="" class="label label-primary" data-toggle="modal"
                                           data-target="#exampleModal<?php echo $agen['agent_id']; ?>">View</a>
                                        <a href="<?php echo base_url(); ?>agent/editagent/<?php echo $agen['agent_id']; ?>"
                                           class="label label-success">Edit</a>
                                        <a href="" data-agentmodel-id="<?php echo $agen['agent_id']; ?>"
                                           class="label label-danger agentmodelConfirm">Delete</a></td>

                                </tr>
                                <?php endforeach; ?>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

        </div>


    </div>
</section>

<!-- Content Wrapper. Contains page content -->

<?php foreach ($agent as $data) {
    ; ?>


    <div class="modal fade" id="exampleModal<?php echo $data['agent_id']; ?>" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel"
                        >Agent Details</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body" style="overflow-y: auto; color: #ffffff; font-size: 15px;">

                    <div class="form-group">

                        <img src="<?php echo base_url()?>upload/agent/<?php echo $data['image']; ?>" height="83px" width="100px" >
                    </div>

                    <div class="form-group">
                        <label>NIC No :</label>&nbsp;
                        <?php echo $data['NIC_number']; ?>

                    </div>
                    <div class="form-group">
                        <label>Email Address:</label>&nbsp;
                        <?php echo $data['adress']; ?>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
<?php } ?>