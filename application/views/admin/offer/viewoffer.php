<section class="content">
<div class="container-fluid">
            </div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-filled">
            <div class="panel-heading">
                <div class="panel-tools">
                    <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                    <a class="panel-close"><i class="fa fa-times"></i></a>
                </div>
                Customer Offer Table
            </div>
            <div class="panel-body">
              <?php echo $this->session->flashdata('msg'); ?>
              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
                <div class="table-responsive">
                    <table id="tableExample3" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Inquiry ID</th>
                            <th>Vehicle ID</th>
                            <th>Customer Name</th>
                            <th>Contact Number</th>
                            <th>Comment</th>                            
                            <th>Offered Price</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                                $count = 0;
                                foreach($offer as $data):
                                    $id = $data['offer_id'];
                            ?>
                              <tr>
                                <td><?php echo ++$count; ?>.</td>
                                <td><?php echo 'offer'.sprintf("%03d",$data['offer_id']); ?></td>
                                <td>
                                  <?php echo 'vehicle'.sprintf("%03d",$data['vehical_id']); ?> <!-- Vehicle ID -->
                                  &nbsp;&nbsp;
                                  
                                  <button type="button" class="label label-info" data-toggle="modal" data-target="#vehicleModal<?php echo $data['vehical_id'];?>">info</button>
                               
                                </td>
                                
                                <td>
                                  <?php echo $data['customer_name']; ?> <!-- Customer Name -->
                                  &nbsp;&nbsp;
                                  <button type="button" class="label label-info" data-toggle="modal" data-target="#peopleModal<?php echo $data['offer_id'];?>">info</button>
                                </td>
                                <td><?php echo $data['phone_number']; ?></td>
                                <td><?php echo $data['comments']; ?></td>                                
                                <td><?php echo $data['price']; ?></td>
                                <!--  -->
                              </td>
                        <?php
                         endforeach; ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
</section>

<!-- customer details model -->
<?php
  foreach($offer as $data): 
    
?>
  <div class="modal fade" id="peopleModal<?php echo $data['offer_id'];?>" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Contact Person</h4>
        </div>
        <div class="modal-body">
          <table>
            <tr>
              <td><b>Customer Name</b></td>
              <td><b> : </b></td>
              <td><?php echo $data['customer_name'];?></td>
            </tr>
            <tr>
              <td><b>Contact Number</b></td>
              <td><b> : </b></td>
              <td><?php echo $data['phone_number'];?></td>
            </tr>
            
            <tr>
              <td><b>Email</b></td>
              <td><b> : </b></td>
              <td><?php echo $data['email_address'];?></td>
            </tr>
           
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<?php 

endforeach;
?>

<!-- Vehcle details modal -->
<?php
  foreach($vehicles as $vehicle):
?>
  <div class="modal fade" id="vehicleModal<?php echo $vehicle['vehicle_id'];?>" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Vehicle Details</h4>
        </div>
        
        <div class="modal-body">
          <table>
            <!-- <?php echo $vehicle['vehicle_id'];?> -->
            <tr>
              <td><b>Vehicle Type</b></td>
              <td><b> : </b></td>
              <td>
                <?php foreach($types as $vehicleType){
                    if($vehicleType['vehicle_type_id']==$vehicle['vehicle_type']){
                      echo $vehicleType['vehicle_type_name'];
                    }
                } ?>
              </td>
            </tr>
            <tr>
              <td><b>Make</b></td>
              <td><b> : </b></td>
              <td>
                <?php foreach($brands as $vehicleBrand){
                      if($vehicleBrand['vehicle_brand_id']==$vehicle['vehicle_brand']){
                        echo $vehicleBrand['vehicle_brand_name'];
                      }
                  } ?>
              </td>
            </tr>
            <tr>
               <td><b>Model</b></td>
              <td><b> : </b></td>
              <td>
                <?php foreach($model as $vehicleModel){
                      if($vehicleModel['vehicle_model_id']==$vehicle['vehicle_model']){
                        echo $vehicleModel['vehicle_model_name'];
                      }
                  } ?>
              </td>
            </tr>
            <tr>
              <td><b>Transmission</b></td>
              <td><b> : </b></td>
              <td><?php if($vehicle['vehicle_transmission']==1){
              		echo "Automatic";
              }
              else if($vehicle['vehicle_transmission']==2){
              		echo "Manual";
              }
              else{
              		echo "Triptonic";
              }
              
              ?></td>
            </tr>
            <tr>
              <td><b>Engine Capacity</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['vehicle_engine_capacity'];?>
              	<?php if($vehicle['engine_capacity_unit']==1){
              		echo "CC";
              	}
              	else{
              		echo "kW";
              	}
              	 ?>
              </td>
            </tr>
            <tr>
              <td><b>Seating Capacity</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['vehicle_seating_capacity'];?></td>
            </tr>
            <tr>
              <td><b>Year of Manufacture</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['year_of_manufacture'];?></td>
            </tr>
            <tr>
              <td><b>Year of Registration</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['year_of_registration'];?></td>
            </tr>
            <tr>
              <td><b>Average Fuel Consuption</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['average_of_fuel_consuption'];?>
              	<?php if($vehicle['avarage_fuel_consumption_unit']==1){
              		echo "Km/l";
              	}
              	else{
              		echo "mil/l";
              	}
              	 ?>
              </td>
            </tr>
            <tr>
              <td><b>Number of Owners</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['number_of_owners'];?></td>
            </tr>
            <tr>
              <td><b>Fuel Type</b></td>
              <td><b> : </b></td>
              <td><?php if($vehicle['fuel_type']==1){
              		echo "Petrol";
              }
              else if($vehicle['fuel_type']==2){
              		echo "Diesel";
              }
              else if($vehicle['fuel_type']==3){
              		echo "Hybrid";
              }
              
              else{
              		echo "Electric";
              }
              ?>
              </td>
            </tr>
            <tr>
              <td><b>Vehicle Total Value</b></td>
              <td><b> : </b></td>
              <td>Rs. <?php echo $vehicle['vehicle_total_value'];?></td>
            </tr>
            <tr>
              <td><b>Vehicle Features</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['vehicle_features'];?></td>
            </tr>
            <tr>
              <td><b>Minimum Downpayment</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['vehicle_minimum_downpayment'];?></td>
            </tr>
            <tr>
              <td><b>Images</b></td>
              <?php foreach($inside_images as $key):?>
              <?php if($key['vehicle_vehicle_id']==$vehicle['vehicle_id']){?>
              <td></td>
              <td> <img src="<?php echo base_url(); ?>upload/selling/<?php echo $key['inside_images_name']; ?>" style="vertical-align:text-top;height: 61px;margin-top: 0px;margin-left: 3px;" ></td>
              <?php }?>
              <?php endforeach;?>
            </tr>
            <tr>
              <td></td>
            </tr>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<?php 
endforeach;
?>
