

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Advertisement</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?php foreach ($advertisement as $ad){
                    $id = $ad['ad_id'];
                    $ad_image_name = $ad['ad_image_name'];
                    $link = $ad['link'];

                }?>
                <form role="form" action="<?php echo base_url('advertisement/updateadvertisement/'.$id); ?>" method="POST" enctype='multipart/form-data'>
                    <div class="box-body">
                        <?php echo $this->session->flashdata('msg'); ?>
                        <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>


                        <div class="col-md-12">
                            <label>Ad Link</label>
                            <input type="text" class="form-control" value="<?php echo $link; ?>" id="link" placeholder="Link" name="link" required/>
                        </div>

                        <div class="col-md-12">
                            <label>Advertisement Image</label>
                            <input type="hidden" id="ad_image_name" name="ad_image_name"
                                   value="<?php echo $ad_image_name; ?>">
                            <div class="image-group" style="position: relative; margin-top: 20px;">
                                <img src="<?php echo base_url('upload/advertisement'); ?>/<?php echo $ad_image_name; ?>"
                                     class="thumbnail" alt="<?php echo $ad_image_name; ?>" style="width: 20%">
                                <span class="img-close">&#10006;</span>
                            </div>
                            <input type="file" id="input-image" name="fileinput">
                            <p class="help-block">Image size should be below then 3mb and JPG, JPEG,PNG, GIF can be
                                uploaded.</p>
                        </div>



                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Update Advertisement</button>
                        <a class="btn btn-default" href="<?php echo base_url('Advertisement/viewadvertisement'); ?>">Cancel</a>
                    </div>
                </form>
            </div><!-- /.box -->

        </div><!--/.col (full) -->
    </div>   <!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->
