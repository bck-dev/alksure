<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-filled">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                            <a class="panel-close"><i class="fa fa-times"></i></a>
                        </div>
                        List advertisement
                    </div>
                    <div class="panel-body">

                        <div class="table-responsive">

                            <table id="tableExample3" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Image</th>
                                    <th>Link</th>
                                    <th>Option</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $count = 0;
                                foreach ($advertisement as $ad):?>
                                    <tr>
                                        <td><?php echo ++$count; ?>.</td>

                                        <td><img src="<?php echo base_url(); ?>upload/advertisement/<?php echo $ad['ad_image_name']; ?>" style="vertical-align:text-top;height: 61px;margin-top: 0px;margin-left: 3px;">
                                        </td>

                                        <td><?php echo $ad['link']; ?></td>

                                        <td>
                                            <a href="<?php echo base_url(); ?>advertisement/editadvertisement/<?php echo $ad['ad_id']; ?>"
                                               class="label label-success">Edit</a>
                                            </td>

                                    </tr>
                                <?php endforeach; ?>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

        </div>


    </div>
</section>

