<section class="content">
            <div class="container-fluid">
            </div>

  <!-- Main content -->
  <div class="row">
    <div class="col-md-12">
        <div class="panel panel-filled">
            <div class="panel-heading">
                <div class="panel-tools">
                    <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                    <a class="panel-close"><i class="fa fa-times"></i></a>
                </div>
                Add New Admin
            </div>
            <div class="panel-body">
              <?php echo $this->session->flashdata('msg'); ?>
              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
                <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
        
          <!-- form start -->
          <form role="form" action="<?php echo base_url('adminusers/insertadmin'); ?>" method="POST" enctype='multipart/form-data'>
            <div class="box-body">
              

            <div class="form-group">
            <div class="row">

              <div class="col-md-6">
                <label>User Name</label>
                <input type="text" class="form-control" id="username" placeholder="Enter User Name" name="username1">
              </div>

              <div class="col-md-6">
                <label>Admin Type</label>
                 <select class="form-control" id="admin_type" name="admin_type1">
                  <option value="">-- Select Admin Type --</option>
                   <?php foreach($admintypes as $type): ?>
                   <option value="<?php echo $type['admin_type_id']; ?>" data-id="<?php echo $type['admin_type_id']; ?>"><?php echo $type['admin_type_name']; ?></option>
                   <?php endforeach; ?>
                </select>
              </div>

              <div class="col-md-6">
                <label>Email</label>
                <input type="text" class="form-control" id="email" placeholder="Enter Email" name="email">
              </div>

               <div class="col-md-6">
                <label>Password</label>
                <input type="password" class="form-control" id="password" placeholder="Enter Password" name="password">
              </div>       
            </div>    
          </div>
           </div>          
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Add New Admin</button>
              <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
            </div>
          </form>
        </div><!-- /.box -->

      </div><!--/.col (full) -->
    </div>

            </div>
        </div>
    </div>
</div>
</section>