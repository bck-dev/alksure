<section class="content">
    <div class="container-fluid">
    </div>

    <!-- Main content -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-filled">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                        <a class="panel-close"><i class="fa fa-times"></i></a>
                    </div>
                    Edit Admin
                </div>
                <div class="panel-body">
                    <?php echo $this->session->flashdata('msg'); ?>
                    <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <!-- <div class="box-header with-border">
            <h3 class="box-title"><?php echo $pagetitle; ?></h3>
          </div> --><!-- /.box-header -->
    
<?php foreach($admin as $ad1):?>
                                <!-- form start -->
<form role="form" action="<?php echo base_url()?>adminusers/updateadmin/<?php echo $ad1['user_id']; ?>" method="POST" enctype='multipart/form-data'>
    <div class="box-body">
        <!-- <?php echo $this->session->flashdata('msg'); ?>
        <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?> -->

        <div class="form-group">
            <div class="row">
                <div class="col-md-6">
                    <label>Admin Name</label>
                    <input type="text" class="form-control" id="admin_name1" placeholder="Enter Admin Name" name="admin_name1" value="<?php echo $ad1['user_name']?>">
                </div>

                <div class="col-md-6">
                    <label>Admin Type</label>
                    <select class="form-control" id="admin_type2" name="admin_type2">
                        <option value="">-- Select Admin Type --</option>
                        <?php foreach($admintypes as $type):?>
                            <option value="<?php echo $type['admin_type_id']; ?>" <?php echo ($type['admin_type_id'] == $ad1['user_type']) ? 'selected' : ''; ?>><?php echo $type['admin_type_name']; ?></option>
                        <?php endforeach;?>
                    </select>
                </div>

                <div class="col-md-6">
                    <label>Email</label>
                    <input type="text" class="form-control" id="email1" placeholder="Enter Email" name="email1" value="<?php echo $ad1['email'];?>">
                        </div>
                    </div>
                </div>
            </div>
        </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Update Admin</button>
                    <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
                </div>
        </form>
    <?php endforeach; ?>
    </div><!-- /.box -->

</div><!--/.col (full) -->
</div>

                </div>
            </div>
        </div>
    </div>
</section>