<section class="content">
            <div class="container-fluid">
            </div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-filled">
            <div class="panel-heading">
                <div class="panel-tools">
                    <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                    <a class="panel-close"><i class="fa fa-times"></i></a>
                </div>
                View Customers
            </div>
            <div class="panel-body">
              <?php echo $this->session->flashdata('msg'); ?>
              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
                <div class="table-responsive">
                    <table id="tableExample3" class="table table-striped table-hover">
                        
                      <tr>
                        <th style="width: 10px">#</th>
                        <th>Customer name</th>
                        <th>Customers Email</th>
                        <th>Customers Contact Number</th>
                        <th>Customers NIC</th>
                     
                      </tr>
                      <?php 
                      $count = 0;
                      foreach($admin as $ad):?>
                        <?php if($ad['user_type']==0){?>
                        <tr>
                          <td><?php echo ++$count; ?>.</td>
                          <td><?php echo $ad['user_name']; ?></td>
                        <td><?php echo $ad['email'];?> </td>
                          <td><?php echo $ad['contact_number']; ?></td>
                            <td><?php echo $ad['nic_no']; ?></td>
                          
                          <td>
                      <!--         <a href="<?php //echo base_url(); ?>adminusers/editadmin/<?php //echo $ad['user_id']; ?>" class="label label-success">Edit</a> -->
                              <!-- <a href="" data-admin-id="<?php //echo $ad['user_id']; ?>" class="label label-danger adminConfirm">Delete</a></td> -->
                   </tr> 
                 <?php }?>
                   <?php endforeach; ?>
                 </tr> 
                </table>
              </div>
          </div>
        </div>
    </div>
</div>
</section>