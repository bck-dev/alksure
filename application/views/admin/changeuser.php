<div class="content-wrapper">
    <!-- Content Header (Page header) -->
<!--    <section class="content-header">-->
<!--        <h1>-->
<!--            --><?php //echo $pagetitle; ?>
<!--        </h1>-->
<!--        <ol class="breadcrumb">-->
<!--            <li><a href="--><?php //echo base_url('admin'); ?><!--"><i class="fa fa-dashboard"></i> Home</a></li>-->
<!--            <li class="active">--><?php //echo $pagetitle; ?><!--</li>-->
<!--        </ol>-->
<!--    </section>-->

    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo $pagetitle; ?></h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <br/>
                    <?php foreach ($user_details

                                   as $user): ?>
                        <form role="form" action="<?php echo base_url('admin/updateuser') . '/' . $user['user_id']; ?>"
                              method="POST" enctype='multipart/form-data' class="form-horizontal form-label-left">

                            <div class="message-alert-area" id="employee-create-msg">
                                <?php echo validation_errors('<p class="error">', '</p>'); ?>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="employeename">Name of the
                                    employee <span class="">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="employeename" name="employeename"
                                           class="form-control col-md-7 col-xs-12"
                                           value="<?php echo $user['user_name']; ?>">
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="employeeemail">Email <span
                                            class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="email" id="employeeemail" name="employeeemail"
                                           class="form-control col-md-7 col-xs-12"
                                           value="<?php echo $user['email']; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="employeeemail"> Enable
                                    Change Password <span class=""">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="checkbox" id="passwordCheck" name="user_password_checkbox"
                                           class="form-control col-md-7 col-xs-12 form-check-input">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="employeeemail"> Password
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="password" id="user_password" value="password" name="password" readonly
                                           class="form-control col-md-7 col-xs-12 userPassword">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="employeeemail"> Confirm
                                    Password<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="password" id="user_confirm_password" value="password"
                                           name="user_confirm_password" readonly
                                           class="form-control col-md-7 col-xs-12 userPassword">
                                </div>
                            </div>

                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success">Submit</button>
                                    <button class="btn btn-primary" type="reset">Reset</button>
                                </div>
                            </div>

                        </form>
                    <?php endforeach; ?>
                </div><!--/.col (full) -->
            </div>   <!-- /.row -->
    </section><!-- /.content -->
</div>
