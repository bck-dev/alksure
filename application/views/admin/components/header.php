<!DOCTYPE html>
<html>

<!-- Mirrored from webapplayers.com/luna_admin-v1.3/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 27 Dec 2017 09:46:34 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Page title -->
    <title>Autosure</title>

    <!-- Vendor styles -->
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/dist/vendor/')?>fontawesome/css/font-awesome.css"/>
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/dist/vendor/')?>animate.css/animate.css"/>
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/dist/vendor/')?>bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/dist/vendor/')?>toastr/toastr.min.css"/>

    <!-- App styles -->
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/dist/css/')?>pe-icons/pe-icon-7-stroke.css"/>
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/dist/css/')?>pe-icons/helper.css"/>
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/dist/css/')?>stroke-icons/style.css"/>
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/dist/css/')?>style.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/')?>rating.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/common/css/'); ?>jquery-confirm.min.css">
</head>
<body>

<!-- Wrapper-->
<div class="wrapper">

    <!-- Header-->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <div id="mobile-menu">
                    <div class="left-nav-toggle">
                        <a href="#">
                            <i class="stroke-hamburgermenu"></i>
                        </a>
                    </div>
                </div>
                <a class="navbar-brand" href="<?php echo base_url('admin');?>">
                    AUTO SURE
                    <span></span>
                </a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <div class="left-nav-toggle">
                    <a href="#">
                        <i class="stroke-hamburgermenu"></i>
                    </a>
                </div>
                <form class="navbar-form navbar-left">
                    <input type="text" class="form-control" placeholder="Search data for analysis" style="width: 175px">
                </form>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="<?php echo base_url('home')?>" >Autosure website click here
                            <span class="label label-warning pull-right"></span>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="<?php echo base_url('Admin/settings/'.$this->session->userdata('id'))?>" >Profile
                            <span class="label label-warning pull-right"></span>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="<?php echo base_url('login/logout')?>" >Log Out
                            <span class="label label-warning pull-right"></span>
                        </a>
                    </li>
                    <li class=" profil-link">
                        <a href="">
                            <span class="profile-address"><?php echo $this->session->userdata('username');?></span>
                            <img src="<?php echo base_url('assets/admin/dist/images/')?>profile.jpg" class="img-circle" alt="">
                        </a>
                    </li>

                </ul>
            </div>
        </div>
    </nav>
    <!-- End header-->