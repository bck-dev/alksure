   <!-- Navigation-->
    <aside class="navigation">
        <nav>
            <ul class="nav luna-nav">
                <li class="nav-category">
                    Main
                </li>
                <li class="active">
                    <a href="<?php echo base_url('admin');?>">Dashboard</a>
                </li>

               
                
                  <?php if($_SESSION['usertype']==1 || $_SESSION['usertype']==2){ ?>
                  <li class="nav-category">
                    Trade
                </li>
                <li>
                    <a href="#uielements" data-toggle="collapse" aria-expanded="false">
                        Buying <span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>
                    <ul id="uielements" class="nav nav-second collapse">

                        <li class="nav-category">
                            Buying Vehicle Inquery
                        </li>
                        
                        <li><a href="<?php echo base_url('inquiry/viewInquiry');?>">New Inquery</a></li>
                        <li><a href="<?php echo base_url('inquiry/viewConfirm');?>">Confirm Leeds</a></li>
                        <li><a href="<?php echo base_url('inquiry/viewRejected');?>">Rejected Leeds</a></li>

                        <li class="nav-category">
                         Buying Vehicle Reservation
                        </li>
                        <li><a href="<?php echo base_url('reservation/viewReservation');?>">New Reservation Leeds</a></li>
                        <li><a href="<?php echo base_url('reservation/viewConfirmReservations');?>">Payement Collected Leeds</a></li>
                        <li><a href="<?php echo base_url('reservation/rejectReservation');?>">Rejected Leeds</a></li>

                        <li class="nav-category">
                            Customer Offer
                        </li>
                        
                        <li><a href="<?php echo base_url('inquiry/viewOffer');?>">View Offers</a></li>
                    </ul>
                </li>
                  <?php } ?>
                <?php if($_SESSION['usertype']==1 || $_SESSION['usertype']==3){ ?>
                <li>
                    <a href="#tables" data-toggle="collapse" aria-expanded="false">
                        Selling<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>
                    <ul id="tables" class="nav nav-second collapse">
                        <li class="nav-category">
                           Selling New Vehicle
                        </li>
                        <li><a href="<?php echo base_url('selling/addvehicle'); ?>">Add Vehicle</a></li>
                        <li><a href="<?php echo base_url('vehicle/viewvehicle'); ?>">View Vehicle</a></li>
                        <li><a href="<?php echo base_url('vehicle/viewrejected'); ?>">Rejected Vehicle</a></li>
                        <li><a href="<?php echo base_url('vehicle/viewpublised'); ?>">Published Vehicle</a></li>

                        <li class="nav-category">
                            Add New Vehicle Type
                        </li>
                        <li><a href="<?php echo base_url('vehicle/addtype'); ?>">Add Vehicle Type</a></li>
                        <li><a href="<?php echo base_url('vehicle/viewtype'); ?>">View Vehicle Type</a></li>


                        <li class="nav-category">
                            Add New Vehicle Brand
                        </li>
                        <li><a href="<?php echo base_url('vehicle/addbrand'); ?>">Add Vehicle Brand</a></li>
                        <li><a href="<?php echo base_url('vehicle/viewbrand'); ?>">View Vehicle Brand</a></li>

                        <li class="nav-category">
                            Add New Vehicle Model
                        </li>
                        <li><a href="<?php echo base_url('vehicle/addmodel'); ?>">Add Vehicle Model</a></li>
                        <li><a href="<?php echo base_url('vehicle/viewmodel'); ?>">View Vehicle Model</a></li>

                        <li class="nav-category">
                            Add New Vehicle Status
                        </li>
                        <li><a href="<?php echo base_url('vehicle/addstatus'); ?>">Add Vehicle Status</a></li>
                        <li><a href="<?php echo base_url('vehicle/viewstatus'); ?>">View Vehicle Status</a></li>


                    </ul>
                </li>
   <?php } ?>
   <?php if($_SESSION['usertype']==1 || $_SESSION['usertype']==5){ ?>
   		<li>
                    <a href="#common" data-toggle="collapse" aria-expanded="false">
                       Operating Leasing <span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>
                    <ul id="common" class="nav nav-second collapse">
                        <li><a href="<?php echo base_url('oparating_leasing/viewInquiry');?>">Pending Inquiries</a></li>
                        <li><a href="<?php echo base_url('oparating_leasing/viewFollowUp');?>">Followup Inquiries</a></li>
                        <li><a href="<?php echo base_url('oparating_leasing/viewConfirm');?>">Confirm Inquiries</a></li>
                        <li><a href="<?php echo base_url('oparating_leasing/viewRejected');?>">Rejected Inquiries</a></li>
                    </ul>
                </li>


               <!-- <li>
                    <a href="#extras" data-toggle="collapse" aria-expanded="false">
                        Vehicle Leasing <span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>
                    <ul id="extras" class="nav nav-second collapse">
                        <li><a href="profile.html">Profile</a></li>
                        <li><a href="contacts.html">Contacts</a></li>
                        <li><a href="projects.html">Projects</a></li>
                        <li><a href="support.html">Support</a></li>
                        <li><a href="nestableList.html">List</a></li>
                        <li><a href="timeline.html">Timeline</a></li>
                    </ul>
                </li> -->
                <!--<li>
                    <a href="#common" data-toggle="collapse" aria-expanded="false">
                       Operating Leasing <span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>
                    <ul id="common" class="nav nav-second collapse">
                        <li><a href="login.html">Login</a></li>
                        <li><a href="register.html">Register</a></li>
                        <li><a href="forgotPassword.html">Forgot password</a></li>
                        <li><a href="error.html">Error page</a></li>
                    </ul>
                </li>-->
                <?php } ?>
                  <?php if($_SESSION['usertype']==1 || $_SESSION['usertype']==4){ ?>
               <li>
                    <a href="#common1" data-toggle="collapse" aria-expanded="false">
                       Renting <span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>

                    <ul id="common1" class="nav nav-second collapse">
                        <li class="nav-category">
                            Renting Vehicles
                        </li>
                        <li class="nav-category">
                            Add New Vehicle
                        </li>
                        <li><a href="<?php echo base_url('renting/addnewvehicle');?>">Add Vehicle</a></li>
                        <li><a href="<?php echo base_url('renting/viewvehicle');?>">View Vehicle</a></li>

                        <li class="nav-category">
                            Renting Inquiries
                        </li>
                        <li><a href="<?php echo base_url('renting/viewInquiry');?>">New Leeds</a></li>
                        <li><a href="<?php echo base_url('renting/viewConfirm');?>">Confirm Leeds</a></li>
                        <li><a href="<?php echo base_url('renting/viewRejected');?>">Rejected Leeds</a></li>

                        <li class="nav-category">
                            Book a Vehicle
                        </li>
                        <li><a href="<?php echo base_url('renting/viewBooking');?>">New Leeds</a></li>
                        <li><a href="<?php echo base_url('renting/viewConfirmBookings');?>">Confirm Leeds</a></li>
                        <li><a href="<?php echo base_url('renting/viewrejectBookings');?>">Rejected Leeds</a></li>

                        <li class="nav-category">
                            Add New Vehicle Type
                        </li>
                            <li><a href="<?php echo base_url('renting/addtype');?>">Add Vehicle Type</a></li>
                            <li><a href="<?php echo base_url('renting/viewtype');?>">View Vehical Type</a></li>

                        <li class="nav-category">
                            Add New Vehicle Make
                        </li>
                            <li><a href="<?php echo base_url('renting/addbrand');?>">Add Vehicle Make</a></li>
                            <li><a href="<?php echo base_url('renting/viewbrand');?>">View Vehicle Make</a></li>

                        <li class="nav-category">
                            Add New Vehicle Modal
                        </li>   
                            <li><a href="<?php echo base_url('renting/addmodal');?>">Add New vehicle Modal</a></li>
                            <li><a href="<?php echo base_url('renting/viewmodal');?>">View vehicle Modal</a></li>



                    </ul>
                
                </li>
                 <?php } ?>
                 <?php if($_SESSION['usertype']==1 || $_SESSION['usertype']==6){ ?>
                 <li>
                    <a href="#common5" data-toggle="collapse" aria-expanded="false">
                       Import <span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>

                    <ul id="common5" class="nav nav-second collapse">
                        <li class="nav-category">
                            Import Vehicles
                        </li>
                        <li class="nav-category">
                            Add New Vehicle
                        </li>
                        <li><a href="<?php echo base_url('import/addnewvehicle');?>">Add Vehicle</a></li>
                        <li><a href="<?php echo base_url('import/viewvehicle');?>">View Vehicle</a></li>

                        <li class="nav-category">
                            Import Inquiries
                        </li>
                        <li><a href="<?php echo base_url('import/viewInquiry');?>">Pending Inquiries</a></li>
                        <li><a href="<?php echo base_url('import/viewfollowup');?>">Follow up Inquiries</a></li>
                        <li><a href="<?php echo base_url('import/viewConfirm');?>">Confirm Inquiries</a></li>
                        <li><a href="<?php echo base_url('import/viewRejected');?>">Rejected Inquiries</a></li>

                        <li class="nav-category">
                            Add New Vehicle Type
                        </li>
                            <li><a href="<?php echo base_url('import/addtype');?>">Add Vehicle Type</a></li>
                            <li><a href="<?php echo base_url('import/viewtype');?>">View Vehical Type</a></li>

                        <li class="nav-category">
                            Add New Vehicle Make
                        </li>
                            <li><a href="<?php echo base_url('import/addbrand');?>">Add Vehicle Make</a></li>
                            <li><a href="<?php echo base_url('import/viewbrand');?>">View Vehicle Make</a></li>

                        <li class="nav-category">
                            Add New Vehicle Modal
                        </li>   
                            <li><a href="<?php echo base_url('import/addmodal');?>">Add New vehicle Modal</a></li>
                            <li><a href="<?php echo base_url('import/viewmodal');?>">View vehicle Modal</a></li>

                    </ul>
                
                </li>
                <?php } ?>

              <!--  <li>
                    <a href="#common2" data-toggle="collapse" aria-expanded="false">
                       Services <span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>
                    <ul id="common2" class="nav nav-second collapse">
                        <li><a href="login.html">Login</a></li>
                        <li><a href="register.html">Register</a></li>
                        <li><a href="forgotPassword.html">Forgot password</a></li>
                        <li><a href="error.html">Error page</a></li>
                    </ul>
                </li> -->


               <!--  <li class="nav-category">
                    3Wheel And Bike
                </li>
                <li>
                    <a href="#agent" data-toggle="collapse" aria-expanded="false">
                        Agent<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>

                    <ul id="agent" class="nav nav-second collapse">

                        <li><a href="<?php echo base_url('agent/addagent'); ?>">Add Agent Type</a></li>
                        <li><a href="<?php echo base_url('agent/viewagent'); ?>">View Agent Type</a></li>

                    </ul>

                </li> -->
             <?php if($_SESSION['usertype']==1 ){ ?>
                <li>
                    <a href="#slider" data-toggle="collapse" aria-expanded="false">
                        Main Banner<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>

                    <ul id="slider" class="nav nav-second collapse">

                        <li><a href="<?php echo base_url('slider/addslider'); ?>">Add Slider</a></li>
                        <li><a href="<?php echo base_url('slider/viewslider'); ?>">View Slider</a></li>

                    </ul>

                </li>

                <li>
                    <a href="#grid" data-toggle="collapse" aria-expanded="false">
                        Grid<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>

                    <ul id="grid" class="nav nav-second collapse">

<!--                        <li><a href="--><?php //echo base_url('grid/addgrid'); ?><!--">Add Grid</a></li>-->
                        <li><a href="<?php echo base_url('grid/viewgrid'); ?>">View Grid</a></li>

                    </ul>

                </li>
                <li>
                    <a href="#admin" data-toggle="collapse" aria-expanded="false">
                        Admin<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>

                    <ul id="admin" class="nav nav-second collapse">

                        <li><a href="<?php echo base_url('adminusers/addnewadmin'); ?>">Add Admin</a></li>
                        <li><a href="<?php echo base_url('adminusers/viewadmins'); ?>">View Admins</a></li>
  			<li><a href="<?php echo base_url('adminusers/viewcostomer'); ?>">View Customer</a></li>
                    </ul>

                </li>
                 <li>
                     <a href="#agent" data-toggle="collapse" aria-expanded="false">
                         Agent<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                     </a>

                     <ul id="agent" class="nav nav-second collapse">

                         <li><a href="<?php echo base_url('agent/addagent'); ?>">Add Agent Type</a></li>
                         <li><a href="<?php echo base_url('agent/viewagent'); ?>">View Agent Type</a></li>

                     </ul>

                 </li>

<?php }?>
 <?php if($_SESSION['usertype']==1 || $_SESSION['usertype']==7){ ?>
<li>
                    <a href="#Services" data-toggle="collapse" aria-expanded="false">
                        Services<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>
                    <ul id="Services" class="nav nav-second collapse">
                        <li class="nav-category">
                            Add New Vehicle Type
                        </li>
                        <li><a href="<?php echo base_url('services/addtype'); ?>">Add Vehicle Type</a></li>
                        <li><a href="<?php echo base_url('services/viewtype'); ?>">View Vehicle Type</a></li>

                        <li class="nav-category">
                            Add Available Services
                        </li>
                        <li><a href="<?php echo base_url('services/addservicesname'); ?>">Add Services Name</a></li>
                        <li><a href="<?php echo base_url('services/viewservicesname'); ?>">View Services Name</a></li>
                        <li class="nav-category">
                            Services Enquiry
                        </li>
                        <li><a href="<?php echo base_url('services/viewenquiry'); ?>">View Services Enquiry</a></li>
                        <!--<li><a href="<?php echo base_url('services/viewinqueryconfirm'); ?>">View Confirm Services Enquiry</a></li>
                        <li><a href="<?php echo base_url('services/viewinqueryrejct'); ?>">View Reject Services Enquiry</a></li>-->



                    </ul>
                </li>
                <?php }?>
                
                <?php if($_SESSION['usertype']==1 || $_SESSION['usertype']==7){ ?>
<li>
                    <a href="#advertisement" data-toggle="collapse" aria-expanded="false">
                        advertisement<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>
                    <ul id="advertisement" class="nav nav-second collapse">
                        <li class="nav-category">
                            Edit Advertisement
                        </li>
                        <li><a href="<?php echo base_url('Advertisement/viewadvertisement'); ?>">Edit Advertisement</a></li>
                    </ul>
                </li>
                
                <?php }?>
                 
<?php if($_SESSION['usertype']==1 || $_SESSION['usertype']==2){ ?>
<li class="nav-category">
                    Trade 3Wheel & Bike
                </li>
                <li>
                    <a href="#uielements1" data-toggle="collapse" aria-expanded="false">
                        Buying <span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>
                    <ul id="uielements1" class="nav nav-second collapse">

                        <li class="nav-category">
                            3Wheel & Bike Inquery
                        </li>
                        <li>

                      <li><a href="<?php echo base_url('Bike/viewInquiry');?>">Pending Inquery Leeds</a></li>
                      <li><a href="<?php echo base_url('Bike/viewFollowUp');?>">Follow up Inquery Leeds</a></li>
                      <li><a href="<?php echo base_url('Bike/viewConfirm');?>">Confirm Leeds</a></li>
                      <li><a href="<?php echo base_url('Bike/viewRejected1');?>">Rejected Leeds</a></li>

                        <li class="nav-category">
                            3Wheel & Bike Reservation
                        </li>
                            <li><a href="<?php echo base_url('Bike/viewReservation');?>">New Reservation Leeds</a></li>
                            <li><a href="<?php echo base_url('Bike/viewConfirmReservations');?>">Payement Collected Leeds</a></li>
                            <li><a href="<?php echo base_url('Bike/rejectReservation');?>">Rejected Leeds</a></li>
                    </ul>
                </li>
<?php }?>
<?php if($_SESSION['usertype']==1 || $_SESSION['usertype']==3){ ?>
                <li>
                    <a href="#tables1" data-toggle="collapse" aria-expanded="false">
                        Selling 3Wheel & Bike<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>
                    <ul id="tables1" class="nav nav-second collapse">
                        <li class="nav-category">
                            New 3wheel & Bike
                        </li>
                        <li><a href="<?php echo base_url('bike/add3wheel'); ?>">Add 3wheel & Bike</a></li>
                        <li><a href="<?php echo base_url('bike/viewbike'); ?>">View 3wheel & Bike</a></li>
                        <li><a href="<?php echo base_url('bike/viewrejected'); ?>">Rejected 3wheel & Bike</a></li>
                        <li><a href="<?php echo base_url('bike/viewpublised'); ?>">Published 3wheel & Bike</a></li>

                        <li class="nav-category">
                            Add New 3wheel & Bike Type
                        </li>
                        <li><a href="<?php echo base_url('Bike/addtype'); ?>">Add Vehicle Type3wheel & Bike</a></li>
                        <li><a href="<?php echo base_url('Bike/viewtype'); ?>">View 3wheel & Bike</a></li>


                        <li class="nav-category">
                            Add New 3wheel & Bike Brand
                        </li>
                        <li><a href="<?php echo base_url('bike/addbrand'); ?>">Add 3wheel & Bike Brand</a></li>
                        <li><a href="<?php echo base_url('bike/viewbrand'); ?>">View 3wheel & Bike Brand</a></li>

                        <li class="nav-category">
                            Add New 3wheel & Bike Model
                        </li>
                        <li><a href="<?php echo base_url('bike/addmodel'); ?>">Add 3wheel & Bike Model</a></li>
                        <li><a href="<?php echo base_url('bike/viewmodel'); ?>">View 3wheel & Bike Model</a></li>

                        <li class="nav-category">
                            Add New 3wheel & Bike Status
                        </li>
                        <li><a href="<?php echo base_url('bike/addstatus'); ?>">Add 3wheel & Bike Status</a></li>
                        <li><a href="<?php echo base_url('bike/viewstatus'); ?>">View 3wheel & Bike Status</a></li>


                    </ul>
                </li>
            </ul>
            <?php }?>
            <?php if($_SESSION['usertype']==0 ){ ?>
                <li>
                    <a href="#tables" data-toggle="collapse" aria-expanded="false">
                        vehicle<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>
                    <ul id="tables" class="nav nav-second collapse">
                        <li class="nav-category">
                            New Vehicle
                        </li>
                        <li><a href="<?php echo base_url('vehicle/viewvehiclecustomer/'.$this->session->userdata('id')); ?>">View Vehicle</a></li>

                    </ul>
                </li>
            <?php }?>
        </nav>
    </aside>
    <!-- End navigation-->