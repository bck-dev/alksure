<script src="<?php echo base_url('assets/admin/dist/vendor/')?>jquery/dist/jquery.min.js"></script>

<script src="https://code.jquery.com/ui/1.12.0-rc.2/jquery-ui.min.js"></script>

<script src="<?php echo base_url('assets/common/js/'); ?>jquery-confirm.min.js"></script>
<script src="<?php echo base_url('assets/admin/dist/vendor/')?>bootstrap/js/bootstrap.min.js"></script>

<script src="<?php echo base_url('assets/common/css/'); ?>jquery-confirm.min.css"></script>
<script src="<?php echo base_url('assets/admin/dist/js/'); ?>url.js"></script>


<script src="<?php echo base_url('assets/admin/dist/vendor/')?>pacejs/pace.min.js"></script>

<script src="<?php echo base_url('assets/admin/dist/vendor/')?>toastr/toastr.min.js"></script>
<script src="<?php echo base_url('assets/admin/dist/vendor/')?>sparkline/index.js"></script>
<script src="<?php echo base_url('assets/admin/dist/vendor/')?>flot/jquery.flot.min.js"></script>
<script src="<?php echo base_url('assets/admin/dist/vendor/')?>flot/jquery.flot.resize.min.js"></script>
<script src="<?php echo base_url('assets/admin/dist/vendor/')?>flot/jquery.flot.spline.js"></script>

<script src="<?php echo base_url('assets/admin/dist/vendor/')?>datatables/datatables.min.js"></script>

<!-- App scripts -->
<script src="<?php echo base_url('assets/admin/dist/js/')?>luna.js"></script>

<!--<script src="--><?php //echo base_url('assets/admin/'); ?><!--main/js/main.js"></script>-->


<script src="<?php echo base_url('assets/admin/dist/')?>js/custom.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>main/js/main.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>custom/js/script.js"></script>

<script src="<?php echo base_url('assets/admin/')?>custom/js/rating.js"></script>
<script>

function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}




</script>
