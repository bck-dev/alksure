<section class="content">
            <div class="container-fluid">
            </div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-filled">
            <div class="panel-heading">
                <div class="panel-tools">
                    <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                    <a class="panel-close"><i class="fa fa-times"></i></a>
                </div>
                Payment Collected Table
            </div>
            <div class="panel-body">
            	<?php echo $this->session->flashdata('msg'); ?>
              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
                <div class="table-responsive">
                    <table id="tableExample3" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Reservation ID</th>
                            <th>Vehicle ID</th>
                            <th>Customer Name</th>
                            <th>Contact Number</th>
                            <th>Buying Method</th>
                            <th>Price</th>
                            <th>Confirmed Date</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                                $count = 0;
                                foreach($reservation as $res):
                                    if($res['status']==2):
                                    $id = $res['threewheel_bike_reservation_id'];
                            ?>
                              <tr>
                                <td><?php echo ++$count; ?>.</td>
                                <td><?php echo 't&b reservation'.sprintf("%03d",$res['threewheel_bike_reservation_id']); ?></td>
                                <td>
                                  <?php echo 't&b'.sprintf("%03d",$res['threewheel_bike_id']); ?> <!-- Vehicle ID -->
                                  &nbsp;&nbsp;
                                  
                                  <button type="button" class="label label-info" data-toggle="modal" data-target="#vehicleModal<?php echo $res['threewheel_bike_id'];?>">info</button>
                               
                                </td>
                                <?php foreach($customer as $cus): ?>
                                    <?php if($cus['customer_id'] == $res['customer_id']): ?>
                                <td>
                                  <?php echo $cus['customer_name']; ?> <!-- Customer Name -->
                                  &nbsp;&nbsp;
                                  <button type="button" class="label label-info" data-toggle="modal" data-target="#peopleModal<?php echo $res['customer_id'];?>">info</button>
                                </td>
                                <td><?php echo $cus['phone_number']; ?></td>
                                <?php 
                                 endif;
                                 endforeach;?>
                                 <?php if($res['buying_method'] == 1){ ?>
                                 <td><?php echo "Out Right";?></td>
                                 <?php } else{ ?>
                                  <td><?php echo "Leasing";?></td>
                                <?php }?>

                                <?php if($res['buying_method'] == 2 ){?>
                                 <td>Downpayment Rs.<?php echo $res['downpayment'];?>, Monthly Insatllment Rs.<?php echo $res['monthly_installment'];?>, Duration <?php echo $res['duration'];?> Years</td>
                                <?php } else{?>
                                  <td>Rs.<?php echo $res['price'];?></td>
                                <?php } ?>
                                <td> <?php echo $res['status_changed_date'];?> </td>
                            </tr>

                        <?php
                        endif;
                         endforeach;?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
</section>

<!-- customer details model -->
<?php
  foreach($customer as $cus): 
    
?>
  <div class="modal fade" id="peopleModal<?php echo $cus['customer_id'];?>" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Contact Person</h4>
        </div>
        <div class="modal-body">
          <table>
            <tr>
              <td><b>Customer Name</b></td>
              <td><b> : </b></td>
              <td><?php echo $cus['customer_name'];?></td>
            </tr>
            <tr>
              <td><b>Contact Number</b></td>
              <td><b> : </b></td>
              <td><?php echo $cus['phone_number'];?></td>
            </tr>
            <tr>
              <td><b>NIC Number</b></td>
              <td><b> : </b></td>
              <td><?php echo $cus['NIC_number'];?></td>
            </tr>
            <tr>
              <td><b>Email</b></td>
              <td><b> : </b></td>
              <td><?php echo $cus['email'];?></td>
            </tr>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<?php 

endforeach;
?>

<!-- Vehcle details modal -->
<?php
  foreach($vehicles as $vehicle):
?>
  <div class="modal fade" id="vehicleModal<?php echo $vehicle['threewheel_bike_id'];?>" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Vehicle Details</h4>
        </div>
        
        <div class="modal-body">
          <table>
            <tr>
              <td><b>Vehicle Type</b></td>
              <td><b> : </b></td>
              <td>
                <?php foreach($types as $vehicleType){
                    if($vehicleType['threewheel_bike_type_id']==$vehicle['threewheel_bike_type_id']){
                      echo $vehicleType['threewheel_bike_type_name'];
                    }
                } ?>
              </td>
            </tr>
            <tr>
              <td><b>Make</b></td>
              <td><b> : </b></td>
              <td>
                <?php foreach($brands as $vehicleBrand){
                      if($vehicleBrand['threewheel_bike_brand_id']==$vehicle['threewheel_bike_brand_id']){
                        echo $vehicleBrand['threewheel_bike_brand_name'];
                      }
                  } ?>
              </td>
            </tr>
            <tr>
               <td><b>Model</b></td>
              <td><b> : </b></td>
              <td>
                <?php foreach($model as $vehicleModel){
                      if($vehicleModel['threewheel_bike_model_id']==$vehicle['threewheel_bike_model_id']){
                        echo $vehicleModel['threewheel_bike_model_name'];
                      }
                  } ?>
              </td>
            </tr>
            <tr>
              <td><b>Transmission</b></td>
              <td><b> : </b></td>
              <td><?php if($vehicle['transmission']==1){
                  echo "Automatic";
              }
              
              else{
                  echo "Manual";
              }
              
              ?></td>
            </tr>
            <tr>
              <td><b>Engine Capacity</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['engine_capacity'];?> CC
              
              </td>
            </tr>
            
            <tr>
              <td><b>Year of Manufacture</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['Year_of_manufacture'];?></td>
            </tr>
            <tr>
              <td><b>Year of Registration</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['year_of_registration'];?></td>
            </tr>
            <tr>
              <td><b>Average Fuel Consuption</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['average_fuel_consuption'];?> Km/l
              
              </td>
            </tr>
            
            <tr>
              <td><b>Fuel Type</b></td>
              <td><b> : </b></td>
              <td><?php if($vehicle['fuel_type']==1){
                  echo "Petrol";
              }
              else if($vehicle['fuel_type']==2){
                  echo "Diesel";
              }
              
              else{
                  echo "Electric";
              }
              ?></td>
            </tr>

            <tr>
              <td><b>Vehicle Total Value</b></td>
              <td><b> : </b></td>
              <td>Rs. <?php echo $vehicle['total_price'];?></td>
            </tr>
            <tr>
              <td><b>Vehicle Features</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['features'];?></td>
            </tr>
            <tr>
              <td><b>Minimum Downpayment</b></td>
              <td><b> : </b></td>
              <td>Rs. <?php echo $vehicle['minimum_downpayment'];?></td>
            </tr>
            <tr>
              <td><b>Images</b></td>
              <?php foreach($outside_images as $key):?>
              <?php if($key['threewheel_bike_id']==$vehicle['threewheel_bike_id']){?>
              <td></td>
              <td> <img src="<?php echo base_url(); ?>upload/3wheel/<?php echo $key['threewheel_bike_image_name']; ?>" style="vertical-align:text-top;height: 61px;margin-top: 0px;margin-left: 3px;" ></td>
              <?php }?>
              <?php endforeach;?>
            </tr>
            <tr>
              <td></td>
            </tr>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<?php 
endforeach;
?>
