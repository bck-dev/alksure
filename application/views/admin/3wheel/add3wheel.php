<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
   <!-- <h1>
      <?php echo $pagetitle; ?>
    </h1> -->
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><?php echo $pagetitle; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><?php echo $pagetitle; ?></h3>
          </div><!-- /.box-header -->
          <!-- form start -->
           <form role="form" action="<?php echo base_url('bike/insert3wheel'); ?>" method="POST" enctype='multipart/form-data'>
            <div class="box-body">
              <?php echo $this->session->flashdata('msg'); ?>
              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>

              <div class="form-group">
            <div class="row">         
              <div class="col-md-6">
                <label>3Wheel & Bike Type</label>
                <select class="form-control threewheel_bike_type_id" id="threewheel_bike_type_id" name="threewheel_bike_type_id" required>
                  <option value="">-- Select Vehicle Type --</option>
                   <?php foreach($threewheel_bike_type as $type): ?>
                   <option value="<?php echo $type['threewheel_bike_type_id']; ?>" data-id="<?php echo $type['threewheel_bike_type_id']; ?>"><?php echo $type['threewheel_bike_type_name']; ?></option>
                   <?php endforeach; ?>
                </select>
              </div>
              <div class="col-md-6">
                <label>3wheel & Bike Brand</label>
                 <select class="form-control threewheel_bike_brand_id" id="threewheel_bike_brand_id" name="threewheel_bike_brand_id" disabled required>
                  <option value="">-- Select Vehicle Brand --</option>
                   <?php foreach($threewheel_bike_brand as $brand): ?>
                   <option value="<?php echo $brand['threewheel_bike_brand_id']; ?>" class="<?php echo $brand['threewheel_type_id'];?>" data-id="<?php echo $brand['threewheel_bike_brand_id']; ?>"><?php echo $brand['threewheel_bike_brand_name']; ?></option>
                   <?php endforeach; ?>
                </select>
              </div>
              <div class="col-md-6">
                <label>3Wheel & Bike Model</label>
                <select class="form-control threewheel_bike_model_id" name="threewheel_bike_model_id" required id="threewheel_bike_model_id" disabled>
                      <option value="">-- Select Vehicle Brand --</option>
                       <?php foreach($threewheel_bike_model as $vehiclemodel): ?>
                      <option value="<?php echo $vehiclemodel['threewheel_bike_model_id']; ?>" class="<?php echo $vehiclemodel['threewheel_bike_brand_id'];?>" data-id="<?php echo $vehiclemodel['threewheel_bike_model_id']; ?>"><?php echo $vehiclemodel['threewheel_bike_model_name']; ?></option>
                       <?php endforeach; ?>
                    </select>                 
              </div>

              

              <div class="col-md-6">
                <label>Transmission</label>
                <select class="form-control" id="transmission" name="transmission">
	            <option>Transmission</option>
                    <option value="1">Auto</option>
                    <option value="2">Manual</option>
                    <!-- <option value="3">Electric</option> -->
	        </select>
                <!-- <input type="text" class="form-control" id="vehicle_transmission" placeholder="Enter Vehicle Transmission" name="vehicle_transmission"> -->
              </div>
              <div class="col-md-6">
                <label>Engine Capacity</label>
                <input type="number" class="form-control" id="engine_capacity" placeholder="Enter Vehicle Engine Capacity (CC)" name="engine_capacity">
              </div>
              <div class="col-md-6">
                <label>Year of Manufacture</label>
                <input type="number" class="form-control" id="Year_of_manufacture" placeholder="Enter Year of Manufacture" name="Year_of_manufacture" required>
              </div>
              <div class="col-md-6">
                <label>Year of Registration</label>
                <input type="number" class="form-control" id="year_of_registration" placeholder="Enter Year of Registration" name="year_of_registration">
              </div>
              <div class="col-md-6">
                <label>Vehicle Registration Number</label>
                <input type="text" class="form-control" id="vehicle_number" placeholder="Enter Vehicle Registration Number" name="vehicle_number" required>
              </div>
            <div class="col-md-6">
                <label>Fuel Type</label>
               <select class="form-control" id="fuel_type" name="fuel_type">
                  <option value="">-- Select Fuel Type --</option>
                   <option value="1">Petrol</option>
                   <option value="2">Diesel</option>
                   <option value="3">Electric</option>
                </select>
              </div>
              <div class="col-md-6">
                <label>Average of Fuel Consuption</label>
                <input type="number" class="form-control" id="average_fuel_consuption" placeholder="Enter Average of Fuel Consuption" name="average_fuel_consuption">
              </div>
              <div class="col-md-6">
                <label>Number of Owners</label>
                <input type="number" class="form-control" id="number_of_owners" placeholder="Enter Number of Owners" name="number_of_owners">
              </div>
              <div class="col-md-6">
                <label>Total Value</label>
                <input type="number" class="form-control" id="total_price" placeholder="Enter Vehicle Total Value" name="total_price">
              </div>
              <div class="col-md-6">
                <label>Features</label>
                <input type="text" class="form-control" id="features" placeholder="EnterVehicle Features" name="features">
              </div>
              <div class="col-md-6">
                <label>Minimum Downpayment</label>
                <input type="number" class="form-control" id="minimum_downpayment" placeholder="Enter Vehicle Minimum Downpayment (Rs)" name="minimum_downpayment">
              </div>
              
              <div class="col-md-6">
                <label>Mileage Range</label>
                <input type="number" class="form-control" id="vehicle_milage_range" placeholder="Enter Vehicle Mileage Range (Km)" name="mileage_range">
              </div>
              
              <div class="col-md-6">
                <label>Status</label>
                <select class="form-control" id="vehicle_status" name="vehicle_status" required>
                  <option value="">-- Select Vehicle Status --</option>
                   <?php foreach($threewheel_bike_status as $status): ?>
                   <option value="<?php echo $status['threewheel_bike_status_id']; ?>"><?php echo $status['threewheel_status_name']; ?></option>
                   <?php endforeach; ?>
                </select>
              </div>
              
              <div class="col-md-6">
                <label>Sellers Description</label>
                <textarea class="form-control" id="description" placeholder="Enter Sellers Description" name="seller_description"></textarea>
              </div>

              <div class="col-md-6">
                <label>Images</label>
                <input type="file" name="userfile[]" id="image_file" accept=".png,.jpg,.jpeg,.gif" multiple>
                <p class="help-block">Image size should be below then 3mb and JPG, JPEG,PNG, GIF can be uploaded.<br/>
                Recomanded resolution for images is <strong>400 x 600</strong> pixels.
                </p>
              </div>

              <!-- <div class="col-md-6">
                <label>Inside 360 Images</label>
                <input type="file" name="inside_360_images" accept=".png,.jpg,.jpeg,.gif">
                <p class="help-block">Image size should be below then 3mb and JPG, JPEG,PNG, GIF can be uploaded.<br/>
                Recomanded resolution for images is <strong>400 x 600</strong> pixels.
                </p>
              </div> -->

              <!-- <div class="col-md-6">
                <label>Outside Images</label>
                <input type="file" name="userfile1[]"  accept=".png,.jpg,.jpeg,.gif" multiple>
                <p class="help-block">Image size should be below then 3mb and JPG, JPEG,PNG, GIF can be uploaded.<br/>
                Recomanded resolution for images is <strong>400 x 600</strong> pixels.
                </p>
              </div> -->

              <!-- <div class="col-md-6">
                <label>Outside 360 Images</label>
                <input type="file" name="outside_360_images" accept=".png,.jpg,.jpeg,.gif" >
                <p class="help-block">Image size should be below then 3mb and JPG, JPEG,PNG, GIF can be uploaded.<br/>
                Recomanded resolution for images is <strong>400 x 600</strong> pixels.
                </p>
              </div> -->

              <!-- <div class="col-md-6">
                <label>Vehicle Videos</label>
                <input type="file" name="userfile2[]"  id="image_file" accept=".png,.jpg,.jpeg,.gif" multiple>
                <p class="help-block">Image size should be below then 3mb and JPG, JPEG,PNG, GIF can be uploaded.<br/>
                Recomanded resolution for images is <strong>400 x 600</strong> pixels.
                </p>
              </div> -->
              

            </div>
           
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Add New Vehicle</button>
              <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
            </div>
          </form>
        </div><!-- /.box -->

      </div><!--/.col (full) -->
    </div>   <!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
