<section class="content">
  <div class="container-fluid">
  </div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-filled">
            <div class="panel-heading">
                <div class="panel-tools">
                    <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                    <a class="panel-close"><i class="fa fa-times"></i></a>
                </div>
               Three Wheel and Bike Confirmed Inquiry Table
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                  <!-- Reservation table -->
                    <table id="tableExample3" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Inquiry ID</th>
                            <th>Vehicle ID</th>
                            <th>Customer Name</th>
                            <th>Contact Number</th>
                            <th>Confirmed Date</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                                $count = 0;
                                foreach($inquiry as $data):
                                    if($data['status']==2):
                                    $id = $data['threewheel_bike_inquery_id'];
                            ?>
                              <tr>
                                <td><?php echo ++$count; ?>.</td>
                                <td><?php echo 'inquiry'.sprintf("%03d",$data['threewheel_bike_inquery_id']); ?></td>
                                <td>
                                  <?php echo 'vehicle'.sprintf("%03d",$data['threewheel_bike_id']); ?> <!-- Vehicle ID -->
                                  &nbsp;&nbsp;
                                  
                                  <button type="button" class="label label-info" data-toggle="modal" data-target="#vehicleModal<?php echo $data['threewheel_bike_id'];?>">info</button>
                               
                                </td>
                                
                                <td>
                                  <?php echo $data['customer_name']; ?> <!-- Customer Name -->
                                  &nbsp;&nbsp;
                                  <button type="button" class="label label-info" data-toggle="modal" data-target="#peopleModal<?php echo $data['threewheel_bike_inquery_id'];?>">info</button>
                                </td>
                                <td><?php echo $data['phone_number']; ?></td>
                                
                                 <td>
                                  <?php echo $data['status_change_date']; ?>
                      
                                  <a href="" data-inquiry-id="<?php echo $data['threewheel_bike_inquery_id']; ?>" data-toggle="modal" data-target="#collectedModal<?php echo $data['threewheel_bike_inquery_id'];?>" data-vehicle-id="<?php echo $data['threewheel_bike_id'];?>" class="label label-success inquiryButton collected" id="collected">Payment Collected</a>

                                </td>
                            </tr>

                        <?php
                          endif;
                         endforeach;?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
</section>

<!-- customer details model -->
<?php
 foreach($inquiry as $data):
    
?>
  <div class="modal fade" id="peopleModal<?php echo $data['threewheel_bike_inquery_id'];?>" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Contact Person</h4>
        </div>
        <div class="modal-body">
          <table>
            <tr>
              <td><b>Customer Name</b></td>
              <td><b> : </b></td>
              <td><?php echo $data['customer_name'];?></td>
            </tr>
            <tr>
              <td><b>Contact Number</b></td>
              <td><b> : </b></td>
              <td><?php echo $data['phone_number'];?></td>
            </tr>
           
            <tr>
              <td><b>Email</b></td>
              <td><b> : </b></td>
              <td><?php echo $data['email_address'];?></td>
            </tr>
            
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<?php 
endforeach;
?>

<!-- Vehcle details modal -->
<?php
  foreach($vehicles as $vehicle):
?>
  <div class="modal fade" id="vehicleModal<?php echo $vehicle['threewheel_bike_id'];?>" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Vehicle Details</h4>
        </div>
        
        <div class="modal-body">
          <table>
            <tr>
              <td><b>Vehicle Type</b></td>
              <td><b> : </b></td>
              <td>
                <?php foreach($types as $vehicleType){
                    if($vehicleType['threewheel_bike_type_id']==$vehicle['threewheel_bike_type_id']){
                      echo $vehicleType['threewheel_bike_type_name'];
                    }
                } ?>
              </td>
            </tr>
            <tr>
              <td><b>Make</b></td>
              <td><b> : </b></td>
              <td>
                <?php foreach($brands as $vehicleBrand){
                      if($vehicleBrand['threewheel_bike_brand_id']==$vehicle['threewheel_bike_brand_id']){
                        echo $vehicleBrand['threewheel_bike_brand_name'];
                      }
                  } ?>
              </td>
            </tr>
            <tr>
               <td><b>Model</b></td>
              <td><b> : </b></td>
              <td>
                <?php foreach($model as $vehicleModel){
                      if($vehicleModel['threewheel_bike_model_id']==$vehicle['threewheel_bike_model_id']){
                        echo $vehicleModel['threewheel_bike_model_name'];
                      }
                  } ?>
              </td>
            </tr>
            <tr>
              <td><b>Transmission</b></td>
              <td><b> : </b></td>
              <td><?php if($vehicle['transmission']==1){
              		echo "Automatic";
              }
              
              else{
              		echo "Manual";
              }
              
              ?></td>
            </tr>
            <tr>
              <td><b>Engine Capacity</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['engine_capacity'];?>
              
              </td>
            </tr>
           
            <tr>
              <td><b>Year of Manufacture</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['Year_of_manufacture'];?></td>
            </tr>
            <tr>
              <td><b>Year of Registration</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['year_of_registration'];?></td>
            </tr>
            <tr>
              <td><b>Average Fuel Consuption</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['average_fuel_consuption'];?>
              
              </td>
            </tr>
            
            <tr>
              <td><b>Fuel Type</b></td>
              <td><b> : </b></td>
              <td><?php if($vehicle['fuel_type']==1){
              		echo "Petrol";
              }
              else if($vehicle['fuel_type']==2){
              		echo "Diesel";
              }
              
              else{
              		echo "Electric";
              }
              ?></td>
            </tr>

            <tr>
              <td><b>Vehicle Total Value</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['total_price'];?></td>
            </tr>
            <tr>
              <td><b>Vehicle Features</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['features'];?></td>
            </tr>
            <tr>
              <td><b>Minimum Downpayment</b></td>
              <td><b> : </b></td>
              <td>Rs. <?php echo $vehicle['minimum_downpayment'];?></td>
            </tr>
            <tr>
              <td><b>Images</b></td>
              <?php foreach($outside_images as $key):?>
              <?php if($key['threewheel_bike_image_id']==$vehicle['threewheel_bike_id']){?>
              <td></td>
              <td> <img src="<?php echo base_url(); ?>upload/3wheel/<?php echo $key['threewheel_bike_image_name']; ?>" style="vertical-align:text-top;height: 61px;margin-top: 0px;margin-left: 3px;" ></td>
              <?php }?>
              <?php endforeach;?>
            </tr>
            <tr>
              <td></td>
            </tr>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<?php 
endforeach;
?>

<?php foreach($inquiry as $data): 

$inq_id = $data['threewheel_bike_inquery_id'];
?>

<div class="modal fade" id="collectedModal<?php echo $data['threewheel_bike_inquery_id'];?>" role="dialog">
<div class="modal-dialog">
  <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">Payment Details</h4>
    </div>
      <!-- <?php print_r($inq_id);?> -->
    <div class="modal-body">
        <form role="form" action="<?php echo base_url('bike/paymentCollected'); ?>" method="POST" enctype='multipart/form-data' class="formpc">
        <input type="hidden" name="inq_id" id="inq_id" value="<?php echo $inq_id;?>">
        <div class="form-group">                     
          <label for="home_page_grid_description">Buying Method<b></label>
        </div>
        <div class="form-group">
          <label>Outright</label>
          <input type="checkbox" name="checkbox" class="outright" id="outright" value="1">
          <label>Leasing</label>
          <input type="checkbox" name="checkbox" class="leasing" id="leasing" value="2">
        </div>

        <div id="pcform1" class="pcform1">
          <div class="form-group">
            <label>Price</label>
            <input type="text" name="price" class="form-control" id="price" placeholder="Price">
          </div>
        </div>

        <div id="pcform2" class="pcform2">
          <div class="form-group">
            <label>Downpayment</label>
            <input type="text" name="downpayment" id="downpayment" class="form-control" placeholder="Downpayment">
          </div>
          <div class="form-group" id="form2">
            <label>Monthly Installment</label>
            <input type="text" name="monthly_ins" id="monthly_ins" class="form-control" placeholder="Monthly Installment">
          </div>
          <div class="form-group" id="form2">
            <label>Duration</label>
            <input type="text" name="dura" id="dura" class="form-control" placeholder="Duration">
          </div>
        </div>

    </div>
    <div class="modal-footer">
      <button type="submit" class="btn btn-success">Create Reservation</button>
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>
  </form>
  
</div>
</div>
<?php 

endforeach;
?>