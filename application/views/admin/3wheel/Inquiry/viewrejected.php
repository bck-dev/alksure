<section class="content">
            <div class="container-fluid">
            </div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-filled">
            <div class="panel-heading">
                <div class="panel-tools">
                    <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                    <a class="panel-close"><i class="fa fa-times"></i></a>
                </div>
                Rejected Inquiry Table
            </div>
            <div class="panel-body">
              <?php echo $this->session->flashdata('msg'); ?>
              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
                <div class="table-responsive">
                    <table id="tableExample3" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Inquiry ID</th>
                            <th>Vehicle ID</th>
                            <th>Customer Name</th>
                            <th>Contact Number</th>
                            <th>Reject Reason</th>
                            <th>Options</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                                $count = 0;
                                foreach($inquiry as $data):
                                    if($data['status'] == 3):
                                    $id = $data['threewheel_bike_inquery_id'];
                            ?>
                              <tr>
                                <td><?php echo ++$count; ?>.</td>
                                <td><?php echo 'inquiry'.sprintf("%03d",$data['threewheel_bike_inquery_id']); ?></td>
                                <td>
                                  <?php echo 'vehicle'.sprintf("%03d",$data['threewheel_bike_id']); ?> <!-- Vehicle ID -->
                                  &nbsp;&nbsp;
                                  
                                  <button type="button" class="label label-info" data-toggle="modal" data-target="#vehicleModal<?php echo $data['threewheel_bike_id'];?>">info</button>
                               
                                </td>
                               
                                <td>
                                  <?php echo $data['customer_name']; ?> <!-- Customer Name -->
                                  &nbsp;&nbsp;
                                  <button type="button" class="label label-info" data-toggle="modal" data-target="#peopleModal<?php echo $data['threewheel_bike_inquery_id'];?>">info</button>
                                </td>
                                <td><?php echo $data['phone_number']; ?></td>
                                

                                <td><?php echo $data['reject_reason']; ?></td>
                                 <td>
                                  <a href="<?php echo base_url();?>bike/pending/<?php echo $data['threewheel_bike_inquery_id']; ?>" data-inquiry-id="<?php echo $data['threewheel_bike_inquery_id']; ?>" data-vehicle-id="<?php echo $data['threewheel_bike_id'];?>" data-toggle="modal" class="label label-warning inquiryButton">Back to Pending</a>

                                  <a href="<?php echo base_url();?>bike/confirm1/<?php echo $data['threewheel_bike_inquery_id']; ?>" data-inquiry-id="<?php echo $data['threewheel_bike_inquery_id']; ?>" data-action-id="2" data-vehicle-id="<?php echo $data['threewheel_bike_id'];?>" class="label label-success inquiryButton" name="confirm">Confirm</a>

                                  <a href="" data-inquiry-id="<?php echo $data['threewheel_bike_inquery_id']; ?>" data-action-id="3" data-vehicle-id="<?php echo $data['threewheel_bike_id'];?>" data-toggle="modal" data-target="#followUpModal<?php echo $data['threewheel_bike_inquery_id'];?>" id="follow-up"  class="label label-warning inquiryButton">Follow up</a>

                                </td>
                            </tr>

                        <?php
                        endif;
                         endforeach;?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
</section>

<!-- customer details model -->
<?php
 foreach($inquiry as $data):
    
?>
  <div class="modal fade" id="peopleModal<?php echo $data['threewheel_bike_inquery_id'];?>" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Contact Person</h4>
        </div>
        <div class="modal-body">
          <table>
            <tr>
              <td><b>Customer Name</b></td>
              <td><b> : </b></td>
              <td><?php echo $data['customer_name'];?></td>
            </tr>
            <tr>
              <td><b>Contact Number</b></td>
              <td><b> : </b></td>
              <td><?php echo $data['phone_number'];?></td>
            </tr>
           
            <tr>
              <td><b>Email</b></td>
              <td><b> : </b></td>
              <td><?php echo $data['email_address'];?></td>
            </tr>
            
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<?php 
endforeach;
?>

<!-- Vehcle details modal -->
<?php
  foreach($vehicles as $vehicle):
?>
  <div class="modal fade" id="vehicleModal<?php echo $vehicle['threewheel_bike_id'];?>" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Vehicle Details</h4>
        </div>
        
        <div class="modal-body">
          <table>
            <tr>
              <td><b>Vehicle Type</b></td>
              <td><b> : </b></td>
              <td>
                <?php foreach($types as $vehicleType){
                    if($vehicleType['threewheel_bike_type_id']==$vehicle['threewheel_bike_type_id']){
                      echo $vehicleType['threewheel_bike_type_name'];
                    }
                } ?>
              </td>
            </tr>
            <tr>
              <td><b>Make</b></td>
              <td><b> : </b></td>
              <td>
                <?php foreach($brands as $vehicleBrand){
                      if($vehicleBrand['threewheel_bike_brand_id']==$vehicle['threewheel_bike_brand_id']){
                        echo $vehicleBrand['threewheel_bike_brand_name'];
                      }
                  } ?>
              </td>
            </tr>
            <tr>
               <td><b>Model</b></td>
              <td><b> : </b></td>
              <td>
                <?php foreach($model as $vehicleModel){
                      if($vehicleModel['threewheel_bike_model_id']==$vehicle['threewheel_bike_model_id']){
                        echo $vehicleModel['threewheel_bike_model_name'];
                      }
                  } ?>
              </td>
            </tr>
            <tr>
              <td><b>Transmission</b></td>
              <td><b> : </b></td>
              <td><?php if($vehicle['transmission']==1){
              		echo "Automatic";
              }
              
              else{
              		echo "Manual";
              }
              
              ?></td>
            </tr>
            <tr>
              <td><b>Engine Capacity</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['engine_capacity'];?>
              
              </td>
            </tr>
           
            <tr>
              <td><b>Year of Manufacture</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['Year_of_manufacture'];?></td>
            </tr>
            <tr>
              <td><b>Year of Registration</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['year_of_registration'];?></td>
            </tr>
            <tr>
              <td><b>Average Fuel Consuption</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['average_fuel_consuption'];?>
              
              </td>
            </tr>
            
            <tr>
              <td><b>Fuel Type</b></td>
              <td><b> : </b></td>
              <td><?php if($vehicle['fuel_type']==1){
              		echo "Petrol";
              }
              else if($vehicle['fuel_type']==2){
              		echo "Diesel";
              }
              
              else{
              		echo "Electric";
              }
              ?></td>
            </tr>

            <tr>
              <td><b>Vehicle Total Value</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['total_price'];?></td>
            </tr>
            <tr>
              <td><b>Vehicle Features</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['features'];?></td>
            </tr>
            <tr>
              <td><b>Minimum Downpayment</b></td>
              <td><b> : </b></td>
              <td>Rs. <?php echo $vehicle['minimum_downpayment'];?></td>
            </tr>
            <tr>
              <td><b>Images</b></td>
              <?php foreach($outside_images as $key):?>
              <?php if($key['threewheel_bike_image_id']==$vehicle['threewheel_bike_id']){?>
              <td></td>
              <td> <img src="<?php echo base_url(); ?>upload/3wheel/<?php echo $key['threewheel_bike_image_name']; ?>" style="vertical-align:text-top;height: 61px;margin-top: 0px;margin-left: 3px;" ></td>
              <?php }?>
              <?php endforeach;?>
            </tr>
            <tr>
              <td></td>
            </tr>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<?php 
endforeach;
?>

<!-- follow up modal -->
<?php foreach($inquiry as $data): 

    $inq_id = $data['threewheel_bike_inquery_id'];
?>

  <div class="modal fade" id="followUpModal<?php echo $data['threewheel_bike_inquery_id'];?>" role="dialog">
  
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Follow Up</h4>
        </div>
          <!-- <?php print_r($inq_id);?> -->
        <div class="modal-body">
            <form role="form" action="<?php echo base_url(); ?>bike/followUp1/" method="POST" enctype='multipart/form-data'>

            <input type="hidden" name="inq_id" id="inq_id" value="<?php echo $inq_id;?>"> 
            <div class="form-group">                     
              <label for="home_page_grid_description">Follow Up Reason</label>
              <textarea class="form-control" rows="3" id="Follow_up_description" name="Follow_up_description" placeholder="Enter Follow Up Reason" required></textarea>
            </div>
            <div class="form-group">                     
              <label for="home_page_grid_description">Next Follow Up Date</label>
              <input type="date" name="next_date" id="next_date" class="form-control" required>
            </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Create Follow Up</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      </form>
      
    </div>
  </div>
<?php 
endforeach;
?>

