<section class="content">
    <div class="container-fluid">
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-filled">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                        <a class="panel-close"><i class="fa fa-times"></i></a>
                    </div>
                    Selling Threewheel & Bike  List
                </div>

                <div>
                    <a href="<?php echo base_url(); ?>bike/viewbike"  class="btn btn-primary">Admin</a>
                    <a href="<?php echo base_url(); ?>bike/viewbikes" style="background-color: #449d44;" class="btn btn-success">Customer</a>
                </div>
                <div class="panel-body">
                    <?php echo $this->session->flashdata('msg'); ?>
                    <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
                    <div class="table-responsive">
                        <table id="tableExample3" class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Vehicle Id</th>
                                <th>Image</th>
                                <th>Options</th>
                                <th>Status</th>
                                <th>Assign Agent</th>
                                <th>Inspection Status</th>
                                <th>Option</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $count = 0;
                            foreach ($vehicle as $data):?>
                                <?php if($data['admin_customer'] == 0){ ?>
                                    <?php foreach ($insideimage as $inside): ?>
                                        <?php if ($inside['threewheel_bike_id'] == $data['threewheel_bike_id']): ?>
                                            <tr>
                                                <td><?php echo ++$count; ?>.</td>
                                                <td><?php echo 't&b/sel'.sprintf("%03d",$data['threewheel_bike_id']); ?></td>
                                                <td><img src="<?php echo base_url(); ?>upload/3wheel/<?php echo $inside['threewheel_bike_image_name']; ?>"
                                                         style="vertical-align:text-top;height: 61px;margin-top: 0px;margin-left: 3px;">


                                                </td>

                                                <td>

                                                    <a href="<?php echo base_url('bike/details/' . $data['threewheel_bike_id']) ?>" class="label label-primary">View</a>
                                                    <a  data-model-id="<?php echo $data['threewheel_bike_id']; ?>"
                                                        class="label label-success " <?php if ($data['inspection_status'] == 0) { ?> style="background-color: #636862;" disabled="disabled"
                                                    <?php }else{ ?>href="<?php echo base_url('bike/viewinspection/' . $data['threewheel_bike_id']) ?>" <?php } ?> >Feed Inspection Data</a></td>
                                                <td><?php if($data['status'] == 1){?>assigned<?php } ?></td>


                                                <td><?php if($data['status'] == 1){ echo $data['agent_name'];?>
                                                    <a href="" class="label label-primary" data-toggle="modal"
                                                       data-target="#agenModal<?php echo $data['agent_id']; ?>">View</a><?php }?></td>


                                                <td><?php if($data['status'] == 1){?><input type="checkbox" <?php if ($data['inspection_status'] == 1) { ?> checked <?php } ?> id=" <?php echo $data['inspection_status']; ?>"
                                                                                            data-vehi-id="<?php echo $data['threewheel_bike_id']; ?>" class="inspectedwheel" name="<?php $data['inspection_status']; ?>" value="1">Inspected<?php } ?></td>
                                                <td>

                                                    <a href="<?php echo base_url(); ?>bike/editbike3wheel/<?php echo $data['threewheel_bike_id']; ?>"
                                                       class="label label-success">Edit</a>
                                                    <a href="" data-wheelsellbike-id="<?php echo $data['threewheel_bike_id']; ?>"
                                                       class="label label-danger wheelselConfirm">Delete</a>
                                                </td>

                                            </tr>
                                            <?php break; ?>
                                        <?php endif ?>


                                    <?php endforeach ?>
                                <?php }?>

                            <?php endforeach; ?>

                            </tbody>

                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Content Wrapper. Contains page content -->


<?php foreach ($vehicle as $agg) {
    ; ?>


    <div class="modal fade" id="agenModal<?php echo $agg['agent_id']; ?>" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">

            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel"
                    >Agent Details</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body" style="overflow-y: auto; color: #ffffff; font-size: 15px;">



                    <div class="form-group">

                        <img src="<?php echo base_url()?>upload/agent/<?php echo $agg['image']; ?>" height="83px" width="100px" >
                    </div>

                    <div class="form-group">
                        <label>Agent Name:</label>&nbsp;
                        <?php echo $agg['agent_name']; ?>

                    </div>
                    <div class="form-group">
                        <label>Agent Address:</label>&nbsp;
                        <?php echo $agg['adress']; ?>
                    </div>
                    <div class="form-group">
                        <label>Agent NIC No:</label>&nbsp;
                        <?php echo $agg['NIC_number']; ?>
                    </div>
                    <div class="form-group">
                        <label>Agent Contact No:</label>&nbsp;
                        <?php echo $agg['contact_number']; ?>
                    </div>

                    <div class="form-group">
                        <label>Agent Id :</label>&nbsp;
                        <?php echo 'agent'.sprintf("%03d",$agg['emp_id']); ?>

                    </div>

                </div>

                <div class="modal-footer">

                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
<?php } ?>
