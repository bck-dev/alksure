<section class="content">
    <div class="container-fluid">
    </div>

    <!-- Main content -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-filled">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                        <a class="panel-close"><i class="fa fa-times"></i></a>
                    </div>
                    Edit Vehicle
                </div>
                <div class="panel-body">
                    <?php echo $this->session->flashdata('msg'); ?>
                    <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">

                                <?php foreach ($wheel as $vehi):
    
                                    $type_id = $vehi['threewheel_bike_type_id'];
                                    $brand_id = $vehi['threewheel_bike_brand_id'];
                                    $model_id = $vehi['threewheel_bike_model_id'];
                                    $vehicle_id = $vehi['threewheel_bike_id'];
                                    $transmission = $vehi['transmission'];
                                    $mileage_range = $vehi['mileage_range'];
                                    $engine_capacity = $vehi['engine_capacity'];
                                    $Year_of_manufacture = $vehi['Year_of_manufacture'];
                                    $year_of_registration = $vehi['year_of_registration'];
                                    $fuel_type = $vehi['fuel_type'];
                                    $number_of_owners = $vehi['number_of_owners'];
                                    $avarage_fuel_consumption = $vehi['average_fuel_consuption'];
                                    $total_price = $vehi['total_price'];
                                    $features = $vehi['features'];
                                    $minimum_downpayment = $vehi['minimum_downpayment'];
                                    $threewheel_bike_condition = $vehi['threewheel_bike_condition'];
                                    $seller_description = $vehi['seller_description'];

                                    ?>

                                <!-- form start -->
                                <?php ?>


                                <form role="form" action="<?php echo base_url('bike/updatebike/' . $vehicle_id); ?>"
                                      method="POST" enctype='multipart/form-data'>
                                    <div class="box-body">
                                        <!-- <?php echo $this->session->flashdata('msg'); ?>
                                           <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?> -->

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>3Wheel & Bike Type</label>
                                                    <select class="form-control" id="threewheel_bike_type_id" name="threewheel_bike_type_id" required>
                                                        <option value="">-- Select Vehicle Type --</option>
                                                        <?php foreach ($threewheel_bike_type as $type): ?>
                                                            <option value="<?php echo $type['threewheel_bike_type_id']; ?>" data-id="<?php echo $type['threewheel_bike_type_id']; ?>" <?php echo ($type_id == $type['threewheel_bike_type_id']) ? 'selected' : ''; ?>><?php echo $type['threewheel_bike_type_name']; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>3Wheel & Bike Brand</label>
                                                    <select class="form-control" id="threewheel_bike_brand_id"
                                                            name="threewheel_bike_brand_id" required  disabled>
                                                        <option value="">-- Select Vehicle Brand --</option>
                                                        <?php foreach ($brthreewheel_bike_brandands as $brand): ?>
                                                            <option value="<?php echo $brand['threewheel_bike_brand_id']; ?>"  data-id="<?php echo $brand['threewheel_bike_brand_id']; ?>" <?php echo ($brand_id == $brand['threewheel_bike_brand_id']) ? 'selected' : ''; ?> class="<?php echo $brand['threewheel_type_id'];?>"><?php echo $brand['threewheel_bike_brand_name']; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>3Wheel & Bike Model</label>
                                                    <select class="form-control" name="threewheel_bike_model_id" id="threewheel_bike_model_id" required disabled>
                                                        <option value="">-- Select Vehicle Modal --</option>
                                                        <?php foreach ($threewheel_bike_model as $vehiclemodel): ?>
                                                            <option value="<?php echo $vehiclemodel['threewheel_bike_model_id']; ?>" class="<?php echo $vehiclemodel['threewheel_bike_brand_id'];?>" data-id="<?php echo $vehiclemodel['threewheel_bike_model_id']; ?>" <?php echo ($model_id == $vehiclemodel['threewheel_bike_model_id']) ? 'selected' : ''; ?>><?php echo $vehiclemodel['threewheel_bike_model_name'] ?> </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>

                                                <div class="col-md-6">
                                                    <label>Year of Registration</label>
                                                    <input type="text" class="form-control" id="year_of_registration"
                                                           placeholder="Enter Year of Registration"
                                                           name="year_of_registration"
                                                           value="<?php echo $year_of_registration ?>">
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Year of Manufacture</label>
                                                    <input type="text" class="form-control" id="Year_of_manufacture"
                                                           placeholder="Enter Year of Manufacture"
                                                           name="Year_of_manufacture"
                                                           value="<?php echo $Year_of_manufacture ?>">
                                                </div>

                                                <div class="col-md-6">
                                                    <label>Transmission</label>
                                                    <select class="form-control" id="transmission"
                                                            name="transmission">
                                                        <option value="">-- Select Transmission --</option>
                                                        <?php for ($x = 1; $x <= 2; $x++) : ?>
                                                            <option value="<?php echo $x; ?>" <?php echo ($x == $transmission) ? 'selected' : ''; ?>><?php echo ($x == '2') ? 'Manual' : 'Automatic'; ?></option>
                                                        <?php endfor; ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Vehicle Mileage Range</label>
                                                    <input type="text" class="form-control" id="mileage_range"
                                                           placeholder="Enter Vehicle Mileage Range (Km)"
                                                           name="mileage_range"
                                                           value="<?php echo $mileage_range ?>">
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Fuel Type</label>
                                                    <select class="form-control" id="fuel_type" name="fuel_type">
                                                        <option value="">-- Select Fuel Type --</option>
                                                        <?php for ($x = 1; $x <= 2; $x++) : ?>
                                                            <option value="<?php echo $x; ?>" <?php echo ($x == $fuel_type) ? 'selected' : ''; ?>><?php echo ($x == '2') ? 'Diesel' : 'Petrol'; ?></option>
                                                        <?php endfor; ?>
                                                    </select>
                                                </div>


                                                <div class="col-md-6">
                                                    <label>Vehicle Engine Capacity</label>
                                                    <input type="text" class="form-control" id="engine_capacity"
                                                           placeholder="Enter Vehicle Engine Capacity (CC)"
                                                           name="engine_capacity"
                                                           value="<?php echo $engine_capacity; ?>">
                                                </div>

                                                <div class="col-md-6">
                                                    <label>Vehicle Total Price</label>
                                                    <input type="text" class="form-control" id="total_price"
                                                           placeholder="Enter Total Price" name="total_price"
                                                           value="<?php echo $total_price; ?>">
                                                </div>


                                                <div class="col-md-6">
                                                    <label>Number of Owners</label>
                                                    <input type="text" class="form-control" id="number_of_owners"
                                                           placeholder="Enter Number of Owners" name="number_of_owners"
                                                           value="<?php echo $number_of_owners; ?>">
                                                </div>

                                                <?php //if($admin_customer == 1){ ?>
                                                <!-- <div class="col-md-6">
        <label>Vehicle Number</label>
        <input type="text" class="form-control" id="vehicle_number" placeholder="Enter Vehicle Number" name="vehi_number" value="<?php echo $vehicle_num; ?>">
        </div> -->

                                                <div class="col-md-6">
                                                    <label>Vehicle Features</label>
                                                    <textarea class="form-control" placeholder="Vehicle Features"
                                                              name="features"><?php echo $features; ?></textarea>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Downpayment</label>
                                                    <input type="text" class="form-control" id="minimum_downpayment"
                                                           placeholder="Enter Downpayment" name="minimum_downpayment"
                                                           value="<?php echo $minimum_downpayment; ?>">
                                                </div>
                                                <!-- <div class="col-md-6">
        <label>Vehicle Seating Capacity</label>
        <input type="text" class="form-control" id="vehicle_seating_capacity" placeholder="Enter Vehicle Seating Capacity" name="vehicle_seating_capacity" value="<?php echo $seating_capacity; ?>">
        </div> -->
                                                <div class="col-md-6">
                                                    <label>Average of Fuel Consuption</label>
                                                    <input type="text" class="form-control"
                                                           id="average_fuel_consuption"
                                                           placeholder="Enter Average of Fuel Consuption"
                                                           name="average_fuel_consuption"
                                                           value="<?php echo $avarage_fuel_consumption; ?>">
                                                </div>

                                                <?php //} ?>

                                                <div class="col-md-12">
                                                    <label>sellers Description</label>
                                                    <textarea class="form-control" placeholder="Sellers description"
                                                              name="seller_description"><?php echo $seller_description; ?></textarea>
                                                </div>
                                                <br><br><br>
                                                <div class="row">
                                                <div class="form-group">
                                                    <?php
                                                    if (isset($threewheel_bike_image) && is_array($wheel) && count($wheel)): $i = 1;
                                                        foreach ($threewheel_bike_image as $key => $data) {?>
                                                            <div class="imagelocation<?php echo $data['threewheel_bike_image_id'] ?>">
                                                                <br/>
                                                                <img src="<?php echo base_url(); ?>upload/3wheel/<?php echo $data['threewheel_bike_image_name']; ?>"
                                                                     style="vertical-align:text-top;" width="200" height="200">
                                                                <a href="" style="cursor:pointer;"
                                                                   data-imagewhelsell-id="<?php echo $data['threewheel_bike_image_id']; ?>"
                                                                   class="label label-danger imagesellwhelConfirm">X</a>
                                                            </div>
                                                        <?php }endif; ?>
                                                </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Images</label>
                                                    <input type="file" name="userfile[]" id="image_file"
                                                           accept=".png,.jpg,.jpeg,.gif"
                                                           multiple><p style="color: red">Minimum Upload 2 or more images</p>
                                                    <p class="help-block">Image size should be below then 3mb and JPG,
                                                        JPEG,PNG, GIF can be
                                                        uploaded.<br/>
                                                        Recomanded resolution for images is <strong>120 x 120</strong>
                                                        pixels.
                                                    </p>
                                                </div>
                                                <br><br>
                                            </div>

                                        </div>
                                        <!-- /.box-body -->
                                        <div class="box-footer">
                                            <button type="submit" class="btn btn-primary">Update Threewheel</button>
                                            <a class="btn btn-default"
                                               href="<?php echo base_url('admin'); ?>">Cancel</a>
                                        </div>

                                    </div> <!-- /.box -->
                                </form>
                                <?php endforeach; ?>
                            </div><!--/.col (full) -->
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>