<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-filled">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                            <a class="panel-close"><i class="fa fa-times"></i></a>
                        </div>
                        List Status
                    </div>
                    <div class="panel-body">

                        <div class="table-responsive">

                            <table id="tableExample3" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Status</th>
                                    <th>Options</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $count = 0;
                                foreach($threewheel_bike_status as $status):?>
                                    <tr>
                                        <td><?php echo ++$count; ?>.</td>
                                        <td><?php echo $status['threewheel_status_name']; ?></td>
                                        <td> <a href="<?php echo base_url(); ?>bike/editstatus/<?php echo $status['threewheel_bike_status_id']; ?>" class="label label-success">Edit</a>
                                            <a href="" data-bikestatus-id="<?php echo $status['threewheel_bike_status_id']; ?>" class="label label-danger bikestatusConfirm">Delete</td>
                                    </tr>
                                <?php endforeach; ?>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

        </div>


    </div>
</section>












