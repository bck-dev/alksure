<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-filled">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                            <a class="panel-close"><i class="fa fa-times"></i></a>
                        </div>
                        List Rejected Threewheel & Bike
                    </div>
                    <div class="panel-body">

                        <div class="table-responsive">

                            <table id="tableExample3" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Vehicle ID</th>
                                    <th>Customer Name</th>
                                    <th>Customer Number</th>
                                    <th>Reason for rejected</th>
                                    <th>Rejected date</th>
                                    <th>Options</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $count = 0;
                                foreach($sellingreject as $reg):?>
                                    <tr>
                                        <td><?php echo ++$count; ?>.</td>
                                        <td><?php echo 'vehicle'.sprintf("%03d",$reg->threewheel_bike_id); ?></td>
                                        <td><?php echo $reg->customer_name; ?></td>
                                        <td><?php echo $reg->phone_number; ?></td>
                                        <td><?php echo $reg->reason; ?></td>
                                        <td><?php echo $reg->rejected_date; ?></td>
                                        <td><a href="" data-reject-id="<?php echo $reg->reject_id; ?>"
                                               data-action-id="0" data-vehicle-id="<?php echo $reg->threewheel_bike_id;?>" class="label label-warning rejectwheelButton">Send back to selling threewheel list</a></td>

                                    </tr>
                                <?php endforeach; ?>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

        </div>


    </div>
</section>