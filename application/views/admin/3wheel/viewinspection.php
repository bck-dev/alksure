

<section class="content">
    <div class="container-fluid">
        <?php foreach ($vehicle as $value): {
            ?>
            <?php
            $vehicle_id = $value['threewheel_bike_id'];
            $vehicle_type_name = $value['threewheel_bike_type_name'];
            $vehicle_brand_name = $value['threewheel_bike_brand_name'];
            $vehicle_transmission = $value['transmission'];
            $vehicle_number = $value['threewheel_bike_number'];
            $vehicle_engine_capacity = $value['engine_capacity'];
            $year_of_manufacture = $value['Year_of_manufacture'];
            $year_of_registration = $value['year_of_registration'];
            $fuel_type = $value['fuel_type'];
            $average_of_fuel_consuption = $value['average_fuel_consuption'];
            $vehicle_total_value = $value['total_price'];
            $vehicle_mileage_range = $value['mileage_range'];
            $vehicle_model_name = $value['threewheel_bike_model_name'];
            $customer_name = $value['customer_name'];
            $customer_id = $value['threewheel_bike_customer_id'];
            $phone_number = $value['phone_number'];
//            $address = $value['address'];
            $email = $value['email'];
            $admin_customer = $value['admin_customer'];
            $agent_name = $value['agent_name'];
            $contact_number = $value['contact_number'];
            $address = $value['adress'];

            ?>



            <div class="row">
                <div class="col-lg-12">
                    <div class=" panel-filled">
                        <div class="panel-heading">
                            <div class="panel-tools">
                                <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                                <a class="panel-close"><i class="fa fa-times"></i></a>
                            </div>
                            <div style="color:#ffffff; font-size: 18px; font-weight: bold;">Vehicle Details</div>
                        </div>
                        <div class="panel-body">

                            <div class="row">
                                <div class="modal-body" style="max-height: 500px;  color: #ffffff">
                                    <div>
                                        <h4 class="modal-title" id="" style="color: #0f83c9; font-size: 16px">Vehicle Details</h4><br>
                                    </div>
                                    <div class="form-group">
                                        <label>ID :</label>&nbsp;
                                        <?php echo 'vehicle'.sprintf("%03d",$vehicle_id) ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Vehicle Type :</label>&nbsp;
                                        <?php echo $vehicle_type_name; ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Vehicle Brand :</label>&nbsp;
                                        <?php echo $vehicle_brand_name; ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Vehicle Model :</label>&nbsp;
                                        <?php echo $vehicle_model_name; ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Vehicle Transmission :</label>&nbsp;
                                        <?php echo $vehicle_transmission; ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Vehicle Mileage Range  :</label> 
                                        <?php echo $vehicle_mileage_range; ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Vehicle Engine Capacity :</label>&nbsp;
                                        <?php echo $vehicle_engine_capacity; ?>cc
                                    </div>
                                    <div class="form-group">
                                        <label>Year of Manufacture :</label>&nbsp;
                                        <?php echo $year_of_manufacture; ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Year of Registration :</label>&nbsp;
                                        <?php echo $year_of_registration; ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Average Fuel Consuption :</label>&nbsp;
                                        <?php echo $average_of_fuel_consuption; ?>km/l
                                    </div>
                                    <div class="form-group">
                                        <label>Fuel Type :</label>&nbsp;
                                        <?php echo $fuel_type; ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Vehicle Total Value :</label>&nbsp;
                                        Rs.<?php echo $vehicle_total_value; ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Vehicle Images :</label>&nbsp;


                                        <?php foreach ($insideimage as $inside): ?>
                                            <?php if ($inside['threewheel_bike_id'] == $vehicle_id): ?>
                                                <img src="<?php echo base_url(); ?>upload/3wheel/<?php echo $inside['threewheel_bike_image_name']; ?>"
                                                     style="vertical-align:text-top;height: 61px;margin-top: 44px;margin-left: 3px;">

                                            <?php endif ?>


                                        <?php endforeach ?>
                                    </div>


                                    <!--  <div class="form-group">
          <label>Outside Images :</label>&nbsp;
          <?php
                                    foreach ($outside as $key1) {
                                        ?>
            <?php if ($key1->vehicle_vehicle_id == $value->vehicle_id) { ?>
            <img src="<?php echo base_url(); ?>upload/vehicle/outside/<?php echo $key1->out_side_image_name; ?>" style="vertical-align:text-top;height: 61px;margin-top: 44px;margin-left: 3px;">
            <?php } ?>
            <?php } ?>
          </div> -->

                                    <div><h4 class="modal-title" id="" style="color: #0f83c9; font-size: 16px">Contact details</h4></div>
                                    <br>
                                    <div class="form-group">
                                        <label>Customer Name:</label>&nbsp;
                                        <?php echo $customer_name; ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Phone Number :</label>&nbsp;
                                        <?php echo $phone_number; ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Address :</label>&nbsp;
                                        <?php echo $email; ?>
                                    </div>

                                    <form role="form" action="<?php echo base_url('bike/insertrating'); ?>" method="post" enctype='multipart/form-data'>

                                        <input type="hidden" id="vehicle_id" name="vehicle_id" required
                                               value="<?php echo $vehicle_id; ?>">

                                        <input type="hidden" id="selling_customer_id" name="selling_customer_id" required
                                               value="<?php echo $customer_id; ?>">

                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-3 col-sm-3 col-xs-3 pag">
                                                    <p>Extirier condition </p>
                                                </div>
                                                <div class="col-md-3 col-sm-3 col-xs-3 str">
                                                    <input name="rating1" value="0" id="rating_star1" class="rating_star" type="hidden"
                                                           post_id="1"/>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Star rating 2 -->
                                        <div class="row">

                                            <div class="col-md-3 col-sm-3 col-xs-3 pag">
                                                <p>Interior condition </p>
                                            </div>
                                            <div class="col-md-3 col-sm-3 col-xs-3 str">
                                                <input name="rating2" value="0" id="rating_star2" class="rating_star" type="hidden"
                                                       post_id="2"/>

                                                <!-- End Star rating 3 -->
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 col-sm-3 col-xs-3 pag">
                                                <p>Engine condition</p>
                                            </div>
                                            <div class="col-md-3 col-sm-3 col-xs-3 str">
                                                <input name="rating3" value="0" id="rating_star3" class="rating_star" type="hidden"
                                                       post_id="3"/>

                                                <!-- End Star  -->
                                            </div>
                                        </div><br>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 col-sm-3 col-xs-3">
                                                    <label for="image">Upload Image</label>
                                                    <input type="file" id="image" name="image" >
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label>More Details</label>

                                                    <textarea class="form-control" placeholder="details" style="color: #ffffff"
                                                              rows="4" maxlength="300" name="details"
                                                              id="details" required></textarea>
                                                </div>
                                            </div>
                                        </div>




                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success">Publish</button>
                                            <a href="<?php echo base_url('bike/viewbikes'); ?>"  type="button" class="btn btn-primary" data-dismiss="modal" data-toggle="modal"
                                               data-target="">Reject</a>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>



                    </div>
                </div>
            </div>


        <?php } ?><?php break; ?>
        <?php endforeach; ?>

    </div>


</section>

