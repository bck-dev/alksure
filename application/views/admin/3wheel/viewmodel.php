<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-filled">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                            <a class="panel-close"><i class="fa fa-times"></i></a>
                        </div>
                        List Model
                    </div>
                    <div class="panel-body">

                        <div class="table-responsive">

                            <table id="tableExample3" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Brand</th>
                                    <th>model</th>
                                    <th>Options</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $count = 0;
                                foreach ($threewheel_bike_model as $model):?>
                                    <tr>
                                        <td><?php echo ++$count; ?>.</td>
                                        <td>
                                            <?php foreach ($threewheel_bike_brand as $brand) {
                                                if ($model['threewheel_bike_brand_id'] == $brand['threewheel_bike_brand_id']) {
                                                    echo $brand['threewheel_bike_brand_name'];
                                                }
                                            } ?>
                                        </td>
                                        <td><?php echo $model['threewheel_bike_model_name']; ?></td>
                                        <td>
                                            <a href="<?php echo base_url(); ?>bike/editmodel/<?php echo $model['threewheel_bike_model_id']; ?>"
                                               class="label label-success">Edit</a>
                                            <a href=""
                                               data-bikemodel-id="<?php echo $model['threewheel_bike_model_id']; ?>"
                                               class="label label-danger bikemodelConfirm">Delete</a></td>
                                    </tr>
                                <?php endforeach; ?>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

        </div>


    </div>
</section>








