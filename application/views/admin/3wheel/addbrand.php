<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
<h1>
<?php echo $pagetitle; ?>
</h1>
<ol class="breadcrumb">
<li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
<li class="active"><?php echo $pagetitle; ?></li>
</ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
<!-- left column -->
<div class="col-md-12">
<!-- general form elements -->
<div class="box box-primary">
<div class="box-header with-border">
<h3 class="box-title"><?php echo $pagetitle; ?></h3>
</div><!-- /.box-header -->
<!-- form start -->
<form action="<?php echo base_url('Bike/insertbrand'); ?>" method="POST"
enctype='multipart/form-data'>
<div class="box-body">
<?php echo $this->session->flashdata('msg'); ?>
<?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
<div class="form-group">

<div class="form-group">

<div class="row">
  <div class="col-md-6">
    <label>Type</label>
    <select class="form-control" id="threewheel_type_id" name="threewheel_type_id" required>
      <option value="">-- Select Vehicle Brand --</option>
      <?php foreach($type as $brand): ?>
       <option value="<?php echo $brand['threewheel_bike_type_id']; ?>"><?php echo $brand['threewheel_bike_type_name']; ?></option>
     <?php endforeach; ?>
   </select>
 </div>
</div>
<div class="form-group">
<div class="row">
<div class="col-md-6">
<label>3Wheel & Bike Brand</label>
<input type="text" class="form-control" id="threewheel_bike_brand_name"
placeholder="Enter Three Wheel or Bike Brand" name="threewheel_bike_brand_name">
</div>
</div>
</div>
</div>

<!-- /.box-body -->
<div class="box-footer">
<button type="submit" class="btn btn-primary">Add New Brand</button>
<a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
</div>
</form>
<!-- /.box -->

</div><!--/.col (full) -->
</div>   <!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->
