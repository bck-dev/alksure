

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Grid</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?php foreach ($grid as $data){
                    $id = $data['grid_id'];
                    $image = $data['image'];
                    $title = $data['title'];
                    $description = $data['description'];
                    $link = $data['link'];
                }?>
                <form role="form" action="<?php echo base_url('Grid/updategrid/'.$id); ?>" method="POST" enctype='multipart/form-data'>
                    <div class="box-body">
                        <?php echo $this->session->flashdata('msg'); ?>
                        <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>


                        <div class="col-md-12">
                            <label>Grid Image</label>
                            <input type="hidden" id="grid_image" name="grid_image"
                                   value="<?php echo $image; ?>">
                            <div class="image-group" style="position: relative; margin-top: 20px;">
                                <img src="<?php echo base_url('upload/grid'); ?>/<?php echo $image; ?>"
                                     class="thumbnail" alt="<?php echo $image; ?>" style="width: 20%">
                                <span class="img-close">&#10006;</span>
                            </div>
                            <input type="file" id="input-image" name="fileinput">

                            <p class="help-block">Image size should be below then 3mb and JPG, JPEG,PNG, GIF can be
                                uploaded.</p>
                        </div>


                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Title</label>
                                    <input type="text" class="form-control" id="title" placeholder="Enter title" name="title" value="<?php  echo $title ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Description</label>
                                    <input type="text" class="form-control" id="description" placeholder="Enter description " name="description" value="<?php echo $description ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Link</label>
                                    <input type="text" class="form-control" id="link" placeholder="Enter link" name="link" value="<?php echo $link ?>">
                                </div>
                            </div>
                        </div>




                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Update Gride</button>
                        <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
                    </div>
                </form>
            </div><!-- /.box -->

        </div><!--/.col (full) -->
    </div>   <!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->
