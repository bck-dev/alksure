

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo $pagetitle; ?></h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="<?php echo base_url('Grid/insertgrid'); ?>" method="POST" enctype='multipart/form-data'>
                    <div class="box-body">
                        <?php echo $this->session->flashdata('msg'); ?>
                        <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>



                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="main_banner_image">Agent Image</label>
                                    <input type="file" class="form-control" id="grid_image" name="grid_image">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Title</label>
                                    <input type="text" class="form-control" id="title" placeholder="Enter title" name="title">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Description</label>
                                    <input type="text" class="form-control" id="description" placeholder="Enter description " name="description">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Link</label>
                                    <input type="text" class="form-control" id="link" placeholder="Enter link" name="link">
                                </div>
                            </div>
                        </div>




                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Add New Gride</button>
                        <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
                    </div>
                </form>
            </div><!-- /.box -->

        </div><!--/.col (full) -->
    </div>   <!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->
