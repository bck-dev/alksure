<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-filled">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                            <a class="panel-close"><i class="fa fa-times"></i></a>
                        </div>
                        List Grid
                    </div>
                    <div class="panel-body">

                        <div class="table-responsive">

                            <table id="tableExample3" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Image</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Link</th>
                                    <th>Option</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $count = 0;
                                foreach ($grid as $data):?>
                                    <tr>
                                        <td><?php echo ++$count; ?>.</td>

                                        <td><img src="<?php echo base_url()?>upload/grid/<?php echo $data['image']; ?>" height="83px" width="100px"></td>

                                        <td><?php echo $data['title']; ?></td>
                                        <td><?php echo $data['description']; ?></td>
                                        <td><?php echo $data['link']; ?></td>
                                        <td>
                                            <a href="<?php echo base_url(); ?>grid/editgrid/<?php echo $data['grid_id']; ?>"
                                               class="label label-success">Edit</a></td>


                                    </tr>
                                <?php endforeach; ?>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

        </div>


    </div>
</section>

