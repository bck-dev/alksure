<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-filled">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                            <a class="panel-close"><i class="fa fa-times"></i></a>
                        </div>
                        List Slider
                    </div>
                    <div class="panel-body">

                        <div class="table-responsive">

                            <table id="tableExample3" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Image</th>
                                    <th>Option</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $count = 0;
                                foreach ($slider as $slid):?>
                                    <tr>
                                        <td><?php echo ++$count; ?>.</td>

                                        <td><img src="<?php echo base_url()?>upload/slider/<?php echo $slid['image']; ?>" height="83px" width="100px" ></td>
                                        <td>

                                            <a href="<?php echo base_url(); ?>slider/editslider/<?php echo $slid['slider_id']; ?>"
                                               class="label label-success">Edit</a>
                                            <a href="" data-slider-id="<?php echo $slid['slider_id']; ?>"
                                               class="label label-danger sliderConfirm">Delete</a></td>

                                    </tr>
                                <?php endforeach; ?>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

        </div>


    </div>
</section>

