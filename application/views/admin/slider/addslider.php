<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>
  <style type="text/css">
    input[type=file]{
      display: inline;
    }
    #image_preview1{
      border: 1px solid black;
      padding: 10px;
    }
    #image_preview1 img{
      width: 200px;
      padding: 5px;
    }
  </style>
<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo $pagetitle; ?></h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="<?php echo base_url('Slider/insertslider'); ?>" method="POST" enctype='multipart/form-data'>
                    <div class="box-body">
                        <?php echo $this->session->flashdata('msg'); ?>
                        <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>


                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="main_banner_image">Slider Image</label>
                                    <input type="file" class="form-control" id="uploadFile1" name="slider_image">
                                </div>
                            </div>
                        </div>




                    </div>
                    <!-- /.box-body -->
                     <div id="image_preview1"></div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Add New Slider</button>
                        <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
                    </div>
                </form>
            </div><!-- /.box -->

        </div><!--/.col (full) -->
    </div>   <!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">
  
  $("#uploadFile1").change(function(){
     $('#image_preview1').html("");
     var total_file=document.getElementById("uploadFile1").files.length;

     for(var i=0;i<total_file;i++)
     {
      $('#image_preview1').append("<img src='"+URL.createObjectURL(event.target.files[i])+"'>");
     }

  });

  

</script>

