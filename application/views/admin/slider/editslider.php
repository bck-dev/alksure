

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Slider</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?php foreach ($slider as $slid){
                    $id = $slid['slider_id'];
                    $image = $slid['image'];


                }?>
                <form role="form" action="<?php echo base_url('slider/updateslider/'.$id); ?>" method="POST" enctype='multipart/form-data'>
                    <div class="box-body">
                        <?php echo $this->session->flashdata('msg'); ?>
                        <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>

                        <div class="col-md-12">
                            <label>Slider Image</label>
                            <input type="hidden" id="slider_image" name="slider_image"
                                   value="<?php echo $image; ?>">
                            <div class="image-group" style="position: relative; margin-top: 20px;">
                                <img src="<?php echo base_url('upload/slider'); ?>/<?php echo $image; ?>"
                                     class="thumbnail" alt="<?php echo $image; ?>" style="width: 20%">
                                <span class="img-close">&#10006;</span>
                            </div>
                            <input type="file" id="input-image" name="fileinput">

                            <p class="help-block">Image size should be below then 3mb and JPG, JPEG,PNG, GIF can be
                                uploaded.</p>
                        </div>



                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Update Slider</button>
                        <a class="btn btn-default" href="<?php echo base_url('slider/viewslider'); ?>">Cancel</a>
                    </div>
                </form>
            </div><!-- /.box -->

        </div><!--/.col (full) -->
    </div>   <!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->
