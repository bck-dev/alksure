<section class="content">
            <div class="container-fluid">
            </div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-filled">
            <div class="panel-heading">
                <div class="panel-tools">
                    <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                    <a class="panel-close"><i class="fa fa-times"></i></a>
                </div>
                View Import Vehicle Brand
            </div>
            <div class="panel-body">
              <?php echo $this->session->flashdata('msg'); ?>
              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
                <div class="table-responsive">
                    <table id="tableExample3" class="table table-striped table-hover">
                        
                      <tr>
                        <th style="width: 10px">#</th>
                        <th>Type</th>
                        <th>Brand</th>
                        <th>Options</th>
                      </tr>
                      <?php 
                      $count = 0;
                      foreach($Brands as $brand):?>
                        <tr>
                          <td><?php echo ++$count; ?>.</td>
                          <td><?php foreach($Types as $type): 
                                if($type['vehical_type_id'] == $brand['vehicle_type_id']){
                                        echo $type['vehical_type_name'];
                                      }
                                    endforeach;
                                ?>
                          </td>
                          <td><?php echo $brand['vehicle_brand_name']; ?></td>
                          
                          <td>
                             <a href="<?php echo base_url(); ?>import/editbrand/<?php echo $brand['vehicle_brand_id']; ?>" class="label label-success">Edit</a>
                             <a href="" data-brandim-id="<?php echo $brand['vehicle_brand_id']; ?>" class="label label-danger importbrandConfirm">Delete</a>
                   </tr> 
                   <?php endforeach; ?>
                 </tr> 
                </table>
              </div>
          </div>
        </div>
    </div>
</div>
</section>