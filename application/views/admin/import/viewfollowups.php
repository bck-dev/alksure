<section class="content">
            <div class="container-fluid">
            </div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-filled">
            <div class="panel-heading">
                <div class="panel-tools">
                    <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                    <a class="panel-close"><i class="fa fa-times"></i></a>
                </div>
                Follow Up Import Inquiry Table
            </div>
            <div class="panel-body">
              <?php echo $this->session->flashdata('msg'); ?>
              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
                <div class="table-responsive">
                    <table id="tableExample3" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Inquiry ID</th>
                            <th>Vehicle ID</th>
                            <th>Customer Name</th>
                            <th>Contact Number</th>
                            <th>Comment</th>
                            <th>Options</th>
                            <th>Follow-up Status</th>
                            <th>Next follow-up date</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                                $count = 0;
                                foreach($inquiry1 as $data):
                                  if($data['status']==1):
                                    $id = $data['import_inquiry_id'];
                            ?>
                              <tr>
                                <td><?php echo ++$count; ?>.</td>
                                <td><?php echo 'import'.sprintf("%03d",$data['import_inquiry_id']); ?></td>
                                <td>
                                  <?php echo 'vehicle'.sprintf("%03d",$data['import_vehical_id']); ?> <!-- Vehicle ID -->
                                  &nbsp;&nbsp;
                                  
                                  <button type="button" class="label label-info" data-toggle="modal" data-target="#vehicleModal<?php echo $data['import_vehical_id'];?>">info</button>
                               
                                </td>
                                
                                <td>
                                
                                  <?php echo $data['customer_name']; ?> <!-- Customer Name -->
                                
                                  &nbsp;&nbsp;
                                  <button type="button" class="label label-info" data-toggle="modal" data-target="#peopleModal<?php echo $data['import_inquiry_id'];?>">info</button>
                                </td>
                                
                                <td>
                                  <?php echo $data['phone_number']; ?>
                                </td>
                                
                                <td>
                                  <?php echo $data['comment']; ?>
                                </td>
                                
                                 <td>
                                    <?php if($data['followUp_description'] != NULL && $data['next_followup_date'] != NULL){ ?>

                                        <button class="label label-warning inquiryButton" disabled>Follow-Up</button>
                                    <?php } else{ ?>
                                  <a href="" data-inquiry-id="<?php echo $data['import_inquiry_id']; ?>" data-vehicle-id="<?php echo $data['import_vehical_id'];?>" data-toggle="modal" data-target="#followUpModal<?php echo $data['import_inquiry_id'];?>" id="follow-up" class="label label-warning inquiryButton">Follow-Up</a>
                                   <?php } ?>

                                  <a href="<?php echo base_url();?>import/confirm11/<?php echo $data['import_inquiry_id']; ?>" data-inquiry-id="<?php echo $data['import_inquiry_id']; ?>" data-action-id="2" data-vehicle-id="<?php echo $data['import_vehical_id'];?>" class="label label-success inquiryButton" name="confirm">Confirm</a>

                                  <a href="" data-inquiry-id="<?php echo $data['import_inquiry_id']; ?>" data-action-id="3" data-vehicle-id="<?php echo $data['import_vehical_id'];?>" data-toggle="modal" data-target="#rejectModal<?php echo $data['import_inquiry_id'];?>" id="reject" class="label label-danger inquiryButton">Reject</a>
                                </td>
                                <td><?php echo $data['followUp_description']; ?></td>

                                <td><?php echo $data['next_followup_date']; ?>

                                <?php if($data['next_followup_date'] != NULL): ?>
                                  
                                <form action="<?php echo base_url();?>/import/updateDate/<?php echo $data['import_inquiry_id'];?>" method="post">

                                  <input type="date" name="changed_date" id="changed_date" class="form-control" required>
                                    <button type="submit" class="btn-success">Change Date</button>
                                </form>
                                <?php endif; ?>
                              </td>
                        <?php
                         endif;
                         endforeach; ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
</section>

<!-- customer details model -->
<?php
  foreach($inquiry1 as $data):
    
?>
  <div class="modal fade" id="peopleModal<?php echo $data['import_inquiry_id'];?>" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Contact Person</h4>
        </div>
        <div class="modal-body">
          <table>
            <tr>
              <td><b>Customer Name</b></td>
              <td><b> : </b></td>
              <td><?php echo $data['customer_name'];?></td>
            </tr>
            <tr>
              <td><b>Contact Number</b></td>
              <td><b> : </b></td>
              <td><?php echo $data['phone_number'];?></td>
            </tr>
            <tr>
              <td><b>NIC Number</b></td>
              <td><b> : </b></td>
              <td><?php echo $data['NIC_number'];?></td>
            </tr>
            <tr>
              <td><b>Email</b></td>
              <td><b> : </b></td>
              <td><?php echo $data['email_address'];?></td>
            </tr>
            
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<?php 

endforeach;
?>

<!-- Vehcle details modal -->
<?php
  foreach($vehicles as $vehicle):
?>
  <div class="modal fade" id="vehicleModal<?php echo $vehicle['vehicle_id'];?>" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Vehicle Details</h4>
        </div>
        
        <div class="modal-body">
          <table>
             <!-- <?php echo $vehicle['renting_vehical_id'];?> -->
            <tr>
              <td><b>Vehicle Type</b></td>
              <td><b> : </b></td>
              <td>
                <?php foreach($types as $vehicleType){
                    if($vehicleType['vehical_type_id']==$vehicle['import_vehicle_type']){
                      echo $vehicleType['vehical_type_name'];
                    }
                } ?>
              </td>
            </tr>
            <tr>
              <td><b>Make</b></td>
              <td><b> : </b></td>
              <td>
                <?php foreach($brands as $vehicleBrand){
                      if($vehicleBrand['vehicle_brand_id']==$vehicle['import_brand_id']){
                        echo $vehicleBrand['vehicle_brand_name'];
                      }
                  } ?>
              </td>
            </tr>
            <tr>
               <td><b>Model</b></td>
              <td><b> : </b></td>
              <td>
                <?php foreach($model as $vehicleModel){
                      if($vehicleModel['vehicle_model_id']==$vehicle['import_model_id']){
                        echo $vehicleModel['vehicle_model_name'];
                      }
                  } ?>
              </td>
            </tr>
            <tr>
              <td><b>Transmission</b></td>
              <td><b> : </b></td>
              <td><?php if($vehicle['vehicle_transmission'] == 1){
                      echo 'Automatic';}
                      else if($vehicle['vehicle_transmission'] == 2){
                      echo 'Manual';
                  }
                  else{
                  	echo 'Triptonic';
                  } 
                  
                  ?></td>
            </tr>
            <tr>
              <td><b>Engine Capacity</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['vehicle_engine_capacity'];?>
               <?php if($vehicle['engine_capacity_unit']==1){
              		echo 'CC';
              	}
              	else{
              		echo 'kW';			
              	}
              	 ?>
              </td>
            </tr>
            <tr>
              <td><b>Seating Capacity</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['vehicle_seating_capacity'];?></td>
            </tr>
            <tr>
              <td><b>Year of Manufacture</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['year_of_manufacture'];?></td>
            </tr>
            <tr>
              <td><b>Year of Registration</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['year_of_registration'];?></td>
            </tr>
            <tr>
              <td><b>Average Fuel Consuption</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['average_of_fuel_consuption'];?>
               <?php if ($vehicle['fuel_consumption_unit']==1){
              		echo 'km/l';
              	}
              	else{
              		echo 'mil/l';
              	}
              	
              	?>
              </td>
            </tr>
            <tr>
              <td><b>Vehicle Number</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['vehical_number'];?></td>
            </tr>
            <tr>
              <td><b>Fuel Type</b></td>
              <td><b> : </b></td>
              <td><?php if($vehicle['fuel_type']==1){
                      echo 'Petrol';}
                      else if($vehicle['fuel_type']==2){
                      echo 'Diesel';
                  	}
                  	else if($vehicle['fuel_type']==3){
                  		echo 'Hybrid';
                  	}
                  	else{
                  		echo 'Electic';
                  	}
                  
                  ?></td>
            </tr>
            <tr>
              <td><b>Availabilty</b></td>
              <td><b> : </b></td>
              <td><?php if($vehicle['availability']==1){
                      echo 'Upcoming';}else{
                      echo 'Intransition';
                  }?></td>
            </tr>


            <tr>
              <td><b>Vehicle Total Value</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['vehicle_total_value'];?></td>
            </tr>
            <tr>
              <td><b>Vehicle Features</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['vehicle_features'];?></td>
            </tr>
            <tr>
              <td><b>Minimum Downpayment</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['vehicle_minimum_downpayment'];?></td>
            </tr>
            <tr>
              <td><b>Images</b></td>
              <?php foreach($outside_images as $key):?>
              <?php if($key['vehicle_vehicle_id']==$vehicle['vehicle_id']){?>
              <td></td>
              <td> <img src="<?php echo base_url(); ?>upload/import/outside_multi/<?php echo $key['out_side_image_name']; ?>" style="vertical-align:text-top;height: 61px;margin-top: 0px;margin-left: 3px;" ></td>
              <?php }?>
              <?php endforeach;?>
            </tr>
            <tr>
              <td></td>
            </tr>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<?php 
endforeach;
?>

<!-- follow up modal -->
<?php foreach($inquiry1 as $data): 

    $inq_id = $data['import_inquiry_id'];
?>

  <div class="modal fade" id="followUpModal<?php echo $data['import_inquiry_id'];?>" role="dialog">
  
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Follow Up</h4>
        </div>
          <!-- <?php print_r($inq_id);?> -->
        <div class="modal-body">
            <form role="form" action="<?php echo base_url(); ?>import/followUp1/" method="POST" enctype='multipart/form-data'>

            <input type="hidden" name="inq_id" id="inq_id" value="<?php echo $inq_id;?>"> 
            <div class="form-group">                     
              <label for="home_page_grid_description">Follow Up Reason</label>
              <textarea class="form-control" rows="3" id="Follow_up_description" name="Follow_up_description" placeholder="Enter Follow Up Reason" required></textarea>
            </div>
            <div class="form-group">                     
              <label for="home_page_grid_description">Next Follow Up Date</label>
              <input type="date" name="next_date" id="next_date" class="form-control" required>
            </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Create Follow Up</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      </form>
      
    </div>
  </div>
<?php 
endforeach;
?>

<!-- reject modal -->
<?php foreach($inquiry1 as $data): 

    $inq_id = $data['import_inquiry_id'];
?>

  <div class="modal fade" id="rejectModal<?php echo $data['import_inquiry_id'];?>" role="dialog">
  
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Reject</h4>
        </div>
          <!-- <?php print_r($inq_id);?> -->
        <div class="modal-body">
            <form role="form" action="<?php echo base_url('import/reject1/'); ?>" method="POST" enctype='multipart/form-data'>
            <input type="hidden" name="inq_id" id="inq_id" value="<?php echo $inq_id;?>">
            <div class="form-group">                     
              <label for="home_page_grid_description">Reject Reason</label>
              <textarea class="form-control" rows="3" id="reject_reason" name="reject_reason" placeholder="Enter Reject Reason" required></textarea>
            </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-danger" id="reject">Reject</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      </form>
      
    </div>
  </div>
<?php 

endforeach;
?>