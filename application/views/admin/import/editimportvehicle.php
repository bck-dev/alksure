<section class="content">
    <div class="container-fluid">
    </div>

    <!-- Main content -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-filled">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                        <a class="panel-close"><i class="fa fa-times"></i></a>
                    </div>
                    Edit Import Vehicle
                </div>
                <div class="panel-body">
                    <?php echo $this->session->flashdata('msg'); ?>
                    <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <!-- <div class="box-header with-border">
            <h3 class="box-title"><?php echo $pagetitle; ?></h3>
          </div> --><!-- /.box-header -->
                                <?php foreach($vehicle as $vehi){
                                    $type_id = $vehi['import_vehicle_type'];
                                    $brand_id = $vehi['import_brand_id'];
                                    $model_id = $vehi['import_model_id'];
                                    $vehicle_id = $vehi['vehicle_id'];
                                    // $per_day_price = $vehi['per_day_price'];
                                    $vehical_number1 = $vehi['vehical_number'];
                                    $transmission = $vehi['vehicle_transmission'];
                                    $millage_range = $vehi['vehicle_mileage_range'];
                                    $millage_unit = $vehi['millage_range_unit'];
                                    $engine_capacity = $vehi['vehicle_engine_capacity'];
                                    $engine_cap_unit = $vehi['engine_capacity_unit'];
                                    $seating_capacity = $vehi['vehicle_seating_capacity'];
                                    $year_of_manufactured = $vehi['year_of_manufacture'];
                                    $year_of_registration = $vehi['year_of_registration'];
                                    $fuel_type = $vehi['fuel_type'];
                                    $number_of_owners = $vehi['number_of_owners'];
                                    $avarage_fuel_consumption = $vehi['average_of_fuel_consuption'];
                                    $fuel_con_unit = $vehi['fuel_consumption_unit'];
                                    $features = explode(',',$vehi['vehicle_features']);
                                    $additional_features = $vehi['additional_features'];
                                    $total = $vehi['vehicle_total_value'];
                                    $downpay = $vehi['vehicle_minimum_downpayment'];
                                    $availability = $vehi['availability'];
                                    $sellers_des = $vehi['sellers_description'];
                                    $front_image = $vehi['import_inside_image'];
                                    
                                    
                                   
                                    

                                }?>

                                <!-- form start -->
        <form role="form" action="<?php echo base_url('import/updatevehicle/'.$vehicle_id); ?>" method="POST" enctype='multipart/form-data'>
            <div class="box-body">
                <!-- <?php echo $this->session->flashdata('msg'); ?>
                <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?> -->

                <!--<div class="form-group">-->
                <!--    <div class="row">-->
                <!--        <div class="col-md-12">-->
                <!--            <label>inside Image</label>-->
                <!--                <?php foreach ($front as $fr): ?>-->
                <!--                    <input type="hidden" id="inside_images" name="inside_images" value="<?php echo $fr['image']; ?>">-->
                <!--                    <div class="image-group" style="position: relative; margin-top: 20px;">-->
                <!--                        <?php if ($fr['vehicle_id'] == $vehicle_id ): ?>-->
                <!--                        <img src="<?php echo base_url('upload/import/inside'); ?>/<?php echo $fr['image']; ?>" class="thumbnail" alt="<?php echo $fr['image']; ?>" style="width: 20%">-->
                <!--                        <span class="img-close">&#10006;</span>-->
                <!--                        <?php break; ?>-->
                <!--                <?php endif ?>-->
                <!--            <?php endforeach ?>-->
                <!--        </div>-->
                <!--    <input type="file" id="input-image" name="fileinput">-->
                <!--    <p class="help-block">Image size should be below then 3mb and JPG, JPEG,PNG, GIF can be uploaded.</p>-->
                <!--</div>-->
                <!--<br><br>-->
                
                <div class="form-group">
                    <?php
                    if (isset($vehicle_images) && is_array($vehicle) && count($vehicle)): $i = 1;
                        foreach ($vehicle_images as $key => $data) {
                            ?>
                        <div class="imagelocation<?php echo $data['out_side_image_id'] ?>">
                        <br/>
                        <img src="<?php echo base_url(); ?>upload/import/outside_multi/<?php echo $data['out_side_image_name']; ?>" style="vertical-align:text-top;" width="200" height="200">
                        <a href="" style="cursor:pointer;" data-imageimport-id="<?php echo $data['out_side_image_id']; ?>" class="label label-danger imageimportConfirm">X</a>
                        </div>
                    <?php }endif; ?>
                    </div>
                    <div class="form-group">
                        <label>Outside Images</label>
                            <input type="file" name="userfile[]" id="image_file" accept=".png,.jpg,.jpeg,.gif"
                                   multiple>
                                    <p style="color: red">Minimum Upload 2 or more images</p>
                            <p class="help-block">Image size should be below then 3mb and JPG, JPEG,PNG, GIF can be
                                uploaded.<br/>
                                Recomanded resolution for images is <strong>120 x 120</strong> pixels.
                            </p>
                    </div><br><br>
                    
                    <div class="col-md-12">
                    <label>Front Image</label>
                        <input type="hidden" id="inside_images" name="new_front_image" value="<?php echo $front_image; ?>">
                            <div class="image-group" style="position: relative; margin-top: 20px;">
                                <div class="image-group" style="position: relative; margin-top: 20px;">
                                    <img src="<?php echo base_url(); ?>upload/import/inside/<?php echo $front_image; ?>" class="thumbnail" alt="<?php echo $front_image; ?>" style="width: 20%">
                                    <span class="img-close">&#10006;</span>
                                </div>
                            </div>
                            <input type="file" id="input-image" name="fileinput">
                            <p class="help-block">Image size should be below then 3mb and JPG, JPEG,PNG, GIF can be
                                uploaded.</p>
                        </div>

                        <div class="col-md-6">
                            <label>Vehicle Type</label>
                            <select class="form-control" id="typeSelect" name="vehicle_type" required>
                                <option value="">-- Select Vehicle Type --</option>
                                <?php foreach($types as $type): ?>
                                    <option value="<?php echo $type['vehical_type_id']; ?>" <?php echo ($type_id == $type['vehical_type_id']) ? 'selected' : ''; ?>><?php echo $type['vehical_type_name']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>Vehicle Brand</label>
                            <select class="form-control" id="" name="vehicle_brand" required>
                                <option value="">-- Select Vehicle Brand --</option>
                                <?php foreach($brands as $brand): ?>
                                    <option value="<?php echo $brand['vehicle_brand_id']; ?>" <?php echo ($brand_id == $brand['vehicle_brand_id']) ? 'selected' : ''; ?>><?php echo $brand['vehicle_brand_name']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>Vehicle Model</label>
                            <select class="form-control " name="vehicle_model" required >
                                <option value="">-- Select Vehicle Modal --</option>
                                <?php foreach($modals as $vehiclemodel): ?>
                                    <option value="<?php echo $vehiclemodel['vehicle_model_id']; ?>" <?php echo ($model_id == $vehiclemodel['vehicle_model_id']) ? 'selected' : ''; ?>><?php echo $vehiclemodel['vehicle_model_name'] ?> </option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <div class="col-md-6">
                            <label>Year of Registration</label>
                            <input type="text" class="form-control" id="year_of_registration" placeholder="Enter Year of Registration" name="year_of_registration" value="<?php echo $year_of_registration ?>">
                        </div>
                        <div class="col-md-6">
                            <label>Year of Manufacture</label>
                            <input type="text" class="form-control" id="year_of_manufacture" placeholder="Enter Year of Manufacture" name="year_of_manufacture" value="<?php echo $year_of_manufactured ?>">
                        </div>
                        <div class="col-md-6">
                            <label>Vehicle Number</label>
                            <input type="text" class="form-control" id="vehicle_num" placeholder="Enter vehicle number" name="vehical_number" value="<?php echo $vehical_number1 ?>">
                        </div>
                        <div class="col-md-6">
                            <label>Transmission</label>
                            <select class="form-control" id="vehicle_transmission" name="vehicle_transmission">
                                <option value="">-- Select Transmission --</option>
                                <?php for($x=1; $x<=3; $x++) : ?>
                                    <option value="<?php echo $x; ?>" <?php echo ($x == $transmission) ? 'selected' : ''; ?>><?php if($x == 3){
                                        echo 'Triptonic';
                                    }
                                    elseif($x == 2){
                                        echo "Manual";
                                    } 
                                    else{
                                        echo'Automatic' ; 
                                    }?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Vehicle Mileage Range</label>
                            <input type="text" class="form-control" id="vehicle_milage_range" placeholder="Enter Vehicle Mileage Range (Km)" name="vehicle_mileage_range" value="<?php echo $millage_range ?>">
                        </div>
                        <div class="col-md-2">
                            <label> Range</label>
                            <select class="form-control" id="vehicle_range1" name="vehicle_range" >
                                <option>--Select Range type--</option>
                                <?php for($x=1; $x<=2; $x++) : ?>
                                    <option value="<?php echo $x; ?>" <?php echo ($x == $millage_unit) ? 'selected' : ''; ?>><?php echo ($x == '2') ? 'Mil' :'Km' ; ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>Fuel Type</label>
                            <select class="form-control" id="fuel_type" name="fuel_type">
                                <option value="">-- Select Fuel Type --</option>
                                <?php for($x=1; $x<=4; $x++) : ?>
                                    <option value="<?php echo $x; ?>" <?php echo ($x == $fuel_type) ? 'selected' : ''; ?>><?php if($x == '4'){
                                        echo "Electric";
                                    }  
                                    elseif($x == 3){
                                        echo 'Hybrid';
                                    } 
                                    elseif ($x == 2) {
                                        echo "Diesel";
                                    }
                                    else{
                                        echo "Petrol";
                                    } ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>

                        <div class="col-md-4">
                            <label>Average of Fuel Consuption</label>
                            <input type="text" class="form-control" id="average_of_fuel_consuption" placeholder="Enter Average of Fuel Consuption" name="average_of_fuel_consuption" value="<?php echo $avarage_fuel_consumption; ?>">
                        </div>
                        <div class="col-md-2">
                            <label>Per leter</label>
                            <select class="form-control" id="fuel_con1" name="fuel_con" >
                                <option>--Select Consumption--</option>
                                <?php for($x=1; $x<=2; $x++) : ?>
                                    <option value="<?php echo $x; ?>" <?php echo ($x == $fuel_con_unit) ? 'selected' : ''; ?>><?php echo ($x == '2') ? 'Mil/l' :'Km/l' ; ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>

                        <div class="col-md-4">
                            <label>Vehicle Engine Capacity</label>
                            <input type="text" class="form-control" id="vehicle_engine_capacity" placeholder="Enter Vehicle Engine Capacity (CC)" name="vehicle_engine_capacity" value="<?php echo  $engine_capacity; ?>">
                        </div>
                        <div class="col-md-2">
                            <label>Unit</label>
                            <select class="form-control" id="vehicle_range" name="engine_cap" >
                                <option>--Select Unit--</option>
                                <?php for($x=1; $x<=2; $x++) : ?>
                                    <option value="<?php echo $x; ?>" <?php echo ($x == $engine_cap_unit) ? 'selected' : ''; ?>><?php echo ($x == '2') ? 'kW' :'CC' ; ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>

                        <div class="col-md-6">
                            <label>Vehicle Seating Capacity</label>
                            <input type="text" class="form-control" id="vehicle_seating_capacity" placeholder="Enter Vehicle Seating Capacity" name="vehicle_seating_capacity" value="<?php echo $seating_capacity; ?>">
                        </div>
                        <div class="col-md-6">
                            <label>Availability</label>
                            <select class="form-control" id="availability" name="availability">
                                <option value="">-- Select Availability --</option>
                                <?php for($x=1; $x<=2; $x++) : ?>
                                    <option value="<?php echo $x; ?>" <?php echo ($x == $availability) ? 'selected' : ''; ?>><?php echo ($x == '2') ? 'Intransition' :'Upcoming' ; ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>


                        <div class="col-md-6">
                            <label>Total</label>
                            <input type="text" class="form-control" id="per_day_price" placeholder="Enter per day price" name="total" value="<?php echo $total; ?>">
                        </div>

                        <div class="col-md-6">
                            <label>Downpayment</label>
                            <input type="text" class="form-control" id="discount" placeholder="Enter Discount" name="down" value="<?php echo $downpay; ?>">
                        </div>
                        
                        <div class="col-md-6">
                            <label>Number of Owners</label>
                            <input type="text" class="form-control" id="discount" placeholder="Enter Discount" name="num_of_owners" value="<?php echo $number_of_owners; ?>">
                        </div>
                        
                        
            <div class="col-md-12" >
                <div><label>Vehicle Features</label></div>
                
                <div class="col-md-3">
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="A/C: Front" <?php echo in_array('A/C: Front',$features) ? 'checked': '' ?>>A/C: Front</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Power Locks" <?php echo in_array('Power Locks',$features) ? 'checked': '' ?>>Power Locks</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Airbag: Driver" <?php echo in_array('Airbag: Driver',$features) ? 'checked': '' ?>>Airbag: Driver</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Anti-Lock Brakes" <?php echo in_array('Anti-Lock Brakes',$features) ? 'checked': '' ?>>Anti-Lock Brakes</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Rear Window Wiper" <?php echo in_array('Rear Window Wiper',$features) ? 'checked': '' ?>>Rear Window Wiper</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="USB" <?php echo in_array('USB',$features) ? 'checked': '' ?>>USB</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Tow Package" <?php echo in_array('Tow Package',$features) ? 'checked': '' ?>>Tow Package</label>
                </div>
                <div class="col-md-3">
                  
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="A/C: Rear" <?php echo in_array('A/C: Rear',$features) ? 'checked': '' ?>>A/C: Rear</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Multifunction Steering" <?php echo in_array('Multifunction Steering',$features) ? 'checked': '' ?>>Multifunction Steering</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Alarm" <?php echo in_array('Alarm',$features) ? 'checked': '' ?>>Alarm</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Rear Window Defroster" <?php echo in_array('Rear Window Defroster',$features) ? 'checked': '' ?>>Rear Window Defroster</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="DVD" <?php echo in_array('DVD',$features) ? 'checked': '' ?>>DVD</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Third Row Seats" <?php echo in_array('Third Row Seats',$features) ? 'checked': '' ?>>Third Row Seats</label><br>
                </div>
                <div class="col-md-3">

                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Cruise Control" <?php echo in_array('Cruise Control',$features) ? 'checked': '' ?>>Cruise Control</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Power Steering" <?php echo in_array('Power Steering',$features) ? 'checked': '' ?>>Power Steering</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Airbag: Passenger" <?php echo in_array('Airbag: Passenger',$features) ? 'checked': '' ?>>Airbag: Passenger</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Fog Lights" <?php echo in_array('Fog Lights',$features) ? 'checked': '' ?>>Fog Lights</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Tinted Glass" <?php echo in_array('Tinted Glass',$features) ? 'checked': '' ?>>Tinted Glass</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Alloy Wheels" <?php echo in_array('Alloy Wheels',$features) ? 'checked': '' ?>>Alloy Wheels</label><br>
                </div>
                <div class="col-md-3" style="margin-bottom: 26px;">
                
                <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Navigation System" <?php echo in_array('Navigation System',$features) ? 'checked': '' ?>>Navigation System</label><br>
                <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Remote Keyless Entry" <?php echo in_array('Remote Keyless Entry',$features) ? 'checked': '' ?>>Remote Keyless Entry</label><br>
                <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Airbag: Side" <?php echo in_array('Airbag: Side',$features) ? 'checked': '' ?>>Airbag: Side</label><br>
                <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Power Windows" <?php echo in_array('Power Windows',$features) ? 'checked': '' ?>>Power Windows</label><br>
                <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="CD" <?php echo in_array('CD',$features) ? 'checked': '' ?>>CD</label><br>
                <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Sunroof/Moonroof" <?php echo in_array('Sunroof/Moonroof',$features) ? 'checked': '' ?>>Sunroof/Moonroof</label><br>
                </div>
                <div class="form-group">
                  <label for="comment">Additional Features:</label>
                  <textarea class="form-control" rows="5" id="comment" name="additional_features"></textarea>
                </div>
              </div>
			
			<div class="col-md-12">
                        <label>Selleres Description</label>
                            <textarea class="form-control" placeholder="Sellers Description" name="seller_description_edit" ><?php echo $sellers_des; ?></textarea>
                        </div>
			
                        <br><br>
                        
                    </div>
                        
                  </div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Update Vehicle</button>
                    <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
                </div>
        </form>
                            </div><!-- /.box -->

                        </div><!--/.col (full) -->
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>