<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-filled">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                            <a class="panel-close"><i class="fa fa-times"></i></a>
                        </div>
                        Import Vehicle List 
                    </div>
                    <div class="panel-body">

                        <div class="table-responsive">
                            <table id="tableExample3" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Image</th>
                                    <th>Type</th>
                                    <th>Brand</th>
                                    <th>Model</th>
                                    <th>Availability</th>
                                    <th>Options</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $count = 0;
                                foreach($vehicles as $data):?>

                                    <?php
                                        $vehicle_id = $data['vehicle_id'];
                                    ?>

                                    <tr>
                                        <td><?php echo ++$count; ?>.</td>
                                        <td>
                                            <img src="<?php echo base_url(); ?>upload/import/inside/<?php echo $data['import_inside_image']; ?>"
                                                         style="vertical-align:text-top;height: 61px;margin-top: 0px;margin-left: 3px;">

                                            <!--<?php foreach ($front_image as $front): ?>-->
                                            <!--    <?php if ($front['vehicle_id'] == $vehicle_id ): ?>-->
                                            <!--        <img src="<?php echo base_url(); ?>upload/import/inside/<?php echo $front['image']; ?>" style="vertical-align:text-top;height: 61px;margin-top: 0px;margin-left: 3px;" >-->
                                            <!--        <?php break; ?>-->
                                            <!--    <?php endif ?>-->

                                            <!--<?php endforeach ?>-->
                                        </td>

                                        <td> <?php foreach ($types as $type): ?>
                                                <?php if($type['vehical_type_id'] == $data['import_vehicle_type'] ): ?>
                                                    <?php echo $type['vehical_type_name'];?>
                                                    <?php break; ?>
                                                <?php endif ?>
                                            <?php endforeach ?></td>

                                        <td><?php foreach ($brands as $brand): ?>
                                                <?php if($brand['vehicle_brand_id'] == $data['import_brand_id'] ): ?>
                                                    <?php echo $brand['vehicle_brand_name'];?>
                                                    <?php break; ?>
                                                <?php endif ?>
                                            <?php endforeach ?></td>

                                        <td><?php foreach ($modals as $modal): ?>
                                                <?php if($modal['vehicle_model_id'] == $data['import_model_id'] ): ?>
                                                    <?php echo $modal['vehicle_model_name'];?>
                                                    <?php break; ?>
                                                <?php endif ?>
                                            <?php endforeach ?></td>

                                        <td><?php if($data['availability']==1){
                                                  echo "Up Coming";
                                                }else{
                                                  echo "In transistion";    
                                                }


                                         ?></td>
                                        <td>
                                            <a href="" class="label label-primary" data-toggle="modal" data-target="#viewVehicle<?php echo $data['vehicle_id'];?>" data-whatever="@mdo">View</a>

                                            <a href="<?php echo base_url(); ?>import/editvehicle/<?php echo $data['vehicle_id']; ?>" class="label label-success">Edit</a>

                                            <a href="" data-vehicleim-id="<?php echo $data['vehicle_id']; ?>" class="label label-danger vehicleim1Confirm">Delete</a></td>
                                    </tr>

                                <?php endforeach; ?>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

        </div>


    </div>
</section>



<?php foreach($vehicles as $vehi):

    $vehicle_id = $vehi['vehicle_id'];

    foreach ($brands as $brand): 
        if($brand['vehicle_brand_id'] == $vehi['import_brand_id'] ):
            $brand_name = $brand['vehicle_brand_name'];
            break; 
        endif;
    endforeach;

    foreach ($modals as $modal): 
        if($modal['vehicle_model_id'] == $vehi['import_model_id'] ): 
            $modal_name = $modal['vehicle_model_name'];
            break;
        endif;
    endforeach;

?>

    <div class="modal fade" id="viewVehicle<?php echo $vehi['vehicle_id'];?>" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><?php echo $brand_name." ".$modal_name;?></h4>
                </div>
                <!-- <?php print_r($inq_id);?> -->
                <div class="modal-body">

                        <label for="home_page_grid_description"><b></label>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Front Image</label>
                                    <img src="<?php echo base_url(); ?>upload/import/inside/<?php echo $data['import_inside_image']; ?>"
                                                         style="vertical-align:text-top;height: 61px;margin-top: 0px;margin-left: 3px;">
                                                         <br>
                                    <!--<?php foreach ($front_image as $front): ?>-->
                                    <!--            <?php if ($front['vehicle_id'] == $vehicle_id ): ?>-->
                                    <!--                <img src="<?php echo base_url(); ?>upload/import/inside/<?php echo $front['image']; ?>" style="vertical-align:text-top;height: 61px;margin-top: 0px;margin-left: 3px;" >-->
                                    <!--                <?php break; ?>-->
                                    <!--            <?php endif ?>-->

                                    <!--        <?php endforeach ?>-->
                                    <label>Inside Images</label>
                                    <?php foreach ($outside_images as $out): ?>
                                                <?php if ($out['vehicle_vehicle_id'] == $vehicle_id ): ?>
                                                    <img src="<?php echo base_url(); ?>upload/import/outside_multi/<?php echo $out['out_side_image_name']; ?>" style="vertical-align:text-top;height: 61px;margin-top: 0px;margin-left: 3px;" >
                                                    
                                                <?php endif ?>
                                            <?php endforeach ?>
                                </div>
                            </div>
                        </div>
                      
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Vehicle number</label>
                                    <?php echo $vehi['vehical_number'];?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Transmission</label>
                                    <?php if($vehi['vehicle_transmission'] == 1){
                                        echo 'Automatic';}else{
                                        echo 'Manual';
                                    }?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Millage range</label>
                                    <?php echo $vehi['vehicle_mileage_range'];?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Engine capacity</label>
                                    <?php echo $vehi['vehicle_engine_capacity'];?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Seating capacity </label>
                                    <?php echo $vehi['vehicle_seating_capacity'];?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Year of manufactured</label>
                                    <?php echo $vehi['year_of_manufacture'];?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Year of registration</label>
                                    <?php echo $vehi['year_of_registration'];?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Fuel type</label>
                                    <?php if($vehi['fuel_type']==1){
                                        echo 'Petrol';}else{
                                        echo 'Diesel';
                                    }?>
                                </div>
                            </div>
                        </div>
                     
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Avarage fuel consumption</label>
                                    <?php echo $vehi['average_of_fuel_consuption'];?>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Features</label>
                                    <?php echo $vehi['vehicle_features'];?>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                          <div class="row">
                              <div class="col-md-6">
                                <label>Total</label>
                                Rs.<?php echo $vehi['vehicle_total_value'];?>
                              </div>
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="row">
                              <div class="col-md-6">
                                <label>Minimum Downpayment</label>
                                Rs.<?php echo $vehi['vehicle_minimum_downpayment'];?>
                              </div>
                          </div>
                        </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <?php

endforeach;
?>




