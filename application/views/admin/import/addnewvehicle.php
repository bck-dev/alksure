<section class="content">
            <div class="container-fluid">
            </div>

  <!-- Main content -->
  <div class="row">
    <div class="col-md-12">
        <div class="panel panel-filled">
            <div class="panel-heading">
                <div class="panel-tools">
                    <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                    <a class="panel-close"><i class="fa fa-times"></i></a>
                </div>
                Add New Import Vehicle
            </div>
            <div class="panel-body">
              <?php echo $this->session->flashdata('msg'); ?>
              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
                <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <!-- <div class="box-header with-border">
            <h3 class="box-title"><?php echo $pagetitle; ?></h3>
          </div> --><!-- /.box-header -->
          <!-- form start -->
          <form role="form" action="<?php echo base_url('Import/insertvehicle'); ?>" method="POST" enctype='multipart/form-data'>
            <div class="box-body">
              <!-- <?php echo $this->session->flashdata('msg'); ?>
              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?> -->

          <div class="form-group">
            <div class="row">        
              <div class="col-md-6">
                <label>Vehicle Type</label>
                <select class="form-control vehicle_type" id="typeIm" name="vehicle_type">
                  <option value="">-- Select Vehicle Type --</option>
                   <?php foreach($types as $type): ?>
                   <option value="<?php echo $type['vehical_type_id']; ?>"><?php echo $type['vehical_type_name']; ?></option>
                   <?php endforeach; ?>
                </select>
              </div>
              <div class="col-md-6">
                <label>Vehicle Brand</label>
                 <select class="form-control typeSelect" id="makeIm" name="vehicle_brand" disabled>
                  <option value="">-- Select Vehicle Brand --</option>
                   <?php foreach($Brands as $brand): ?>
                   <option value="<?php echo $brand['vehicle_brand_id']; ?>" class ="<?php echo "aa".$brand['vehicle_type_id']."aa"; ?>"><?php echo $brand['vehicle_brand_name']; ?></option>
                   <?php endforeach; ?>
                </select>
              </div>
              <div class="col-md-6">
                <label>Vehicle Model</label>
                <select class="form-control vehicle_model" name="vehicle_model" id="ModelIm" disabled>
                  <option value="">-- Select Vehicle Modal --</option>
                   <?php foreach($modals as $vehiclemodel): ?>
                  <option value="<?php echo $vehiclemodel['vehicle_model_id']; ?>" data-id="<?php echo $vehiclemodel['vehicle_model_id']?>" class="<?php echo "aa".$vehiclemodel['vehicle_brand_id']."aa";?>"><?php echo $vehiclemodel['vehicle_model_name']; ?></option>
                   <?php endforeach; ?>
                </select>               
              </div>

              <div class="col-md-6">
                <label>Year of Registration</label>
                <input type="number" class="form-control" id="year_of_registration" placeholder="Enter Year of Registration" name="registration">
              </div>
              <div class="col-md-6">
                <label>Year of Manufacture</label>
                <input type="number" class="form-control" id="year_of_manufacture" placeholder="Enter Year of Manufacture" name="manufacture">
              </div>
               <div class="col-md-6">
                <label>Vehicle Number</label>
                <input type="text" class="form-control" id="vehicle_num" placeholder="Enter Vehicle Number" name="number">
              </div>
              <div class="col-md-6">
                <label>Select Transmission</label>
               <select class="form-control" id="Transmission" name="transmiss">
                  <option value="">-- Select Transmission --</option>
                   <option value="1">Automatic</option>
                   <option value="2">Manual</option>
                   <option value="3">Triptonic</option>
                </select>
              </div>
              <div class="col-md-4">
                <label>Vehicle Mileage Range</label>
                <input type="number" class="form-control" id="vehicle_milage_range" placeholder="Enter Vehicle Mileage Range (Km)" name="mileage_range">
              </div>
              <div class="col-md-2">
                <label> Range</label>
                <select class="form-control" id="vehicle_range1" name="vehicle_range" >
                  <option>--Select Consumption--</option>
                  <option value="1" data-id="1">Km</option>
                  <option value="2" data-id="2">mi</option>
                </select>
              </div>
               <div class="col-md-6">
                <label>Fuel Type</label>
               <select class="form-control" id="fuel_type" name="fuel_ty">
                  <option value="">-- Select Fuel Type --</option>
                   <option value="1">Petrol</option>
                   <option value="2">Diesel</option>
                   <option value="3">Hybrid</option>
                   <option value="4">Electric</option>
                </select>
              </div>

              <div class="col-md-4">
                <label>Average of Fuel Consuption</label>
                <input type="number" class="form-control" id="average_of_fuel_consuption" placeholder="Enter Average of Fuel Consuption" name="fuel_consuption">
              </div>
              <div class="col-md-2">
                <label> Leters per</label>
                <select class="form-control" id="fuel_con1" name="letersPer" >
                  <option>--Select Consumption type--</option>
                  <option value="1">Km/l</option>
                  <option value="2">mi/l</option>

                </select>
              </div>

              <div class="col-md-4">
                <label>Vehicle Engine Capacity</label>
                <input type="number" class="form-control" id="vehicle_engine_capacity" placeholder="Enter Vehicle Engine Capacity (CC)" name="engine_capacity">
              </div>
              <div class="col-md-2">
                <label> Capacity</label>
                <select class="form-control" id="Capacity" name="capacity" >
                    <option>--Select Capacity--</option>
                    <option value="1">CC</option>
                    <option value="2">kW</option>
                </select>
              </div>

              <div class="col-md-6">
                <label>Vehicle Seating Capacity</label>
                <input type="number" class="form-control" id="vehicle_seating_capacity" placeholder="Enter Vehicle Seating Capacity" name="seating_capacity">
              </div>
              <div class="col-md-6">
                <label>Availability</label>
               <select class="form-control" id="availability" name="availabil">
                  <option value="">-- Select Availability --</option>
                   <option value="1">Upcoming</option>
                   <option value="2">Intransition</option>
                </select>
              </div>

              <div class="col-md-6">
                <label>Vehicle Total Value</label>
                <input type="number" class="form-control" id="vehicle_total_value" placeholder="Enter Vehicle Total Value" name="vehicle_total">
              </div>

              <div class="col-md-6">
                <label>Vehicle Minimum Downpayment</label>
                <input type="number" class="form-control" id="vehicle_minimum_downpayment" placeholder="Enter Vehicle Minimum Downpayment (Rs)" name="minimum_downpayment">
              </div>
              
               <div class="col-md-6">
                <label>Number of Owners</label>
                <input type="number" class="form-control" id="number_of_owners" placeholder="Enter Number Of Owner" name="num_of_owners">
              </div>
              
              <div class="col-md-6">
                <label>Duration</label>
                <input type="text" class="form-control" id="Duration" placeholder="duration" name="duration">
              </div>

              <div class="col-md-12" >
                <div><label>Vehicle Features</label></div>
                
                <div class="col-md-3">
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="A/C: Front">A/C: Front</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Power Locks">Power Locks</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Airbag: Driver">Airbag: Driver</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Anti-Lock Brakes">Anti-Lock Brakes</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Rear Window Wiper">Rear Window Wiper</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="USB">USB</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Tow Package">Tow Package</label>
                </div>
                <div class="col-md-3">
                  
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="A/C: Rear">A/C: Rear</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Multifunction Steering">Multifunction Steering</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Alarm">Alarm</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Rear Window Defroster">Rear Window Defroster</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="DVD">DVD</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Third Row Seats">Third Row Seats</label><br>
                </div>
                <div class="col-md-3">

                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Cruise Control">Cruise Control</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Power Steering">Power Steering</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Airbag: Passenger">Airbag: Passenger</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Fog Lights">Fog Lights</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Tinted Glass">Tinted Glass</label><br>
                  <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Alloy Wheels">Alloy Wheels</label><br>
                </div>
                <div class="col-md-3" style="margin-bottom: 26px;">
                
                <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Navigation System">Navigation System</label><br>
                <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Remote Keyless Entry">Remote Keyless Entry</label><br>
                <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Airbag: Side">Airbag: Side</label><br>
                <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Power Windows">Power Windows</label><br>
                <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="CD">CD</label><br>
                <label class="checkbox-inline"><input type="checkbox" name="vehicle_features[]" value="Sunroof/Moonroof">Sunroof/Moonroof</label><br>
                </div>
                <br>
                <div class="form-group">
                  <label for="comment">Additional Features:</label>
                  <textarea class="form-control" rows="5" id="comment" name="additional_features"></textarea>
                </div>
              </div>
              
              <div class="col-md-6">
                <label>Seller Description</label>
                <textarea class="form-control" placeholder="Seller Description" name="sellers_description"></textarea>
              </div>

             <div class="col-md-6">
                <label>Front image</label>
                <input type="file" name="front_image" accept=".png,.jpg,.jpeg,.gif">
              </div>
              <br><br>
              
              <!-- problem -->
               <div class="col-md-6">
                <label>Vehicle Images</label>
                  <input type="file" id="uploadFile" name="userfile[]" multiple/>
                <p class="help-block">Image size should be below then 3mb and JPG, JPEG,PNG, GIF can be uploaded.<br/>
                
                </p>
              </div>

             <!-- <div class="col-md-6">
                <label>Outside Images</label>
                <input type="file" name="userfile[]" id="image_file" accept=".png,.jpg,.jpeg,.gif" multiple>
                <p class="help-block">Image size should be below then 3mb and JPG, JPEG,PNG, GIF can be uploaded.<br/>
                Recomanded resolution for images is <strong>400 x 600</strong> pixels.
                </p>
              </div>   -->
              
            </div>
           
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Add New Vehicle</button>
              <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
            </div>
          </form>
        </div><!-- /.box -->

      </div><!--/.col (full) -->
    </div>

            </div>
        </div>
    </div>
</div>
</section>