<section class="content">
            <div class="container-fluid">
            </div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-filled">
            <div class="panel-heading">
                <div class="panel-tools">
                    <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                    <a class="panel-close"><i class="fa fa-times"></i></a>
                </div>
                Rejected Reservations Table
            </div>
            <div class="panel-body">
              <?php echo $this->session->flashdata('msg'); ?>
              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
                <div class="table-responsive">
                    <table id="tableExample3" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Reservation ID</th>
                            <th>Vehicle ID</th>
                            <th>Customer Name</th>
                            <th>Contact Number</th>
                            <th>Buying Method</th>
                            <th>Price</th>
                            <th>Reject Reason</th>
                            <th>Options</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                                $count = 0;
                                foreach($reservation as $res):
                                    if($res['status']==3):
                                    $id = $res['reservation_id'];
                            ?>
                              <tr>
                                <td><?php echo ++$count; ?>.</td>
                                <td><?php echo 'reservation'.sprintf("%03d",$res['reservation_id']); ?></td>
                                <td>
                                  <?php echo 'vehicle'.sprintf("%03d",$res['vehical_vehical_id']); ?> <!-- Vehicle ID -->
                                  &nbsp;&nbsp;
                                  
                                  <button type="button" class="label label-info" data-toggle="modal" data-target="#vehicleModal<?php echo $res['vehical_vehical_id'];?>">info</button>
                               
                                </td>
                                <?php foreach($customer as $cus): ?>
                                    <?php if($cus['customer_id'] == $res['customer_customer_id']): ?>
                                <td>
                                  <?php echo $cus['customer_name']; ?> <!-- Customer Name -->
                                  &nbsp;&nbsp;
                                  <button type="button" class="label label-info" data-toggle="modal" data-target="#peopleModal<?php echo $res['customer_customer_id'];?>">info</button>
                                </td>
                                <td><?php echo $cus['phone_number']; ?></td>
                                <?php 
                                 endif;
                                 endforeach;?>
                                 <?php if($res['buying_method'] == 1){ ?>
                                 <td><?php echo "Out Right";?></td>
                                 <?php } else{ ?>
                                  <td><?php echo "Leasing";?></td>
                                <?php }?>

                                <?php if($res['buying_method'] == 2 ){?>
                                 <td>Downpayment Rs.<?php echo $res['downpayment'];?>, Monthly Insatllment Rs.<?php echo $res['monthly_installment'];?>, Duration <?php echo $res['duration'];?> Years</td>
                                <?php } else{?>
                                  <td>Rs.<?php echo $res['price'];?></td>
                                <?php } ?>

                                 <td><?php echo $res['reject_reason']; ?></td>
                                 <td>
                                  <a href="<?php echo base_url();?>reservation/pending/<?php echo $res['reservation_id']; ?>" data-inquiry-id="<?php echo $res['reservation_id']; ?>" data-vehicle-id="<?php echo $res['reservation_id'];?>" data-toggle="modal" class="label label-warning inquiryButton">Back to Pending</a>

                                  <a href="<?php echo base_url();?>reservation/confirm1/<?php echo $res['reservation_id']; ?>" data-inquiry-id="<?php echo $res['reservation_id']; ?>" data-action-id="2" data-vehicle-id="<?php echo $res['vehical_vehical_id'];?>" class="label label-success inquiryButton" name="confirm">Payment Collected</a>

                                  <a href="" data-inquiry-id="<?php echo $res['reservation_id']; ?>" data-action-id="3" data-vehicle-id="<?php echo $res['reservation_id']; ?>" data-toggle="modal" data-target="#followUpModal<?php echo $res['reservation_id']; ?>" id="follow-up"  class="label label-warning inquiryButton">Follow up</a>

                                </td>
                            </tr>

                        <?php
                        endif;
                         endforeach;?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
</section>

<!-- customer details model -->
<?php
  foreach($customer as $cus): 
    
?>
  <div class="modal fade" id="peopleModal<?php echo $cus['customer_id'];?>" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Contact Person</h4>
        </div>
        <div class="modal-body">
          <table>
            <tr>
              <td><b>Customer Name</b></td>
              <td><b> : </b></td>
              <td><?php echo $cus['customer_name'];?></td>
            </tr>
            <tr>
              <td><b>Contact Number</b></td>
              <td><b> : </b></td>
              <td><?php echo $cus['phone_number'];?></td>
            </tr>
            <tr>
              <td><b>NIC Number</b></td>
              <td><b> : </b></td>
              <td><?php echo $cus['NIC_number'];?></td>
            </tr>
            <tr>
              <td><b>Email</b></td>
              <td><b> : </b></td>
              <td><?php echo $cus['email'];?></td>
            </tr>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<?php 

endforeach;
?>

<!-- Vehcle details modal -->
<?php
  foreach($vehicles as $vehicle):
?>
  <div class="modal fade" id="vehicleModal<?php echo $vehicle['vehicle_id'];?>" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Vehicle Details</h4>
        </div>
        
        <div class="modal-body">
          <table>
            <?php echo $vehicle['vehicle_id'];?>
            <tr>
              <td><b>Vehicle Type</b></td>
              <td><b> : </b></td>
              <td>
                <?php foreach($types as $vehicleType){
                    if($vehicleType['vehicle_type_id']==$vehicle['vehicle_type']){
                      echo $vehicleType['vehicle_type_name'];
                    }
                } ?>
              </td>
            </tr>
            <tr>
              <td><b>Make</b></td>
              <td><b> : </b></td>
              <td>
                <?php foreach($brands as $vehicleBrand){
                      if($vehicleBrand['vehicle_brand_id']==$vehicle['vehicle_brand']){
                        echo $vehicleBrand['vehicle_brand_name'];
                      }
                  } ?>
              </td>
            </tr>
            <tr>
               <td><b>Model</b></td>
              <td><b> : </b></td>
              <td>
                <?php foreach($model as $vehicleModel){
                      if($vehicleModel['vehicle_model_id']==$vehicle['vehicle_model']){
                        echo $vehicleModel['vehicle_model_name'];
                      }
                  } ?>
              </td>
            </tr>
            <tr>
              <td><b>Transmission</b></td>
              <td><b> : </b></td>
              <td><?php if($vehicle['vehicle_transmission']==1){
              		echo "Automatic";
              }
              else if($vehicle['vehicle_transmission']==2){
              		echo "Manual";
              }
              else{
              		echo "Triptonic";
              }
              
              ?></td>
            </tr>
            <tr>
              <td><b>Engine Capacity</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['vehicle_engine_capacity'];?>
              <?php if($vehicle['engine_capacity_unit']==1){
              		echo "CC";
              	}
              	else{
              		echo "kW";
              	}
              	 ?>
              </td>
            </tr>
            <tr>
              <td><b>Seating Capacity</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['vehicle_seating_capacity'];?></td>
            </tr>
            <tr>
              <td><b>Year of Manufacture</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['year_of_manufacture'];?></td>
            </tr>
            <tr>
              <td><b>Year of Registration</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['year_of_registration'];?></td>
            </tr>
            <tr>
              <td><b>Average Fuel Consuption</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['average_of_fuel_consuption'];?>
              <?php if($vehicle['avarage_fuel_consumption_unit']==1){
              		echo "Km/l";
              	}
              	else{
              		echo "mil/l";
              	}
              	 ?>
              </td>
            </tr>
            <tr>
              <td><b>Number of Owners</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['number_of_owners'];?></td>
            </tr>
            <tr>
              <td><b>Fuel Type</b></td>
              <td><b> : </b></td>
              <td><?php if($vehicle['fuel_type']==1){
              		echo "Petrol";
              }
              else if($vehicle['fuel_type']==2){
              		echo "Diesel";
              }
              else if($vehicle['fuel_type']==3){
              		echo "Hybrid";
              }
              
              else{
              		echo "Electric";
              }
              ?></td>
            </tr>
            <tr>
              <td><b>Vehicle Total Value</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['vehicle_total_value'];?></td>
            </tr>
            <tr>
              <td><b>Vehicle Features</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['vehicle_features'];?></td>
            </tr>
            <tr>
              <td><b>Minimum Downpayment</b></td>
              <td><b> : </b></td>
              <td><?php echo $vehicle['vehicle_minimum_downpayment'];?></td>
            </tr>
            <tr>
              <td><b>Images</b></td>
              <?php foreach($inside_images as $key):?>
              <?php if($key['vehicle_vehicle_id']==$vehicle['vehicle_id']){?>
              <td></td>
              <td> <img src="<?php echo base_url(); ?>upload/selling/<?php echo $key['inside_images_name']; ?>" style="vertical-align:text-top;height: 61px;margin-top: 0px;margin-left: 3px;" ></td>
              <?php }?>
              <?php endforeach;?>
            </tr>
            <tr>
              <td></td>
            </tr>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<?php 
endforeach;
?>

<!-- follow up modal -->
<?php foreach($reservation as $res): 

    $res_id = $res['reservation_id'];
?>

  <div class="modal fade" id="followUpModal<?php echo $res['reservation_id'];?>" role="dialog">
  
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Follow Up</h4>
        </div>
        <div class="modal-body">
            <form role="form" action="<?php echo base_url(); ?>reservation/followUp1/" method="POST" enctype='multipart/form-data'>
                <!-- <?php echo ($data['inquiry_id']);?> -->
            <input type="hidden" name="res_id" id="res_id" value="<?php echo $res_id;?>"> 
            <div class="form-group">                     
              <label for="home_page_grid_description">Follow Up Reason</label>
              <textarea class="form-control" rows="3" id="Follow_up_description" name="Follow_up_description" placeholder="Enter Follow Up Reason" required></textarea>
            </div>
            <div class="form-group">                     
              <label for="home_page_grid_description">Next Follow Up Date</label>
              <input type="date" name="next_date" id="next_date" class="form-control" required>
            </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Create Follow Up</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      </form>
      
    </div>
  </div>
<?php 
endforeach;
?>
