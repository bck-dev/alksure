<style>
    .btn-is-disabled {
        pointer-events: none; /* Disables the button completely. Better than just cursor: default; */
    @include opacity(0.7);
    }
</style>
<!-- banner -->
<section class="banner" style="background-image: url('<?php echo base_url();?>assets/frontend/assets/images/banner/operate-lease-banner.jpg')"></section>
<!-- end of banner -->

<!-- Add vehicle downpayments to an array -->
<?php
$down_payment = "";
foreach ($vehicle as $vehi){
    $id = $vehi['vehicle_id'];
    $tot_Value = floor($vehi['vehicle_total_value']);
    $down_payment .= $tot_Value.',';
}
$arr = explode(",",$down_payment); ?>

<div class="page-content">
    <!-- operating lease search results section -->
    <section class="search-results floating-section operate-lease-search-results">
        <div class="container">
            <div class="results-container">
                <div class="column filter-column">
                    <a href="" class="refine-by">Refine by</a>

                    <div class="inner-filter-column">
                        <form action="" method="POST">

                            <select name="" id="opType">
                                <option value=0>Select Type</option>

                                <option value="" data-id="bb" id="all">All</option>

                                <?php foreach ($vehicletype as $type): ?>
                                    <option value="<?php echo $type['vehicle_type_id']; ?>" data-id="<?php echo $type['vehicle_type_id']; ?>"><?php echo $type['vehicle_type_name']; ?></option>

                                <?php endforeach;?>
                            </select>

                            <select name="" id="opMake" disabled>
                                <option value=0>Select Make</option>
                                <?php foreach($vehiclebrand as $brand): ?>
                                    <option value="<?php echo $brand['vehicle_brand_id']; ?>"
                                            class="<?php echo 'so'.$brand['vehicle_type_id'].'so'; ?>"
                                            data-id="<?php echo $brand['vehicle_brand_id']; ?>"><?php echo $brand['vehicle_brand_name']; ?></option>
                                <?php endforeach;?>

                            </select>


                            <select name="" id="opModel" disabled>
                                <option value=0>Select Model</option>
                                <?php foreach($vehiclemodel as $model): ?>
                                    <option value="<?php echo $model['vehicle_model_id']; ?>"
                                            class="<?php echo 'so'.$model['vehicle_brand_id'].'so'; ?>" data-id="<?php echo $model['vehicle_model_id'];?>"><?php echo $model['vehicle_model_name']; ?></option>

                                <?php endforeach;?>
                            </select>

                            <div class="bars">
                                <div class="inner-group">
                                    <h4>Down Payment</h4>
                                    <div class="bar">
                                        <input
                                                type="range"
                                                class="operate-lease-bar search-down-payment-bar filtrationSlider"
                                                min="1000000"
                                                max="<?php echo max($arr)/2; ?>"
                                                step="100000"
                                                value="0"
                                                id="operateLease">
                                        <div class="min-value">min</div>
                                        <div class="selected-value">Rs. <span class="amount">0</span></div>
                                        <div class="max-value">max</div>
                                    </div>
                                </div>
                                <div class="inner-group" style="display: none;">
                                    <h4>Monthly Installment</h4>
                                    <div class="bar">
                                        <input
                                                type="range"
                                                class="operate-lease-bar search-monthly-installment-bar filtrationSlider"
                                                min="10000"
                                                max="500000"
                                                step="5000"
                                                value="0"
                                                id="installment"
                                                disabled>
                                        <div class="min-value">min</div>
                                        <div class="selected-value">Rs. <span class="amount">0</span></div>
                                        <div class="max-value">max</div>
                                    </div>
                                </div>
                                <div class="inner-group">
                                    <h4>Duration</h4>
                                    <div class="bar">
                                        <input
                                                type="range"
                                                class="operate-lease-bar search-duration-bar filtrationSlider"
                                                min="1"
                                                max="7"
                                                step="1"
                                                value="0"
                                                id="duration">
                                        <div class="min-value">min</div>
                                        <div class="selected-value"><span class="months"></span> Years</div>
                                        <div class="max-value">max</div>
                                    </div>
                                </div>
                            </div>

                        </form>

                        <!--<div id="operating" class="compare-wrapper aa">
                        <div class="compare-vehicles">Vehicles Added: <span class="selected"></span>/5</div>
                        <a href="<?php echo base_url('home/comparison');?>" class="cta btn-green compare-btn" style="display: block !important; padding: 5px 15px !important; margin-top:0px !important; " >compare</a>
                    </div>-->

                        <div class="compare-wrapper  aa" id="operating" cursor: not-allowed;>
                            <div class="compare-vehicles">Vehicles Added: <span class="selected"></span>/5</div>
                            <a href="<?php echo base_url('home/comparisonoperating');?>" class="cta btn-green compare-btn btn-is-disabled" style=" background-color: gray !important; border-radius:0px !important;
 display: block !important; padding: 5px 15px !important; margin-top:0px !important; cursor: not-allowed;">compare</a>
                        </div>
                    </div>
                </div>
                <div class="column results-column">
                    <!-- <p class="results-count"><span id="resultCount"><?php echo count($vehicle);?></span><span id="vehicleNumber"> vehicles</span></p> -->

                    <div id="resultId">
                        <div class="results-wrap" style="margin-left: 30px;" id="results-wrap">
                        
                        	<?php $count = 0; ?>
	                        <?php foreach($vehicle as $vehi):?>
	                            <?php $count++; ?>
	                        <?php endforeach;?>
	
	                        <p class="results-count"><?php if ($count == 0) {
	                            echo 'No';
	                        } else {
	                            echo $count;
	                        } ?>&nbsp;Vehicles</p>
                        
                            <?php foreach($vehicle as $vehi): ?>

                                <?php

                                $id = $vehi['vehicle_id'];
                                $trans = $vehi['vehicle_transmission'];
                                $millge = $vehi['vehicle_mileage_range'];
                                $millage_unit = $vehi['millage_range_unit'];
                                // $p_d_price = $vehicle['per_day_price'];
                                $fuel_type = $vehi['fuel_type'];
                                // $disc = $vehicle['discount'];
                                $reg_year = $vehi['year_of_registration'];
                                $man_year = $vehi['year_of_manufacture'];
                                $capacity = $vehi['vehicle_seating_capacity'];
                                $total = $vehi['vehicle_total_value'];
                                $brand = $vehi['vehicle_brand_name'];
                                $model = $vehi['vehicle_model_name'];
                                $image = $vehi['vehicle_vehicle_id'];
                                $min_downpayment = $vehi['vehicle_minimum_downpayment'];
                                $publish_status = $vehi['publish_status'];
                                $reject_status = $vehi['reject_status'];
                                $autosure_certified = $vehi['autosure_certified'];
                                ?>
                                        
                                <?php if ($publish_status == 1 && $reject_status == 0) { ?>
                                <div class="result" id="<?php echo $id ?>" data-dpmax="<?php echo max($arr); ?>"data-mimax="<?php echo min(array_filter($arr)); ?>" style="display: flex !important; margin-bottom: 30px !important; padding-bottom: 30px !important; border-bottom: 1px solid #ebebeb !important;}">
                                    <figure>
                                        <div class="watermakerd">
                                            
                                            
                                        <img src="<?php echo base_url(); ?>upload/selling/Front/<?php echo $vehi['inside_360_images']; ?>" style="width:200px; height:150px;" alt="">
                                                    
                                        </div>
                                    </figure>
                                    <div class="right-side" style="display: block !important;">
                                        <p class="product-name"><?php echo $brand; ?> 
                                            <?php echo $model;?>
                                            <?php if($autosure_certified==1): ?>
                                                <img src="<?php echo base_url('assets/')?>certified.png" class="certified" alt=""/>
                                            <?php endif; ?>
                                        </p>
                                        <div class="price">
                                            <img src="<?php echo base_url('assets/frontend'); ?>/assets/images/misc/price-tag.png" alt="">
                                            <?php $installmetValue = ($total - 1000000)/12; ?>
                                            <?php if($installmetValue > 0): ?>
                                                <p><span class="installmentText">Rs. <?php echo number_format((int)$installmetValue);?></span></p>
                                            <?php else: ?>
                                                <p><span class="installmentText">Rs. 0</span></p>
                                            <?php endif;?>

                                            
                                        </div>

                                        <div class="other-amount-details">
                                            <p class="vehicle-down-payment">
                                                Down payment: <span><?php echo 'Rs. 1,000,000'; ?></span>
                                            </p>
                                            <p class="vehicle-duration">
                                                Duration: <span class="durationText">12</span><span>&nbsp;Months</span>
                                            </p>
                                        </div>

                                        <div class="main-features">
                                            <div class="feature">
                                                <div class="icon">
                                                    <img src="<?php echo base_url('assets/frontend'); ?>/assets/images/misc/calender.png" alt="">
                                                </div>
                                                <div class="info">
                                                    <p>Year</p>
                                                    <p><?php echo $reg_year; ?></p>
                                                </div>
                                            </div>
                                            <div class="feature">
                                                <div class="icon">
                                                    <img src="<?php echo base_url('assets/frontend'); ?>/assets/images/misc/dashboard.png" alt="">
                                                </div>
                                                <div class="info">
                                                    <p>Milage</p>
                                                    <p><?php echo $millge; ?> <?php if($millage_unit==1){
                                                            echo "Km";
                                                        }
                                                        else{
                                                            echo "mil";
                                                        }


                                                        ?>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="feature">
                                                <div class="icon">
                                                    <img src="<?php echo base_url('assets/frontend'); ?>/assets/images/misc/auto.png" alt="">
                                                </div>
                                                <div class="info">
                                                    <p>Transmission</p>
                                                    <p><?php if($trans==1){
                                                            echo "Automatic";
                                                        }
                                                        else if($trans==2){
                                                            echo "Manual";
                                                        }
                                                        else{
                                                            echo "Triptonic";
                                                        }

                                                        ?></p>
                                                </div>
                                            </div>
                                            <div class="feature">
                                                <div class="icon">
                                                    <img src="<?php echo base_url(); ?>assets/fuel.png" alt="">
                                                </div>
                                                <div class="info">
                                                    <p>Fuel Type</p>
                                                    <p><?php if($fuel_type==1){
                                                            echo "Petrol";
                                                        }
                                                        else if($fuel_type==2){
                                                            echo "Diesel";
                                                        }
                                                        else if($fuel_type==3){
                                                            echo "Hybrid";
                                                        }
                                                        else{
                                                            echo "Electric";
                                                        }

                                                        ?></p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="cta-group">
                                            <a href="<?php echo base_url();?>home/operating_result/<?php echo $vehi['vehicle_id']; ?>" class="cta btn-green">VIEW</a>
                                            <a href="" class="cta enquire checkresult open-popup" data-id="<?php echo $vehi['vehicle_id']; ?>">INQUIRE</a>
                                            <a href="" class="add-to-compare compare" value="<?php echo $vehi['vehicle_id']; ?>" data-hotel-name='<?php echo $vehi['vehicle_id']; ?>'>
                                                <span class="plus-icon"><i class="fa fa-plus"></i></span>
                                                <span class="text">compare</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            <?php endforeach; ?>
                        </div>
                    </div>

                    <!-- <div class="pagination-wrap">
                        <ul class="pagination">
                            <li class="prev"><a href="#"><</a></li>
                            <li class="active"><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li class="next"><a href="#">></a></li>
                        </ul>
                    </div> -->
                </div>
                <div class="column ads-column">
                    <?php foreach ($ad as $a){ ?>
                        <a href="<?php echo $a['link'] ?>" class="ads-block" target="_blank">
                            <img src="<?php echo base_url(); ?>upload/advertisement/<?php echo $a['ad_image_name']; ?>" >
                        </a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- end of operating lease search results -->



<script src="<?php echo base_url('assets/frontend');?>/assets/js/inquery.js"></script>
<!--<script src="<?php echo base_url('assets/frontendone/');?>js/lib.js"></script>-->
<!--<script src="<?php echo base_url('assets/frontendone/');?>js/main.js"></script>-->

<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>

<!-- enquire popup -->

<div class="popup enquire-popup" id="enquire-popup">
    <div class="popup-container">
        <div class="left-side">
            <form action="<?php echo base_url('home/operatingInquiry')?>" method="POST">
                <input type="text" name="fulname" placeholder="Full Name">
                <input type="email" name="email" placeholder="Email Address">
                <input type="text" name="phone" placeholder="Phone">
                <textarea name="comments" id="" placeholder="Comments"></textarea>
                <input type="hidden" id="resyltInqyry" class="resyltInqyry" name="vehicle_id" value="">
                <button type="submit">send inquiry</button>
            </form>
        </div>
        <div class="right-side">
            <h5>Call directly</h5>
            <p>Call one of our agents directly to reserve this vehicle or obtain more information.</p>

            <a href="tel:0766377666">(+94) 766 377 666</a>
            <p>Contact between 9.00am - 5.00pm on weekdays and except for poyadays.</p>
        </div>
        <div class="popup-close"><i class="fa fa-close"></i></div>
    </div>
</div>








</div>

</div>


<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>

<script type="text/javascript">

    var base_url ='http://autosure.lk/';
    var hotelName = [];
    var i =0;
    $(".compare").click(function(e) {

        e.preventDefault();
        hotelName.push( $(this).data('hotel-name') );

        var x =hotelName.toString();
        if(i>4){
            alert('only five vehicles can be selected');
        }else{
            i++;

            Cookies.set('yasas', x);
        }

        $.ajax({
            type: "POST",
            url: base_url + "home/divresultoperating",
            //dataType: "JSON",
            data: {i:i},
            success: function(result) {
                //alert(results);
                $('#operating').html(result);


            }

        });



    });

</script>