<script https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js></script>
<div class="results-wrap">
    <?php $count = 0; ?>
        <?php foreach($import as $im):?>
            <?php $count++; ?>
        <?php endforeach;?>

        <p class="results-count"><?php if ($count == 0) {
            echo 'No';
        } else {
            echo $count;
        } ?>&nbsp;Vehicles</p>

    <?php foreach($import as $im): 
        $vtype = $im['vehical_type_name'];   
        $vbrand = $im['vehicle_brand_name']; 
        $vmodal = $im['vehicle_model_name'];
        $id = $im['vehicle_id'];
        $trans = $im['vehicle_transmission'];
        $millge = $im['vehicle_mileage_range'];
        // $p_d_price = $vehicle['per_day_price'];
        $fuel_type = $im['fuel_type'];
        // $disc = $vehicle['discount'];
        $reg_year = $im['year_of_registration'];
        $man_year = $im['year_of_manufacture'];
        $capacity = $im['vehicle_seating_capacity'];
        $total = $im['vehicle_total_value'];
        $millage_unit = $im['millage_range_unit'];


        ?>
        <div class="result">
            <figure>
                <div class="watermakerd">
                                <img src="<?php echo base_url(); ?>upload/import/inside/<?php echo $im['import_inside_image']; ?>" style="width:230px; height:180px; ">
                            </div>
            </figure>
            <div class="right-side">
                <p class="product-name"><?php echo $vbrand; ?> <?php echo $vmodal;?></p>
                <div class="price">
                    <img src="<?php echo base_url();?>assets/frontendone/images/misc/price-tag.png" alt="">
                    <p>Rs. <?php echo number_format($total);?></p>
                </div>

                <div class="main-features">
                    <div class="feature">
                        <div class="icon">
                            <img src="<?php echo base_url();?>assets/frontendone/images/misc/calender.png" alt="">
                        </div>
                        <div class="info">
                            <p>Year</p>
                            <p><?php echo $man_year; ?></p>
                        </div>
                    </div>
                    <div class="feature">
                        <div class="icon">
                            <img src="<?php echo base_url();?>assets/frontendone/images/misc/dashboard.png" alt="">
                        </div>
                        <div class="info">
                            <p>Milage</p>
                            <p><?php echo $millge;?></p>
                            <p><?php if($millage_unit==1){
                                            echo "Km";
                                        } 
                                        else{
                                            echo "mil";
                                        }
                                        ?></p>
                        </div>
                    </div>
                    <div class="feature">
                        <div class="icon">
                            <img src="<?php echo base_url();?>assets/frontendone/images/misc/auto.png" alt="">
                        </div>
                        <div class="info">
                            <p>Transmission</p>
                            <p><?php if($trans==1){
                                echo "Automatic";
                            }
                            elseif($trans==2){
                                echo "Manual";
                            }
                            else{
                                echo"Triptonic";
                            }
                            ?>
                                
                            </p>
                        </div>
                    </div>
                    <div class="feature">
                        <div class="icon">
                            <img src="<?php echo base_url(); ?>assets/fuel.png" alt="">
                        </div>
                        <div class="info">
                            <p>Fuel Type</p>
                            <p><?php if($fuel_type==1){
                                echo "Petrol";
                            }
                            elseif($fuel_type==2){
                            echo 'Diesel';
                            }
                            elseif($fuel_type==3){
                                echo 'Hybrid';
                            }
                            else{
                                echo 'Electric';
                            }
                            ?></p>
                            
                        </div>
                    </div>
                </div>

                <div class="cta-group">
                    <a href="<?php echo base_url();?>home/importVehicle/<?php echo $im['vehicle_id']; ?>" class="cta btn-green">VIEW</a>
                    <a href="" class="cta enquire checkresult open-popup" data-id="<?php echo $im['vehicle_id']; ?>">INQUIRE</a>
                    <a href="" class="add-to-compare compare" value="<?php echo $im['vehicle_id']; ?>" data-hotel-name='<?php echo $im['vehicle_id']; ?>'>
                        <span class="plus-icon"><i class="fa fa-plus"></i></span>
                        <span class="text">compare</span>
                    </a>
                </div>
            </div>
        </div>
        <?php 
        // break;

        endforeach;
    // endforeach;
                ?>
    </div>
  


<script src="<?php echo base_url('assets/frontend/'); ?>assets/js/inquery.js"></script>
<!--<script src="<?php echo base_url('assets/frontendone/');?>js/lib.js"></script>-->
<!--<script src="<?php echo base_url('assets/frontendone/');?>js/main.js"></script>-->
<!-- inquiry form -->

<div class="popup enquire-popup" id="enquire-popup">
    <?php echo $im['vehicle_id'];?>
    <div class="popup-container">
        <div class="left-side">
            <form action="<?php echo base_url('home/importInquiry')?>" method="POST">
                <input type="text" name="name" placeholder="Full Name">
                <input type="email" name="address" placeholder="Email Address">
                <input type="text" name="phone" placeholder="Phone">
                <textarea name="comments" id="" placeholder="Comments"></textarea>
                <input type="hidden" id="resyltInqyry" class="resyltInqyry" name="im_vehicle_id" value="">
                <button type="submit">send inquiry</button>
            </form>
        </div>
        <div class="right-side">
            <h5>Call directly</h5>
            <p>Call one of our agents directly to reserve this vehicle or obtain more information.</p>

            <a href="tel:0766377666">(+94) 766 377 666</a>
            <p>Contact between 9.00am - 5.00pm on weekdays and except for poyadays.</p>
        </div>
        <div class="popup-close"><i class="fa fa-close"></i></div>
    </div>
</div>

<!-- end inquiry form -->

<script>
var base_url ='http://autosure.lk/'
var hotelName = []; 
 var i =0;
 $(".compare").click(function(e) {

     e.preventDefault(); 
      hotelName.push( $(this).data('hotel-name') );  
       
       var x =hotelName.toString();
       if(i>4){
        alert('only five vehicles can be selected');
       }else{
       i++;
    
       Cookies.set('yasas', x);
     }
        
   $.ajax({
            type: "POST",
            url: base_url + "home/divresultimport",
            //dataType: "JSON",
            data: {i:i},
            success: function(result) {
              //alert(results);
              $('#com').html(result);
           
                
            }

        });

        
      
   }); 

</script>