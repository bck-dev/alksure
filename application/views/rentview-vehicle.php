    <div class="page-content">
        <!-- search results -->
<section class="search-results view-vehicle">
    <div class="container">
        <div class="results-container">
            <div class="column test-column">
                <div class="test-drive">
                    <h4>Book a Test Drive</h4>

                    <div class="test-drive-info">
                        <div class="reservation-block">
                            <a href="tel:01123456263">(+94) 766 377 666</a>
                            <p>Call for reservations</p>
                        </div>

                        <p>We bring the vehicle to your door-step for inspections and for a test drive.</p>

                        <p class="note">*Conditions Apply</p>
                    </div>
                </div>
            </div>
           <?php foreach($Rvehicle as $vehicle):?>
                <?php foreach($rentingtype as $type): 
                    if($type['vehical_type_id']==$vehicle['vehical_vehical_type_id']):
                        $vtype = $type['vehical_type_name'];
                    endif;
                endforeach;
                   
                foreach($rentingbrand as $brand): 
                    if($brand['vehical_brand_id']==$vehicle['vehical_vehical_brand_id']):
                        $vbrand = $brand['vehical_brand_name'];
                    endif;
                endforeach;

                foreach($rentingmodal as $modal): 
                    if($modal['vehical_model_id']==$vehicle['vehical_vehical_model_id']):
                        $vmodal = $modal['vehical_model_name'];
                    endif;
                endforeach;

                $id = $vehicle['renting_vehical_id'];
                $trans = $vehicle['transmission'];
                $millge = $vehicle['millage_range'];
                $p_d_price = $vehicle['per_day_price'];
                $fuel_type = $vehicle['fuel_type'];
                $disc = $vehicle['discount'];
                $reg_year = $vehicle['year_of_registration'];
                $man_year = $vehicle['year_of_manufactured'];
                $capacity = $vehicle['engine_capacity'];
                $seating = $vehicle['seating_capacity'];
                $fuel_con = $vehicle['avarage_fuel_consumption'];
                $Owned = $vehicle['owned_by'];
                $feature = $vehicle['vehicle_features'];
                $average_of_fuel_consuption = $vehicle['avarage_fuel_consumption'];
               $image = $vehicle['renting_vehical_inside_image'];

                ?>
            <div class="column results-column">
                <!-- <?php if($this->uri->segment(4)==1){?>
                
<?php }else{?> -->
                <a href="<?php echo base_url('home/rent')?>" class="back-to"><i class="fa fa-angle-left"></i> Back to results</a>
                <!-- <?php }?> -->

                <div class="single-vehicle">
                    <p class="product-name"><?php echo $vbrand." ".$vmodal ;?></p>
                    <p class="ref">Veh/Rent/00<?php echo  $id;?></p>

                    <div class="details">
                        <div class="single-vehicle-group">
                            <div class="owl-carousel single-vehicle-slider" data-slider-id="1">
                                <div class="item" style="background-image: url(<?php echo base_url(); ?>upload/renting/inside/<?php echo $image; ?>)"></div>
                                <?php foreach ($images as $inside): ?>
                                 <?php if ($inside['renting_vehical_id'] == $vehicle['renting_vehical_id']): ?>
                                <div class="item" style="background-image: url(<?php echo base_url(); ?>upload/renting/outside/<?php echo $inside['renting_vehical_images']; ?>)"></div>
                               
                        <?php endif ?>
                    <?php endforeach ?>
                            </div>
                            
                            <div class="owl-thumbs" data-slider-id="1">
                                <button class="owl-thumb-item" style="background-image: url(<?php echo base_url(); ?>upload/renting/inside/<?php echo $image; ?>)"></button>
                               <?php foreach($images as $img): 
                            if($img['renting_vehical_id'] == $vehicle['renting_vehical_id'] ):
                            ?>
                                <button class="owl-thumb-item" style="background-image: url(<?php echo base_url(); ?>upload/renting/outside/<?php echo $img['renting_vehical_images']; ?>)"></button>
                                <?php endif ?>
                                   <?php endforeach ?>
                            </div>                            
                        </div>  

                        <div class="right-side">

                            <div class="main-features">
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url('assets/frontendone/')?>images/misc/calender.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Year</p>
                                        <p><?php echo $man_year;?></p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url('assets/frontendone/')?>images/misc/dashboard.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Milage</p>
                                        <p><?php if($vehicle['millage_range_unit']==1){
                                                    echo $vehicle['millage_range']. "Km";
                                                }
                                                else{
                                                    echo $vehicle['millage_range']. "Ml";
                                                }
                                        ?></p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url('assets/frontendone/')?>images/misc/auto.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Transmission</p>
                                        <p><?php if($trans == 1){
                                            echo "Automatic";
                                        }else if($trans== 2){
                                            echo "Manual";
                                        }
                                        else{
                                            echo "Triptonic";
                                        }

                                         ?></p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url();?>assets/frontendone/images/misc/auto.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Fuel Type</p>
                                        <p><?php if($fuel_type ==1){
                                            echo "Petrol";
                                        }else if($fuel_type == 2){
                                            echo "Diesel";
                                        }
                                        else if($fuel_type== 3){
                                            echo "Hybird";
                                        }
                                        else{
                                            echo "Electric";
                                        }
                                        ?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="price">Per day price
                                <img src="<?php echo base_url();?>assets/frontendone/images/misc/price-tag.png" alt="">
                                <!-- <p></p> -->
                                <p>Rs.<?php echo number_format($p_d_price).""?></p>
                            </div>
                            <?php if($difference!=0): ?>
                            <div class="price">Total
                                <img src="<?php echo base_url();?>assets/frontendone/images/misc/price-tag.png" alt="">
                                <p>Rs. <?php 
                                        $total = $p_d_price*$difference;
                                        echo $total;
                                    ?></p>
                            </div>
                            <?php endif;?>

                            <p class="note">Drive Away Price</p>

                            <div class="cta-group">
                                <a href="" class="cta enquire checkresult open-popup" data-id="<?php echo $vehicle['renting_vehical_id']; ?>">INQUIRE</a>
                                
                            </div>
                        </div>
                    </div>                    
                </div>
<!-- <?php if($sellers_description!=NULL): ?>
                <div class="description-wrap">
                    <h5>Seller's Description</h5>

                    <p><?php echo $sellers_description; ?></p>
                </div>
                <?php endif; ?> -->

                <div class="more-details">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#details" data-toggle="tab">Details</a></li>
                        <li><a href="#features" data-toggle="tab">Features</a></li>
                    </ul>
                    
                    <div class="tab-content">
                        <div id="details" class="tab-pane fade in active">
                            <div class="detail">
                                <div class="detail-name">Make</div>
                                <div class="detail-description"><?php echo $vbrand;?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Model</div>
                                <div class="detail-description"><?php echo $vmodal; ?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Transmission</div>
                                <div class="detail-description">
                                    <?php if($trans == 1){
                                            echo "Automatic";
                                        }else if($trans== 2){
                                            echo "Manual";
                                        }
                                        else{
                                            echo "Triptonic";
                                        }

                                         ?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Mileage Range</div>
                                <div class="detail-description">
                                    <?php if($vehicle['millage_range_unit']==1){
                                            echo $vehicle['millage_range']. "Km";
                                            }
                                        else{
                                            echo $vehicle['millage_range']. "mil";
                                        }
                                        ?>
                                </div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Engine Capacity</div>
                                <div class="detail-description">
                                     <?php if($vehicle['engine_capacity_unit']==1){ 
                                        echo $vehicle['engine_capacity']."CC";
                                    }
                                    else{
                                        echo $vehicle['engine_capacity']."kW";
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Seating Capacity</div>
                                <div class="detail-description"><?php echo $seating;?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Year of manufacture</div>
                                <div class="detail-description"><?php echo $man_year;?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Year of registration</div>
                                <div class="detail-description"><?php echo $reg_year;?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Fuel type</div>
                                <div class="detail-description">
                                     <?php if($fuel_type ==1){
                                            echo "Petrol";
                                        }else if($fuel_type == 2){
                                            echo "Diesel";
                                        }
                                        else if($fuel_type== 3){
                                            echo "Hybird";
                                        }
                                        else{
                                            echo "Electric";
                                        }
                                        ?>
                                </div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Average fuel consumption</div>
                                <div class="detail-description">
                                     <?php if($vehicle['avarage_fuel_consumption_unit']==1){
                                        echo $average_of_fuel_consuption."Km/l";
                                    }
                                    else{
                                        echo $average_of_fuel_consuption."mil/l";
                                    }

                                    ?>
                                </div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Owned By</div>
                                <div class="detail-description">
                                    <?php if($Owned ==0){
                                            echo "Company";
                                        }else{
                                            echo "Individual";
                                        }
                                    ?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Per Day Price</div>
                                
                                <div class="detail-description">Rs.<?php echo $p_d_price; ?></div>
                            
                            </div>
                            <!-- <div class="detail">
                                <div class="detail-name">Instalment</div>
                                <div class="detail-description">BMW X5</div>
                            </div> -->
                            <?php if($difference!=0): ?>
                            <div class="detail">
                                <div class="detail-name">Total Price</div>
                                <div class="detail-description">Rs.

                                    <?php $total = $p_d_price*$difference;
                                        echo $total; 
                                    ?>
                                </div>
                            </div>
                            <?php endif;?>
                            <!-- <div class="detail">
                                <div class="detail-name">Duration</div>
                                <div class="detail-description">BMW X5</div>
                            </div> -->
                        </div>
                        <div id="features" class="tab-pane fade">
                            <?php $count=0;?>
                        <?php $varr; $varr1;?>
                        <?php foreach ($Rvehicle as $value): ?>
                            <?php 
                           $vehicle_features1 = $value['vehicle_features'];
                           $additional_features = $value['additional_features'];

                             ?>
                        <?php
                        
                         $varr = explode(",",$vehicle_features1);
                         $varr1 = explode(",",$additional_features);
                        ?> 
                            <ul class="feature">
                                <?php foreach ($varr as $var): ?> 
                                 <?php if($count%4==0){?>
                                 </ul><ul class="feature">
                                    <?php }?>
                                <li><a href=""><?php print_r($var) ; ?></a></li>
                                <?php $count++; ?>
                                <?php endforeach ?>
                                
                                <?php if(!empty($varr1)) : ?>
                                
                                <?php foreach ($varr1 as $var1): ?>
                                
                                <?php if($var1 !== "") : ?>
                                
                                <?php if($count%4==0){?> 
                                    </ul><ul class="feature">
                                 <?php } ?>
                                  
                                <li><a href="">
                        <?php print_r ($var1);?></a></li><br>
                                
                               <?php endif; ?>
				<?php $count++; ?>
                                <?php endforeach ?>
                                <?php endif; ?>
                            </ul>

                        <?php break; ?>
                             <?php endforeach ?>   
                        </div>

                    </div>                    
                </div>
                <?php break; ?>
            <?php endforeach ?>
            </div>
            <div class="column ads-column">
                <a href="" class="ads-block">
                    <img src="<?php echo base_url('assets/frontendone')?>images/misc/ad.jpg" alt="">
                </a>                                
            </div>
        </div>        
    </div>
</section>
<!-- end of search results -->
    </div>



<!-- enquire popup -->

<!-- end of enquire popup -->

<!-- signup popup -->

<!-- end of signup popup -->

<!-- signin popup -->

<!-- end of register popup -->

<!-- lease popup -->

<script src="<?php echo base_url('assets/frontend/'); ?>assets/js/inquery.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/lib.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/main.js"></script>

<div class="popup enquire-popup" id="enquire-popup">
    <?php echo $id;?>
    <div class="popup-container">
        <div class="left-side">
            <form action="<?php echo base_url('home/rentingInquiry')?>" method="POST">
                <input type="text" name="fullname" placeholder="Full Name">
                <input type="email" name="address" placeholder="Email Address">
                <input type="text" name="phone" placeholder="Phone">
                <textarea name="comments" id="" placeholder="Comments"></textarea>
                <input type="hidden" id="resyltInqyry" class="resyltInqyry" name="vehicle_id" value="">
                <button type="submit">send inquiry</button>
            </form>
        </div>
        <div class="right-side">
            <h5>Call directly</h5>
            <p>Call one of our agents directly to reserve this vehicle or obtain more information.</p>

            <a href="tel:0766377666">(+94) 766 377 666</a>
            <p>Contact between 9.00am - 5.00pm on weekdays and except for poyadays.</p>
        </div>
        <div class="popup-close"><i class="fa fa-close"></i></div>
    </div>
</div>
<!-- end of lease popup -->
<script src="assets/js/lib.js"></script>
<script src="assets/js/main.js"></script>
<script src="assets/js/plugins/rangeslider.js"></script>
<script src="assets/js/plugins/tablesaw.jquery.js"></script>
<script src="assets/js/plugins/tablesaw-init.js"></script>
<script src="assets/js/plugins/jquery-ui.min.js"></script>
<script src="assets/js/plugins/jquery.validate.min.js"></script>
<script src="assets/js/plugins/wow.min.js"></script>
<script src="assets/js/script.js"></script>
