	<div class="page-content">
		<!-- search results -->
<section class="search-results view-vehicle">
    <div class="container">
        <div class="results-container">
            <div class="column test-column">
                <div class="test-drive">
                    <h4>Book a Test Drive</h4>

                    <div class="test-drive-info">
                        <div class="reservation-block">
                            <a href="tel:01123456263">112 3456 263</a>
                            <p>Call for reservations</p>
                        </div>

                        <p>We bring the vehicle to your door-step for inspections and for a test drive.</p>

                        <p class="note">*Conditions Apply</p>
                    </div>
                </div>
            </div>

            <?php foreach ($bike as $value): ?>

            <?php

            $threewheel_bike_id = $value['threewheel_bike_id'];
            $threewheel_bike_type_name = $value['threewheel_bike_type_name'];
            $threewheel_bike_brand_name = $value['threewheel_bike_brand_name'];
            $threewheel_bike_model_name = $value['threewheel_bike_model_name'];
            $transmission = $value['transmission'];
            $engine_capacity = $value['engine_capacity'];
            $Year_of_manufacture = $value['Year_of_manufacture'];
            $year_of_registration = $value['year_of_registration'];
            $fuel_type = $value['fuel_type'];
            $average_fuel_consuption = $value['average_fuel_consuption'];
            $total_price = $value['total_price'];
            $vehicle_total_value1 = (int)$total_price;
            $mileage_range = $value['mileage_range'];
            $features = $value['features'];
            $number_of_owners = $value['number_of_owners'];
            $minimum_downpayment = $value['minimum_downpayment'];
            $threewheel_bike_number = $value['threewheel_bike_number'];
            $vehicle_minimum_downpayment2 = (int)$minimum_downpayment;
            $seller_description = $value['seller_description'];
            ?>

            <div class="column results-column">
                <a href="<?php echo base_url('Home/bikeResults'); ?>" class="back-to"><i class="fa fa-angle-left"></i> Back to results</a>

                <div class="single-vehicle">
                    <p class="product-name"><?php echo $threewheel_bike_brand_name; ?> <?php echo $threewheel_bike_model_name; ?></p>
                    <p class="ref">Ref# 0735</p>

                    <div class="details">
                        <div class="single-vehicle-group">
                            <div class="owl-carousel single-vehicle-slider" data-slider-id="1">
                                <?php foreach ($image as $images): ?>
                                    <?php if ($images['threewheel_bike_id'] == $threewheel_bike_id): ?>
                                        <div class="item"
                                             style="background-image: url(<?php echo base_url(); ?>upload/3wheel/<?php echo $images['threewheel_bike_image_name']; ?>)"></div>

                                    <?php endif ?>
                                <?php endforeach ?>
                            </div>
                            
                            <div class="owl-thumbs" data-slider-id="1">
                                <?php foreach ($image as $images): ?>
                                    <?php if ($images['threewheel_bike_id'] == $threewheel_bike_id): ?>
                                        <button class="owl-thumb-item"
                                                style="background-image: url(<?php echo base_url(); ?>upload/3wheel/<?php echo $images['threewheel_bike_image_name']; ?>)"></button>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </div>                            
                        </div>  

                        <div class="right-side">

                            <div class="main-features">
                                <div class="feature">
                                    <div class="icon">
                                        <img src="assets/images/misc/calender.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Year</p>
                                        <p><?php echo $year_of_registration; ?></p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="assets/images/misc/dashboard.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Milage</p>
                                        <p><?php echo $mileage_range; ?>Km</p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="assets/images/misc/auto.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Transmission</p>
                                        <p><?php if($transmission==1){
                                    echo "Automatic";
                                    } elseif($transmission==2){
                                    echo "Manual";
                                    }
                                    else{
                                    echo "Triptonic";
                                    } ?></p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="assets/images/misc/auto.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Fuel Type</p>
                                        <p>
                                   
                                <?php if($fuel_type==1){
                                echo "Petrol";
                                }
                                elseif($fuel_type==2){
                                echo "Diesel";
                                }
                                else{
                                echo "Electric";
                                }  ?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="price">
                                <img src="assets/images/misc/price-tag.png" alt="">
                                <p><?php echo number_format($vehicle_total_value1) . ""  ?></p>
                            </div>

                            <div>
                                <img src="<?php echo base_url('assets/')?>drive-away.png" alt="">
                            </div>

                            <div class="cta-group">
                                <a href="" class="cta enquire open-popup checkresult" data-id="<?php echo $value['threewheel_bike_id']; ?>">INQUIRE</a>
                                <!-- <a href="" class="cta enquire lease">LEASE</a> -->
                            </div>
                        </div>
                    </div>                    
                </div>

                <div class="description-wrap">
                    <h5>Seller's Description</h5>

                    <p><?php echo $seller_description; ?></p>
                </div>

                <div class="more-details">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#details" data-toggle="tab">Details</a></li>
                        <li><a href="#features" data-toggle="tab">Features</a></li>
                    </ul>
                    
                    <div class="tab-content">
                        <div id="details" class="tab-pane fade in active">
                            <div class="detail">
                                <div class="detail-name">Make</div>
                                <div class="detail-description"><?php echo $threewheel_bike_brand_name; ?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Model</div>
                                <div class="detail-description"><?php echo $threewheel_bike_model_name; ?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Transmission</div>
                                <div class="detail-description"><?php if($transmission==1){
                                    echo "Automatic";
                                    } elseif($transmission==2){
                                    echo "Manual";
                                    }
                                    else{
                                    echo "Triptonic";
                                    } ?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Mileage Range</div>
                                <div class="detail-description"><?php echo $mileage_range; ?> Km</div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Engine Capacity</div>
                                <div class="detail-description"><?php echo $engine_capacity; ?> cc</div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Year of manufacture</div>
                                <div class="detail-description"><?php echo $Year_of_manufacture; ?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Year of registration</div>
                                <div class="detail-description"><?php echo $year_of_registration; ?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Fuel type</div>
                                <div class="detail-description"><?php if($fuel_type==1){
                                echo "Petrol";
                                }
                                elseif($fuel_type==2){
                                echo "Diesel";
                                }
                                else{
                                echo "Electric";
                                }  ?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Average fuel consumption</div>
                                <div class="detail-description"><?php echo $average_fuel_consuption; ?>Km/l</div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Number of Owners</div>
                                <div class="detail-description"><?php echo $number_of_owners; ?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Three wheel Number</div>
                                <div class="detail-description"><?php echo $threewheel_bike_number; ?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Total Value</div>
                                <div class="detail-description">Rs. <?php echo number_format($vehicle_total_value1) . "" ?></div>
                            </div>
                            <!-- <div class="detail">
                                <div class="detail-name">Instalment</div>
                                <div class="detail-description">Rs. <?php echo number_format($vehicle_minimum_downpayment2) . "" ?></div>
                            </div> -->
                            <div class="detail">
                                <div class="detail-name">Downpayment</div>
                                <div class="detail-description">Rs. <?php echo number_format($vehicle_minimum_downpayment2) . "" ?></div>
                            </div>
                            <!-- <div class="detail">
                                <div class="detail-name">Duration</div>
                                <div class="detail-description">BMW X5</div>
                            </div> -->
                        </div>
                       
                        
                        
                        <div id="features" class="tab-pane fade">
                             <?php $count=0;?>
                        <?php $varr; ?>
                        <?php foreach ($bike as $value): ?>
                            <?php 
                           $vehicle_features1 = $value['features'];
                       
                             ?>
                        

                        <?php
                        
                         $varr = explode(",",$vehicle_features1);
                        ?>
                        <ul class="feature">
                                <?php foreach ($varr as $var): ?>   
                            
                                <?php if($count%4==0){?> 
                                </ul><ul class="feature">
                                 <?php }?>
                                <li><a href=""><?php print_r($var); ?></a></li><br>
                                
                               
    <?php $count++; ?>
                                <?php endforeach ?>
                            </ul>
                            <?php break; ?>
                             <?php endforeach ?>
                        </div>
                        
                        
                    </div>                    
                </div>
            </div>
            <?php break; ?>
                    <?php endforeach ?>
            <div class="column ads-column">
                <a href="" class="ads-block">
                    <img src="assets/images/misc/ad.jpg" alt="">
                </a>                                
            </div>
        </div>        
    </div>
</section>
<!-- end of search results -->
	</div>


<!-- enquire popup -->

<div class="popup enquire-popup" id="enquire-popup">
    <?php echo $value['threewheel_bike_id'];?>
    <div class="popup-container">
        <div class="left-side">
            <form action="<?php echo base_url('home/inserttrewheeliquery')?>" method="POST">
                <input type="text" placeholder="Full Name" name="fulname">
                <input type="email" placeholder="Email Address" name="email">
                <input type="text" placeholder="Phone" name="phone">
                <textarea name="comments" id="" placeholder="Comments" name=""></textarea>
                <input type="hidden" id="resyltInqyry" class="resyltInqyry" name="vehicle_id" value="">
                <button type="submit">send enquiry</button>
            </form>
        </div>
        <div class="right-side">
            <h5>Call directly</h5>
            <p>Call one of our agents directly to reserve this vehicle or obtain more information.</p>

            <a href="tel:1123456263">(+94) 112 3456 263</a>
            <p>Contact between 9.00am - 5.00pm on weekdays and except for poyadays.</p>
        </div>
        <div class="popup-close"><i class="fa fa-close"></i></div>
    </div>
</div>

<!-- end of enquire popup -->

<!-- lease popup -->
<div class="popup signup-popup lease-popup" id="lease-popup">
    <div class="popup-container">
        <div class="popup-title-wrap">
            <h3>Lease</h3>
        </div>
        <div class="left-side">
            <form action="" method="POST">
                <div class="multiple-radio">
                    <div class="radio-group">
                        <input type="radio" name="lease-type" id="autosure-lease" checked>
                        <label for="autosure-lease">Autosure</label>
                    </div>
                    <div class="radio-group">
                        <input type="radio" name="lease-type" id="other-lease">
                        <label for="other-lease">Other</label>
                    </div>                    
                </div>                
                <div class="input-group">
                    <input type="text" name="name" id="down-payment" placeholder="Down payment">
                </div>
                <div class="input-group">
                    <input type="text" name="duration" id="duration" placeholder="Duration">
                </div>
                <div class="input-group">
                    <input type="text" name="interest" id="interest" placeholder="Interest rate">
                </div>
                <button type="submit">Calculate</button>
            </form>
        </div>
        <div class="popup-close"><i class="fa fa-close"></i></div>
    </div>
</div>
