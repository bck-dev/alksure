<body>

<div class="da-da AppMainBody">
    <div class="da-da wh100">
        <div class="da-da wh100">
            
            <!--body starts-->
            <div class="da-da AppMainBodyHl">
                <div class="da-da wh100">
                    <div class="da-da wh100">
                    
                        
                   
                                   
                                                  
                                                   
                                    
                                    
                                    </div>
                                </div>
                            </div>
                            <!--bottom menu-->
                                
                                
                           
                                    
                 
                        
                        
                    <!--container-->
                    <div class="da-da container">
                        
                   
                    
                    <!--header-->
                    <div class="da-da DifFontHeader maxwidth980">
                        <div class="da-da wh100">
                            <div class="da-da wh100">
                            <h2 class="da-da DifFontHeaderH3" style="text-align: left;font-size:18px;">Privacy Policy</h2>
                            <p class="da-da DifFontHeaderP" font-size:12px; ><br>
                              This Policy document covers the information given by any person or entity through
the corporate website of Alliance Finance Co PLC. The company ensures that your
privacy is protected in all means and no data collected from this website which can
be used to identify yourself will be handed over to a third party for commercial
purpose.<br></p>
 <p class="da-da DifFontHeaderH3" style="text-align: left;  font-size:15px;">1. What information do we collect?</p>
 <p style="font-size:12px;">
We collect information from you when you fill out a form.<br>
When ordering or registering on our site, as appropriate, you may be asked to
enter your: name, e-mail address, mailing address, phone number or National
Identity Card Number.<br></p>
 <p class="da-da DifFontHeaderH3"  style="text-align: left;font-size:15px;">2. What do we use your information for?</p>
<p style="font-size:12px;">
Any of the information we collect from you may be used in one of the<br>
following ways:<br></p>
<p style="margin-left: 20px; font-size:12px;">
• To personalize your experience
(your information helps us to better respond to your individual needs)<br>
• To improve our website
(we continually strive to improve our website offerings based on the
information and feedback we receive from you)<br>
• To improve customer service
(your information helps us to more effectively respond to your
customer service requests and support needs)<br>
• To process transactions
Your information, whether public or private, will not be sold,
exchanged, transferred, or given to any other company for any reason
whatsoever, without your consent, other than for the express purpose
of delivering the purchased product or service requested.<br></p>

<p class="da-da DifFontHeaderH3"  style="text-align: left;left;font-size:15px;">3. How do we protect your information?</p>
<p style="font-size:12px;">
We implement a variety of security measures to maintain the safety of your
personal information when you enter, submit, or access your personal
information.<br></p>

<p class="da-da DifFontHeaderH3"  style="text-align: left; left;font-size:15px;">5. Third party links?</p>
<p style="font-size:12px;">
Occasionally, at our discretion, we may include or offer third party products
or services on our website. These third-party sites have separate and
independent privacy policies. We therefore have no responsibility or liability
for the content and activities of these linked sites. Nonetheless, we seek to
protect the integrity of our site and welcome any feedback about these sites.<br></p>

<p class="da-da DifFontHeaderH3"  style="text-align: left;font-size:15px;">6. Your consent?</p>
<p style="font-size:12px;">
By using our site, you consent to our websites privacy policy</p>

                                </p>
                              
                            </div>
                        </div>
                    </div>
                    <!--header-->
                        
                        
                     </div>
                    <!--container-->
                        
                        
                        
           
                            
                          
                        
                       
                  
                        
                        
                    </div>
                </div>
            </div>
            <!--body starts-->
            
        </div>
    </div>
</div>
    
    
</body>
</html>