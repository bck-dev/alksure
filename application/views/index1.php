<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>


    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <link href="<?php echo base_url('assets/frontend/') ?>css/main_10112018.css" type="text/css" rel="stylesheet"/>
    <link href="<?php echo base_url('assets/frontend/') ?>css/main_320px.css" type="text/css" rel="stylesheet"/>
    <link href="<?php echo base_url('assets/frontend/') ?>css/main_480px.css" type="text/css" rel="stylesheet"/>
    <link href="<?php echo base_url('assets/frontend/') ?>css/main_720px.css" type="text/css" rel="stylesheet"/>
    <link href="<?php echo base_url('assets/frontend/') ?>css/main_1024px.css" type="text/css" rel="stylesheet"/>


    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
          integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
            integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
            crossorigin="anonymous"></script>
    <script>
        $('.carousel').carousel();
    </script>
    <title>Home - Auto Sure</title>
</head>

<body>

<div class="da-da AppMainBody">
    <div class="da-da wh100">
        <div class="da-da wh100">

            <!--body starts-->
            <div class="da-da AppMainBodyHl">
                <div class="da-da wh100">
                    <div class="da-da wh100">


                        <!--top header-->
                        <div class="da-da AppMainHeaderBox">
                            <div class="da-da wh100">
                                <div class="da-da wh100">
                                    <!--top layer-->
                                    <div class="da-da AppMainHeaderBox-top">
                                        <div class="da-da wh100">
                                            <div class="da-da wh100">
                                                <!--container-->
                                                <div class="da-da container">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="da-da wh100">
                                                                <div class="da-da wh100">
                                                                    <!--sign in and sign up options-->
                                                                    <div class="da-da AppMainHeaderBox-top-sinout">
                                                                        <div class="da-da wh100">
                                                                            <div class="da-da wh100 displayFlex">
                                                                                <ul class="da-da wh100 AppMainHeaderBox-top-sinoutUl">
                                                                                    <li>
                                                                        <span>
                                                                        <a href="#">
                                                                        Register    
                                                                        </a>
                                                                        </span>
                                                                                    </li>
                                                                                    <li>
                                                                        <span>
                                                                        <a href="#" class="da-da IsSignin">
                                                                        Sign In
                                                                        </a>
                                                                        </span>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--sign in and sign up options-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--container-->
                                            </div>
                                        </div>
                                    </div>
                                    <!--top layer-->

                                    <!--bottom menu-->
                                    <div class="da-da AppMainHeaderBox-bottom">
                                        <div class="da-da wh100">
                                            <div class="da-da wh100">

                                                <!--container-->
                                                <div class="da-da container">
                                                    <div class="da-da wh100">
                                                        <div class="da-da wh100">

                                                            <div class="row">
                                                                <!--left-->
                                                                <div class="da-da col-md-5">
                                                                    <div class="da-da wh100">
                                                                        <div class="da-da wh100">
                                                                            <ul class="da-da wh100 mainMenuBarsApp_navigation">
                                                                                <li>
                                                                                    <a href="#">
                                                                            <span>
                                                                            trade-in
                                                                            </span>
                                                                                    </a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="#">
                                                                            <span>
                                                                            Operating Lease
                                                                            </span>
                                                                                    </a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="#">
                                                                            <span>
                                                                            Rent
                                                                            </span>
                                                                                    </a>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--left-->

                                                                <!--center-->
                                                                <div class="da-da col-md-2">
                                                                    <div class="da-da wh100">
                                                                        <div class="da-da wh100">
                                                                            <a href="">
                                                                                <img src="<?php echo base_url('assets/frontend/') ?>assets/logo/brand-logo.png"
                                                                                     class="da-da mainLogo">
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--center-->

                                                                <!--riht-->
                                                                <div class="da-da col-md-5">
                                                                    <div class="da-da wh100">
                                                                        <div class="da-da wh100">
                                                                            <ul class="da-da wh100 mainMenuBarsApp_navigation">
                                                                                <li>
                                                                                    <a href="#">
                                                                            <span>
                                                                            Importing
                                                                            </span>
                                                                                    </a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="#">
                                                                            <span>
                                                                            Services
                                                                            </span>
                                                                                    </a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="#">
                                                                            <span>
                                                                            Contact us
                                                                            </span>
                                                                                    </a>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--riht-->
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!--container-->


                                            </div>
                                        </div>
                                    </div>
                                    <!--bottom menu-->


                                    <!--mobile menu-->
                                    <div class="da-da AppMainHeaderBox-MobileNav">
                                        <div class="da-da wh100">
                                            <div class="da-da wh100 displayFlex">
                                                <!--left logo-->
                                                <div class="da-da AppMainHeaderBox-MobileNavLogo">
                                                    <div class="da-da wh100">
                                                        <div class="da-da wh100">
                                                            <img src="<?php echo base_url('assets/frontend/') ?>assets/logo/brand-logo.png">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--left logo-->

                                                <!--right menu icon-->
                                                <div class="da-da AppMainHeaderBox-MobileNavIcon">
                                                    <div class="da-da wh100">
                                                        <div class="da-da wh100">
                                                            <nav class="navbar navbar-expand-lg navbar-light bg-light">

                                                                <button class="navbar-toggler" type="button"
                                                                        data-toggle="collapse"
                                                                        data-target="#navbarSupportedContent"
                                                                        aria-controls="navbarSupportedContent"
                                                                        aria-expanded="false"
                                                                        aria-label="Toggle navigation">
                                                                    <span class="navbar-toggler-icon"></span>
                                                                </button>

                                                                <div class="collapse navbar-collapse"
                                                                     id="navbarSupportedContent">
                                                                    <ul class="navbar-nav mr-auto">
                                                                        <li class="nav-item active">
                                                                            <a class="nav-link" href="#">Home <span
                                                                                        class="sr-only">(current)</span></a>
                                                                        </li>
                                                                        <li class="nav-item">
                                                                            <a class="nav-link" href="#">Link</a>
                                                                        </li>
                                                                        <li class="nav-item dropdown">
                                                                            <a class="nav-link dropdown-toggle" href="#"
                                                                               id="navbarDropdown" role="button"
                                                                               data-toggle="dropdown"
                                                                               aria-haspopup="true"
                                                                               aria-expanded="false">
                                                                                Dropdown
                                                                            </a>
                                                                            <div class="dropdown-menu"
                                                                                 aria-labelledby="navbarDropdown">
                                                                                <a class="dropdown-item"
                                                                                   href="#">Action</a>
                                                                                <a class="dropdown-item" href="#">Another
                                                                                    action</a>
                                                                                <div class="dropdown-divider"></div>
                                                                                <a class="dropdown-item" href="#">Something
                                                                                    else here</a>
                                                                            </div>
                                                                        </li>

                                                                    </ul>

                                                                </div>
                                                            </nav>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--right menu icon-->
                                            </div>
                                        </div>
                                    </div>
                                    <!--mobile menu-->
                                </div>
                            </div>
                        </div>
                        <!--top header-->


                        <!--banner top-->
                        <div class="da-da BannerSearchTopIndex">
                            <div class="da-da wh100">
                                <div class="da-da wh100">

                                    <!--position relative-->
                                    <div class="da-da BannerSearchTopIndexRel">
                                        <div class="da-da wh100">
                                            <div class="da-da wh100">


                                                <!--banner image-->
                                                <div class="da-da BannerSearchTopIndexRel-banner">
                                                    <div class="da-da wh100">
                                                        <div class="da-da wh100">
                                                            <!--banner starts-->

                                                            <div id="carouselExampleControls" class="carousel slide"
                                                                 data-ride="carousel">

                                                                <div class="carousel-inner">
                                                                    <?php $i = 0 ?>
                                                                    <?php foreach ($slider

                                                                    as $slid): ?>
                                                                    <?php if ($i == 0){ ?>
                                                                    <div class="carousel-item active">
                                                                        <?php }else{ ?>
                                                                        <div class="carousel-item ">
                                                                            <?php } ?>
                                                                            <img class="d-block w-100"
                                                                                 src="<?php echo base_url() ?>upload/slider/<?php echo $slid['image']; ?>"
                                                                                 alt="First slide">

                                                                        </div>
                                                                        <?php $i++; ?>
                                                                        <?php endforeach; ?>
                                                                    </div>


                                                                    <a class="carousel-control-prev"
                                                                       href="#carouselExampleControls" role="button"
                                                                       data-slide="prev">
                                                                    <span class="carousel-control-prev-icon"
                                                                          aria-hidden="true"></span>
                                                                        <span class="sr-only">Previous</span>
                                                                    </a>
                                                                    <a class="carousel-control-next"
                                                                       href="#carouselExampleControls" role="button"
                                                                       data-slide="next">
                                                                    <span class="carousel-control-next-icon"
                                                                          aria-hidden="true"></span>
                                                                        <span class="sr-only">Next</span>
                                                                    </a>

                                                                </div>

                                                                <!--banner starts-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--banner image-->

                                                    <!--search option menu-->
                                                    <div class="da-da SearchConfigurationIndex">
                                                        <div class="da-da wh100">
                                                            <div class="da-da wh100 SearchConfigurationIndexBg">
                                                                <!--container-->
                                                                <div class="da-da container h100">
                                                                    <div class="da-da wh100">
                                                                        <div class="da-da wh100 SearchConfigurationIndexSpanvhmdDv">
                                                                            <div class="da-da wh100 SearchConfigurationIndexSpanvhmd maxwidth780">

                                                                                <!--title-->
                                                                                <div class="da-da DefaultTitleOnindexbanner">
                                                                                    <div class="da-da wh100">
                                                                                        <div class="da-da wh100">
                                                                                            <h3>Buying a car ?</h3>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!--title-->

                                                                                <!--search bar-->
                                                                                <div class="da-da searchBarIndex">
                                                                                    <div class="da-da wh100">
                                                                                        <div class="da-da wh100 displayFlex">
                                                                                            <input type="text"
                                                                                                   placeholder="I am looking for.."
                                                                                                   class="da-da InputFieldAppIndex isseachInputFieldAppIndex">
                                                                                            <input type="button"
                                                                                                   class="da-da InputFieldAppIndexBtn IssearchInputFieldAppIndexBtn"
                                                                                                   value="Search">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!--search bar-->

                                                                                <!--title-->
                                                                                <div class="da-da DefaultTitleOnindexbanner DefaultTitleOnindexbanner2nd">
                                                                                    <div class="da-da wh100">
                                                                                        <div class="da-da wh100">
                                                                                            <h3>Browse by</h3>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!--title-->

                                                                                <!--filter options-->
                                                                                <div class="da-da searchformFilerIndex">
                                                                                    <div class="da-da wh100">
                                                                                        <div class="da-da wh100">
                                                                                            <ul class="da-da inputFieldIsonFormUlMain">
                                                                                                <li>
                                                                                                    <select class="da-da wh100 inputFieldIsonForm">
                                                                                                        <option>Type
                                                                                                        </option>
                                                                                                    </select>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <select class="da-da wh100 inputFieldIsonForm">
                                                                                                        <option>Make
                                                                                                        </option>
                                                                                                    </select>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <select class="da-da wh100 inputFieldIsonForm">
                                                                                                        <option>Model
                                                                                                        </option>
                                                                                                    </select>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!--filter options-->


                                                                                <!--title-->
                                                                                <div class="da-da DefaultTitleOnindexbanner">
                                                                                    <div class="da-da wh100">
                                                                                        <div class="da-da wh100">
                                                                                            <h3>Selling a car? <a
                                                                                                        href="#"
                                                                                                        class="da-da BtnIntheIndexSelACar">Sell
                                                                                                    a car</a></h3>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!--title-->

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--container-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--search option menu-->

                                                </div>
                                            </div>
                                        </div>
                                        <!--position relative-->

                                    </div>
                                </div>
                            </div>
                            <!--banner top-->


                            <!--category 5 types-->
                            <div class="da-da container">
                                <div class="da-da wh100">
                                    <div class="da-da wh00 positionrelative">
                                        <div class="da-da categoryList5OnIndex">
                                            <div class="da-da wh100">
                                                <div class="da-da wh100">
                                                    <!--content-->
                                                    <div class="da-da maxwidth980 categoryList5OnIndexConTnt">
                                                        <div class="da-da wh100">

                                                            <!--box 5-->
                                                            <ul class="da-da categoryList5OnIndexConTntUl">

                                                                <?php foreach ($grid as $data): ?>
                                                                    <!--one category-->
                                                                    <li>
                                                                        <div class="paddingadjustedforCagry">
                                                                            <!--image-->
                                                                            <div class="da-da categoryList5OnIndexConTntImg">
                                                                                <img src="<?php echo base_url() ?>upload/grid/<?php echo $data['image']; ?>">
                                                                            </div>
                                                                            <!--image-->
                                                                            <!--title-->
                                                                            <div class="da-da categoryList5OnIndexConTntTitle">
                                                                                <h4>
                                                                                    <?php echo $data['title']; ?>
                                                                                </h4>
                                                                            </div>
                                                                            <!--title-->
                                                                            <!--description-->
                                                                            <div class="da-da categoryList5OnIndexConTntdescrptn">
                                                                                <p>
                                                                                    <?php echo $data['description']; ?>
                                                                                </p>
                                                                            </div>
                                                                            <!--description-->

                                                                            <!--button-->
                                                                            <div class="da-da categoryList5OnIndexConTntdescrptn">
                                                                                <a href="<?php echo $data['link']; ?>">Sell/Buy
                                                                                    <i
                                                                                            class="fas fa-caret-right"></i></a>
                                                                            </div>
                                                                            <!--button-->
                                                                        </div>
                                                                    </li>
                                                                    <!--one category-->
                                                                <?php endforeach; ?>
                                                            </ul>
                                                            <!--box 5-->

                                                        </div>
                                                    </div>
                                                    <!--content-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--category 5 types-->


                            <!--container-->
                            <div class="da-da container">


                                <!--header-->
                                <div class="da-da DifFontHeader maxwidth980">
                                    <div class="da-da wh100">
                                        <div class="da-da wh100">
                                            <h3 class="da-da DifFontHeaderH3">Where your deam come true</h3>
                                            <p class="da-da DifFontHeaderP">
                                                The first online automobile mall in Sri-Lanka. Best choice for buying
                                                and
                                                selling cars, vans, bikes, trucks, SUVs, vehicle parts and to be updated
                                                with the latest news in Sri-Lanka's Car market.
                                            </p>
                                            <a href="#" class="da-da DifFontHeaderBtn">About Us</a>
                                        </div>
                                    </div>
                                </div>
                                <!--header-->


                            </div>
                            <!--container-->


                            <!--products list-->
                            <div class="da-da container">
                                <div class="da-da wh100">
                                    <div class="da-da wh100" style="
    overflow: auto;
">

                                        <!--products starts-->
                                        <div class="da-da IndexProductsListsSatsrs maxwidth980">
                                            <div class="da-da wh100">
                                                <div class="da-da wh100 displayFlex" style="
    overflow: auto;
    padding-top: 10px;    background: #f1f1f1;
">


                                                    <?php foreach ($vehicle as $value): ?>

                                                    <?php

                                                    $year_of_manufacture = $value['year_of_manufacture'];
                                                    $vehicle_transmission = $value['vehicle_transmission'];
                                                    $vehicle_mileage_range = $value['vehicle_mileage_range'];
                                                    $vehicle_total_value = $value['vehicle_total_value'];
                                                    $vehicle_total_value1 = (int)$vehicle_total_value;
                                                    $vehicle_status =$value['vehicle_status'];
                                                    ?>

                                                    <!--one product-->
                                                    <?php if($vehicle_status == 1) { ?>
                                                    <div class="da-da one-products-homepage brandNewSign">
                                                        <?php }else{ ?>
                                                        <div class="da-da one-products-homepage">
                                                            <?php } ?>
                                                        <div class="da-da wh100">
                                                            <div class="da-da wh100">
                                                                <!--image-->
                                                                <div class="da-da one-products-homepageImage">
                                                                    <img src="<?php echo base_url() ?>upload/vehicle/inside/<?php echo $value['inside_images_name']; ?>">
                                                                </div>
                                                                <!--image-->

                                                                <!--!info tag-->
                                                                <div class="da-da one-products-homepageinftag">
                                                                    <span><img src="<?php echo base_url('assets/frontend/') ?>assets/index-products-ico/calender.png"> </span>
                                                                    <?php echo $year_of_manufacture; ?>
                                                                </div>
                                                                <!--!info tag-->

                                                                <!--!info tag-->
                                                                <div class="da-da one-products-homepageinftag">
                                                                    <span><img src="<?php echo base_url('assets/frontend/') ?>assets/index-products-ico/dashboard.png"> </span>
                                                                    <?php echo $vehicle_mileage_range; ?> Km
                                                                </div>
                                                                <!--!info tag-->

                                                                <!--!info tag-->
                                                                <div class="da-da one-products-homepageinftag">
                                                                    <span><img src="<?php echo base_url('assets/frontend/') ?>assets/index-products-ico/auto.png"> </span>
                                                                    <?php echo $vehicle_transmission; ?>
                                                                </div>
                                                                <!--!info tag-->

                                                                <!--!info tag-->
                                                                <div class="da-da one-products-homepageinftag Isprice334">
                                                                    <span><img src="<?php echo base_url('assets/frontend/') ?>assets/index-products-ico/label.png"> </span>
                                                                    <?php echo number_format($vehicle_total_value1)."";?>
                                                                </div>
                                                                <!--!info tag-->

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <?php endforeach; ?>
                                                    <!--one product-->

                                                    <!--one product-->

                                                    <!--one product-->

                                                </div>
                                            </div>
                                        </div>
                                        <!--products starts-->


                                    </div>
                                </div>
                            </div>
                            <!--products list-->


                            <!--sign up banner-->
                            <div class="da-da SignUpGreen">
                                <div class="da-da wh100">
                                    <div class="da-da container">
                                        <div class="da-da  maxwidth980">
                                            <div class="row">

                                                <div class="col-md-8 displayFlex">
                                                    <div class="da-da SignUpGreenLeftSide">
                                                        <img src="<?php echo base_url('assets/frontend/') ?>assets/sign-up-area/sign-up.png">
                                                    </div>
                                                    <div class="da-da SignUpGreenRightSide">
                                                        <h3>SIGN UP FOR UPATES</h3>
                                                        <p>Will update you with all our exciting new offers as we
                                                            introduce
                                                            them</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 SignUpGreenTxtdtcr">
                                                    <a href="" class="cta btn-green">SIGN UP</a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--sign up banner-->


                            <!--contaner-->
                            <div class="da-da container">
                                <!--footer-->
                                <div class="da-da FooterMain maxwidth980">
                                    <div class="da-da wh100">
                                        <div class="da-da wh100">
                                            <div class="row">
                                                <!--first footer-->
                                                <div class="col-md-2">
                                                    <div class="da-da wh100">
                                                        <div class="da-da wh00">
                                                            <!--footer title-->
                                                            <div class="da-da FooterTitleMain">
                                                                <h5>
                                                                    Cars for sale
                                                                </h5>
                                                            </div>
                                                            <!--footer title-->

                                                            <!--one link-->
                                                            <div class="FooterOneLinkBox">
                                                                <div class="da-da wh100">
                                                                    <a href="#">
                                                                        Pre-owned cars
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <!--one link-->

                                                            <!--one link-->
                                                            <div class="FooterOneLinkBox">
                                                                <div class="da-da wh100">
                                                                    <a href="#">
                                                                        Brand new
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <!--one link-->

                                                            <!--one link-->
                                                            <div class="FooterOneLinkBox">
                                                                <div class="da-da wh100">
                                                                    <a href="#">
                                                                        Important
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <!--one link-->


                                                        </div>
                                                    </div>
                                                </div>
                                                <!--first footer-->

                                                <!--second footer-->
                                                <div class="col-md-2">
                                                    <div class="da-da wh100">
                                                        <div class="da-da wh00">
                                                            <!--footer title-->
                                                            <div class="da-da FooterTitleMain">
                                                                <h5>
                                                                    Cars for sale
                                                                </h5>
                                                            </div>
                                                            <!--footer title-->

                                                            <!--one link-->
                                                            <div class="FooterOneLinkBox">
                                                                <div class="da-da wh100">
                                                                    <a href="#">
                                                                        Pre-owned cars
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <!--one link-->

                                                            <!--one link-->
                                                            <div class="FooterOneLinkBox">
                                                                <div class="da-da wh100">
                                                                    <a href="#">
                                                                        Brand new
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <!--one link-->

                                                            <!--one link-->
                                                            <div class="FooterOneLinkBox">
                                                                <div class="da-da wh100">
                                                                    <a href="#">
                                                                        Important
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <!--one link-->


                                                        </div>
                                                    </div>
                                                </div>
                                                <!--second footer-->

                                                <!--third footer-->
                                                <div class="col-md-2">
                                                    <div class="da-da wh100">
                                                        <div class="da-da wh00">
                                                            <!--footer title-->
                                                            <div class="da-da FooterTitleMain">
                                                                <h5>
                                                                    Cars for sale
                                                                </h5>
                                                            </div>
                                                            <!--footer title-->

                                                            <!--one link-->
                                                            <div class="FooterOneLinkBox">
                                                                <div class="da-da wh100">
                                                                    <a href="#">
                                                                        Pre-owned cars
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <!--one link-->

                                                            <!--one link-->
                                                            <div class="FooterOneLinkBox">
                                                                <div class="da-da wh100">
                                                                    <a href="#">
                                                                        Brand new
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <!--one link-->

                                                            <!--one link-->
                                                            <div class="FooterOneLinkBox">
                                                                <div class="da-da wh100">
                                                                    <a href="#">
                                                                        Important
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <!--one link-->


                                                        </div>
                                                    </div>
                                                </div>
                                                <!--third footer-->

                                                <!--fourth footer-->
                                                <div class="col-md-6 FooterTitleMainRightBigger">
                                                    <div class="da-da wh100">
                                                        <div class="da-da wh00">
                                                            <!--footer title-->
                                                            <div class="da-da FooterTitleMain">
                                                                <h5>

                                                                </h5>
                                                            </div>
                                                            <!--footer title-->

                                                            <!--one link-->
                                                            <div class="FooterOneLinkBox">
                                                                <div class="da-da wh100">
                                                                    <a href="#">
                                                                        Terms & Conditions
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <!--one link-->

                                                            <!--one link-->
                                                            <div class="FooterOneLinkBox">
                                                                <div class="da-da wh100">
                                                                    <a href="#">
                                                                        Privacy
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <!--one link-->

                                                            <!--one link-->
                                                            <div class="FooterOneLinkBox">
                                                                <div class="da-da wh100">
                                                                    <a href="#">
                                                                        Shareholders
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <!--one link-->


                                                        </div>
                                                    </div>
                                                </div>
                                                <!--fourth footer-->

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--footer-->
                            </div>
                            <!--contaner-->


                        </div>
                    </div>
                </div>
                <!--body starts-->

            </div>
        </div>
    </div>


</body>
</html>