<style>
.btn-is-disabled {
  pointer-events: none; / Disables the button completely. Better than just cursor: default; /
  @include opacity(0.7);
}
</style>

<div class="page-content">
        <!-- search results -->
<section class="search-results floating-section">
    <div class="container">
        <div class="results-container">
            <div class="column filter-column">
                <a href="" class="refine-by">Refine by</a>
        
                <form action="" method="POST">
                    <select name="" id="vehicle_type3wheel" class="vehitype" data-id="vehicle_type1">
                        <option value="featured">Type</option>
                        <option value="" data-id="aa" id="all">All</option>
                        <?php foreach ($threewheel_bike_type as $type): ?>
                            <option value="<?php echo $type['threewheel_bike_type_id']; ?>"
                                    data-id="<?php echo $type['threewheel_bike_type_id']; ?>"><?php echo $type['threewheel_bike_type_name']; ?></option>
                        <?php endforeach; ?>
                    </select>

                    <div id="vehiclew">
                        <select class="form-control brand" id="vehicle_brand3wheel" name="threewheel_bike_brand_id"
                                data-id="vehicle_brand1" disabled>
                            <option value=""> Select Brand</option>
                            <?php foreach ($threewheel_bike_brand as $brand): ?>
                                <option value="<?php echo $brand['threewheel_bike_brand_id']; ?>"
                                        class="<?php echo $brand['threewheel_type_id']; ?>"
                                        data-id="<?php echo $brand['threewheel_bike_brand_id']; ?>"><?php echo $brand['threewheel_bike_brand_name']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <select class="form-control brand" name="vehicle_model3wheel" id="vehicle_model3wheel" data-id="select1" disabled>
                        <option value=" ">Select Model</option>
                        <?php foreach ($threewheel_bike_model as $vehiclemodel): ?>
                            <option value="<?php echo $vehiclemodel['threewheel_bike_model_id']; ?>"
                                    class="<?php echo $vehiclemodel['threewheel_bike_brand_id']; ?>" data-id="<?php echo $vehiclemodel['threewheel_bike_model_id'];?>"><?php echo $vehiclemodel['threewheel_bike_model_name']; ?></option>
                        <?php endforeach; ?>
                    </select>
                </form>

                  <!-- <div class="compare-wrapper  aa" id="com" cursor: not-allowed;>
                        <div class="compare-vehicles">Vehicles Added: <span class="selected"></span>/5</div>
                        <a href="<?php echo base_url('home/comparison');?>" class="cta btn-green compare-btn btn-is-disabled" style=" background-color: gray !important; border-radius:0px !important;  
 display: block !important; padding: 5px 15px !important; margin-top:0px !important; cursor: not-allowed;">compare</a>
                    </div> --> 
                
            </div>
            
            <?php $count = 0; ?>
            <?php foreach ($bike as $val) { ?>
                <?php $count++; ?>
                <?php }; ?>

            <div class="column results-column">
                <p class="results-count">
                    <?php if ($count == 0) {
                        echo 'No';
                    } else {
                        echo $count;
                    } ?>&nbsp;Vehicles</p>
        
                <div class="results-wrap" id="results-wrap">

                            <?php foreach ($bike as $value): ?>

                        <?php 

                            $threewheel_bike_id = $value['threewheel_bike_id'];
                            $total_price = $value['total_price'];
                            $vehicle_total_value1 = (int)$total_price;
                            $year_of_manufacture = $value['Year_of_manufacture'];
                            $mileage_range = $value['mileage_range'];
                            $transmission = $value['transmission'];
                            $fuel_type = $value['fuel_type'];
                            $threewheel_bike_brand_name = $value['threewheel_bike_brand_name'];
                            $threewheel_bike_model_name = $value['threewheel_bike_model_name'];

                         ?>

                    <div class="result">
                        <?php foreach ($image as $value1):  ?>
                           <?php   if ($value['threewheel_bike_id'] == $value1['threewheel_bike_id']): ?>
                        <figure>
                            <div class="watermakerd">
                                <img src="<?php echo base_url(); ?>upload/3wheel/<?php echo $value1['threewheel_bike_image_name']; ?>" alt="">
                            </div>
                        </figure>
                        <?php break; ?>
                        <?php endif ?>

                    <?php endforeach ?>
                        <div class="right-side">
                            <p class="product-name"><?php echo $threewheel_bike_brand_name; ?> <?php echo $threewheel_bike_model_name; ?> </p>
                            <div class="price">
                                <img src="<?php echo base_url();?>assets/frontendone/images/misc/price-tag.png" alt=""/>
                                <p>Rs. <?php echo number_format($vehicle_total_value1) . ""; ?></p>
                                <img src="<?php echo base_url('assets/')?>drive-away.png" class="drive-away" alt=""/>
                            </div>

                            <div class="main-features">
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url();?>assets/frontendone/images/misc/calender.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Year</p>
                                        <p><?php echo $year_of_manufacture; ?></p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url();?>assets/frontendone/images/misc/dashboard.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Milage</p>
                                        <p><?php echo $mileage_range ?> Km</p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url();?>assets/frontendone/images/misc/auto.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Transmission</p>
                                        <p><?php if($transmission==1){
                                    echo "Automatic";
                                    } 
                                    else{
                                    echo "Manual";
                                    } ?></p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url();?>assets/frontendone/images/misc/auto.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Fuel Type</p>
                                        <p>
                                   
                                <?php if($fuel_type==1){
                                echo "Petrol";
                                }
                                elseif($fuel_type==2){
                                echo "Diesel";
                                }
                                else{
                                echo "Electric";
                                }  ?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="cta-group">
                                <a href="<?php echo base_url('home/bikeview/' . $value['threewheel_bike_id']) ?>" class="cta btn-green">VIEW</a>
                                <a href="" class="cta enquire checkresult open-popup" data-id="<?php echo $value['threewheel_bike_id']; ?>">INQUIRE</a>
                                <!-- <a href="" class="add-to-compare compare" value="<?php echo $value['vehicle_id']; ?>" data-hotel-name='<?php echo $value['vehicle_id']; ?>'>
                                    <span class="plus-icon"><i class="fa fa-plus"></i></span>
                                    <span class="text">compare</span>
                                </a> -->
                            </div>
                        </div>
                    </div> 
                   
        <?php endforeach ?>       
    </div>
    </div>
            <div class="column ads-column">
                <?php foreach ($ad as $a){ ?>
                    <a href="<?php echo $a['link'] ?>" class="ads-block" target="_blank">
                        <img src="<?php echo base_url(); ?>upload/advertisement/<?php echo $a['ad_image_name']; ?>" >
                    </a>
                <?php } ?>
            </div>
        </div>
    </div>
</section>
<!-- end of search results -->
    </div>
<script src="<?php echo base_url('assets/frontend');?>/assets/js/inquery.js"></script>

<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>

<script type="text/javascript">

var base_url ='http://localhost/autosurefull/'
var hotelName = []; 
 var i =0;
 $(".compare").click(function(e) {

     e.preventDefault(); 
      hotelName.push( $(this).data('hotel-name') );  
       
       var x =hotelName.toString();

       if(i>4){
        alert('only five vehicles can be selected');
       }else{
       i++;
  
       Cookies.set('yasas', x);
     }
        
   $.ajax({
            type: "POST",
            url: base_url + "home/divresult",
            //dataType: "JSON",
            data: {i:i},
            success: function(result) {
              //alert(results);
              $('#com').html(result);
           
                
            }

        });

   }); 

</script>
    
<!-- enquire popup -->

<div class="popup enquire-popup" id="enquire-popup">
    <?php echo $value['threewheel_bike_id'];?>
    <div class="popup-container">
        <div class="left-side">
            <form action="<?php echo base_url('home/inserttrewheeliquery')?>" method="POST">
                <input type="text" placeholder="Full Name" name="fulname">
                <input type="email" placeholder="Email Address" name="email">
                <input type="text" placeholder="Phone" name="phone">
                <textarea name="comments" id="" placeholder="Comments" name=""></textarea>
                <input type="hidden" id="resyltInqyry" class="resyltInqyry" name="vehicle_id" value="">
                <button type="submit">send enquiry</button>
            </form>
        </div>
        <div class="right-side">
            <h5>Call directly</h5>
            <p>Call one of our agents directly to reserve this vehicle or obtain more information.</p>

            <a href="tel:1123456263">(+94) 112 3456 263</a>
            <p>Contact between 9.00am - 5.00pm on weekdays and except for poyadays.</p>
        </div>
        <div class="popup-close"><i class="fa fa-close"></i></div>
    </div>
</div>

<!-- end of enquire popup -->

<!-- signup popup -->

<!-- end of signup popup -->

<!-- signin popup -->

<!-- end of signin popup -->

<!-- seller popup -->

<!-- end of seller popup -->

<!-- register popup -->

<!-- end of lease popup -->

<!-- </body>
</html> -->