    <div class="page-content">
        <!-- search results -->
<section class="search-results view-vehicle">
    <div class="container">
        <div class="results-container">
            <div class="column test-column">
                <div class="test-drive">
                    <h4>Book a Test Drive</h4>

                    <div class="test-drive-info">
                        <div class="reservation-block">
                            <a href="tel:01123456263">(+94) 766 377 666</a>
                            <p>Call for reservations</p>
                        </div>

                        <p>We bring the vehicle to your door-step for inspections and for a test drive.</p>

                        <p class="note">*Conditions Apply</p>
                    </div>
                </div>
            </div>
            <?php foreach ($view_vehicle as $value): ?>

                <?php 

                $vehicle_id = $value['vehicle_id'];
                $vehicle_type_name = $value['vehicle_type_name'];
                $vehicle_brand_name = $value['vehicle_brand_name'];
                $vehicle_model_name = $value['vehicle_model_name'];
                $vehicle_transmission = $value['vehicle_transmission'];
                $vehicle_engine_capacity = $value['vehicle_engine_capacity'];
                $vehicle_seating_capacity = $value['vehicle_seating_capacity'];
                $year_of_manufacture = $value['year_of_manufacture'];
                $year_of_registration = $value['year_of_registration'];
                $fuel_type = $value['fuel_type'];
                $average_of_fuel_consuption = $value['average_of_fuel_consuption'];
                $vehicle_total_value = $value['vehicle_total_value'];
                $vehicle_total_value1 = (int)$vehicle_total_value;
                $vehicle_mileage_range = $value['vehicle_mileage_range'];
                $number_of_owners = $value['number_of_owners']; 
                $vehicle_number = $value['vehicle_number']; 
                $vehicle_minimum_downpayment = $value['vehicle_minimum_downpayment'];
                $vehicle_minimum_downpayment2 = (int)$vehicle_minimum_downpayment ;
                $sellers_description = $value['sellers_description'];
                $vehicle_total_value1_half = $vehicle_total_value1/2 ;
                $vehicle_status=$value['vehicle_status'];
                $warranty= $value['warranty'];

                     ?>
            <div class="column results-column">
                <?php if($this->uri->segment(4)==2){?>
                    <a href="<?php echo base_url('home/results')?>" class="back-to"><i class="fa fa-angle-left"></i> Back to results</a>
                <?php }elseif($this->uri->segment(4)==3){?>
                    <a href="<?php echo base_url('home/popular')?>" class="back-to"><i class="fa fa-angle-left"></i> Back to results</a>
                <?php }?>

                <div class="single-vehicle">
                    <p class="product-name"><?php echo $vehicle_brand_name;?> <?php echo $vehicle_model_name;?></p>
                    <p class="ref">Veh/Sel/00<?php echo  $vehicle_id;?></p>

                    <div class="details">
                        <div class="single-vehicle-group">
                            <div class="owl-carousel single-vehicle-slider" data-slider-id="1">
                                <div class="item" style="background-image: url(<?php echo base_url(); ?>upload/selling/Front/<?php echo $value['inside_360_images']; ?>)">
                                    <?php if($vehicle_status==8): ?>
                                        <img src="<?php echo base_url('assets/');?>sold.png" style="width: 50%; position: absolute; bottom: -10px; right: -10px;" alt="autosure">
                                    <?php endif; ?>                                </div>
                                <?php foreach ($insideimage as $inside): ?>
                                    <?php if ($inside['vehicle_vehicle_id'] == $vehicle_id): ?>
                                        <div class="item" style="background-image: url(<?php echo base_url(); ?>upload/selling/<?php echo $inside['inside_images_name']; ?>)">
                                            <?php if($vehicle_status==8): ?>
                                                <img src="<?php echo base_url('assets/');?>sold.png" style="width: 50%; position: absolute; bottom: -10px; right: -10px;" alt="autosure">
                                            <?php endif; ?>
                                        </div>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </div>
                            
                            <div class="owl-thumbs" data-slider-id="1">
                                <button class="owl-thumb-item" style="background-image: url(<?php echo base_url(); ?>upload/selling/Front/<?php echo $value['inside_360_images']; ?>)"></button>
                               <?php foreach ($insideimage as $inside): ?>
                            <?php if ($inside['vehicle_vehicle_id'] == $vehicle_id): ?>
                                <button class="owl-thumb-item" style="background-image: url(<?php echo base_url(); ?>upload/selling/<?php echo $inside['inside_images_name']; ?>)"></button>
                                <?php endif ?>
                                   <?php endforeach ?>
                            </div>                            
                        </div>  

                        <div class="right-side">

                            <div class="main-features">
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url('assets/frontendone/')?>images/misc/calender.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Year</p>
                                        <p><?php echo $year_of_manufacture;?></p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url('assets/frontendone/')?>images/misc/dashboard.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Milage</p>
                                        <p><?php if($value['millage_range_unit']==1){
                                        echo $value['vehicle_mileage_range']. "Km";
                                    }
                                    else{
                                        echo $value['vehicle_mileage_range']. "mil";
                                    }
                                    ?></p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url('assets/frontendone/')?>images/misc/auto.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Transmission</p>
                                        <p><?php if($vehicle_transmission==1){
                                    echo "Automatic";
                                    } elseif($vehicle_transmission==2){
                                    echo "Manual";
                                    }
                                    else{
                                    echo "Triptonic";
                                    } ?></p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url(); ?>assets/fuel.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Fuel Type</p>
                                        <p><?php if($fuel_type==1){
                                echo "Petrol";
                                }
                                elseif($fuel_type==2){
                                echo "Diesel";
                                }
                                elseif($fuel_type==3){
                                echo "Hybrid";
                                }
                                else{
                                echo "Electric";
                                }  ?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="price">
                                <img src="<?php echo base_url('assets/frontendone/')?>images/misc/price-tag.png" alt="">
                                <p>Rs. <?php echo number_format($vehicle_total_value1).""?></p>

                            </div>

                            <div>
                                <img src="<?php echo base_url('assets/')?>drive-away.png" alt="">
                            </div>

                            <div class="cta-group">
                                <a href="" class="cta enquire checkresult open-popup" data-id="<?php echo $value['vehicle_id']; ?>">INQUIRE</a>
                                <a href="" class="cta enquire make-offer" data-id="<?php echo $value['vehicle_id']; ?>">MAKE OFFER</a>
                                <?php if($vehicle_status!=8){ ?>
                                    <a href="" class="cta enquire lease lesebtn">LEASE</a>
                                <?php } else{ ?>
                                    <button class="cta enquire lesebtn-disabled">LEASE</button>
                                <?php } ?>
                            </div>

                            <?php if($value['warranty']==5): ?>
                                <div>
                                    <img src="<?php echo base_url('assets/')?>warranty.png" alt="">
                                </div>
                            <?php endif; ?>

                            <?php if($value['autosure_certified']==1): ?>
                                <div>
                                    <img src="<?php echo base_url('assets/')?>certified.png" class="certified-big" alt=""/>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>                    
                </div>
<?php if($sellers_description!=NULL): ?>
                <div class="description-wrap">
                    <h5>Sellers Description</h5>

                    <p><?php echo $sellers_description; ?></p>
                </div>
                <?php endif; ?>

                <div class="more-details">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#details" data-toggle="tab">Details</a></li>
                        <li><a href="#features" data-toggle="tab">Features</a></li>
                    </ul>
                    
                    <div class="tab-content">
                        <div id="details" class="tab-pane fade in active">
                            <div class="detail">
                                <div class="detail-name">Make</div>
                                <div class="detail-description"><?php echo $vehicle_brand_name;?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Model</div>
                                <div class="detail-description"><?php echo $vehicle_model_name; ?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Transmission</div>
                                <div class="detail-description">
                                    <?php if($vehicle_transmission==1){
                                    echo "Automatic";
                                    } elseif($vehicle_transmission==2){
                                    echo "Manual";
                                    }
                                    else{
                                    echo "Triptonic";
                                    } ?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Mileage Range</div>
                                <div class="detail-description">
                                    <?php if($value['millage_range_unit']==1){
                                        echo $value['vehicle_mileage_range']. "Km";
                                    }
                                    else{
                                        echo $value['vehicle_mileage_range']. "mil";
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Engine Capacity</div>
                                <div class="detail-description">
                                     <?php if($value['engine_capacity_unit']==1){ 
                                        echo $value['vehicle_engine_capacity']."CC";
                                    }
                                    else{
                                        echo $value['vehicle_engine_capacity']."kW";
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Seating Capacity</div>
                                <div class="detail-description"><?php echo $vehicle_seating_capacity;?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Year of manufacture</div>
                                <div class="detail-description"><?php echo $year_of_manufacture;?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Year of registration</div>
                                <div class="detail-description"><?php echo $year_of_registration;?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Fuel type</div>
                                <div class="detail-description">
                                     <?php if($fuel_type==1){
                                echo "Petrol";
                                }
                                elseif($fuel_type==2){
                                echo "Diesel";
                                }
                                elseif($fuel_type==3){
                                echo "Hybrid";
                                }
                                else{
                                echo "Electric";
                                }  ?>
                                </div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Average fuel consumption</div>
                                <div class="detail-description">
                                     <?php if($value['avarage_fuel_consumption_unit']==1){
                                        echo $average_of_fuel_consuption."Km/l";
                                    }
                                    else{
                                        echo $average_of_fuel_consuption."mil/l";
                                    }

                                    ?>
                                </div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Number of Owners</div>
                                <div class="detail-description"><?php echo $number_of_owners;?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Vehicle Number</div>
                                <div class="detail-description"><?php echo $vehicle_number;?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Total Value</div>
                                <div class="detail-description">Rs. <?php echo number_format($vehicle_total_value1).""?></div>
                            </div>
                            <!-- <div class="detail">
                                <div class="detail-name">Instalment</div>
                                <div class="detail-description">BMW X5</div>
                            </div> -->
                            <div class="detail">
                                <div class="detail-name">Downpayment</div>
                                <div class="detail-description">Rs. <?php echo number_format($vehicle_minimum_downpayment2).""?> </div>
                            </div>
                            <!-- <div class="detail">
                                <div class="detail-name">Duration</div>
                                <div class="detail-description">BMW X5</div>
                            </div> -->
                        </div>
                        <div id="features" class="tab-pane fade">
                             <?php $count=0;?>
                        <?php $varr; 
                        $varr1
                        ?>
                        <?php foreach($vehicle as $vehi): ?>
                            <?php 
                           $vehicle_features1 = $vehi['vehicle_features'];
                           $additional_features = $vehi['additional_features'];
                             ?>
                        
                        <?php
                        
                         $varr = explode(",",$vehicle_features1);
                         $varr1 = explode(",",$additional_features);
                        ?>
                        <ul class="feature">
                            
                                
                                <?php foreach ($varr as $var): ?>   
                            
                                <?php if($count%4==0){?> 
                                </ul><ul class="feature">
                                 <?php }?>
                                <li><a href="">
                        <?php print_r ($var);?></a></li><br>
                                
                               
				            <?php $count++; ?>
                                <?php endforeach ?>

                                <?php if(!empty($varr1)) : ?>
                                
                                
                                <?php foreach ($varr1 as $var1): ?>
                                
                                <?php if($var1 !== "") : ?>
                                
                                <?php if($count%4==0){?> 
                                    </ul><ul class="feature">
                                 <?php } ?>
                                  
                                <li><a href="">
                        <?php print_r ($var1);?></a></li><br>
                                
                               <?php endif; ?>
				<?php $count++; ?>
                                <?php endforeach ?>
                                <?php endif; ?>
                            </ul>
                            <?php break; ?>
                             <?php endforeach ?>
                        </div>
                    </div>                    
                </div>
                <?php break; ?>
            <?php endforeach ?>
            </div>
            <div class="column ads-column">
                <?php foreach ($ad as $a){ ?>
                    <a href="<?php echo $a['link'] ?>" class="ads-block" target="_blank">
                        <img src="<?php echo base_url(); ?>upload/advertisement/<?php echo $a['ad_image_name']; ?>" >
                    </a>
                <?php } ?>
            </div>
        </div>        
    </div>
</section>
<!-- end of search results -->
    </div>


<script src="<?php echo base_url('assets/frontend/'); ?>assets/js/inquery.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/lib.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/main.js"></script>

<!-- enquire popup -->
<div class="popup enquire-popup" id="enquire-popup">
    <div class="popup-container">
        <div class="left-side">
            <form action="<?php echo base_url('home/insertiquery')?>" method="POST">
                <input type="text" placeholder="Full Name" name="fullname">
                <input type="email" placeholder="Email Address" name="address">
                <input type="text" placeholder="Phone" name="phone">
                <textarea name="comments" id="" placeholder="Comments"></textarea>
                <input type="hidden" id="resyltInqyry" class="resyltInqyry" name="vehicle_id" value="">
                <button type="submit">send enquiry</button>
            </form>
        </div>
        <div class="right-side">
            <h5>Call directly</h5>
            <p>Call one of our agents directly to reserve this vehicle or obtain more information.</p>

            <a href="tel:0766377666">(+94) 766 377 666</a>
            <p>Contact between 9.00am - 5.00pm on weekdays and except for poya days.</p>
        </div>
        <div class="popup-close"><i class="fa fa-close"></i></div>
    </div>
</div>
<!-- end of enquire popup -->

<!-- make offer popup -->
<div class="popup make-offer-popup" id="make-offer-popup">
    <div class="popup-container">
        <div class="left-side">
            <form action="<?php echo base_url('home/insertoffer')?>" method="POST">
                <input type="text" placeholder="Full Name" name="fullname">
                <input type="email" placeholder="Email Address" name="address">
                <input type="text" placeholder="Phone" name="phone">                
                <input type="text" placeholder="Price you offer" name="price">
                <textarea name="comments" id="" placeholder="Comments"></textarea>
                <input type="hidden" name="vehicle_id" value="<?php echo $value['vehicle_id'];?>">
                <button type="submit">Make Offer</button>
            </form>
        </div>
        <div class="right-side">
            <h5>Call directly</h5>
            <p>Call one of our agents directly to reserve this vehicle or obtain more information.</p>

            <a href="tel:0766377666">(+94) 766 377 666</a>
            <p>Contact between 9.00am - 5.00pm on weekdays and except for poya days.</p>
        </div>
        <div class="popup-close"><i class="fa fa-close"></i></div>
    </div>
</div>
<!-- end of make offer popup -->

<!-- signup popup -->

<!-- end of signup popup -->

<!-- lease popup -->
<div class="popup signup-popup lease-popup" id="lease-popup">
    <div class="popup-container">
        <div class="popup-title-wrap">
            <h3>Lease</h3>
        </div>
        <div class="left-side">
            <form action="" method="POST">
                <div class="radio-group">
                    <input type="radio"  placeholder="" value="1"  class="autosurerad" id="otherr" name="aradio" checked>
                    <label for="autosure-lease">Autosure</label>
                </div>
                <div class="radio-group">
                    <input type="radio" value="2" class="otherrad" id="other" name="aradio" id="other-lease">
                    <label for="other-lease">Other</label>
                </div>
                <div class="formlease">
                    <div class="input-group">
                        <input type="number" name="downpayment" class="autoauto" id="downpayment"value="<?php echo $vehicle_total_value1_half;?>" disabled="disabled" placeholder="Down payment">
                    </div>
                    <div class="input-group">
                        <select class="autoauto" name="duration" id="duration" disabled="disabled">
                            <option selected="selected" value="">Duration</option>
                            <option value="12">1 year</option>
                            <option value="24">2 year</option>
                            <option value="36">3 year</option>
                            <option value="48">4 year</option>
                            <option value="60">5 year</option>
                        </select>
                    </div>
                    <div class="input-group">
                        <input type="hidden" name="interest" class="autoauto" id="interest" value=".0167" placeholder="Interest rate" disabled="disabled">
                    </div>
                    <button type="button" id="submitlease" class="submitlease">Calculate</button>

                </div>

                <div class="formlease1">
                    <div class="input-group">
                        <input type="number" placeholder="Down Payment" name="downpaymentone" id="downpaymentone"
                               class="otherother" disabled="disabled"> </div>

                    <div class="input-group">
                        <select class="otherother" name="durationone" id="durationone" disabled="disabled">
                            <option selected="selected" value="">Duration</option>
                            <option value="12">1 year</option>
                            <option value="24">2 year</option>
                            <option value="36">3 year</option>
                            <option value="48">4 year</option>
                            <option value="60">5 year</option>
                        </select></div>
                    <div class="input-group">
                        <input type="number" placeholder="Interest" name="interestone" id="interestone" class="otherother"
                               disabled="disabled"> </div>
                    <button type="button" id="submitleasecal" class="submitlease">Calculate</button>

                </div>
                <div class="toval" id="instal">

                </div>
            </form>

        </div>
        <div class="popup-close"><i class="fa fa-close"></i></div>
    </div>
</div>
<!-- end of lease popup -->

