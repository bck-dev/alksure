<section class="banner" style="background-image: url('<?php echo base_url('assets/frontendone/'); ?>images/banner/vehicle-import-banner.jpg')"></section>
<!-- end of banner -->

<!-- vehicle import section -->
<section class="search-results floating-section vehicle-import">
    <div class="container">
        <div class="results-container">
            <div class="column filter-column">
                <a href="" class="refine-by">Refine by</a>

                <form action="" method="POST">
                    <select name="" id="">
                        <option value="type">Type</option>
                    </select>

                    <select name="" id="">
                        <option value="make">Make</option>
                    </select>

                    <select name="" id="">
                        <option value="model">Model</option>
                    </select>
                </form>

                <div class="compare-wrapper">
                    <div class="compare-vehicles">Vehicles Added: <span class="selected">2</span>/5</div>
                    <a href="" class="cta btn-green compare-btn">compare</a>
                </div>

                <div class="compare-wrapper disabled">
                    <div class="compare-vehicles">Vehicles Added: <span class="selected">2</span>/5</div>
                    <a href="" class="cta btn-green compare-btn">compare</a>
                </div>
            </div>
            <div class="column results-column">
                <p class="results-count">28 vehicles</p>

                <div class="results-wrap">
                    <div class="result">
                        <figure>
                            <div class="watermakerd">
                                <img src="<?php echo base_url('assets/frontendone/'); ?>images/product/bmw-x5.jpg" alt="">
                            </div>
                        </figure>
                        <div class="right-side">
                            <p class="product-name">2017 BMW X5 xDrive50i</p>
                            <div class="price">
                                <img src="<?php echo base_url('assets/frontendone/'); ?>images/misc/price-tag.png" alt="">
                                <p>8,650,000</p>
                            </div>

                            <div class="main-features">
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url('assets/frontendone/'); ?>images/misc/calender.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Year</p>
                                        <p>2015</p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url('assets/frontendone/'); ?>images/misc/dashboard.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Milage</p>
                                        <p>75,000 Km</p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url('assets/frontendone/'); ?>images/misc/auto.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Transmission</p>
                                        <p>Automatic</p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url('assets/frontendone/'); ?>images/misc/auto.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Fuel Type</p>
                                        <p>Electric</p>
                                    </div>
                                </div>
                            </div>

                            <div class="cta-group">
                                <a href="pages/view-vehicle.html" class="cta btn-green">VIEW</a>
                                <a href="" class="cta enquire open-popup">ENQUIRE</a>
                                <a href="" class="add-to-compare">
                                    <span class="plus-icon"><i class="fa fa-plus"></i></span>
                                    <span class="text">compare</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="result">
                        <figure>
                            <div class="watermakerd">
                                <img src="<?php echo base_url('assets/frontendone/'); ?>images/product/bmw-x5.jpg" alt="">
                            </div>
                        </figure>
                        <div class="right-side">
                            <p class="product-name">2017 BMW X5 xDrive50i</p>
                            <div class="price">
                                <img src="<?php echo base_url('assets/frontendone/'); ?>images/misc/price-tag.png" alt="">
                                <p>8,650,000</p>
                            </div>

                            <div class="main-features">
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url('assets/frontendone/'); ?>images/misc/calender.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Year</p>
                                        <p>2015</p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url('assets/frontendone/'); ?>images/misc/dashboard.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Milage</p>
                                        <p>75,000 Km</p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url('assets/frontendone/'); ?>images/misc/auto.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Transmission</p>
                                        <p>Automatic</p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url('assets/frontendone/'); ?>images/misc/auto.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Fuel Type</p>
                                        <p>Electric</p>
                                    </div>
                                </div>
                            </div>

                            <div class="cta-group">
                                <a href="pages/view-vehicle.html" class="cta btn-green">VIEW</a>
                                <a href="" class="cta enquire open-popup">ENQUIRE</a>
                                <a href="" class="add-to-compare">
                                    <span class="plus-icon"><i class="fa fa-plus"></i></span>
                                    <span class="text">compare</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="result">
                        <figure>
                            <div class="watermakerd">
                                <img src="<?php echo base_url('assets/frontendone/'); ?>images/product/bmw-x5.jpg" alt="">
                            </div>
                        </figure>
                        <div class="right-side">
                            <p class="product-name">2017 BMW X5 xDrive50i</p>
                            <div class="price">
                                <img src="<?php echo base_url('assets/frontendone/'); ?>images/misc/price-tag.png" alt="">
                                <p>8,650,000</p>
                            </div>

                            <div class="main-features">
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url('assets/frontendone/'); ?>images/misc/calender.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Year</p>
                                        <p>2015</p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url('assets/frontendone/'); ?>images/misc/dashboard.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Milage</p>
                                        <p>75,000 Km</p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url('assets/frontendone/'); ?>images/misc/auto.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Transmission</p>
                                        <p>Automatic</p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url('assets/frontendone/'); ?>images/misc/auto.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Fuel Type</p>
                                        <p>Electric</p>
                                    </div>
                                </div>
                            </div>

                            <div class="cta-group">
                                <a href="pages/view-vehicle.html" class="cta btn-green">VIEW</a>
                                <a href="" class="cta enquire open-popup">ENQUIRE</a>
                                <a href="" class="add-to-compare">
                                    <span class="plus-icon"><i class="fa fa-plus"></i></span>
                                    <span class="text">compare</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="result">
                        <figure>
                            <div class="watermakerd">
                                <img src="<?php echo base_url('assets/frontendone/'); ?>images/product/bmw-x5.jpg" alt="">
                            </div>
                        </figure>
                        <div class="right-side">
                            <p class="product-name">2017 BMW X5 xDrive50i</p>
                            <div class="price">
                                <img src="<?php echo base_url('assets/frontendone/'); ?>images/misc/price-tag.png" alt="">
                                <p>8,650,000</p>
                            </div>

                            <div class="main-features">
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url('assets/frontendone/'); ?>images/misc/calender.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Year</p>
                                        <p>2015</p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url('assets/frontendone/'); ?>images/misc/dashboard.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Milage</p>
                                        <p>75,000 Km</p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url('assets/frontendone/'); ?>images/misc/auto.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Transmission</p>
                                        <p>Automatic</p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url('assets/frontendone/'); ?>images/misc/auto.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Fuel Type</p>
                                        <p>Electric</p>
                                    </div>
                                </div>
                            </div>

                            <div class="cta-group">
                                <a href="pages/view-vehicle.html" class="cta btn-green">VIEW</a>
                                <a href="" class="cta enquire open-popup">ENQUIRE</a>
                                <a href="" class="add-to-compare">
                                    <span class="plus-icon"><i class="fa fa-plus"></i></span>
                                    <span class="text">compare</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="result">
                        <figure>
                            <div class="watermakerd">
                                <img src="<?php echo base_url('assets/frontendone/'); ?>images/product/bmw-x5.jpg" alt="">
                            </div>
                        </figure>
                        <div class="right-side">
                            <p class="product-name">2017 BMW X5 xDrive50i</p>
                            <div class="price">
                                <img src="<?php echo base_url('assets/frontendone/'); ?>images/misc/price-tag.png" alt="">
                                <p>8,650,000</p>
                            </div>

                            <div class="main-features">
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url('assets/frontendone/'); ?>images/misc/calender.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Year</p>
                                        <p>2015</p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url('assets/frontendone/'); ?>images/misc/dashboard.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Milage</p>
                                        <p>75,000 Km</p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url('assets/frontendone/'); ?>images/misc/auto.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Transmission</p>
                                        <p>Automatic</p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url('assets/frontendone/'); ?>images/misc/auto.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Fuel Type</p>
                                        <p>Electric</p>
                                    </div>
                                </div>
                            </div>

                            <div class="cta-group">
                                <a href="pages/view-vehicle.html" class="cta btn-green">VIEW</a>
                                <a href="" class="cta enquire open-popup">ENQUIRE</a>
                                <a href="" class="add-to-compare">
                                    <span class="plus-icon"><i class="fa fa-plus"></i></span>
                                    <span class="text">compare</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="result">
                        <figure>
                            <div class="watermakerd">
                                <img src="<?php echo base_url('assets/frontendone/'); ?>images/product/bmw-x5.jpg" alt="">
                            </div>
                        </figure>
                        <div class="right-side">
                            <p class="product-name">2017 BMW X5 xDrive50i</p>
                            <div class="price">
                                <img src="<?php echo base_url('assets/frontendone/'); ?>images/misc/price-tag.png" alt="">
                                <p>8,650,000</p>
                            </div>

                            <div class="main-features">
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url('assets/frontendone/'); ?>images/misc/calender.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Year</p>
                                        <p>2015</p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url('assets/frontendone/'); ?>images/misc/dashboard.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Milage</p>
                                        <p>75,000 Km</p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url('assets/frontendone/'); ?>images/misc/auto.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Transmission</p>
                                        <p>Automatic</p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url('assets/frontendone/'); ?>images/misc/auto.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Fuel Type</p>
                                        <p>Electric</p>
                                    </div>
                                </div>
                            </div>

                            <div class="cta-group">
                                <a href="pages/view-vehicle.html" class="cta btn-green">VIEW</a>
                                <a href="" class="cta enquire open-popup">ENQUIRE</a>
                                <a href="" class="add-to-compare">
                                    <span class="plus-icon"><i class="fa fa-plus"></i></span>
                                    <span class="text">compare</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pagination-wrap">
                    <ul class="pagination">
                        <li class="prev"><a href="#"><</a></li>
                        <li class="active"><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li class="next"><a href="#">></a></li>
                    </ul>
                </div>
            </div>
            <div class="column ads-column">
                <a href="" class="ads-block">
                    <img src="<?php echo base_url('assets/frontendone/'); ?>images/misc/ad.jpg" alt="">
                </a>
            </div>
        </div>
    </div>
</section>

<!-- enquire popup -->
<div class="popup enquire-popup">
    <div class="popup-container">
        <div class="left-side">
            <form action="" method="POST">
                <input type="text" placeholder="Full Name">
                <input type="email" placeholder="Email Address">
                <input type="text" placeholder="Phone">
                <textarea name="" id="" placeholder="Comments"></textarea>
                <button type="submit">send enquiry</button>
            </form>
        </div>
        <div class="right-side">
            <h5>Call directly</h5>
            <p>Call one of our agents directly to reserve this vehicle or obtain more information.</p>

            <a href="tel:0766377666">(+94) 766 377 666</a>
            <p>Contact between 9.00am - 5.00pm on weekdays and except for poyadays.</p>
        </div>
        <div class="popup-close"><i class="fa fa-close"></i></div>
    </div>
</div>
<!-- end of enquire popup -->

<!-- enquire popup -->
<div class="popup signup-popup">
    <div class="popup-container">
        <div class="left-side">
            <form action="" method="POST">
                <input type="text" placeholder="Name">
                <input type="email" placeholder="Email Address">
                <button type="submit">sign up</button>
            </form>
        </div>
        <div class="popup-close"><i class="fa fa-close"></i></div>
    </div>
</div>