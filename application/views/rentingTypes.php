                <!-- <p class="results-count">1,287 vehicles</p> -->
                
                	

            <?php $count = 0; ?>
                    <?php foreach($Rvehicle as $vehicle1):?>
                    <?php $count++; ?>
<?php endforeach;?>

                    <p class="results-count"><?php if ($count == 0) {
                        echo 'No';
                    } else {
                        echo $count;
                    } ?>&nbsp;Vehicles</p>
                
                <div class="results-wrap" id="results-wrap">
                
                <?php foreach($Rvehicle as $vehicle):?>
                <?php foreach($rentingtype as $type): 
                    if($type['vehical_type_id']==$vehicle['vehical_vehical_type_id']):
                        $vtype = $type['vehical_type_name'];
                    endif;
                endforeach;
                   
                foreach($rentingbrand as $brand): 
                    if($brand['vehical_brand_id']==$vehicle['vehical_vehical_brand_id']):
                        $vbrand = $brand['vehical_brand_name'];
                    endif;
                endforeach;

                foreach($rentingmodal as $modal): 
                    if($modal['vehical_model_id']==$vehicle['vehical_vehical_model_id']):
                        $vmodal = $modal['vehical_model_name'];
                    endif;
                endforeach;

                $id = $vehicle['renting_vehical_id'];
                $trans = $vehicle['transmission'];
                $millge = $vehicle['millage_range'];
                $p_d_price = $vehicle['per_day_price'];
                $fuel_type = $vehicle['fuel_type'];
                $disc = $vehicle['discount'];
                $reg_year = $vehicle['year_of_registration'];
                $man_year = $vehicle['year_of_manufactured'];
                $capacity = $vehicle['engine_capacity'];
                $image = $vehicle['renting_vehical_inside_image'];

                ?>

                <?php foreach($images as $img): 
                    if($img['renting_vehical_id'] == $vehicle['renting_vehical_id'] ):
                    ?>
                    <div class="result">
                      <figure>
                            <div class="watermakerd">
                                <img src="<?php echo base_url(); ?>upload/renting/inside/<?php echo $image; ?>" alt="" style="width:230px; height:180px;">
                            </div>
                        </figure>
                        
                        <div class="right-side">
                            <p class="product-name"><?php echo $vbrand." ".$vmodal; ?></p>
                            <div class="price">
                                <img src="" alt="">
                                <?php if($difference!=0){ ?>
                                <div class="col-md-6">
                                    <div>Per day price</div>
                                    <p>Rs. <?php echo $p_d_price; ?></p>
                                </div>
                                <div class="col-md-6">
                                    <div>Total</div>
                                    <p>Rs. <?php 
                                        $total = $p_d_price*$difference;
                                        echo $total;
                                    ?></p>
                                </div>
                            <?php } else{ ?>
                                <div class="col-md-6">
                                    <div>Per day price</div>
                                    <p>Rs. <?php echo $p_d_price; ?></p>
                                </div>
                            <?php }?>
                            </div>

                            <div class="main-features">
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url();?>assets/frontendone/images/misc/calender.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Year</p>
                                        <p><?php echo $reg_year; ?></p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url();?>assets/frontendone/images/misc/dashboard.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Milage</p>
                                        <p><?php echo $millge;?> Km</p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url();?>assets/frontendone/images/misc/auto.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Transmission</p>
                                        <p><?php if($trans == 1){
                                            echo "Automatic";
                                        }else if($trans== 2){
                                            echo "Manual";
                                        }
                                        else{
                                        	echo "Triptonic";
                                        }

                                         ?></p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url(); ?>assets/fuel.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Fuel Type</p>
                                        <p><?php if($fuel_type ==1){
                                            echo "Petrol";
                                        }else if($fuel_type == 2){
                                            echo "Diesel";
                                        }
                                        else if($fuel_type== 3){
                                            echo "Hybird";
                                        }
                                        else{
                                            echo "Electric";
                                        }
                                        ?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="cta-group">
                                <a href="<?php echo base_url();?>home/rentingVehicle/<?php echo $id; ?>/<?php echo $difference ;?>" class="cta btn-green">VIEW</a>
                                <a href="" class="cta checkresult enquire open-popup" data-id="<?php echo $vehicle['renting_vehical_id']; ?>">INQUIRE</a>
                            </div>
                        </div>
                    </div>
                    <?php break;?>
                    <?php endif;?>
                    <?php endforeach; ?>
                    <?php endforeach; ?>
                </div>
                
<script src="<?php echo base_url('assets/frontend/'); ?>assets/js/inquery.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/lib.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/main.js"></script>
                <!-- enquire popup -->
<div class="popup enquire-popup">
    <div class="popup-container">
        <div class="left-side">
            <form action="<?php echo base_url('home/rentingInquiry')?>" method="POST">
                 <input type="text" placeholder="Full Name" name="name">
                <input type="email" placeholder="Email Address" name="email">
                <input type="text" placeholder="Phone" name="number">
                <textarea name="comment" id="" placeholder="Comments"></textarea>
                <input type="hidden" id="resyltInqyry" class="resyltInqyry" name="vehicle_id" value="">
                <button type="submit">send enquiry</button>
            </form>
        </div>
        <div class="right-side">
            <h5>Call directly</h5>
            <p>Call one of our agents directly to reserve this vehicle or obtain more information.</p>

            <a href="tel:0766377666">(+94) 766 377 666</a>
            <p>Contact between 9.00am - 5.00pm on weekdays and except for poyadays.</p>
        </div>
        <div class="popup-close"><i class="fa fa-close"></i></div>
    </div>
</div>
<!-- end of enquire popup -->