<style>
.btn-is-disabled {
  pointer-events: none; / Disables the button completely. Better than just cursor: default; /
  @include opacity(0.7);
}
</style>

<script https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js></script>

<div class="results-wrap">
                    <?php $count = 0; ?>
                        <?php foreach($results as $value):?>
                            <?php $count++; ?>
                        <?php endforeach;?>

                        <p class="results-count"><?php if ($count == 0) {
                            echo 'No';
                        } else {
                            echo $count;
                        } ?>&nbsp;Vehicles</p>

                        <?php foreach ($results as $value): ?>

                            <?php

                            $year_of_manufacture = $value['year_of_manufacture'];
                            $vehicle_id = $value['vehicle_id'];
                            $vehicle_total_value = $value['vehicle_total_value'];
                            $vehicle_total_value1 = (int)$vehicle_total_value;
                            $vehicle_transmission = $value['vehicle_transmission'];
                            $vehicle_model = $value['vehicle_model_name'];
                            $vehicle_mileage_range = $value['vehicle_mileage_range'];
                            $vehicle_brand_name = $value['vehicle_brand_name'];
                            $fuel_type = $value['fuel_type'];
                            $id = $value['vehicle_id'];
                            $publish_status = $value['publish_status'];
                            $reject_status = $value['reject_status'];

                            ?>

                        <?php if ($publish_status == 1 && $reject_status == 0) { ?>
                            

                    <div class="result">
                        <figure>
                            <div class="watermakerd">
                                <img src="<?php echo base_url(); ?>upload/selling/Front/<?php echo $value['inside_360_images']; ?>" style="width:230px; height:180px; ">
                            </div>
                        </figure>
                        <div class="right-side">
                            <p class="product-name"><?php echo $vehicle_brand_name; ?> <?php echo $vehicle_model; ?> </p>
                            <div class="price">
                                <img src="<?php echo base_url();?>assets/frontendone/images/misc/price-tag.png" alt="">
                                <p>Rs. <?php echo number_format($vehicle_total_value1) . ""; ?></p>
                            </div>

                            <div class="main-features">
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url();?>assets/frontendone/images/misc/calender.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Year</p>
                                        <p><?php echo $year_of_manufacture; ?></p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url();?>assets/frontendone/images/misc/dashboard.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Milage</p>
                                        <p><?php if($value['millage_range_unit']==1){
                                                    echo $value['vehicle_mileage_range']. "Km";
                                                }
                                                else{
                                                    echo $value['vehicle_mileage_range']. "mil";
                                                }
                                        ?></p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url();?>assets/frontendone/images/misc/auto.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Transmission</p>
                                        <p><?php if($vehicle_transmission==1){
                                            echo "Automatic";
                                            } elseif($vehicle_transmission==2){
                                            echo "Manual";
                                            }
                                            else{
                                            echo "Triptonic";
                                            } ?></p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url();?>assets/frontendone/images/misc/auto.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Fuel Type</p>
                                        <p><?php if($fuel_type==1){
                                            echo "Petrol";
                                            }
                                            elseif($fuel_type==2){
                                            echo "Diesel";
                                            }
                                            elseif($fuel_type==3){
                                            echo "Hybrid";
                                            }
                                            else{
                                            echo "Electric";
                                            }  ?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="cta-group">
                                <a href="<?php echo base_url('home/viewvehicle/' . $value['vehicle_id']) ?>" class="cta btn-green">VIEW</a>
                                <a href="" class="cta enquire checkresult open-popup" data-id="<?php echo $value['vehicle_id']; ?>">INQUIRE</a>
                                <a href="" class="add-to-compare compare" value="<?php echo $value['vehicle_id']; ?>" data-hotel-name='<?php echo $value['vehicle_id']; ?>'>
                                    <span class="plus-icon"><i class="fa fa-plus"></i></span>
                                    <span class="text">compare</span>
                                </a>
                            </div>
                        </div>
                    </div> 
                    
            <?php } ?>
        <?php endforeach ?>       
    </div>

<!-- end of search results -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="<?php echo base_url('assets/frontend');?>/assets/js/inquery.js"></script>
<script src="<?php echo base_url('assets/frontend/');?>javascripts/script.js"></script>
<script src="<?php echo base_url('assets/frontend/');?>javascripts/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/lib.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/main.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/plugins/rangeslider.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/plugins/tablesaw.jquery.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/plugins/tablesaw-init.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/plugins/jquery-ui.min.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/plugins/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/plugins/wow.min.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/script.js"></script> 


<!--<script src="<?php echo base_url('assets/frontend');?>/assets/js/inquery.js"></script>-->
<!--<script src="<?php echo base_url('assets/frontendone/');?>js/lib.js"></script>-->
<!--<script src="<?php echo base_url('assets/frontendone/');?>js/main.js"></script>-->

<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>

<!-- enquire popup -->

<div class="popup enquire-popup" id="enquire-popup">
    <div class="popup-container">
        <div class="left-side">
            <form action="<?php echo base_url('home/insertiquery')?>" method="POST">
                <input type="text" name="fullname" placeholder="Full Name">
                <input type="email" name="address" placeholder="Email Address">
                <input type="text" name="phone" placeholder="Phone">
                <textarea name="comments" id="" placeholder="Comments"></textarea>
                <input type="hidden" id="resyltInqyry" class="resyltInqyry" name="vehicle_id" value="">
                <button type="submit">send inquiry</button>
            </form>
        </div>
        <div class="right-side">
            <h5>Call directly</h5>
            <p>Call one of our agents directly to reserve this vehicle or obtain more information.</p>

            <a href="tel:0766377666">(+94)766 377 666</a>
            <p>Contact between 9.00am - 5.00pm on weekdays and except for poyadays.</p>
        </div>
        <div class="popup-close"><i class="fa fa-close"></i></div>
    </div>
</div>

<!--<script src="<?php echo base_url('assets/frontend');?>/assets/js/inquery.js"></script>-->

<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>

<script type="text/javascript">

var base_url ='http://autosure.lk/';
var hotelName = []; 
 var i =0;
 $(".compare").click(function(e) {

     e.preventDefault(); 
      hotelName.push( $(this).data('hotel-name') );  
       
       var x =hotelName.toString();
    
       if(i>4){
        alert('only five vehicles can be selected');
       }else{
       i++;
  
       Cookies.set('yasas', x);
     }
        
   $.ajax({
            type: "POST",
            url: base_url + "home/divresult",
            //dataType: "JSON",
            data: {i:i},
            success: function(result) {
              //alert(results);
              $('#com').html(result);
           
                
            }

        });

   }); 

</script>