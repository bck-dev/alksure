<!DOCTYPE html>
<html lang="en">
	<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Autosure</title>

    <meta name="description" content="">
    <link rel="shortcut icon" href="">

  
</head>
<body>
	

	<!-- services form -->
<section class="services-form">
    <div class="container">
        <div class="service-wrapper">
            <form action="<?php echo base_url('home/insertservice'); ?>" method="POST"
                  enctype='multipart/form-data'>
                <div class="group">
                    <label for="vehicle-type">Vehicle type</label>
                    <select name="vehicle-type" id="vehicle-type" >
                        <option value="">Type</option>
                        <?php foreach ($vehicle as $val): ?>
                        <option value="<?php echo $val['service_vehical_type_id']; ?>"><?php echo strtoupper($val['vehical_type_name']); ?></option>
                        <?php endforeach;?>

                    </select>
                </div>
                <div class="group">
                    <label for="vehicle-model">Model</label>
                    <input type="text" id="vehicle-model" name="vehicle-model">
                </div>
                <div class="group">
                    <label for="vehicle-number">Vehicle registration number</label>
                    <input type="text" id="vehicle-number" name="vehicle-number">
                </div>
                <div class="group">
                    <label for="booking-date">Booking date</label>
                    <input type="text" id="booking-date" class="date-from-today" name="booking-date">
                </div>

                <h4 class="form-head">Customer Details</h4>
                <input type="hidden" name="customer_id" value="<?php echo $this->session->userdata('id');?>">  

                <div class="group">
                    <label for="customer-name">Name</label>
                    <input type="text" id="customer-name" name="customer-name" value="<?php echo $this->session->userdata('username');?>">
                </div>
                <div class="group">
                    <label for="customer-nic">NIC Number</label>
                    <input type="text" id="customer-nic" name="customer-nic" value="<?php echo $this->session->userdata('usernic');?>">
                </div>
                <div class="group">
                    <label for="mobile-number">Mobile</label>
                    <input type="text" id="mobile-number" name="mobile-number" value="<?php echo $this->session->userdata('usermobile');?>">
                </div>
                <div class="group">
                    <label for="email">Email</label>
                    <input type="email" id="email" name="email" value="<?php echo $this->session->userdata('email');?>">
                </div>
                <div class="group">
                    <label for="available-services">Available services</label>
                    <select name="available-services" id="available-services">
                        <option value="">Services Type</option>
                        <?php foreach ($service as $data): ?>
                        <option value="<?php echo $data['service_type_id']; ?>"><?php echo strtoupper($data['service_type_name']); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="group">
                    <label for="comments">Comments</label>
                    <textarea name="comments" id="comments"></textarea>
                </div>
                <div class="group button-group">
                    <button type="submit">Make an inquiry</button>
                </div>
            </form>
        </div>
    </div>
</section>
<!-- end of services form -->

	

<!-- enquire popup -->
<div class="popup enquire-popup">
    <div class="popup-container">
        <div class="left-side">
            <form action="" method="POST">
                <input type="text" placeholder="Full Name">
                <input type="email" placeholder="Email Address">
                <input type="text" placeholder="Phone">
                <textarea name="" id="" placeholder="Comments"></textarea>
                <button type="submit">send enquiry</button>
            </form>
        </div>
        <div class="right-side">
            <h5>Call directly</h5>
            <p>Call one of our agents directly to reserve this vehicle or obtain more information.</p>

            <a href="tel:1123456263">112 3456 263</a>
            <p>Contact between 9.00am - 5.00pm on weekdays and except for poyadays.</p>
        </div>
        <div class="popup-close"><i class="fa fa-close"></i></div>
    </div>
</div>
<!-- end of enquire popup -->

<!-- enquire popup -->
<div class="popup signup-popup">
    <div class="popup-container">
        <div class="left-side">
            <form action="" method="POST">
                <input type="text" placeholder="Name">
                <input type="email" placeholder="Email Address">
                <button type="submit">sign up</button>
            </form>
        </div>
        <div class="popup-close"><i class="fa fa-close"></i></div>
    </div>
</div>
<!-- end of enquire popup -->

</body>
</html>