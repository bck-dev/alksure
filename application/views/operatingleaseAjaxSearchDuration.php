
<style>
    .btn-is-disabled {
        pointer-events: none; /* Disables the button completely. Better than just cursor: default; */
    @include opacity(0.7);
    }
</style>
<!-- Add vehicle downpayments to an array -->
<?php 
$down_payment = "";
//print_r($vehicle);
foreach ($vehicle as $vehi) {
    $id = $vehi['vehicle_id'];
    $tot_Value = floor($vehi['vehicle_total_value']);
    $down_payment .= $tot_Value.',';
} 
$arr = explode(",",$down_payment);
?>
<!-- End add vehicle downpayments to an array -->
<div class="results-wrap" style="margin-left: 30px;">
    <?php foreach($vehicle as $vehi): ?>

        <?php

        $id = $vehi['vehicle_id'];
        $trans = $vehi['vehicle_transmission'];
        $millge = $vehi['vehicle_mileage_range'];
                    // $p_d_price = $vehicle['per_day_price'];
        $fuel_type = $vehi['fuel_type'];
                    // $disc = $vehicle['discount'];
        $reg_year = $vehi['year_of_registration'];
        $man_year = $vehi['year_of_manufacture'];
        $capacity = $vehi['vehicle_seating_capacity'];
        $total = $vehi['vehicle_total_value'];
        $brand = $vehi['vehicle_brand_name'];
        $model = $vehi['vehicle_model_name'];
        $image = $vehi['vehicle_vehicle_id'];
        $min_downpayment = $vehi['vehicle_minimum_downpayment'];
        $publish_status = $vehi['publish_status'];
        $reject_status = $vehi['reject_status'];
        ?>

        <!-- <div id="resultId"> -->
        <?php if ($publish_status == 1 && $reject_status == 0) { ?>
            <div class="result" id="<?php echo $id ?>" data-dpmax="<?php echo max($arr); ?>"data-mimax="<?php echo min($arr); ?>">
                <figure>
                    <div class="watermakerd">
                        
                        <img src="<?php echo base_url(); ?>upload/selling/Front/<?php echo $vehi['inside_360_images']; ?>" style="width:200px; height:150px;" alt="">
                                           
                    </div>
                </figure>
                <div class="right-side">
                    <p class="product-name"><?php echo $brand; ?> <?php echo $model;?></p>
                    <div class="price">
                        <?php $installmetValue = ($total - $downpaymentRange)/$durationRange; ?>
                        <img src="<?php echo base_url('assets/frontend'); ?>/assets/images/misc/price-tag.png" alt="">
                        <?php if($installmetValue > 0): ?>
                        	<p><span class="installmentText">Rs. <?php echo (int)$installmetValue;?></span></p>
                        <?php else: ?>
                        	<p><span class="installmentText">Rs. 0</span></p>
                        <?php endif;?>
                    </div>

                    <div class="other-amount-details">
                        <p class="vehicle-down-payment">
                            Down payment: <span><?php echo 'Rs. '.number_format($downpaymentRange); ?></span>
                        </p>
                        <p class="vehicle-duration">
                            Duration: <span class="durationText"><?php echo $durationRange;?></span><span>&nbsp;Months</span>
                        </p>
                    </div>

                    <div class="main-features">
                        <div class="feature">
                            <div class="icon">
                                <img src="<?php echo base_url('assets/frontend'); ?>/assets/images/misc/calender.png" alt="">
                            </div>
                            <div class="info">
                                <p>Year</p>
                                <p><?php echo $reg_year; ?></p>
                            </div>
                        </div>
                        <div class="feature">
                            <div class="icon">
                                <img src="<?php echo base_url('assets/frontend'); ?>/assets/images/misc/dashboard.png" alt="">
                            </div>
                            <div class="info">
                                <p>Milage</p>
                                <p><?php echo $millge; ?> Km</p>
                            </div>
                        </div>
                        <div class="feature">
                            <div class="icon">
                                <img src="<?php echo base_url('assets/frontend'); ?>/assets/images/misc/auto.png" alt="">
                            </div>
                            <div class="info">
                                <p>Transmission</p>
                                <p><?php if($trans==1){
                                                	echo "Automatic";
                                                }
                                                else if($trans==2){
                                                	echo "Manual";
                                                } 
                                                else{
                                                	echo "Triptonic";
                                                }
                                                
                                                ?></p>
                            </div>
                        </div>
                        <div class="feature">
                            <div class="icon">
                                <img src="<?php echo base_url('assets/frontend'); ?>/assets/images/misc/auto.png" alt="">
                            </div>
                            <div class="info">
                                <p>Fuel Type</p>
                                <p><?php if($fuel_type==1){
                                                	echo "Petrol";
                                                }
                                                else if($fuel_type==2){
                                                	echo "Diesel";
                                                }
                                                else if($fuel_type==3){
                                                	echo "Hybrid";
                                                }
                                                else{
                                                	echo "Electric";
                                                }
                                                
                                                ?></p>
                            </div>
                        </div>
                    </div>                                

                    <div class="cta-group">
                        <a href="<?php echo base_url();?>home/operating_result/<?php echo $vehi['vehicle_id']; ?>" class="cta btn-green">VIEW</a>
                        <a href="" class="cta enquire checkresult open-popup" data-id="<?php echo $vehi['vehicle_id']; ?>">INQUIRE</a>
                        <a href="" class="add-to-compare compare" value="<?php echo $vehi['vehicle_id']; ?>" data-hotel-name='<?php echo $vehi['vehicle_id']; ?>'>
                            <span class="plus-icon"><i class="fa fa-plus"></i></span>
                            <span class="text">compare</span>
                        </a>
                    </div>
                </div>
            </div>
            <?php } ?>
        <?php endforeach; ?>
    </div>
<!-- </div>     -->
<script src="<?php echo base_url('assets/frontend');?>/assets/js/inquery.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/lib.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/main.js"></script>

<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>

<!-- enquire popup -->

<div class="popup enquire-popup" id="enquire-popup">
    <div class="popup-container">
        <div class="left-side">
            <form action="<?php echo base_url('home/operatingInquiry')?>" method="POST">
                <input type="text" name="fulname" placeholder="Full Name">
                <input type="email" name="email" placeholder="Email Address">
                <input type="text" name="phone" placeholder="Phone">
                <textarea name="comments" id="" placeholder="Comments"></textarea>
                <input type="hidden" id="resyltInqyry" class="resyltInqyry" name="vehicle_id" value="">
                <button type="submit">send inquiry</button>
            </form>
        </div>
        <div class="right-side">
            <h5>Call directly</h5>
            <p>Call one of our agents directly to reserve this vehicle or obtain more information.</p>

            <a href="tel:1123456263">(+94) 112 3456 263</a>
            <p>Contact between 9.00am - 5.00pm on weekdays and except for poyadays.</p>
        </div>
        <div class="popup-close"><i class="fa fa-close"></i></div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>

<script type="text/javascript">

    var base_url ='http://autosure.lk/'
    var hotelName = [];
    var i =0;
    $(".compare").click(function(e) {

        e.preventDefault();
        hotelName.push( $(this).data('hotel-name') );

        var x =hotelName.toString();
        if(i>4){
            alert('only five vehicles can be selected');
        }else{
            i++;

            Cookies.set('yasas', x);
        }

        $.ajax({
            type: "POST",
            url: base_url + "home/divresultoperating",
            //dataType: "JSON",
            data: {i:i},
            success: function(result) {
                //alert(results);
                $('#operating').html(result);


            }

        });



    });

</script>