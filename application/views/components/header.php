<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Autosure</title>

    <meta name="description" content="">

    <link rel="shortcut icon" href="<?php echo base_url('assets/frontendone/'); ?>images/misc/favicon.ico">

    <link rel="stylesheet" href="<?php echo base_url('assets/frontendone/'); ?>css/lib.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/frontendone/'); ?>css/jquery-ui.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/frontendone/'); ?>css/main.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/frontendone/'); ?>css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script> 

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127654659-1"></script>
    
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127654659-1');
</script>


    <!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
</head>
<body>
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
window.fbAsyncInit = function() {
    FB.init({
    xfbml            : true,
    version          : 'v5.0'
    });
};

(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>

<!-- Your customer chat code -->
<div class="fb-customerchat"
    attribution=setup_tool
    page_id="2622732494618816"
    theme_color="#ff7e29">
</div>


<header>
    <div class="container">
        <nav>
            <div class="mobile-nav">
                <a href="<?php echo base_url('home')?>" class="brand-logo">
                    <img src="<?php echo base_url('assets/frontendone/');?>images/logo/brand-logo.png" alt="autosure">
                </a>
                <div class="mobile-toggle-button">
                    <span></span>
                </div>
            </div>
            <div class="navigation">
                <div class="navigation-inner"> <!-- this wrap is just to prevent from slideToggle() display:block style -->
                    <?php if (isset($_SESSION['id'])) { ?>
                        <div class="row">
                            <div class="col-lg-3 col-md-3">
                                <ul>
                                    <li class="hotline"><a>Hotline<br /> <span class="hotline-no">0766 377 666</span></a></li>
                                </ul>

                            </div>
                            <div class="col-lg-9 col-md-9">
                                <ul class="top-row">
                                    <li><a href="https://www.facebook.com/autosure.lk/?ref=page_internal" target="_blank"><img src="<?php echo base_url('assets/')?>fb.png" alt=""></a></li>
                                    <li><a href=""><img src="<?php echo base_url('assets/')?>insta.png" alt=""></a></li>
                                    <li><a href=""><img src="<?php echo base_url('assets/')?>youtube.png" alt=""></a></li>
                                    <li><a href="<?php echo base_url('contactus')?>">Contact us</a></li>
                                    <li><a href="" class=""><?php echo $this->session->userdata('username'); ?></a></li>
                                    <li><a href="<?php echo base_url('login_user/logout') ?>" class="" style="color: #f48324">Log Out</a></li>
                                    <li><a href="<?php echo base_url('admin')?>">User dashbord</a></li>
                                </ul>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="row">
                            <div class="col-lg-3 col-md-3">
                                <ul>
                                    <li class="hotline"><a>Hotline<br /> <span class="hotline-no">0766 377 666</span></a></li>
                                </ul>

                            </div>
                            <div class="col-lg-9 col-md-9">
                                <ul class="top-row">
                                    <li><a href="https://www.facebook.com/autosure.lk/?ref=page_internal" target="_blank"><img src="<?php echo base_url('assets/')?>fb.png" alt=""></a></li>
                                    <li><a href=""><img src="<?php echo base_url('assets/')?>insta.png" alt=""></a></li>
                                    <li><a href=""><img src="<?php echo base_url('assets/')?>youtube.png" alt=""></a></li>
                                    <li><a href="<?php echo base_url('contactus')?>">Contact us</a></li>
                                    <li><a href="" class="register">Register</a></li>
                                    <li><a href="" class="sign-in">Sign in</a></li>
                                </ul>
                            </div>
                        </div>
                    <?php } ?>
                    <ul class="bottom-row">
                        <li><a href="<?php echo base_url('home')?>">Trade-in</a></li>
                        <li><a href="<?php echo base_url('operating')?>">Operating Lease</a></li>
                        <li><a href="<?php echo base_url('rent')?>">Rent a car</a></li>
                        <li class="brand-logo-li">
                            <a href="<?php echo base_url('home')?>" class="brand-logo">
                                <img src="<?php echo base_url('assets/frontendone/');?>images/logo/brand-logo.png" alt="autosure">
                            </a>
                        </li>
                        <li><a href="<?php echo base_url('import')?>">Importing</a></li>
                        <li><a href="<?php echo base_url('bikeResults')?>">ත්‍රීවීල්</a></li>
                        <li><a href="<?php echo base_url('services')?>">Services</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>





