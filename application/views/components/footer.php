<footer>
    <div class="container">
        <div class="inner-container">
            <div class="left-side">
                <ul>
                    <li><a href="<?php echo base_url('home');?>" class="column-title">Trade-in</a></li>
                    <li><a href="<?php echo base_url('home');?>">Buy Pre-owned vehicle</a></li>
                    <li><a href="<?php echo base_url('home');?>">Sell your vehicle</a></li>
                </ul>
                <ul>
                    <li><a href="" class="column-title">services</a></li>
                    <li><a href="<?php echo base_url('rent')?>">Rent a vehicle</a></li>
                    <li><a href="<?php echo base_url('import')?>">Vehicle imports</a></li>
                    <li><a href="<?php echo base_url('services')?>">Services & repairs</a></li>
                </ul>
                <ul>
                    <li><a href="" class="column-title">leasing</a></li>
                    <li><a href="<?php echo base_url('operating')?>">Operating Leasing</a></li>
                </ul>
            </div>
            <div class="right-side">
                <ul>
                    <li><a href="" class="column-title">cars for sale</a></li>
                    <li><a href="/">Autosure</a></li>
                    <li><a href="<?php echo base_url('termscondition')?>">Terms & Conditions</a></li>
                    <li><a href="<?php echo base_url('home/privacy')?>">Privacy Policy</a></li>
                    <li><a href="<?php echo base_url('home/sitemap')?>">Sitemap</a></li>
                    <li><a href="http://bckonnect.com" class="copyright-logo" target="_blank"><img src="<?php echo base_url('assets/frontendone/');?>images/logo/bckonnect-logo.png" alt="bckonnect"></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<!-- enquire popup -->
<!--<div class="popup enquire-popup" id="enquire-popup">
    <div class="popup-container">
        <div class="left-side">
            <form action="https://google.com" method="POST">
                <input type="text" name="full_name" placeholder="Full Name">
                <input type="email" name="email" placeholder="Email Address">
                <input type="text" name="phone" placeholder="Phone">
                <textarea name="comments" id="" placeholder="Comments"></textarea>
                <button type="submit">send enquiry</button>
            </form>
        </div>
        <div class="right-side">
            <h5>Call directly</h5>
            <p>Call one of our agents directly to reserve this vehicle or obtain more information.</p>

            <a href="tel:1123456263">112 3456 263</a>
            <p>Contact between 9.00am - 5.00pm on weekdays and except for poyadays.</p>
        </div>
        <div class="popup-close"><i class="fa fa-close"></i></div>
    </div>
</div> -->
<!-- end of enquire popup -->

<!-- signup popup -->
<div class="popup signup-popup" id="signup-popup">
    <div class="popup-container">
        <div class="popup-title-wrap">
            <h3>Newsletter sign up</h3>
        </div>
        <div class="left-side">
            <form action="<?php echo base_url('home/insertsignup');?>" method="POST">
                <div class="input-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" id="name" placeholder="Enter here">
                </div>
                <div class="input-group">
                    <label for="email">email</label>
                    <input type="email" name="email" id="email" placeholder="Enter here">
                </div>
                <input type="hidden" name="uriid" value="<?php echo $this->uri->segment(2);?>">
                <button type="submit">sign up</button>
            </form>
        </div>
        <div class="popup-close"><i class="fa fa-close"></i></div>
    </div>
</div>
<!-- end of signup popup -->

<!-- signin popup -->
<div class="popup signup-popup" id="signin-popup">
    <div class="popup-container">
        <div class="popup-title-wrap">
            <h3>Sign in</h3>
        </div>
        <div class="left-side">
            <form action='<?php echo base_url(); ?>login_user/process' method='post' name='process' id="loginForm" novalidate>
                <div class="input-group">
                    <label for="name">Name</label>
                    <input type="text" value="" name="email" id="email" placeholder="Enter here">
                </div>
                <div class="input-group">
                    <label for="password">password</label>
                    <input type="password" value="" name="password" id="password" placeholder="Enter here">
                </div>
                <button type="submit">sign in</button>
            </form>
        </div>
        <div class="popup-close"><i class="fa fa-close"></i></div>
    </div>
</div>
<!-- end of signin popup -->

<!-- seller popup -->
<div class="popup enquire-popup seller-popup" id="seller-popup">
    <form action="<?php echo base_url('selling/insertvehicle'); ?>" class="popup-container" method="POST" enctype='multipart/form-data'>
    
        <div class="left-side">
            <div class="popup-title-wrap">
                <h3>Vehicle Information</h3>
            </div>

            <p>Please note that this listing will not be published until the information provided are verified by our experts in order to maintain the quality of our service.</p>

            <div class="form-groups">
                <div class="one-column">
                    <select id="typeSelect" name="vehicle_type">
                        <option value="">Type</option>
                        <?php foreach ($type as $types): ?>
                            <option value="<?php echo $types['vehicle_type_id']; ?>"><?php echo strtoupper($types['vehicle_type_name']); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="two-column">
                    <select id="makeSelect" name="vehicle_brand" disabled required>
                        <option value="">Make</option>
                        <?php foreach ($make as $makes): ?>
                            <option class="<?php echo 'type'.$makes['vehicle_type_id'].'id'; ?>" value="<?php echo $makes['vehicle_brand_id']; ?>"><?php echo strtoupper($makes['vehicle_brand_name']); ?></option>
                        <?php endforeach; ?>
                    </select>
                    <select id="modelSelect"
                            name="vehicle_model"
                            disabled
                            required>
                        <option value="">Model</option>
                        <?php foreach ($model as $models): ?>
                            <option class="<?php echo 'brand'.$models['vehicle_brand_id'].'id'; ?>"
                                    value="<?php echo $models['vehicle_model_id']; ?>"><?php echo strtoupper($models['vehicle_model_name']); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="two-column">
                    <select id="fuel_type"
                            name="fuel_type"
                            required>
                        <option value="">Fuel Type</option>
                        <option value="1">
                            PETROL
                        </option>
                        <option value="2">
                            DIESEL
                        </option>
                        <option value="3">
                            HYBRID
                        </option>
                        <option value="4">
                            ELECTRIC
                        </option>
                    </select>
                    <select name="vehicle_transmission"
                            id="vehicle_transmission"
                            required>
                        <option value="">Transmission</option>
                        <option value="1">
                            AUTO
                        </option>
                        <option value="2">
                            MANUAL
                        </option>
                        <option value="3">
                            TRIPTONIC
                        </option>
                    </select>
                </div>
                <div class="two-column">
                    <input type="text" name="year_of_manufacture" id="year_of_manufacture" required placeholder="Year of Make">
                    <input type="text" name="year_of_registration" id="year_of_registration" placeholder="Year of Registered">
                </div>
                <div class="two-column">
                    <input type="text" name="vehicle_number" id="vehicle_number" required placeholder="Registration No.">
                    <select name="number_of_owners"
                            id="number_of_owners"
                            required>
                        <option value="">Owners</option>
                        <option value="1">
                            1
                        </option>
                        <option value="2">
                            2
                        </option>
                        <option value="3">
                            3
                        </option>
                        <option value="4">
                            4 OR MORE
                        </option>
                    </select>
                </div>
                <div class="two-column">
                    <input type="number" name="vehicle_engine_capacity" id="vehicle_engine_capacity" required placeholder="Engine Capacity">
                    <input type="number" name="vehicle_mileage_range" id="vehicle_mileage_range" required placeholder="Mileage">
                </div>
                <div class="one-column">
                    <input type="number"  name="vehicle_total_value" id="vehicle_total_value" required placeholder="Price">
                </div>
                <div class="fileupload-group">
                    <div class="upload-group">
                        <input type="file" name="front_image" accept=".png,.jpg,.jpeg,.gif" id="frontfileupload" required class="inputfile">
                        <label for="frontfileupload">Add photo</label>
                        <span class="input-file-name"></span>
                    </div>
                    <div class="info-group">
                        <p>Upload Front image</p>
                
                    </div></div>
                <div class="fileupload-group">
                    <div class="upload-group">
                        <input type="file" id="fileupload" name="userfile[]" id="image_file" accept=".png,.jpg,.jpeg,.gif" required class="inputfile" data-multiple-caption="{count} files selected" multiple>
                        <label for="fileupload">Add photos</label>
                        <span class="input-file-name"></span>
                    </div>
                    <div class="info-group">
                        <p>Upload upto 5 images</p>
                        <p>Image size should be less then 3MB and JPG,JPEG,PNG,GIF file types can be uploaded.</p>
                    </div></div>
                    
            </div>
        </div>
        <div class="right-side">
            <div class="popup-title-wrap">
                <h3>Seller Information</h3>
            </div>
            <p>Below information will be only used by our experts to contact you and will not be published or shared with outside parties.</p>

            <div class="form-groups">
                <input type="text"  id="customer_name" name="customer_name" value="<?php echo $this->session->userdata('username'); ?>" required placeholder="Full Name">
                <input type="email"  value="<?php echo $this->session->userdata('email'); ?>" name="email" id="email" required placeholder="Email Address">
                <input type="text" name="phone_number" pattern="^[0-9]{6}|[0-9]{8}|[0-9]{10}$" value="<?php echo $this->session->userdata('contactnumber'); ?>" required placeholder="Phone">
                <textarea  name="address_line_1" id="address_line_1" value="<?php echo $this->session->userdata('address'); ?>" required placeholder="Address"></textarea>
                <select id="address_city"
                        name="address_city"
                        required aria-placeholder="District">
                    <option value="">District</option>
                    <option value="ampara">Ampara</option>
                    <option value="anuradhapura">Anuradhapura</option>
                    <option value="badulla">Badulla</option>
                    <option value="batticaloa">Batticaloa</option>
                    <option value="colombo">Colombo</option>
                    <option value="galle">Galle</option>
                    <option value="gampaha">Gampaha</option>
                    <option value="hambantota">Hambantota</option>
                    <option value="jaffna">Jaffna</option>
                    <option value="kalutara">Kalutara</option>
                    <option value="kandy">Kandy</option>
                    <option value="kegalle">Kegalle</option>
                    <option value="kilinochchi">Kilinochchi</option>
                    <option value="kurunegala">Kurunegala</option>
                    <option value="mannar">Mannar</option>
                    <option value="matale">Matale</option>
                    <option value="matara">Matara</option>
                    <option value="monaragala">Monaragala</option>
                    <option value="mullaitivu">Mullaitivu</option>
                    <option value="nuwara eliya">Nuwara Eliya</option>
                    <option value="polonnaruwa">Polonnaruwa</option>
                    <option value="puttalam">Puttalam</option>
                    <option value="ratnapura">Ratnapura</option>
                    <option value="trincomalee">Trincomalee</option>
                    <option value="vavuniya">Vavuniya</option>
                </select>
                <?php if (!isset($_SESSION['id'])) { ?>
                <div class="radio-group">
                    <input type="radio"  name="rdoName" class="registeruser" id="registeruser" value="1" required >
                    <label for="existing-user">Registered user</label>
                </div>

                <div class="radio-group">

                    <input type="radio" name="rdoName" class="notregisteruser" id="registeruser" value="2" required >
                    <label for="new-user">New user</label>
                </div>

                <div class="existing-user-block selform1" id="selform1">
                    <input type="password"  class="passwordselluserr" name="passwordreg" disabled="disabled" id="passwordselluser"
                           required placeholder="Password">
                </div>


                <div class="new-user-block selform2" id="selform2">
                    <input type="password" class="passwordsell" name="password" id="password1"
                           required disabled="disabled" placeholder="Password" >
                    <input type="password" class="passwordsell" name="confirm_password" id="confirm_password1"
                           required disabled="disabled" placeholder="Confirm password" >
                           <span id='message'></span>
                </div>

                <?php } ?>
                <button type="submit">submit</button>

                <p>One of our agents will contact you within 3-4 working days between 9.00am - 5.00pm.</p>
            </div>
        </div>
        <div class="popup-close"><i class="fa fa-close"></i></div>
    
    </form>
</div>
<!-- end of seller popup -->

<!-- register popup -->
<div class="popup signup-popup register-popup" id="register-popup">
    <div class="popup-container">
        <div class="popup-title-wrap">
            <h3>Register</h3>
        </div>
        <div class="message-alert-area"
             id="register-create-msg"
             style="background-color: rgb(255,159,16); width: 100%;  border-radius: 5px;  margin-bottom: 10px;">

            <?php echo validation_errors('<p class="error" >', '</p>'); ?>
        </div>
        <div class="left-side">
            <form id="register-form"  method='post' class="message-alert-area" data-parsley-validate name='register'>
                <div class="input-group multiple-input ">
                    <label for="first-name">Name</label>
                    <div class="inner-multiple-input">
                    <input type="text" id="first_name" class="firstname" name="fname" required value="<?php echo set_value('fname'); ?>" placeholder="First Name">
                    <input type="text" name="last_name" id="lname" required value="<?php echo set_value('lname'); ?>" placeholder="Last Name">
                    </div>
                </div>
                <div class="input-group">
                    <label for="nic">NIC</label>
                    <input type="text" name="nic" id="nic" required value="<?php echo set_value('nic'); ?>" placeholder="Enter here">
                </div>
                <div class="input-group address-group">
                    <label for="address">Address</label>
                    <textarea name="address" id="address" required value="<?php echo set_value('address'); ?>" placeholder="Enter here"></textarea>
                </div>
                <div class="input-group">
                    <label for="phone">Phone</label>
                    <input type="text" name="number" id="number" required value="<?php echo set_value('number'); ?>" placeholder="Enter here">
                </div>
                <div class="input-group">
                    <label for="email">Email</label>
                    <input type="email" name="email" id="regemail" required value="<?php echo set_value('email'); ?>" placeholder="Enter here">
                </div>
                <div class="input-group">
                    <label for="password">Password</label>
                    <input type="password"  name="password" id="regpassword" required value="<?php echo set_value('password'); ?>" placeholder="Enter here">
                </div>
                <div class="input-group">
                    <label for="confirm-password">Confirm Password</label>
                    <input type="password"  name="cconfirm_password" id="cconfirm_password" required value="<?php echo set_value('confirm_password'); ?>" placeholder="Enter here">
                    <div class="message-alert-area"
                         id="password-match">

                        <?php echo validation_errors('<p class="error" >', '</p>'); ?>
                    </div>
                </div>
                <button type="submit" id="registerbutton" class="BtnIsSubmit"
                        value="Register">Register</button>
            </form>
        </div>
        <div class="popup-close"><i class="fa fa-close"></i></div>
    </div>
</div>
<!-- end of register popup -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<script type="text/javascript">


    <?php if($this->session->flashdata('success')){ ?>
    toastr.success("<?php echo $this->session->flashdata('success'); ?>");
    <?php }else if($this->session->flashdata('error')){  ?>
    toastr.error("<?php echo $this->session->flashdata('error'); ?>");
    <?php }else if($this->session->flashdata('warning')){  ?>
    toastr.warning("<?php echo $this->session->flashdata('warning'); ?>");
    <?php }else if($this->session->flashdata('info')){  ?>
    toastr.info("<?php echo $this->session->flashdata('info'); ?>");
    <?php } ?>


</script>




<script src="<?php echo base_url('assets/frontend/'); ?>assets/js/inquery.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/lib.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/main.js"></script>

<script src="<?php echo base_url('assets/frontend/');?>javascripts/script.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/plugins/rangeslider.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/plugins/tablesaw.jquery.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/plugins/tablesaw-init.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/plugins/jquery-ui.min.js"></script>


<script src="<?php echo base_url('assets/frontendone/');?>js/plugins/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/plugins/wow.min.js"></script>

<script src="<?php echo base_url('assets/frontend/assets/');?>js/script.js"></script>
<script src="<?php echo base_url('assets/frontend/');?>javascripts/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/script.js"></script>




 <script>
    $(document).on('change', '#typeSelect', function(){
        $('#makeSelect option:first').prop('selected', 'selected');
        if ($(this).val().length > 0) {
            var t_name = "type" + $(this).val() + "id";
            $("#makeSelect").prop("disabled", false);
            $("#makeSelect").children('option').hide();
            $("#makeSelect").children("option[class^=" + t_name + "]").show();
        }
        else{
            $("#makeSelect").prop("disabled", true);
        }
    });

    $(document).on('change', '#makeSelect', function(){
        $('#modelSelect option:first').prop('selected', 'selected');
        if ($(this).val().length > 0){
            var b_name = "brand" + $(this).val() + "id";
            $("#modelSelect").prop("disabled", false);
            $("#modelSelect").children('option').hide();
            $("#modelSelect").children("option[class^=" + b_name + "]").show();
            //alert(b_name);
        }
        else{
            $("#modelSelect").prop("disabled", true);
        }
    });
</script> 
</body>
</html>