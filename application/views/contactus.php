

<div class="page-content">
    <!-- contact section -->
    <section class="contact">
        <div class="container">
            <div class="contact-container">
                <div class="branch-row">
                    <!--<div class="branch-block">-->
                    <!--    <h3>Autosure Kohuwala</h3>-->
                    <!--    <p>No.178, Dutugemunu Street, Kohuwala</p>-->
                    <!--    <p>Tel: <a href="tel:0115961000">011 59 61 000</a></p>-->
                    <!--    <p>Sam: <a href="tel:0772107385">077 21 07 385</a></p>-->
                    <!--    <p><a href="mailto:sam@alliancefinance.lk">sam@alliancefinance.lk</a></p>-->
                    <!--</div>-->
                    <div class="branch-block">
                        <h3>Autosure Koswatte</h3>
                        <p>No.356, Kaduwela Road, Battaramulla, Koswatte</p>
                        <p>Tel: <a href="tel:0115934775">011 59 61 000 / 011 59 34 775</a></p>
                        <p>Hotline: <a href="tel:0771090176">(+94) 766 377 666</a></p>
                        <p><a href="mailto:lalithl@alliancefinance.lk">lalithl@alliancefinance.lk</a></p>
                    </div>
                    <div class="branch-block">
                        <h3>Autosure Kandy</h3>
                        <p>No.130, Katugasthota Road, Kandy</p>
                        <p>Tel: <a href="tel:0815277200">011 59 61 000 / 081 52 77 200</a></p>
                        <p>Hotline: <a href="tel:0764425296">(+94) 766 377 666 </a></p>
                        <p><a href="mailto:dananjaya@alliancefinance.lk">dananjaya@alliancefinance.lk</a></p>
                    </div>
                </div>
                <form action="<?php echo base_url('home/insertcontact'); ?>" method="POST">
                    <div class="input-group">
                        <label for="fullname">Full Name</label>
                        <input type="text" id="fullname" name="name">
                    </div>
                    <div class="input-group">
                        <label for="phone">Phone</label>
                        <input type="text" id="phone" name="phone">
                    </div>
                    <div class="input-group">
                        <label for="email">Email Address</label>
                        <input type="email" id="email" name="email">
                    </div>
                    <div class="input-group">
                        <label for="message">Message</label>
                        <textarea name="message" id="message" ></textarea>
                    </div>
                    <button type="submit">Send Message</button>
                </form>
            </div>
        </div>
    </section>
    <!-- end of contact section -->
