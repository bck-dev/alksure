<body>

<div class="da-da AppMainBody">
    <div class="da-da wh100">
        <div class="da-da wh100">
            
            <!--body starts-->
            <div class="da-da AppMainBodyHl">
                <div class="da-da wh100">
                    <div class="da-da wh100">
                    
                        
                   
                                   
                                                  
                                                   
                                    
                                    
                                    </div>
                                </div>
                            </div>
                            <!--bottom menu-->
                                
                                
                           
                                    
                 
                        
                        
                    <!--container-->
                    <div class="da-da container">
                        
                   
                    
                    <!--header-->
                    <div class="da-da DifFontHeader maxwidth980">
                        <div class="da-da wh100">
                            <div class="da-da wh100">
                            <h2 class="da-da DifFontHeaderH3" style="text-align: left; font-size:18px;">Terms and Condition</h2>
                            <p class="da-da DifFontHeaderP" >
                              
 <p class="da-da DifFontHeaderH3" style="text-align: left; font-size:15px;">Introduction</p>
 <p style="font-size:12px;">
These terms and conditions govern your use of this website; by using this website,
you accept these terms and conditions in full. If you disagree with these terms and
conditions or any part of these terms and conditions, you must not use this
website.</p>
 <p class="da-da DifFontHeaderH3"  style="text-align: left; font-size:15px;">License to use website</p>
<p style="font-size:12px;">
Unless otherwise stated, Alliance Finance Company PL /or its licensors own the
intellectual property rights in the website and material on the website. Subject to
the license below, all these intellectual property rights are reserved.
You may view, download for caching purposes only, and print pages from the
website for your own personal use, subject to the restrictions set out below and
elsewhere in these terms and conditions.
You must not:</p>
<p style="margin-left: 20px; font-size:12px;">
• Republish material from this website (including republication on another
website);<br>

• Sell, rent or sub-license material from the website;<br>
• Show any material from the website in public;<br>
• Reproduce, duplicate, copy or otherwise exploit material on this website for a
commercial purpose;<br>
• Edit or otherwise modify any material on the website; or<br>
• Redistribute material from this website (except for content specifically and
expressly made available for redistribution)
</p>
<p class="da-da DifFontHeaderH3" style="text-align: left; font-size:15px;">Acceptable use</p>
<p style="font-size:12px;">
You must not use this website in any way that causes, or may cause, damage to the
website or impairment of the availability or accessibility of the website; or in any
way which is unlawful, illegal, fraudulent or harmful, or in connection with any
unlawful, illegal, fraudulent or harmful purpose or activity.
You must not use this website to copy, store, host, transmit, send, use, publish or
distribute any material which consists of (or is linked to) any spyware, computer 
virus, Trojan horse, worm, keystroke logger, rootkit or other malicious computer
software.
You must not conduct any systematic or automated data collection activities
(including without limitation scraping, data mining, data extraction and data
harvesting) on or in relation to this website without Alliance Finance Company PLC
express written consent.
You must not use this website to transmit or send unsolicited commercial
communications.
You must not use this website for any purposes related to marketing without
Alliance Finance Company PLC express written consent.
</p>
<p class="da-da DifFontHeaderH3" style="text-align: left; font-size:15px;">No warranties</p>
<p style="font-size:12px;">
    This website is provided “as is” without any representations or warranties, express
or implied. Alliance Finance Company PLC makes no representations or warranties
in relation to this website or the information and materials provided on this website.
Without prejudice to the generality of the foregoing paragraph, Alliance Finance
Company PLC does not warrant that:</p>
<p style="margin-left: 20px; font-size:12px;">
• This website will be constantly available, or available at all; or<br>
• Information on this website is complete, true, accurate or non-misleading.</p>
<p style="font-size:12px;">
Nothing on this website constitutes, or is meant to constitute, advice of any kind. If
you require advice in relation to any [legal, financial or medical] matter you should
consult an appropriate professional.</p>


<p class="da-da DifFontHeaderH3" style="text-align: left; font-size:15px;">Limitations of liability</p>
<p style="font-size:12px;">
Alliance Finance Company PLC will not be liable to you (whether under the law of
contract, the law of tort or otherwise) in relation to the contents of, or use of, or
otherwise in connection with, this website:</p>
<p style="margin-left: 20px; font-size:12px;">
• To the extent that the website is provided free-of-charge, for any direct loss;<br>
• For any indirect, special or consequential loss; or<br>
• For any business losses, loss of revenue, income, profits or anticipated</p>
<p style="font-size:12px;">
savings, loss of contracts or business relationships, loss of reputation or
goodwill, or loss or corruption of information or data.
These limitations of liability apply even if Alliance Finance Company PLC has been
expressly advised of the potential loss.</p>

<p class="da-da DifFontHeaderH3" style="text-align: left; font-size:15px;">Links to third party websites</p>
<p style="font-size:12px;">
autosure.lk may contain links or references to other websites ('Third Party Websites'). autosure.lk or its owners shall not be responsible for the contents in Third Party Websites. Third Party Websites are not investigated or monitored. In the event the user decides to leave autosure.lk and access Third Party Sites, the user does so at his/her own risk.
autosure.lk does not provide any guarantees that Sellers and or advertisers have been truthful or accurate in their advertisements, they have items in stock, will accept the return of an item or product, or provide any refunds, or that Buyers will complete a transaction successfully</p>
<p class="da-da DifFontHeaderH3" style="text-align: left;font-size:15px;">Exceptions</p>
<p style="font-size:12px;">
Nothing in this website disclaimer will exclude or limit any warranty implied by law
that it would be unlawful to exclude or limit; and nothing in this website disclaimer
will exclude or limit Alliance Finance Company PLC liability in respect of any:</p>
<p style="margin-left: 20px; font-size:12px;">
• Death or personal injury caused by Alliance Finance Company PLC
negligence;<br>
• Fraud or fraudulent misrepresentation on the part of Alliance Finance
Company PLC or<br>
• Matter which it would be illegal or unlawful for Alliance Finance Company
PLC to exclude or limit, or to attempt or purport to exclude or limit, its
liability.</p>
<p class="da-da DifFontHeaderH3" style="text-align: left; font-size:15px;">Reasonableness</p>
<p style="font-size:12px;">
By using this website, you agree that the exclusions and limitations of liability set
out in this website disclaimer are reasonable.
If you do not think they are reasonable, you must not use this website</p>
<p class="da-da DifFontHeaderH3" style="text-align: left; font-size:15px;">Other parties</p>
<p style="font-size:12px;">
You accept that, as a limited liability entity, Alliance Finance Company PLC has an
interest in limiting the personal liability of its officers and employees. You agree
that you will not bring any claim personally against Alliance Finance Company PLC
officers or employees in respect of any losses you suffer in connection with the
website.
Without prejudice to the foregoing paragraph, you agree that the limitations of
warranties and liability set out in this website disclaimer will protect Alliance
Finance Company PLC officers, employees, agents, subsidiaries, successors,
assigns and sub-contractors as well as Alliance Finance Company PLC.</p>

<p class="da-da DifFontHeaderH3" style="text-align: left; font-size:15px;">Unenforceable provisions</p>
<p style="font-size:12px;">
If any provision of this website disclaimer is, or is found to be, unenforceable under
applicable law, that will not affect the enforceability of the other provisions of this
website disclaimer.</p>
<p class="da-da DifFontHeaderH3" style="text-align: left; font-size:15px;">Indemnity</p>
<p style="font-size:12px;">
    You hereby indemnify Alliance Finance Company PLC and undertake to keep
Alliance Finance Company PLC indemnified against any losses, damages, costs,
liabilities and expenses (including without limitation legal expenses and any
amounts paid by Alliance Finance Company PLC to a third party in settlement of a
claim or dispute on the advice of Alliance Finance Company PLC legal advisers)
incurred or suffered by Alliance Finance Company PLC arising out of any breach by
you of any provision of these terms and conditions[, or arising out of any claim that
you have breached any provision of these terms and conditions.</p>

<p class="da-da DifFontHeaderH3" style="text-align: left; font-size:15px;">Breaches of these terms and conditions</p>
<p style="font-size:12px;">
    Without prejudice to Alliance Finance Company PLC other rights under these terms
and conditions, if you breach these terms and conditions in any way, Alliance
Finance Company PLC may take such action as Alliance Finance Company PLC
deems appropriate to deal with the breach, including suspending your access to the
website, prohibiting you from accessing the website, blocking computers using
your IP address from accessing the website, contacting your internet service
provider to request that they block your access to the website and/or bringing court
proceedings against you.</p>
<p class="da-da DifFontHeaderH3" style="text-align: left; font-size:15px;">Variation</p>
<p style="font-size:12px;">
    Alliance Finance Company PLC may revise these terms and conditions from timeto-time.
Revised terms and conditions will apply to the use of this website from the
date of the publication of the revised terms and conditions on this website. Please
check this page regularly to ensure you are familiar with the current version.</p>
<p class="da-da DifFontHeaderH3" style="text-align: left;font-size:15px;">Assignment</p>
<p style="font-size:12px;">
    Alliance Finance Company PLC may transfer, sub-contract or otherwise deal with
Alliance Finance Company PLC rights and/or obligations under these terms and
conditions without notifying you or obtaining your consent.
You may not transfer, sub-contract or otherwise deal with your rights and/or
obligations under these terms and conditions.</p>
<p class="da-da DifFontHeaderH3" style="text-align: left; font-size:15px;">Severability</p>
<p style="font-size:12px;">
    If a provision of these terms and conditions is determined by any court or other
competent authority to be unlawful and/or unenforceable, the other provisions will
continue in effect. If any unlawful and/or unenforceable provision would be lawful
or enforceable if part of it were deleted, that part will be deemed to be deleted, and
the rest of the provision will continue in effect.</p>
<p class="da-da DifFontHeaderH3" style="text-align: left;font-size:15px;">Entire agreement</p>
<p style="font-size:12px;">
    These terms and conditions constitute the entire agreement between you and
Alliance Finance Company PLC in relation to your use of this website, and
supersede all previous agreements in respect of your use of this website.</p>
<p class="da-da DifFontHeaderH3" style="text-align: left;font-size:15px;">Law and jurisdiction</p>
<p style="font-size:12px;">
    These terms and conditions will be governed by and construed in accordance with
Law of Sri Lanka, and any disputes relating to these terms and conditions will be
subject to the non-exclusive jurisdiction of the courts of Sri Lanka.</p>
<p class="da-da DifFontHeaderH3" style="text-align: left;font-size:15px;">Amendment</p>
<p style="font-size:12px;">
    Parties hereby declare that from time to time by mutual consent that may formulate
alter and amend the terms and conditions contained herein. No such amendment to
this agreement shall be effective unless it is writing and signed by the parties
hereto.</p>
<p class="da-da DifFontHeaderH3" style="text-align: left;font-size:15px;">Force Majeure </p>
<p style="font-size:12px;">
    In case the performance of the respective obligations under this agreement is
prevented by circumstances beyond the reasonable control of either Party such as
but not limited to lockouts, strike, fire flood, action of elements, riots, sabotage or
any other acts of god, then in that case, such failure in performance shall not be
construed as or otherwise constitute as a default or violation of the terms of these
Presents.</p>
                                </p>
                              
                            </div>
                        </div>
                    </div>
                    <!--header-->
                        
                        
                     </div>
                    <!--container-->
                        
                        
             
                          
                        
                       
                  
                        
                        
                    </div>
                </div>
            </div>
            <!--body starts-->
            
        </div>
    </div>
</div>
    
    
</body>
</html>