<script https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js></script>

 <?php foreach($vehicle as $vehi){
 
$id = $vehi['vehicle_id'];
$trans = $vehi['vehicle_transmission'];
$millge = $vehi['vehicle_mileage_range'];
// $p_d_price = $vehicle['per_day_price'];
$fuel_type = $vehi['fuel_type'];
// $disc = $vehicle['discount'];
$reg_year = $vehi['year_of_registration'];
$man_year = $vehi['year_of_manufacture'];
$capacity = $vehi['vehicle_seating_capacity'];
$total = $vehi['vehicle_total_value'];
$brand = $vehi['vehicle_brand_name'];
$model1 = $vehi['vehicle_model_name'];
$image = $vehi['vehicle_vehicle_id'];
$Engine_cap = $vehi['vehicle_engine_capacity'];
$ave_fual_con = $vehi['average_of_fuel_consuption'];
$owners = $vehi['number_of_owners'];
$desc = $vehi['sellers_description'];
$min_down_payment = $vehi['vehicle_minimum_downpayment'];
$vehicle_status = $vehi['vehicle_status'];
$warranty = $vehi['warranty'];
}
?>


<section class="search-results view-vehicle import-lease">
    <div class="container">
        <div class="results-container">
            <div class="column test-column">
                <div class="test-drive">
                    <h4>Book a Test Drive</h4>

                    <div class="test-drive-info">
                        <div class="reservation-block">
                            <a href="tel:01123456263">(+94) 766 377 666</a>
                            <p>Call for reservations</p>
                        </div>

                        <p>We bring the vehicle to your door-step for inspections and for a test drive.</p>

                        <p class="note">*Conditions Apply</p>
                    </div>
                </div>
            </div>
            <div class="column results-column">
                <a href="<?php echo base_url();?>home/operating" class="back-to"><i class="fa fa-angle-left"></i> Back to results</a>

                <div class="single-vehicle">
                    <p class="product-name"><?php echo $brand; ?> <?php echo $model1;?></p>
                    <p class="ref"> Veh/OL/Veh/<?php echo $id;?></p>

                    <div class="details">
                        <div class="single-vehicle-group">
                             <div class="owl-carousel single-vehicle-slider" data-slider-id="1">
                                 <div class="item" style="background-image: url(<?php echo base_url(); ?>upload/selling/Front/<?php echo $vehi['inside_360_images']; ?>)">
                                    <?php if($vehicle_status==8): ?>
                                        <img src="<?php echo base_url('assets/');?>sold.png" style="width: 50%; position: absolute; bottom: -10px; right: -10px;" alt="autosure">
                                    <?php endif; ?>
                                 </div>
                                <?php foreach ($images as $inside): ?>
                            <?php if ($inside['vehicle_vehicle_id'] == $id): ?>
                                <div class="item" style="background-image: url(<?php echo base_url(); ?>upload/selling/<?php echo $inside['inside_images_name']; ?>)">
                                     <?php if($vehicle_status==8): ?>
                                            <img src="<?php echo base_url('assets/');?>sold.png" style="width: 50%; position: absolute; bottom: -10px; right: -10px;" alt="autosure">
                                     <?php endif; ?>
                                </div>
                               
                        <?php endif ?>
                    <?php endforeach ?>
                            </div>
                            
                            <div class="owl-thumbs" data-slider-id="1">
                                <button class="owl-thumb-item" style="background-image: url(<?php echo base_url(); ?>upload/selling/Front/<?php echo $vehi['inside_360_images']; ?>)"></button>
                               <?php foreach ($images as $inside): ?>
                            <?php if ($inside['vehicle_vehicle_id'] == $id): ?>
                                <button class="owl-thumb-item" style="background-image: url(<?php echo base_url(); ?>upload/selling/<?php echo $inside['inside_images_name']; ?>)"></button>
                                <?php endif ?>
                                   <?php endforeach ?>
                            </div>                             
                        </div>  

                        <div class="right-side">

                            <div class="main-features">
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url('assets/frontend');?>/assets/images/misc/calender.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Year</p>
                                        <p><?php echo $reg_year; ?></p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url('assets/frontend');?>/assets/images/misc/dashboard.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Milage</p>
                                        <p><?php if($vehi['millage_range_unit']==1){
                                        echo $vehi['vehicle_mileage_range']. "Km";
                                    }
                                    else{
                                        echo $vehi['vehicle_mileage_range']. "Ml";
                                    }
                                    ?></p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url('assets/frontend');?>/assets/images/misc/auto.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Transmission</p>
                                        <p><?php if($trans==1){
                                                	echo "Automatic";
                                                }
                                                else if($trans==2){
                                                	echo "Manual";
                                                } 
                                                else{
                                                	echo "Triptonic";
                                                }
                                                
                                                ?></p>
                                    </div>
                                </div>
                                <div class="feature">
                                    <div class="icon">
                                        <img src="<?php echo base_url('assets');?>/fuel.png" alt="">
                                    </div>
                                    <div class="info">
                                        <p>Fuel Type</p>
                                        <p><?php if($fuel_type==1){
                                                	echo "Petrol";
                                                }
                                                else if($fuel_type==2){
                                                	echo "Diesel";
                                                }
                                                else if($fuel_type==3){
                                                	echo "Hybrid";
                                                }
                                                else{
                                                	echo "Electric";
                                                }
                                                
                                                ?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="price">
                                <img src="<?php echo base_url('assets/frontend');?>/assets/images/misc/price-tag.png" alt="">
                                <p>Rs. <?php echo number_format($total);?></p>
                            </div>

                             <div>
                                <img src="<?php echo base_url('assets/')?>drive-away.png" alt="">
                             </div>

                            <div class="cta-group">
                                <a href="" class="cta checkresult enquire open-popup" data-id="<?php echo $vehi['vehicle_id']; ?>">INQUIRE</a>
                               <!-- <a href="" class="add-to-compare">
                                    <span class="plus-icon"><i class="fa fa-plus"></i></span>
                                    <span class="text">compare</span>-->
                                </a>
                            </div>
                             <?php if($vehi['warranty']==5): ?>
                                 <div>
                                    <img src="<?php echo base_url('assets/')?>warranty.png" alt="">
                                 </div>
                             <?php endif; ?>
                             
                             <?php if($vehi['autosure_certified']==1): ?>
                                <div>
                                    <img src="<?php echo base_url('assets/')?>certified.png" class="certified-big" alt=""/>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>                    
                </div>
	
	<?php if($desc!=NULL): ?>
                <div class="description-wrap">
                    <h5>Sellers Description</h5>

                    <p><?php echo $desc;?></p>
                </div>
<?php endif; ?>
                <div class="more-details">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#details" data-toggle="tab">Details</a></li>
                        <li><a href="#features" data-toggle="tab">Features</a></li>
                    </ul>
                    
                    <div class="tab-content">
                        <div id="details" class="tab-pane fade in active">
                            <div class="detail">
                                <div class="detail-name">Make</div>
                                <div class="detail-description"><?php echo $brand;?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Model</div>
                                <div class="detail-description"><?php echo $model1;?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Transmission</div>
                                <div class="detail-description"><?php if($trans==1){
                                                	echo "Automatic";
                                                }
                                                else if($trans==2){
                                                	echo "Manual";
                                                } 
                                                else{
                                                	echo "Triptonic";
                                                }
                                                
                                                ?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Mileage Range</div>
                                <div class="detail-description"><?php if($vehi['millage_range_unit']==1){
                                        echo $vehi['vehicle_mileage_range']. "Km";
                                    }
                                    else{
                                        echo $vehi['vehicle_mileage_range']. "Ml";
                                    }
                                    ?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Engine Capacity</div>
                                <div class="detail-description"><?php if($vehi['engine_capacity_unit']==1){ 
                                        echo $vehi['vehicle_engine_capacity']. "CC";
                                    }
                                    else{
                                        echo $vehi['vehicle_engine_capacity']. "kW";
                                    }
                                    ?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Seating Capacity</div>
                                <div class="detail-description"><?php echo $capacity;?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Year of manufacture</div>
                                <div class="detail-description"><?php echo $man_year;?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Year of registration</div>
                                <div class="detail-description"><?php echo $reg_year;?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Fuel type</div>
                                <div class="detail-description"><?php if($fuel_type==1){
                                                	echo "Petrol";
                                                }
                                                else if($fuel_type==2){
                                                	echo "Diesel";
                                                }
                                                else if($fuel_type==3){
                                                	echo "Hybrid";
                                                }
                                                else{
                                                	echo "Electric";
                                                }
                                                
                                                ?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Average fuel consumption</div>
                                <div class="detail-description"><?php echo $ave_fual_con;?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Number of Owners</div>
                                <div class="detail-description"><?php echo $owners;?></div>
                            </div>
                            <div class="detail">
                                <div class="detail-name">Total Value</div>
                                <div class="detail-description"><?php echo number_format($total);?></div>
                            </div>
                            <!-- <div class="detail">
                                <div class="detail-name">Instalment</div>
                                <div class="detail-description"></div>
                            </div> -->
                            <div class="detail">
                                <div class="detail-name">Minimum Downpayment</div>
                                <div class="detail-description"><?php echo number_format($min_down_payment);?></div>
                            </div>
                            <!-- <div class="detail">
                                <div class="detail-name">Duration</div>
                                <div class="detail-description"></div>
                            </div> -->
                            <div class="detail">
                                <div class="detail-name">Total Value</div>
                                <div class="detail-description">
                                    <form action="">
                                        <input type="radio" name="total-value-input" id="input-value-out-right" value="out-right" checked> <label for="input-value-out-right">Out right</label>
                                        <input type="radio" name="total-value-input" id="input-value-lease" value="lease"> <label for="input-value-lease">Lease</label>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div id="features" class="tab-pane fade">
                             <?php $count=0;?>
                        <?php $varr; 
                            $varr1;
                        ?>
                        <?php foreach($vehicle as $vehi): ?>
                            <?php 
                           $vehicle_features1 = $vehi['vehicle_features'];
                           $additional_features = $vehi['additional_features'];
                       
                             ?>
                        

                        <?php
                        
                         $varr = explode(",",$vehicle_features1);
                         $varr1 = explode(",",$additional_features);
                        ?>
                        <ul class="feature">
                                <?php foreach ($varr as $var): ?>   
                            
                                <?php if($count%4==0){?> 
                                </ul><ul class="feature">
                                 <?php }?>
                                <li><a href=""><?php print_r($var); ?></a></li><br>
                                
                               
				<?php $count++; ?>
                                <?php endforeach ?>
                                
                                 <?php if(!empty($varr1)) : ?>
                                
                                <?php foreach ($varr1 as $var1): ?>
                                
                                <?php if($var1 !== "") : ?>
                                
                                <?php if($count%4==0){?> 
                                    </ul><ul class="feature">
                                 <?php } ?>
                                  
                                <li><a href="">
                        <?php print_r ($var1);?></a></li><br>
                                
                               <?php endif; ?>
				<?php $count++; ?>
                                <?php endforeach ?>
                                <?php endif; ?>
                            </ul>
                            <?php break; ?>
                             <?php endforeach ?>
                        </div>
                        
                    </div>                    
                </div>

            <!-- lease calculator -->
            <div class="lease-calculator-wrap vehicle-import-lease-calculator">
                <div class="bars">
                    <div class="group">
                        <h4>Lease amount</h4>
                        <div class="bar">
                            <input 
                                type="range" 
                                class="lease-amount-bar" 
                                min="100000"                    
                                max="10000000"                  
                                step="50000"                   
                                value="100000">
                            <div class="min-value">min</div>
                            <div class="selected-value">Rs. <span class="amount">0</span></div>
                            <div class="max-value">max</div>                            
                        </div>
                    </div>
                    <div class="group">
                        <h4>Duration</h4>
                        <div class="bar">
                            <input
                                type="range" 
                                class="duration-bar" 
                                min="6"                    
                                max="60"                  
                                step="6"                   
                                value="6">
                            <div class="min-value">min</div>
                            <div class="selected-value"><span class="months"></span> Months</div>
                            <div class="max-value">max</div>
                        </div>  
                    </div>
                </div>

                <p class="monthly-instalment">Monthly Instalment: Rs. <span class="instalment">0</span></p>
            </div>
            <!-- end of lease calculator -->

            </div>
             <div class="column ads-column">
                 <?php foreach ($ad as $a){ ?>
                     <a href="<?php echo $a['link'] ?>" class="ads-block" target="_blank">
                        <img src="<?php echo base_url(); ?>upload/advertisement/<?php echo $a['ad_image_name']; ?>" >
                     </a>
                 <?php } ?>
             </div>
        </div>        
    </div>
</section>
<!-- end of search results -->


<script src="<?php echo base_url('assets/frontend');?>/assets/js/inquery.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/lib.js"></script>
<script src="<?php echo base_url('assets/frontendone/');?>js/main.js"></script>
    
<!-- enquire popup -->

<div class="popup enquire-popup" id="enquire-popup">
    
    <div class="popup-container">
        <div class="left-side">
            <form action="<?php echo base_url('home/operatingInquiry')?>" method="POST">
                <input type="text" name="fulname" placeholder="Full Name">
                <input type="email" name="email" placeholder="Email Address">
                <input type="text" name="phone" placeholder="Phone">
                <textarea name="comments" id="" placeholder="Comments"></textarea>
                <input type="hidden" id="resyltInqyry" class="resyltInqyry" name="vehicle_id" value="">
                <button type="submit">send inquiry</button>
            </form>
        </div>
        <div class="right-side">
            <h5>Call directly</h5>
            <p>Call one of our agents directly to reserve this vehicle or obtain more information.</p>

            <a href="tel:0766377666">(+94) 766 377 666</a>
            <p>Contact between 9.00am - 5.00pm on weekdays and except for poyadays.</p>
        </div>
        <div class="popup-close"><i class="fa fa-close"></i></div>
    </div>
</div>