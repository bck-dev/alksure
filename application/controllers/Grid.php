<?php


class Grid extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->check_isvalidated();
    }

    public function template($headerData, $sidebarData, $page, $mainData, $footerData)
    {
        $this->load->view('admin/components/header', $headerData);
        $this->load->view('admin/components/sidebar', $sidebarData);
        $this->load->view($page, $mainData);
        $this->load->view('admin/components/footer', $footerData);
    }

    //validate session start
    private function check_isvalidated()
    {
        if (!$this->session->userdata('validated')) {
            redirect('login');
        }
    }
    //validate session end

    //add grid  function start --ishara sewwandi
    public function addgrid()
    {
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/grid/addgrid';
        $mainData = array(
            'pagetitle' => 'Add New grid',

        );

        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }

    //insert grid to database for 'grid' ---ishara sewwandi
    public function insertgrid()
    {
        $data = array(

            'image' =>$this->setting_model->upload('grid_image', 'upload/grid'),
            'title' => $this->input->post('title'),
            'description' => $this->input->post('description'),
            'link' => $this->input->post('link'),
        );

        $insert_id = $this->setting_model->insert($data, 'grid');


        $this->session->set_flashdata('msg', '<div class="alert alert-success">New Grid Added Successfully!</div>');

        redirect('Grid/addgrid');
    }

    //View agent function start-- ishara sewwandi
    public function viewgrid()
    {

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/grid/viewgrid';
        $mainData = array(
            'grid' => $this->setting_model->Get_All('grid'),


        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }

    //edit grid -- Ishara sewwandi

    public function editgrid()
    {

        $id = $this->uri->segment(3);
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/grid/editgrid';
        $mainData = array(
            'grid' => $this->setting_model->Get_Single('grid','grid_id',$id),


        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }

    //update grid --------------------ishara sewwandi
    public function updategrid()
    {

        $id = $this->uri->segment(3);

        if(isset($_FILES['fileinput']) && empty($_FILES['fileinput']['tmp_name'])) {
            $image = $this->input->post('grid_image');
        } else {
            $image = $this->setting_model->upload('fileinput', 'upload/grid');
        }
        $data = array(
            'image' =>$image,
            'title' => $this->input->post('title'),
            'description' => $this->input->post('description'),
            'link' => $this->input->post('link'),
        );

        $insert_id = $this->setting_model->update($data, 'grid','grid_id',$id);


        $this->session->set_flashdata('msg', '<div class="alert alert-success">Update Grid Successfully!</div>');

        redirect('Grid/editgrid/'.$id);
    }
}