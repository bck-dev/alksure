<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reservation extends CI_Controller {

	public function template($headerData, $sidebarData, $page, $mainData, $footerData) {
		$this->load->view('admin/components/header', $headerData);
		$this->load->view('admin/components/sidebar', $sidebarData);
		$this->load->view($page, $mainData);
		$this->load->view('admin/components/footer', $footerData);
	}
// start function for view pending and followup reservation page ----- coded by Gihan 
	public function viewReservation(){

		$headerData = null;
		$sidebarData = null;
		$page = 'admin/reservation/viewreservation';
		$mainData = array(
			
			'vehicles' => $this->setting_model->Get_All('vehicle'),
			'customer' => $this->setting_model->Get_All('customer'),
			'types' => $this->setting_model->Get_All('vehicle_type'),
			'brands' => $this->setting_model->Get_All('vehicle_brand'),
			'model' => $this->setting_model->Get_All('vehicle_model'),
			'inside_images' => $this->setting_model->Get_All('inside_images'),
			'outside_images' => $this->setting_model->Get_All('out_side_images'),
			'reservation' => $this->setting_model->Get_All('reservation')
			
		);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}
// end function for view pending and followup reservation page

// Start function for creating Follow up ------ coded by Gihan

	public function followUp(){
		// print_r($inq_id);
		$id = $this->input->post("res_id");
		// print_r($id);
		$date = date('Y-m-d');
		$data = array(
			'status' => 1,
			'status_changed_date' => $date,
			'followUp_description' =>$this->input->post('Follow_up_description'),
			'next_followup_date' =>$this->input->post('next_date'),
			);

		$data1 = $this->setting_model->update($data,'reservation','reservation_id',$id);

		$this->session->set_flashdata('msg','<div class="alert alert-warning">Follow up Successfully!</div>');

		redirect('reservation/viewReservation');
	}
// End function for creating Follow up

// Start function for confirm inquiry ------- coded by Gihan

	public function confirm(){
		
		$id = $this->uri->segment(3);
		$date = date('Y-m-d');
		$data = array(
			'status' => 2,
			'status_changed_date' => $date,
			);

		$data1 = $this->setting_model->update($data,'reservation','reservation_id',$id);

		$this->session->set_flashdata('msg','<div class="alert alert-success">Confirm Successfully!</div>');

		redirect('reservation/viewReservation');
	}
// End function for confirm inquiry

// start function for view payment collected page -------- coded by Gihan

	public function viewConfirmReservations(){

		$headerData = null;
		$sidebarData = null;
		$page = 'admin/reservation/paymentCollected';
		$mainData = array(
			
			'vehicles' => $this->setting_model->Get_All('vehicle'),
			'customer' => $this->setting_model->Get_All('customer'),
			'types' => $this->setting_model->Get_All('vehicle_type'),
			'brands' => $this->setting_model->Get_All('vehicle_brand'),
			'model' => $this->setting_model->Get_All('vehicle_model'),
			'inside_images' => $this->setting_model->Get_All('inside_images'),
			'outside_images' => $this->setting_model->Get_All('out_side_images'),
			'reservation' => $this->setting_model->Get_All('reservation')
			
		);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}
// End function for view payment collected page

// Start Function for reject reservation on reservation page ------ coded by Gihan

	public function reject(){
		// print_r($inq_id);
		$id = $this->input->post('res_id');
		print_r($id);
		$date = date('Y-m-d');
		$data = array(
			'status' => 3,
			'reject_reason' => $this->input->post('reject_reason'),
			'status_changed_date' => $date,
			);

		$data1 = $this->setting_model->update($data,'reservation','reservation_id',$id);

		$this->session->set_flashdata('msg','<div class="alert alert-danger">Reject Successful!</div>');

		redirect('reservation/viewReservation');
	}
// End Function for reject reservation on reservation page

// Start function for view rejected reservations page ------- coded by Gihan

	public function rejectReservation(){

		$headerData = null;
		$sidebarData = null;
		$page = 'admin/reservation/rejectReservation';
		$mainData = array(
			
			'vehicles' => $this->setting_model->Get_All('vehicle'),
			'customer' => $this->setting_model->Get_All('customer'),
			'types' => $this->setting_model->Get_All('vehicle_type'),
			'brands' => $this->setting_model->Get_All('vehicle_brand'),
			'model' => $this->setting_model->Get_All('vehicle_model'),
			'inside_images' => $this->setting_model->Get_All('inside_images'),
			'outside_images' => $this->setting_model->Get_All('out_side_images'),
			'reservation' => $this->setting_model->Get_All('reservation')
			
		);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}

// End function for view rejected reservations page

// Start function for the followup on rejected reservations page ------ coded by Gihan

	public function followUp1(){
		// print_r($inq_id);
		$id = $this->input->post("res_id");
		// print_r($id);
		$date = date('Y-m-d');
		$data = array(
			'status' => 1,
			'status_changed_date' => $date,
			'followUp_description' =>$this->input->post('Follow_up_description'),
			'next_followup_date' =>$this->input->post('next_date'),
			);

		$data1 = $this->setting_model->update($data,'reservation','reservation_id',$id);

		$this->session->set_flashdata('msg','<div class="alert alert-warning">Follow up Successful!</div>');

		redirect('reservation/rejectReservation');
	}
// End function for the followup on rejected reservations page

// Start function for the paymentcollected on rejected reservations page--- coded by Gihan

	public function confirm1(){
		
		$id = $this->uri->segment(3);
		$date = date('Y-m-d');
		$data = array(
			'status' => 2,
			'status_changed_date' => $date,
			);

		$data1 = $this->setting_model->update($data,'reservation','reservation_id',$id);

		$this->session->set_flashdata('msg','<div class="alert alert-success">Confirm Successful!</div>');

		redirect('reservation/rejectReservation');
	}

// End function for the payment collected on rejected reservations page

// Start function for back to pending on rejected reservation page ----- coded by Gihan  

	public function pending(){
		$id = $this->uri->segment(3);

		$date = date('Y-m-d');
		$data = array(
			'status' => NULL,
			'status_changed_date' => NULL,
			'followUp_description' => NULL,
			'next_followup_date' => NULL,
			);

		$data1 = $this->setting_model->update($data,'reservation','reservation_id',$id);

		$this->session->set_flashdata('msg','<div class="alert alert-success">Pending Successful!</div>');

		redirect('reservation/rejectReservation');
	}
// End function for back to pending on rejected reservation page


// start function for change date
	public function updateDate(){
		// print_r($inq_id);
		$id = $this->uri->segment(3);
		$date = $this->input->post('changed_date');
		$data = array(
			'next_followup_date' => $this->input->post('changed_date')
			);

		$data1 = $this->setting_model->update($data,'reservation','reservation_id',$id);

		$this->session->set_flashdata('msg','<div class="alert alert-success">Update date Successful!</div>');

		redirect('reservation/viewReservation');
	}

}
?>