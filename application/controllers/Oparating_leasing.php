<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Oparating_leasing extends CI_Controller {

	public function template($headerData, $sidebarData, $page, $mainData, $footerData) {
		$this->load->view('admin/components/header', $headerData);
		$this->load->view('admin/components/sidebar', $sidebarData);
		$this->load->view($page, $mainData);
		$this->load->view('admin/components/footer', $footerData);
	}

	// start function for view renting inqiury ------------------------ coded by Gihan
    public function viewInquiry(){

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/opearting_leasing/viewpending';
        $mainData = array(

            'vehicles' => $this->setting_model->Get_All('vehicle'),
            'customer' => $this->setting_model->Get_All('customer'),
            'types' => $this->setting_model->Get_All('vehicle_type'),
            'brands' => $this->setting_model->Get_All('vehicle_brand'),
            'model' => $this->setting_model->Get_All('vehicle_model'),
	    'inside_images' => $this->setting_model->Get_All('inside_images'),
            'outside_images' => $this->setting_model->Get_All('out_side_images'),
            'inquiry' => $this->setting_model->Get_All('oprating_leasing_inquiry')

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
    // end function --------------------------------------------------------------

    // start function for followup renting inquiry --------------------- coded by Gihan

    public function followUp(){

        $id = $this->input->post("inq_id");
        $date = date('Y-m-d');
        $data = array(
            'status' => 1,
            'status_change_date' => $date,
            'followUp_description' =>$this->input->post('Follow_up_description'),
            'next_followup_date' =>$this->input->post('next_date'),
        );

        $data1 = $this->setting_model->update($data,'oprating_leasing_inquiry','leasing_inquiry_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-warning">Follow Up Successfully!</div>');

        redirect('oparating_leasing/viewInquiry');
    }
// end function --------------------------------------------------------------------
    public function viewFollowUp(){

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/opearting_leasing/viewFollowup';
        $mainData = array(

            'vehicles' => $this->setting_model->Get_All('vehicle'),
            'customer' => $this->setting_model->Get_All('customer'),
            'types' => $this->setting_model->Get_All('vehicle_type'),
            'brands' => $this->setting_model->Get_All('vehicle_brand'),
            'model' => $this->setting_model->Get_All('vehicle_model'),
	    'inside_images' => $this->setting_model->Get_All('inside_images'),
            'outside_images' => $this->setting_model->Get_All('out_side_images'),
            'inquiry' => $this->setting_model->Get_All('oprating_leasing_inquiry')

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }

    public function updateDate(){
        // print_r($inq_id);
        $id = $this->uri->segment(3);
        $date = $this->input->post('changed_date');
        $data = array(
            'next_followup_date' => $this->input->post('changed_date')
        );

        $data1 = $this->setting_model->update($data,'oprating_leasing_inquiry','leasing_inquiry_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-success">Update date Successful!</div>');

        redirect('oparating_leasing/viewFollowUp');
    }

    // start function for confirm renting inquiry ------------------ coded by Gihan
    public function confirm(){

        $id = $this->uri->segment(3);
        $date = date('Y-m-d');
        $data = array(
            'status' => 2,
            'status_change_date' => $date,
        );

        $data1 = $this->setting_model->update($data,'oprating_leasing_inquiry','leasing_inquiry_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-success">Inquiry Confirm Successful!</div>');

        redirect('oparating_leasing/viewInquiry');
    }
// end function ------------------------------------------------------------------


// start function for reject renting inquiry -------------------- coded by Gihan

    public function reject(){

        $id = $this->input->post('inq_id');
        $date = date('Y-m-d');
        $data = array(
            'status' => 3,
            'reject_reason' => $this->input->post('reject_reason'),
            'status_change_date' => $date,
        );

        $data1 = $this->setting_model->update($data,'oprating_leasing_inquiry','leasing_inquiry_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-danger"> Reject Successful!</div>');

        redirect('oparating_leasing/viewInquiry');
    }
// end function---------------------------------------------------------------------

// start function for confirm renting inquiry ------------------ coded by Gihan
    public function confirm1(){

        $id = $this->uri->segment(3);
        $date = date('Y-m-d');
        $data = array(
            'status' => 2,
            'status_change_date' => $date,
        );

        $data1 = $this->setting_model->update($data,'oprating_leasing_inquiry','leasing_inquiry_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-success">Inquiry Confirm Successful!</div>');

        redirect('oparating_leasing/viewFollowUp');
    }
// end function ------------------------------------------------------------------


// start function for reject renting inquiry -------------------- coded by Gihan

    public function reject1(){

        $id = $this->input->post('inq_id');
        $date = date('Y-m-d');
        $data = array(
            'status' => 3,
            'reject_reason' => $this->input->post('reject_reason'),
            'status_change_date' => $date,
        );

        $data1 = $this->setting_model->update($data,'oprating_leasing_inquiry','leasing_inquiry_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-danger"> Reject Successful!</div>');

        redirect('oparating_leasing/viewFollowUp');
    }
// end function---------------------------------------------------------------------

public function confirm2(){

        $id = $this->uri->segment(3);
        $date = date('Y-m-d');
        $data = array(
            'status' => 2,
            'status_change_date' => $date,
        );

        $data1 = $this->setting_model->update($data,'oprating_leasing_inquiry','leasing_inquiry_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-success">Inquiry Confirm Successful!</div>');

        redirect('oparating_leasing/viewRejected');
    }
// end function ------------------------------------------------------------------


// start function for reject renting inquiry -------------------- coded by Gihan

   public function followUp1(){

        $id = $this->input->post("inq_id");
        $date = date('Y-m-d');
        $data = array(
            'status' => 1,
            'status_change_date' => $date,
            'followUp_description' =>$this->input->post('Follow_up_description'),
            'next_followup_date' =>$this->input->post('next_date'),
        );

        $data1 = $this->setting_model->update($data,'oprating_leasing_inquiry','leasing_inquiry_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-warning">Follow Up Successfully!</div>');

        redirect('oparating_leasing/viewRejected');
    }
// start function for view confirmed inquiries ---------------------- coded by Gihan

    public function viewConfirm(){

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/opearting_leasing/viewConfirm';
        $mainData = array(

            'vehicles' => $this->setting_model->Get_All('vehicle'),
            'customer' => $this->setting_model->Get_All('customer'),
            'types' => $this->setting_model->Get_All('vehicle_type'),
            'brands' => $this->setting_model->Get_All('vehicle_brand'),
            'model' => $this->setting_model->Get_All('vehicle_model'),
	    'inside_images' => $this->setting_model->Get_All('inside_images'),
            'outside_images' => $this->setting_model->Get_All('out_side_images'),
            'inquiry' => $this->setting_model->Get_All('oprating_leasing_inquiry')

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
// end function -----------------------------------------------------------------------


// start function for view rejected inquiries ------------------------- coded by Gihan

    public function viewRejected(){

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/opearting_leasing/viewRejected';
        $mainData = array(

            'vehicles' => $this->setting_model->Get_All('vehicle'),
            'customer' => $this->setting_model->Get_All('customer'),
            'types' => $this->setting_model->Get_All('vehicle_type'),
            'brands' => $this->setting_model->Get_All('vehicle_brand'),
            'model' => $this->setting_model->Get_All('vehicle_model'),
	    'inside_images' => $this->setting_model->Get_All('inside_images'),
            'outside_images' => $this->setting_model->Get_All('out_side_images'),
            'inquiry' => $this->setting_model->Get_All('oprating_leasing_inquiry')

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
// end function -----------------------------------------------------------------------
    public function paymentStatus(){

    	$id = $this->uri->segment(3);
    	$date = date('Y-m-d');

    	$data = array(
    		'payment_status' => 1,
    		'payment_collected_date' => $date,
    	);

    	$data1 = $this->setting_model->update($data,'oprating_leasing_inquiry','leasing_inquiry_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-success">Payment Collected Successful!</div>');

        redirect('oparating_leasing/viewConfirm');
    }

}
?>