<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bike extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->check_isvalidated();
        $this->CI = & get_instance();
    }

    public function template($headerData, $sidebarData, $page, $mainData, $footerData) {
        $this->load->view('admin/components/header', $headerData);
        $this->load->view('admin/components/sidebar', $sidebarData);
        $this->load->view($page, $mainData);
        $this->load->view('admin/components/footer', $footerData);
    }

    //validate session start
    private function check_isvalidated(){
        if(!$this->session->userdata('validated')){
            redirect('login');
        }
    }
    //validate session end



    //View event function start --ishara sewwandi
    public function viewbike() {
	$id1=$this->uri->segment(4);
        $ageid = $this->input->post('c_id');
        //print_r($ageid);
        $vehiid = $this->input->post('v_id');

        $data = array(
//                'vehicle_id' => $this->input->post('vehicle_id'),
            'inspection_status' =>$ageid,

        );
//            print_r($vehi_id);
        $this->setting_model->update($data,'threewheel_bike','threewheel_bike_id',$vehiid);

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/3wheel/viewbike';


        $mainData = array(
//            'vehicle' => $this->setting_model->Get_All('vehicle'),
            'insideimage' => $this->setting_model->Get_All('threewheel_bike_image'),
            'agent' => $this->setting_model->Get_All('agents'),
            'vehicle' => $this->setting_model->biksjoinnew(),

//            'agents' => $this->setting_model->Get_All_Where('agents','agent_id',$agentid),
            //'vehicleview' => $this->setting_model->vehicleviewjoin(),
//            print_r($agentid),
           


        );


        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
    //View event function end --ishara sewwandi

    //View event function start --ishara sewwandi
    public function viewbikes() {
        $id1=$this->uri->segment(4);
        $ageid = $this->input->post('c_id');
        //print_r($ageid);
        $vehiid = $this->input->post('v_id');

        $data = array(
//                'vehicle_id' => $this->input->post('vehicle_id'),
            'inspection_status' =>$ageid,

        );
//            print_r($vehi_id);
        $this->setting_model->update($data,'threewheel_bike','threewheel_bike_id',$vehiid);


        $headerData = null;
        $sidebarData = null;
        $page = 'admin/3wheel/viewbikes';


        $mainData = array(
//            'vehicle' => $this->setting_model->Get_All('vehicle'),
            'insideimage' => $this->setting_model->Get_All('threewheel_bike_image'),
//            'outside' => $this->setting_model->Get_All('out_side_images'),
            'agent' => $this->setting_model->Get_All('agents'),
            'vehicle' => $this->setting_model->biksjoin(),

//            'agents' => $this->setting_model->Get_All_Where('agents','agent_id',$agentid),
            //'vehicleview' => $this->setting_model->vehicleviewjoin(),
//            print_r($agentid),
            print_r($ageid),


        );


        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
    
     public function editvehicle1() {
        $id = $this->uri->segment(3);

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/vehicle/editvehicle';

        $mainData = array(
            'vehicle1' => $this->setting_model->Get_Single('vehicle','vehicle_id',$id),
            'vehicle_images' => $this->multiple_image_uplaod->edit_data_image_vehicle1($id),
            'types'=> $this->setting_model->Get_All('vehicle_type'),
            'brands'=> $this->setting_model->Get_All('vehicle_brand'),
            'modals'=> $this->setting_model->Get_All('vehicle_model'),
        );

        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);

    }
    

    
    public function deleteimageselling(){
        $deleteid  = $this->input->post('id');
        $this->db->delete('inside_images', array('inside_images_id' => $deleteid));
        $verify = $this->db->affected_rows();
        echo $verify;
    }

//View vehicle datails function start --ishara sewwandi
    public function details() {

        $vehi = $this->uri->segment(3);

        $agentid = $this->input->post('a_id');

        $config = array(
            'wheres' => array(
                'vehicle' => array('vehicle_id', $vehi)
            ),
            'from' => array('vehicle'),
            'joins' => array(
                'inside_images' => array('inside_images.vehicle_vehicle_id = vehicle.vehicle_id', 'left'),
                'out_side_images' => array('out_side_images.vehicle_vehicle_id = vehicle.vehicle_id', 'left'),
                'vehicle_brand' => array('vehicle_brand.vehicle_brand_id = vehicle.vehicle_brand', 'left'),
                'vehicle_type' => array('vehicle_type.vehicle_type_id = vehicle.vehicle_type', 'left'),
                'vehicle_model' => array('vehicle_model.vehicle_brand_id = vehicle.vehicle_model', 'left'),
                'selling_customer' => array('selling_customer.selling_customer_id = vehicle.selling_customer_id', 'left'),
                'agents' => array('agents.agent_id = vehicle.agent_id', 'left')
            )
        );

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/3wheel/details';

        $mainData = array(

            'insideimage' => $this->setting_model->Get_All('threewheel_bike_image'),
//            'outside' => $this->setting_model->Get_All('out_side_images'),
            'agent' => $this->setting_model->Get_All('agents'),
            'vehicle' => $this->setting_model->wheeldetail($vehi),
            'agents' => $this->setting_model->Get_All_Where('agents','agent_id',$agentid),
             'agentid' => $this->setting_model->Get_All_Where('agents','agent_id',$agentid),


        );



        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
    //View vehicle datails function end --ishara sewwandi

    //View agent datails function start --ishara sewwandi

    public function agentdetail() {

        $vehi = $this->uri->segment(3);

        $agentid = $this->input->post('a_id');

        $config = array(
            'wheres' => array(
                'vehicle' => array('vehicle_id', $vehi)
            ),
            'from' => array('vehicle'),
            'joins' => array(
                'inside_images' => array('inside_images.vehicle_vehicle_id = vehicle.vehicle_id', 'left'),
                'out_side_images' => array('out_side_images.vehicle_vehicle_id = vehicle.vehicle_id', 'left'),
                'vehicle_brand' => array('vehicle_brand.vehicle_brand_id = vehicle.vehicle_brand', 'left'),
                'vehicle_type' => array('vehicle_type.vehicle_type_id = vehicle.vehicle_type', 'left'),
                'vehicle_model' => array('vehicle_model.vehicle_brand_id = vehicle.vehicle_model', 'left'),
                'selling_customer' => array('selling_customer.selling_customer_id = vehicle.selling_customer_id', 'left'),
                'agents' => array('agents.agent_id = vehicle.agent_id', 'left')
            )
        );

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/3wheel/agentdetail';

        $mainData = array(
//            'vehicle' => $this->setting_model->Get_All('vehicle'),
            'insideimage' => $this->setting_model->Get_All('inside_images'),
//            'outside' => $this->setting_model->Get_All('out_side_images'),
            'agent' => $this->setting_model->Get_All('agents'),
            'type' => $this->setting_model->Get_All('threewheel_bike_type'),
            'model' => $this->setting_model->Get_All('threewheel_bike_model'),
            'brand' => $this->setting_model->Get_All('threewheel_bike_brand'),
	    'agentid' => $this->setting_model->Get_All_Where('agents','agent_id',$agentid),
            'agents' => $this->setting_model->Get_All_Where('threewheel_bike','agent_id',$agentid),





        );



        $footerData = null;

//        $this->template( $page, $mainData, $footerData);
        $this->load->view('admin/3wheel/agentdetail',$mainData);
    }

//View agent datails function end --ishara sewwandi

//insert agent datails function start --ishara sewwandi
    public function insertagent()
    {
        $vehi_id = $this->uri->segment(3);


            $data = array(
//                'vehicle_id' => $this->input->post('vehicle_id'),
                'agent_id' => $this->input->post('agent'),
                'status' => 1,
            );
//            print_r($vehi_id);
            $this->setting_model->update($data,'threewheel_bike','threewheel_bike_id',$vehi_id);

            $this->send_contact_email();
            $this->send_email_admin();

//                redirect('vehicle/viewvehicle');


    }
//insert agent datails function end --ishara sewwandi


    private function _response(array $data = array(), $code = 200) {
        $this->output
            ->set_status_header($code)
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

//sent email for agent--ishara sewwandi
    public function send_contact_email()
    {
//        $client_name = $this->input->post('vehicle_id');
        $vehicle_number = $this->input->post('vehicle_number');
        $vehicle_type_name = $this->input->post('vehicle_type_name');
        $vehicle_brand_name = $this->input->post('vehicle_brand_name');
        $vehicle_transmission = $this->input->post('vehicle_transmission');
        $vehicle_engine_capacity = $this->input->post('vehicle_engine_capacity');
        $vehicle_seating_capacity= $this->input->post('vehicle_seating_capacity');
	$agent_email= $this->input->post('agent_email');
        $this->_send_contact_email( $vehicle_number, $vehicle_type_name, $vehicle_brand_name, $vehicle_transmission, $vehicle_engine_capacity, $vehicle_seating_capacity,$agent_email);


        $this->_response( array( "status" => "true", "message" => "Thank you for contacting us." ) );
        
    }

    private function _send_contact_email($vehicle_number, $vehicle_type_name, $vehicle_brand_name, $vehicle_transmission, $vehicle_engine_capacity, $vehicle_seating_capacity,$agent_email){


        $log_path =  base_url('/assets/logo.png');
//        $client_communication_star = $this->get_star($communication);
//        $client_quality_of_deliverible = $this->get_star($quality_of_deliverible);
//        $client_proffesionalism = $this->get_star($proffesionalism);
//        $client_effectiveness = $this->get_star($effectiveness);


        $message = "";

        $message .= "<p style=\"font-size: 1.2em; padding: 10px; margin-bottom: 0;\">* This is an auto genarated e-mail for a request of a live web use.</p>";
        $message .= "<table style=\"width: 60%; min-width: 500px;\">";
        $message .= "<tbody style=\"border: 1px solid #a8a8a8;\">";
        $message .= "<tr style=\"background-color: white;\">";
        $message .= "<td style=\"padding: 15px; text-align: center;\" colspan=2>";
        $message .= "<img style=\"padding: 10px; height: auto; width: 30%;\" src=\"$log_path\"/>";
        $message .= "</td>";
        $message .= "</tr>";

        $message .= "<tr style=\"\">";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Vehicle number:</p>";
        $message .= "</td>";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$vehicle_number</p>";
        $message .= "</td>";
        $message .= "</tr>";

        $message .= "<tr style=\"\">";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Vehicle type name:</p>";
        $message .= "</td>";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$vehicle_type_name</p>";
        $message .= "</td>";
        $message .= "</tr>";

        $message .= "<tr style=\"\">";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Vehicle brand name:</p>";
        $message .= "</td>";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$vehicle_brand_name</p>";
        $message .= "</td>";
        $message .= "</tr>";

        $message .= "<tr style=\"\">";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Vehicle transmission:</p>";
        $message .= "</td>";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$vehicle_transmission</p>";
        $message .= "</td>";
        $message .= "</tr>";


        $message .= "<tr style=\"\">";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Vehicle engine capacity:</p>";
        $message .= "</td>";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$vehicle_engine_capacity</p>";
        $message .= "</td>";
        $message .= "</tr>";

        $message .= "<tr style=\"\">";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Vehicle engine capacity:</p>";
        $message .= "</td>";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$vehicle_engine_capacity</p>";
        $message .= "</td>";
        $message .= "</tr>";

        $message .= "<tr style=\"\">";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Vehicle seating capacity:</p>";
        $message .= "</td>";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$vehicle_seating_capacity</p>";
        $message .= "</td>";
        $message .= "</tr>";
        $message .= "</tbody>";
        $message .= "</table>";



        $emailReceivers = array(
            $agent_email
        );

        foreach( $emailReceivers as $emailReceiver) {

           $from = 'Admin';
            $to = $emailReceiver;
            $subject = "Admin Form";

            $config = array();
            $config["protocol"] = 'smpt';
            $config['smtp_host'] = 'bckonnect.com';
            $config["smtp_port"] = 465;
            $config["smtp_user"] = 'publicuser@bckonnect.com';
            $config["smtp_pass"] = 'PaSS@bckonnect.com';
            $config['mailtype'] = 'html';
            $this->email->initialize($config);

            $this->email->from($from, 'Vehicle Details');
            $this->email->to($to);
            $this->email->subject($subject);
            $this->email->message($message);
            $this->email->send();
        }

        return true;
          redirect('bike/viewbikes');

    }

    //send email for admin --ishara sewwandi

    public function send_email_admin(){
        $agent_id = $this->input->post('agent_id');
        $agent_name = $this->input->post('agent_name');
        $vehicle_id = $this->input->post('vehicleid');
        $vehicle_number = $this->input->post('vehicle_number');
        $assign_date = date('Y-m-d H:i:s');



        $this->_send_email_admin( $agent_id, $agent_name, $vehicle_id, $vehicle_number, $assign_date);



        $this->_response( array( "status" => "true", "message" => "Thank you for contacting us." ) );

	redirect('bike/viewbikes');
        
    }



    private function _send_email_admin($agent_id, $agent_name, $vehicle_id, $vehicle_number, $assign_date){
        $log_path =  base_url('/assets/logo.png');

        $message = "";

        $message .= "<p style=\"font-size: 1.2em; padding: 10px; margin-bottom: 0;\">* This is an auto genarated e-mail for a request of a live web use.</p>";
        $message .= "<table style=\"width: 60%; min-width: 500px;\">";
        $message .= "<tbody style=\"border: 1px solid #a8a8a8;\">";
        $message .= "<tr style=\"background-color: white;\">";
        $message .= "<td style=\"padding: 15px; text-align: center;\" colspan=2>";
        $message .= "<img style=\"padding: 10px; height: auto; width: 30%;\" src=\"$log_path\"/>";
        $message .= "</td>";
        $message .= "</tr>";
        
        $message .= "<tr style=\"\">";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Agent id:</p>";
        $message .= "</td>";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$agent_id</p>";
        $message .= "</td>";
        $message .= "</tr>";

        $message .= "<tr style=\"\">";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Agent Name:</p>";
        $message .= "</td>";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$agent_name</p>";
        $message .= "</td>";
        $message .= "</tr>";

        $message .= "<tr style=\"\">";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Vehicle ID:</p>";
        $message .= "</td>";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$vehicle_id</p>";
        $message .= "</td>";
        $message .= "</tr>";

        $message .= "<tr style=\"\">";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Vehicle Number:</p>";
        $message .= "</td>";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$vehicle_number</p>";
        $message .= "</td>";
        $message .= "</tr>";


        $message .= "<tr style=\"\">";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Assign Date/Time:</p>";
        $message .= "</td>";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$assign_date</p>";
        $message .= "</td>";
        $message .= "</tr>";


        $message .= "</tbody>";
        $message .= "</table>";



        $emailReceivers = array(
            'yaisharasewwandi@gmail.com'
        );

        foreach( $emailReceivers as $emailReceiver) {

            $from = 'Admin';
            $to = $emailReceiver;
            $subject = "Admin Form";

            $config = array();
            $config["protocol"] = 'smpt';
            $config['smtp_host'] = 'bckonnect.com';
            $config["smtp_port"] = 465;
            $config["smtp_user"] = 'publicuser@bckonnect.com';
            $config["smtp_pass"] = 'PaSS@bckonnect.com';
            $config['mailtype'] = 'html';
            $this->email->initialize($config);

            $this->email->from($from, 'Agent Details');
            $this->email->to($to);
            $this->email->subject($subject);
            $this->email->message($message);
            $this->email->send();
        }

        return true;
        

    }






//insert rejected 3wheel function start --ishara sewwandi

        public function insertreject(){
//        $vehi_id = $this->uri->segment(3);
        $vehicleid = $this->input->post('vehicle_id');
        $rejecttdate = date('Y-m-d H:i:s');

        $data = array(
//                'vehicle_id' => $this->input->post('vehicle_id'),
            'threewheel_bike_id' => $this->input->post('vehicle_id'),
            'customer_customer_id' => $this->input->post('selling_customer_id'),
            'reason' => $this->input->post('reject_reason'),
            'rejected_date' => $rejecttdate,
        );
//            print_r($vehi_id);
        $this->setting_model->insert($data, 'threewheel_bike_reject');


        $data = array(

            'status' => 0,
            'reject_status' => 1,
            'inspection_status' => 0,
        );
//            print_r($vehi_id);
        $this->setting_model->update($data,'threewheel_bike','threewheel_bike_id',$vehicleid);

        redirect('bike/viewbikes');
    }

    //insert rejected 3wheel function end --ishara sewwandi


    //view rejected vehicle function start --ishara sewwandi
    public function viewrejected(){

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/3wheel/viewrejected';

        $config = array(
            'wheres' => '',
            'from' => array('threewheel_bike_reject'),
            'joins' => array(
                'threewheel_bike_customer' => array('threewheel_bike_customer.threewheel_bike_customer_id = threewheel_bike_reject.reject_id', 'left'),
                'threewheel_bike' => array('threewheel_bike.threewheel_bike_id = threewheel_bike_reject.threewheel_bike_id', 'left'),
            )
        );

        $mainData = array(
            'sellingreject' => $this->setting_model->getJoin($config),


        );


        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
//view rejected vehicle function end --ishara sewwandi

//update rejected status function start --ishara sewwandi
    public function changeactionreject(){

        $id = $this->input->post('id');
        $action = $this->input->post('action');
        $vehicle_id = $this->input->post('vehicleId');
        print_r($action);
        $data = array(
            'reject_status'=> $action,

        );

        $this->setting_model->update($data,'threewheel_bike','threewheel_bike_id',$vehicle_id);

        $this->setting_model->delete('threewheel_bike_reject','reject_id',$id);


    }
//update rejected status function end --ishara sewwandi

    //vehicle inspection view -- ishara sewwandi

    public function viewinspection() {


        $vehi = $this->uri->segment(3);

        $config = array(
            'wheres' => array(
                'threewheel_bike' => array('threewheel_bike_id', $vehi)
            ),
            'from' => array('threewheel_bike'),
            'joins' => array(
                'threewheel_bike_image' => array('threewheel_bike_image.threewheel_bike_id = threewheel_bike.threewheel_bike_id', 'left'),
                'threewheel_bike_brand' => array('threewheel_bike_brand.threewheel_bike_brand_id = threewheel_bike.threewheel_bike_brand_id', 'left'),
                'threewheel_bike_type' => array('threewheel_bike_type.threewheel_bike_type_id = threewheel_bike.threewheel_bike_type_id', 'left'),
                'threewheel_bike_model' => array('threewheel_bike_model.threewheel_bike_model_id = threewheel_bike.threewheel_bike_model_id', 'left'),
                'threewheel_bike_customer' => array('threewheel_bike_customer.threewheel_bike_customer_id = threewheel_bike.threewheel_selling_customer_id', 'left'),
                'agents' => array('agents.agent_id = threewheel_bike.agent_id', 'left')
            )
        );

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/3wheel/viewinspection';




        $mainData = array(
//            'vehicle' => $this->setting_model->Get_All('vehicle'),
            'insideimage' => $this->setting_model->Get_All('threewheel_bike_image'),
//            'outside' => $this->setting_model->Get_All('out_side_images'),
            'agent' => $this->setting_model->Get_All('agents'),
            'vehicle' => $this->setting_model->wheelpublish($vehi),


        );


        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }

    //vehicle inspection view end --ishara sewwandi


    //insert rating  function start --ishara sewwandi

    public function insertrating() {
        $vehi = $this->input->post('vehicle_id');
        $data = array(

            'vehical_vehical_id' =>$this->input->post('vehicle_id'),
            'customer_customer_id' =>$this->input->post('selling_customer_id'),
            'extirier_condition' =>$this->input->post('rating1'),
            'interior_condition' =>$this->input->post('rating2'),
            'engine_condition' =>$this->input->post('rating3'),
            'image' =>$this->setting_model->upload('image', 'upload/threewheelpublish'),
            'details' =>$this->input->post('details'),

            // 'main_banner_description' =>$this->input->post('main_banner_description'),
            // 'main_banner_link' =>$this->input->post('main_banner_link'),
        );

        $this->setting_model->insert($data, 'threewheel_bike_rating');

        $data = array(
            'publish_status' => 1,
        );
        $this->setting_model->update($data,'threewheel_bike','threewheel_bike_id',$vehi);


        redirect('bike/viewbike');
    }
//insert rating  function end --ishara sewwandi

// Star ratings
    public function get_star($count){
        $string = '';
        for($i=1;$i<=$count;$i++){
            $string .= '★';
        }
        return $string;
    }

    // End star ratings

    //view publish vehicle function start --ishara sewwandi

    public function viewpublised(){


//        $vehicall = $this->input->post('vehical_id');


        $pubid = $this->input->post('c_id');
//        print_r($pubid);
        $vehiid = $this->input->post('pu_id');

        $data = array(
//                'vehicle_id' => $this->input->post('vehicle_id'),
            'publish_status' =>$pubid,

        );
//            print_r($vehi_id);
        $this->setting_model->update($data,'threewheel_bike','threewheel_bike_id',$vehiid);

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/3wheel/viewpublised';

        $config = array(
            'wheres' => "",
            'from' => array('threewheel_bike_rating'),
            'joins' => array(
                'threewheel_bike' => array('threewheel_bike.threewheel_bike_id = threewheel_bike_rating.vehical_vehical_id', 'left'),
                'threewheel_bike_customer' => array('threewheel_bike_customer.threewheel_bike_customer_id  = threewheel_bike_rating.customer_customer_id', 'left'),
//                'inside_images' => array('inside_images.vehicle_vehicle_id = vehicle.vehicle_id', 'left'),
//                'out_side_images' => array('out_side_images.vehicle_vehicle_id = vehicle.vehicle_id', 'left'),
//                'vehicle_brand' => array('vehicle_brand.vehicle_brand_id = vehicle.vehicle_brand', 'left'),
//                'vehicle_type' => array('vehicle_type.vehicle_type_id = vehicle.vehicle_type', 'left'),
//                'vehicle_model' => array('vehicle_model.vehicle_brand_id = vehicle.vehicle_model', 'left'),
//                'agents' => array('agents.agent_id = vehicle.agent_id', 'left')


            )
        );

        $config1 = array(
            'wheres' => "",
            'from' => array('threewheel_bike'),
            'joins' => array(
                'threewheel_bike_image' => array('threewheel_bike_image.threewheel_bike_id = threewheel_bike.threewheel_bike_id', 'left'),
                'threewheel_bike_brand' => array('threewheel_bike_brand.threewheel_bike_brand_id = threewheel_bike.threewheel_bike_brand_id', 'left'),
                'threewheel_bike_type' => array('threewheel_bike_type.threewheel_bike_type_id = threewheel_bike.threewheel_bike_type_id', 'left'),
                'threewheel_bike_model' => array('threewheel_bike_model.threewheel_bike_model_id = threewheel_bike.threewheel_bike_model_id', 'left'),
                'threewheel_bike_customer' => array('threewheel_bike_customer.threewheel_bike_customer_id = threewheel_bike.threewheel_selling_customer_id', 'left'),
                'agents' => array('agents.agent_id = threewheel_bike.agent_id', 'left')
            )
        );

        $pub = $this->setting_model->getJoin($config);
//        foreach ($pub as $pu);{
////            if($pu->vahical_vehical_id == ) {
////                $idd = $pu->vahical_vehical_id ;
//                $aa = $pu->extirier_condition;
//                $bb = $pu->interior_condition;
//                $cc = $pu->engine_condition;
////            }
//        }
//
//        $extirier_condition = $this->get_star($aa);
//        $interior_condition = $this->get_star($bb);
//        $engine_condition = $this->get_star($cc);

      

        $mainData = array(
            'rats' => $this->setting_model->getJoin($config),
            'rating' => $this->setting_model->getJoin($config1),
            'insideimage' => $this->setting_model->Get_All('threewheel_bike_image'),
//            'outside' => $this->setting_model->Get_All('out_side_images'),
//            'extirier' => $extirier_condition,
//            'interior' => $interior_condition,
//            'engine' => $engine_condition,
           'rates1'=>$this->setting_model->Get_All('threewheel_bike_rating'),
//            'id' => $idd,



//            'vehicle_brand' => $this->setting_model->Get_All('vehicle_brand')

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);


    }

    //view publish vehicle function end --ishara sewwandi

    //delete entire publish
    public function deletepublish(){
        //$service_id = $this->uri->segment(3);
        $vehicle_publish_id = $this->input->post('id');
        $limit_vehicle = count($this->setting_model->Get_Single('threewheel_bike','threewheel_bike_id',$vehicle_publish_id));
        $this->setting_model->delete_data('threewheel_bike','threewheel_bike_id',$vehicle_publish_id,$limit_vehicle);
        $this->setting_model->delete_data('threewheel_bike_image','threewheel_bike_id',$vehicle_publish_id,$limit_vehicle);
        $this->setting_model->delete_data('threewheel_bike_rating','vehical_vehical_id',$vehicle_publish_id,$limit_vehicle);

        redirect('bike/viewpublised');
    }


    //insert model to database for '' --- ishara sewwandi

    public function insertmodel() {
        $data = array(
            'threewheel_bike_brand_id' => $this->input->post('threewheel_bike_brand_id'),
            'threewheel_bike_model_name' => $this->input->post('threewheel_bike_model_name')
        );

        $insert_id = $this->setting_model->insert($data, 'threewheel_bike_model');



        $this->session->set_flashdata('msg','<div class="alert alert-success">New model Added Successfully!</div>');

        redirect('bike/addmodel');
    }


    //add vehicle model function start --
    public function addmodel() {
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/3wheel/addmodel';
        $mainData = array(
            'pagetitle' => 'Add New Model',
            'threewheel_bike_brand' => $this->setting_model->Get_All('threewheel_bike_brand')
        );

        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }

    //View model function start-- Ishara sewwandi
    public function viewmodel() {

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/3wheel/viewmodel';
        $mainData = array(
            'threewheel_bike_model' => $this->setting_model->Get_All('threewheel_bike_model'),
            'threewheel_bike_brand' => $this->setting_model->Get_All('threewheel_bike_brand')

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }


    //edit model function start-- ishara sewwandi
    public function editmodel() {
        $id = $this->uri->segment(3);
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/3wheel/editmodel';
        $mainData = array(
            'threewheel_bike_model' => $this->setting_model->Get_Single('threewheel_bike_model','threewheel_bike_model_id',$id),
            'threewheel_bike_brand' => $this->setting_model->Get_All('threewheel_bike_brand')

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }

    public function updatemodel() {
        $id = $this->uri->segment(3);
        $data = array(
            'threewheel_bike_brand_id' => $this->input->post('threewheel_bike_brand_id'),
            'threewheel_bike_model_name' => $this->input->post('threewheel_bike_model_name')
        );

        $this->setting_model->update($data, 'threewheel_bike_model','threewheel_bike_model_id',$id);



        $this->session->set_flashdata('msg','<div class="alert alert-success">Model Update Successfully!</div>');

        redirect('bike/viewmodel');
    }



    //add vehicle type function start
    public function addtype() {
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/3wheel/addtype';
        $mainData = array(
            'pagetitle' => 'Add New Type',

        );

        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
    //add vehicle type function end


    //insert type to database for 'Vehicle'
    public function inserttype() {
        $data = array(
            'threewheel_bike_type_name' =>$this->input->post('threewheel_bike_type_name'),
        );

        $insert_id = $this->setting_model->insert($data, 'threewheel_bike_type');



        $this->session->set_flashdata('msg','<div class="alert alert-success">New Type Added Successfully!</div>');

        redirect('bike/viewtype');
    }
    //upload images and details to database for 'vehicle' end

    //View type function start--  ishara sewwandi
    public function viewtype() {

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/3wheel/viewtype';
        $mainData = array(
            'threewheel_bike_type_name' => $this->setting_model->Get_All('threewheel_bike_type'),

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
    //View type function end
    //edit type -- ishara sewwandi

    public function edittype(){
        $id = $this->uri->segment(3);
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/3wheel/edittype';
        $mainData = array(
            'threewheel_bike_type' => $this->setting_model->Get_Single('threewheel_bike_type','threewheel_bike_type_id',$id),

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);

    }
    //update type -- ishara sewwandi

    public function updatetype(){
        $id = $this->uri->segment(3);
        $data = array(
            'threewheel_bike_type_name' =>$this->input->post('threewheel_bike_type_name'),
        );

         $this->setting_model->update($data,'threewheel_bike_type','threewheel_bike_type_id',$id);



        $this->session->set_flashdata('msg','<div class="alert alert-success">type update Successfully!</div>');

        redirect('bike/viewtype/'.$id);
    }

    //delete entire type
    public function deletetype(){
        //$service_id = $this->uri->segment(3);
        $agent_id = $this->input->post('id');
        $this->setting_model->delete('agent','agent_id',$agent_id);
        redirect('vehicle/viewtype');
    }

    //add vehicle type function start -- ishara sewwandi
    public function addbrand() {
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/3wheel/addbrand';
        $mainData = array(
            'pagetitle' => 'Add New 3wheel & Bike Brand',
            'type' =>$this->setting_model->Get_All('threewheel_bike_type'),

        );

        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
    //add vehicle type function end




    //insert type to database for 'Vehicle' -- ishara sewwandi
    public function insertbrand() {
        $data = array(
            'threewheel_bike_brand_name' =>$this->input->post('threewheel_bike_brand_name'),
            'threewheel_type_id' =>$this->input->post('threewheel_type_id'),
        );

        $insert_id = $this->setting_model->insert($data, 'threewheel_bike_brand');



        $this->session->set_flashdata('msg','<div class="alert alert-success">New Brand Added Successfully!</div>');

        redirect('bike/viewbrand');
    }
    //upload images and details to database for 'vehicle' end

    //View type function start -- ishara sewwandi
    public function viewbrand() {

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/3wheel/viewbrand';
        $mainData = array(
            'Brand' => $this->setting_model->Get_All('threewheel_bike_brand'),
            'typee' => $this->setting_model->Get_All('threewheel_bike_type'),

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
    //View type function end

    //edit vehicle type function start -- ishara sewwandi
    public function editbrand() {
        $id = $this->uri->segment(3);
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/3wheel/editbrand';
        $mainData = array(
            'Brand' => $this->setting_model->Get_Single(' threewheel_bike_brand','threewheel_bike_brand_id',$id),
            'type' => $this->setting_model->Get_All(' threewheel_bike_type'),

        );

        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
    //edit vehicle type function end

    //insert type to database for 'Vehicle' -- ishara sewwandi
    public function updatebrand() {
        $id = $this->uri->segment(3);
        $data = array(
            'threewheel_bike_brand_name' =>$this->input->post('threewheel_bike_brand_name'),
            'threewheel_type_id' =>$this->input->post('bike_type'),
        );

         $this->setting_model->update($data, 'threewheel_bike_brand','threewheel_bike_brand_id',$id);



        $this->session->set_flashdata('msg','<div class="alert alert-success"> Brand update Successfully!</div>');

        redirect('bike/viewbrand');
    }
    //upload images and details to database for 'vehicle' end

    //delete entire type -- ishara sewwandi
    public function deletebrand(){
        //$service_id = $this->uri->segment(3);
        $vehicle_brand_id = $this->input->post('id');
        $limit_vehicle=count($this->setting_model->Get_Single('vehicle_brand','vehicle_brand_id',$vehicle_brand_id));
        $this->setting_model->delete_data('vehicle_brand','vehicle_brand_id',$vehicle_brand_id,$limit_vehicle);

        redirect('vehicle/viewbrand');
    }

    //add vehicle status function start -- ishara sewwandi
    public function addstatus() {
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/3wheel/addstatus';
        $mainData = array(
            'pagetitle' => 'Add New 3Wheel Status',

        );

        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
    //add vehicle status function end

    //insert status to database for 'Vehicle' - ishara sewwandi
    public function insertstatus() {
        $data = array(
            'threewheel_status_name' =>$this->input->post('threewheel_status_name'),
        );

        $insert_id = $this->setting_model->insert($data, 'threewheel_bike_status');



        $this->session->set_flashdata('msg','<div class="alert alert-success">New Status Added Successfully!</div>');

        redirect('bike/viewstatus');
    }
    //End insert status to database for 'Vehicle'

    //View status function start -- ishara sewwandi
    public function viewstatus() {

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/3wheel/viewstatus';
        $mainData = array(
            'threewheel_bike_status' => $this->setting_model->Get_All('threewheel_bike_status'),

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
    //View status function end

    // edit status -- Ishara sewwandi
    public function editstatus(){
        $id = $this->uri->segment(3);
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/3wheel/editstatus';
        $mainData = array(
            'threewheel_bike_status' => $this->setting_model->Get_Single('threewheel_bike_status','threewheel_bike_status_id',$id),

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
    // end edit

    //update status -- ishara sewwandi

    public function updatestatus(){
        $id = $this->uri->segment(3);
        $data = array(
            'threewheel_status_name' =>$this->input->post('threewheel_status_name'),
        );

       $this->setting_model->update($data, 'threewheel_bike_status','threewheel_bike_status_id',$id);



        $this->session->set_flashdata('msg','<div class="alert alert-success">3wheel & Bike Status update Successfully!</div>');

        redirect('bike/viewstatus');
    }
    //end update

    //delete entire type
    public function deletestatus(){
        //$service_id = $this->uri->segment(3);
        $vehicle_status_id = $this->input->post('id');
        $limit_vehicle=count($this->setting_model->Get_Single('vehicle_status','vehicle_status_id',$vehicle_status_id));
        $this->setting_model->delete_data('vehicle_status','vehicle_status_id',$vehicle_status_id,$limit_vehicle);

        redirect('vehicle/viewstatus');
    }
    //add vehicle page
	public function add3wheel(){
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/3wheel/add3wheel';
		$mainData = array(
			'pagetitle' => 'Add Bike & 3Wheel',
			'threewheel_bike_type' => $this->setting_model->Get_All('threewheel_bike_type'),
			'threewheel_bike_brand' => $this->setting_model->Get_All('threewheel_bike_brand'),
			'threewheel_bike_model' => $this->setting_model->Get_All('threewheel_bike_model'),
			'threewheel_bike_status' => $this->setting_model->Get_All('threewheel_bike_status'),
		);

		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
    public function insert3wheel(){
	
		$vehicleData = array(
			'threewheel_bike_type_id' =>$this->input->post('threewheel_bike_type_id'),
			'threewheel_bike_brand_id' =>$this->input->post('threewheel_bike_brand_id'),
			'threewheel_bike_model_id' =>$this->input->post('threewheel_bike_model_id'),
			'transmission' =>$this->input->post('transmission'),
			'fuel_type'=>$this->input->post('fuel_type'),
            		'features'=>$this->input->post('features'),
            		'average_fuel_consuption'=>$this->input->post('average_fuel_consuption'),
            		'mileage_range'=>$this->input->post('mileage_range'),
			'Year_of_manufacture' =>$this->input->post('Year_of_manufacture'),
			'year_of_registration' =>$this->input->post('year_of_registration'),
			'number_of_owners' =>$this->input->post('number_of_owners'),
			'threewheel_bike_number' =>$this->input->post('vehicle_number'),
			'engine_capacity' => $this->input->post('engine_capacity'),
			'total_price' =>$this->input->post('total_price'),
            		'minimum_downpayment' =>$this->input->post('minimum_downpayment'),
            		'seller_description' =>$this->input->post('seller_description'),
			'publish_status' => 1,
            		'admin_customer' => 1,

			);

		$insert_id = $this->setting_model->insert($vehicleData, 'threewheel_bike');
		$files = $_FILES;
		$path = "3wheel";
		$fileName = $this->multiple_image_uplaod->multi_image_upload($files,$path);
		$this->multiple_image_uplaod->upload_image_3wheel_vehicle($fileName,$insert_id);
		//print_r($fileName);
		// $this->multiple_image_uplaod->upload_image_outside($this->input->post(),$fileName);
			$this->session->set_flashdata('msg','<div class="alert alert-success">New Vehicle Added Successfully!</div>');
			redirect('bike/add3wheel');
    }
    
    public function editbike3wheel() {
        $id = $this->uri->segment(3);

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/3wheel/edit3wheelbike';

        $mainData = array(
            'wheel' => $this->setting_model->Get_Single('threewheel_bike','threewheel_bike_id',$id),
            'threewheel_bike_image' => $this->multiple_image_uplaod->edit_image_3wheel_vehicle($id),
            'threewheel_bike_type'=> $this->setting_model->Get_All('threewheel_bike_type'),
            'brthreewheel_bike_brandands'=> $this->setting_model->Get_All('threewheel_bike_brand'),
            'threewheel_bike_model'=> $this->setting_model->Get_All('threewheel_bike_model'),
        );

        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);

    }

    public function updatebike(){
        $id = $this->uri->segment(3);

        $data = array(

            'threewheel_bike_type_id' =>$this->input->post('threewheel_bike_type_id'),
            'threewheel_bike_brand_id' =>$this->input->post('threewheel_bike_brand_id'),
            'threewheel_bike_model_id' =>$this->input->post('threewheel_bike_model_id'),
            'transmission' =>$this->input->post('transmission'),
            'fuel_type'=>$this->input->post('fuel_type'),
            'mileage_range'=>$this->input->post('mileage_range'),
            'Year_of_manufacture' =>$this->input->post('Year_of_manufacture'),
            'year_of_registration' =>$this->input->post('year_of_registration'),
            'number_of_owners' =>$this->input->post('number_of_owners'),
            'threewheel_bike_number' =>$this->input->post('vehicle_number'),
            'engine_capacity' => $this->input->post('engine_capacity'),
            'total_price' =>$this->input->post('total_price'),
            'average_fuel_consuption' =>$this->input->post('average_fuel_consuption'),
            'features' =>$this->input->post('features'),
            'minimum_downpayment' =>$this->input->post('minimum_downpayment'),
            'seller_description' =>$this->input->post('seller_description'),

        );

        $data1 = $this->setting_model->update($data,'threewheel_bike','threewheel_bike_id',$id);

        //edit multiple image upload
        if(count($_FILES['userfile']['name'])>1){
        $files = $_FILES;
        $path = "3wheel";
        $fileName = $this->multiple_image_uplaod->multi_image_upload($files,$path);
        $this->multiple_image_uplaod->edit_upload_image_wheel($fileName,$id);
        //end multiple image upload edit
	}
        $this->session->set_flashdata('msg','<div class="alert alert-success">New Car Added Successfully!</div>');

        redirect('bike/viewbike/'.$id);
    }

    public function deletewheel(){
        //$service_id = $this->uri->segment(3);
        $vehicle_wheel_id = $this->input->post('id');
        $limit_vehicle = count($this->setting_model->Get_Single('threewheel_bike','threewheel_bike_id',$vehicle_wheel_id));
        $this->setting_model->delete_data('threewheel_bike','threewheel_bike_id',$vehicle_wheel_id,$limit_vehicle);
        $this->setting_model->delete_data('threewheel_bike_image','threewheel_bike_id',$vehicle_wheel_id,$limit_vehicle);

        redirect('bike/viewbike');
    }
    
 //=========================================================================================================
    
    
    public function viewInquiry(){

	$headerData = null;
	$sidebarData = null;
	$page = 'admin/3wheel/Inquiry/viewpending';
	$mainData = array(
		
		'vehicles' => $this->setting_model->Get_All('threewheel_bike'),
		'customer' => $this->setting_model->Get_All('customer'),
		'types' => $this->setting_model->Get_All('threewheel_bike_type'),
		'brands' => $this->setting_model->Get_All('threewheel_bike_brand'),
		'model' => $this->setting_model->Get_All('threewheel_bike_model'),
		//'inside_images' => $this->setting_model->Get_All('inside_images'),
		'outside_images' => $this->setting_model->Get_All('threewheel_bike_image'),
		'inquiry' => $this->setting_model->Get_All('threewheel_bike_inquery'),
		'inquiry_join' => $this->setting_model->inquiryjoin()
		
	);
	$footerData = null;

	$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}
	
	  // start function for followup renting inquiry --------------------- coded by Gihan

    public function followUp(){

        $id = $this->input->post("inq_id");
        $date = date('Y-m-d');
        $data = array(
            'status' => 1,
            'status_change_date' => $date,
            'followUp_description' =>$this->input->post('Follow_up_description'),
            'next_followup_date' =>$this->input->post('next_date'),
        );

        $data1 = $this->setting_model->update($data,'threewheel_bike_inquery','threewheel_bike_inquery_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-warning">Follow Up Successfully!</div>');

        redirect('Bike/viewInquiry');
    }
// end function --------------------------------------------------------------------

	public function viewFollowUp(){

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/3wheel/Inquiry/viewfollowup';
        $mainData = array(

            'vehicles' => $this->setting_model->Get_All('threewheel_bike'),
	    'customer' => $this->setting_model->Get_All('customer'),
	    'types' => $this->setting_model->Get_All('threewheel_bike_type'),
	    'brands' => $this->setting_model->Get_All('threewheel_bike_brand'),
	    'model' => $this->setting_model->Get_All('threewheel_bike_model'),
	    'outside_images' => $this->setting_model->Get_All('threewheel_bike_image'),
	    'inquiry' => $this->setting_model->Get_All('threewheel_bike_inquery'),
		//'inquiry_join' => $this->setting_model->inquiryjoin()

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }

    public function updateDate1(){
        // print_r($inq_id);
        $id = $this->uri->segment(3);
        $date = $this->input->post('changed_date');
        $data = array(
            'next_followup_date' => $this->input->post('changed_date')
        );

        $data1 = $this->setting_model->update($data,'threewheel_bike_inquery','threewheel_bike_inquery_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-success">Update date Successful!</div>');

        redirect('Bike/viewFollowUp');
    }

    // start function for confirm renting inquiry ------------------ coded by Gihan
    public function confirm(){

        $id = $this->uri->segment(3);
        $date = date('Y-m-d');
        $data = array(
            'status' => 2,
            'status_change_date' => $date,
        );

        $data1 = $this->setting_model->update($data,'threewheel_bike_inquery','threewheel_bike_inquery_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-success">Inquiry Confirm Successful!</div>');

         redirect('Bike/viewInquiry');
    }
// end function ------------------------------------------------------------------


// start function for reject renting inquiry -------------------- coded by Gihan

    public function reject(){

        $id = $this->input->post('inq_id');
        $date = date('Y-m-d');
        $data = array(
            'status' => 3,
            'reject_reason' => $this->input->post('reject_reason'),
            'status_change_date' => $date,
        );

        $data1 = $this->setting_model->update($data,'threewheel_bike_inquery','threewheel_bike_inquery_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-danger"> Reject Successful!</div>');

         redirect('Bike/viewInquiry');
    }
// end function---------------------------------------------------------------------

// start function for confirm renting inquiry ------------------ coded by Gihan
    public function confirm1(){

        $id = $this->uri->segment(3);
        $date = date('Y-m-d');
        $data = array(
            'status' => 2,
            'status_change_date' => $date,
        );

        $data1 = $this->setting_model->update($data,'threewheel_bike_inquery','threewheel_bike_inquery_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-success">Inquiry Confirm Successful!</div>');

        redirect('Bike/viewFollowUp');
    }
// end function ------------------------------------------------------------------


// start function for reject renting inquiry -------------------- coded by Gihan

    public function reject1(){

        $id = $this->input->post('inq_id');
        $date = date('Y-m-d');
        $data = array(
            'status' => 3,
            'reject_reason' => $this->input->post('reject_reason'),
            'status_change_date' => $date,
        );

        $data1 = $this->setting_model->update($data,'threewheel_bike_inquery','threewheel_bike_inquery_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-danger"> Reject Successful!</div>');

        redirect('Bike/viewFollowUp');
    }
// end function---------------------------------------------------------------------

public function confirm2(){

        $id = $this->uri->segment(3);
        $date = date('Y-m-d');
        $data = array(
            'status' => 2,
            'status_change_date' => $date,
        );

        $data1 = $this->setting_model->update($data,'threewheel_bike_inquery','threewheel_bike_inquery_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-success">Inquiry Confirm Successful!</div>');

        redirect('Bike/viewRejected1');
    }
// end function ------------------------------------------------------------------


// start function for reject renting inquiry -------------------- coded by Gihan

   public function followUp1(){

        $id = $this->input->post("inq_id");
        $date = date('Y-m-d');
        $data = array(
            'status' => 1,
            'status_change_date' => $date,
            'followUp_description' =>$this->input->post('Follow_up_description'),
            'next_followup_date' =>$this->input->post('next_date'),
        );

        $data1 = $this->setting_model->update($data,'threewheel_bike_inquery','threewheel_bike_inquery_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-warning">Follow Up Successfully!</div>');

        redirect('Bike/viewRejected1');
    }
// start function for view confirmed inquiries ---------------------- coded by Gihan
	
	public function pending(){

        $id = $this->input->post("inq_id");
        $date = date('Y-m-d');
        $data = array(
            'status' => null,
            'status_change_date' => $date,
            
        );

        $data1 = $this->setting_model->update($data,'threewheel_bike_inquery','threewheel_bike_inquery_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-sucuess">Back to Pending Successfully!</div>');

        redirect('Bike/viewRejected1');
    }


    public function viewConfirm(){

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/3wheel/Inquiry/viewconfirm';
        $mainData = array(

            'vehicles' => $this->setting_model->Get_All('threewheel_bike'),
	    'customer' => $this->setting_model->Get_All('customer'),
	    'types' => $this->setting_model->Get_All('threewheel_bike_type'),
	    'brands' => $this->setting_model->Get_All('threewheel_bike_brand'),
	    'model' => $this->setting_model->Get_All('threewheel_bike_model'),
		//'inside_images' => $this->setting_model->Get_All('inside_images'),
	    'outside_images' => $this->setting_model->Get_All('threewheel_bike_image'),
	    'inquiry' => $this->setting_model->Get_All('threewheel_bike_inquery'),

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
// end function -----------------------------------------------------------------------


// start function for view rejected inquiries ------------------------- coded by Gihan

    public function viewRejected1(){

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/3wheel/Inquiry/viewrejected';
        $mainData = array(
        
 	    'vehicles' => $this->setting_model->Get_All('threewheel_bike'),
	    'customer' => $this->setting_model->Get_All('customer'),
	    'types' => $this->setting_model->Get_All('threewheel_bike_type'),
	    'brands' => $this->setting_model->Get_All('threewheel_bike_brand'),
	    'model' => $this->setting_model->Get_All('threewheel_bike_model'),
		//'inside_images' => $this->setting_model->Get_All('inside_images'),
	    'outside_images' => $this->setting_model->Get_All('threewheel_bike_image'),
	    'inquiry' => $this->setting_model->Get_All('threewheel_bike_inquery'),

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
// end function -----------------------------------------------------------------------
	
	//-------------------add inquiry to reservation table----------------------------------

    public function paymentCollected(){
        $in_id = $this->input->post('inq_id');

        $date = date('Y-m-d');

        $inq_table = $this->setting_model->Get_All_Where('threewheel_bike_inquery','threewheel_bike_inquery_id',$in_id);

            foreach($inq_table as $data){
                $v_id = $data['threewheel_bike_id'];
                $cus_name = $data['customer_name'];
                $cus_email = $data['email_address'];
                $phone = $data['phone_number'];
                $nic_number = $data['NIC_number'];
            }

            $password = (rand(10,100000));
            //print_r($password);

            $data5 = array(
                'customer_name' => $cus_name,
                'email' => $cus_email,
                'phone_number' => $phone,
                'NIC_number' => $nic_number,
                'password' => (md5($password)),
            );
            
            $insert_id = $this->setting_model->insert($data5,'customer');

            $data1 = array(

                'threewheel_bike_id' => $v_id,
                'customer_id' => $insert_id,
                'buying_method' => $this->input->post('checkbox'),
                'price' => $this->input->post('price'),
                'downpayment' => $this->input->post('downpayment'),
                'monthly_installment' => $this->input->post('monthly_ins'),
                'duration' => $this->input->post('dura'),
                'status' => NULL,
                'status_changed_date' => $date
            );

            $data2 = $this->setting_model->insert($data1,'threewheel_bike_reservation');

            $data3 = array(

            'user_name' => $cus_name,
            'contact_number' => $phone,
            'email' => $cus_email,
            'password' => md5($password),
            'verification_status' => 0,
            'user_type' => 0
        );

        $id1 = $this->setting_model->insert($data3,'login');

        $data4 = $this->setting_model->delete('threewheel_bike_inquery','threewheel_bike_inquery_id',$in_id);

        $this->session->set_flashdata('msg','<div class="alert alert-success">Reservation Successful!</div>');

        // $this->senduserverification($id,$password);

        //redirect('bike/viewConfirm');

        redirect('bike/senduserverification/'.$id1.'/'.$password);
    }
    
    public function senduserverification(){
        $id = $this->uri->segment(3);
        $flashdata = $this->uri->segment(5);
        $useradd=$this->setting_model->Get_Single('login','user_id',$id);
        foreach ($useradd as $key ) {
            $user_name = $key['user_name'];
            $user_mail = $key['email'];
            $user_password = $this->uri->segment(4);
            //$user_type = $key['user_type'];
        }
        $admin_name = $this->session->userdata('username');
        $address = $this->config->base_url();
        $link = '<a href ='.$address.'bike/verifynewuser/?email='.$user_mail.'>here</a>'; //link for user verification

       $this->_send_contact_email1( $id, $user_name,$user_password, $user_mail, $link);
         $this->_response( array( "status" => "true", "message" => "Thank you for contacting us." ) );
         $this->session->set_flashdata('success', 'registration successfull.. check your email verification and new password.');
        }


private function _send_contact_email1($id , $user_name, $user_password, $user_mail,$link){

    $log_path =  base_url('/assets/brand-logo.png');

    $message = "";
    
    $message .= "<p style=\"font-size: 1.2em; padding: 10px; margin-bottom: 0;\">* This is an auto genarated e-mail for a request of a live web use.</p>";
    $message .= "<table style=\"width: 60%; min-width: 500px;\">";
    $message .= "<tbody style=\"border: 1px solid #a8a8a8;\">";
    $message .= "<tr style=\"background-color: white;\">";
    $message .= "<td style=\"padding: 15px; text-align: center;\" colspan=2>";
    $message .= "<img style=\"padding: 10px; height: auto; width: 30%;\" src=\"$log_path\"/>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">User Name:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$user_name</p>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">User Password:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$user_password</p>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">User email:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$user_mail</p>";
    $message .= "</td>";
    $message .= "</tr>";   
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Verificatrion Link:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$link</p>";
    $message .= "</td>";
    $message .= "</tr>"; 
   
    $message .= "</tbody>";
    $message .= "</table>";
    
    
    
    $emailReceivers = array(
        $user_mail
    );
    
    foreach( $emailReceivers as $emailReceiver) {
        
        $from = 'Autosure';
        $to = $emailReceiver;
        $subject = "Verification Mail ";
        
        $config = array();
        $config["protocol"] = 'smpt';
        $config['smtp_host'] = 'bckonnect.com';
        $config["smtp_port"] = 465;
        $config["smtp_user"] = 'publicuser@bckonnect.com';
        $config["smtp_pass"] = 'PaSS@bckonnect.com';
        $config['mailtype'] = 'html';
        $this->email->initialize($config);

        $this->email->from($from, 'Verification Email');
        $this->email->to($to);              
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->send();                    
    }            
    
    return true;
    redirect('bike/viewConfirm');
    
    }
    
     public function verifynewuser(){
        $email = $this->input->get('email');
        $user_detail = $this->setting_model->Get_Single('login','email',$email);
        
        if(!empty($user_detail)){
            
            foreach($user_detail as $data){
                $status = $data['verification_status'];
                $name = $data['user_name'];
            }
            
            if($status==0){
            
                $Data = array(
                    'verification_status'=>1
                );
                $this->setting_model->update($Data,'login','email',$email);
                $message = '<div class="alert alert-success">Hi '.$name.',<br/> Your user account is successfully verified !</div>';
                $this->session->set_flashdata('msg',$message);
                $this->load->view('verification');
            }
            
            else{
                $message = '<div class="alert alert-danger">Hi '.$name.',<br/> Your user account is already verified !</div>';
                $this->session->set_flashdata('msg',$message);
                $this->load->view('verification');
            
            }
        }
        
        else{
            $this->session->set_flashdata('msg','<div class="alert alert-danger">Something wrong! Please try again</div>');
            $this->load->view('verification');
        }
    }


//-------------------end function------------------------------------------------------

// start function for view pending and followup reservation page ----- coded by Gihan 
    public function viewReservation(){

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/3wheel/reservation/reservationinquiry';
        $mainData = array(
            
            'vehicles' => $this->setting_model->Get_All('threewheel_bike'),
            'customer' => $this->setting_model->Get_All('customer'),
            'types' => $this->setting_model->Get_All('threewheel_bike_type'),
            'brands' => $this->setting_model->Get_All('threewheel_bike_brand'),
            'model' => $this->setting_model->Get_All('threewheel_bike_model'),
            'outside_images' => $this->setting_model->Get_All('threewheel_bike_image'),
            'reservation' => $this->setting_model->Get_All('threewheel_bike_reservation')
            
        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
// end function for view pending and followup reservation page

// Start function for creating Follow up ------ coded by Gihan

    public function followUpRes(){
        // print_r($inq_id);
        $id = $this->input->post("res_id");
        // print_r($id);
        $date = date('Y-m-d');
        $data = array(
            'status' => 1,
            'status_changed_date' => $date,
            'followUp_description' =>$this->input->post('Follow_up_description'),
            'next_followup_date' =>$this->input->post('next_date'),
            );

        $data1 = $this->setting_model->update($data,'threewheel_bike_reservation','threewheel_bike_reservation_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-warning">Follow up Successfully!</div>');

        redirect('Bike/viewReservation');
    }
// End function for creating Follow up

// Start function for confirm inquiry ------- coded by Gihan

    public function confirmRes(){
        
        $id = $this->uri->segment(3);
        $date = date('Y-m-d');
        $data = array(
            'status' => 2,
            'status_changed_date' => $date,
            );

        $data1 = $this->setting_model->update($data,'threewheel_bike_reservation','threewheel_bike_reservation_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-success">Payment Collected Successfully!</div>');

        redirect('Bike/viewReservation');
    }
// End function for confirm inquiry

// start function for view payment collected page -------- coded by Gihan

    public function viewConfirmReservations(){

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/3wheel/reservation/paymentCollected';
        $mainData = array(
            
            'vehicles' => $this->setting_model->Get_All('threewheel_bike'),
            'customer' => $this->setting_model->Get_All('customer'),
            'types' => $this->setting_model->Get_All('threewheel_bike_type'),
            'brands' => $this->setting_model->Get_All('threewheel_bike_brand'),
            'model' => $this->setting_model->Get_All('threewheel_bike_model'),
            'outside_images' => $this->setting_model->Get_All('threewheel_bike_image'),
            'reservation' => $this->setting_model->Get_All('threewheel_bike_reservation')
            
        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
// End function for view payment collected page

// Start Function for reject reservation on reservation page ------ coded by Gihan

    public function rejectRes(){
        // print_r($inq_id);
        $id = $this->input->post('res_id');
        //print_r($id);
        $date = date('Y-m-d');
        $data = array(
            'status' => 3,
            'reject_reason' => $this->input->post('reject_reason'),
            'status_changed_date' => $date,
            );

        $data1 = $this->setting_model->update($data,'threewheel_bike_reservation','threewheel_bike_reservation_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-danger">Reject Successful!</div>');

        redirect('Bike/viewReservation');
    }
// End Function for reject reservation on reservation page

// Start function for view rejected reservations page ------- coded by Gihan

    public function rejectReservation(){

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/3wheel/reservation/viewRejected';
        $mainData = array(
            
            'vehicles' => $this->setting_model->Get_All('threewheel_bike'),
            'customer' => $this->setting_model->Get_All('customer'),
            'types' => $this->setting_model->Get_All('threewheel_bike_type'),
            'brands' => $this->setting_model->Get_All('threewheel_bike_brand'),
            'model' => $this->setting_model->Get_All('threewheel_bike_model'),
            'outside_images' => $this->setting_model->Get_All('threewheel_bike_image'),
            'reservation' => $this->setting_model->Get_All('threewheel_bike_reservation')
            
        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }

// End function for view rejected reservations page

// Start function for the followup on rejected reservations page ------ coded by Gihan

    public function followUp1Res(){
        // print_r($inq_id);
        $id = $this->input->post("res_id");
        // print_r($id);
        $date = date('Y-m-d');
        $data = array(
            'status' => 1,
            'status_changed_date' => $date,
            'followUp_description' =>$this->input->post('Follow_up_description'),
            'next_followup_date' =>$this->input->post('next_date'),
            );

        $data1 = $this->setting_model->update($data,'threewheel_bike_reservation','threewheel_bike_reservation_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-warning">Follow up Successful!</div>');

        redirect('bike/rejectReservation');
    }
// End function for the followup on rejected reservations page

// Start function for the paymentcollected on rejected reservations page--- coded by Gihan

    public function confirm1Res(){
        
        $id = $this->uri->segment(3);
        $date = date('Y-m-d');
        $data = array(
            'status' => 2,
            'status_changed_date' => $date,
            );

        $data1 = $this->setting_model->update($data,'threewheel_bike_reservation','threewheel_bike_reservation_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-success">Confirm Successful!</div>');

        redirect('bike/rejectReservation');
    }

// End function for the payment collected on rejected reservations page

// Start function for back to pending on rejected reservation page ----- coded by Gihan  

    public function pendingRes(){
        $id = $this->uri->segment(3);

        $date = date('Y-m-d');
        $data = array(
            'status' => NULL,
            'status_changed_date' => NULL,
            'followUp_description' => NULL,
            'next_followup_date' => NULL,
            );

        $data1 = $this->setting_model->update($data,'threewheel_bike_reservation','threewheel_bike_reservation_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-success">Pending Successful!</div>');

        redirect('bike/rejectReservation');
    }
// End function for back to pending on rejected reservation page


// start function for change date
    public function updateDateRes(){
        // print_r($inq_id);
        $id = $this->uri->segment(3);
        $date = $this->input->post('changed_date');
        $data = array(
            'next_followup_date' => $this->input->post('changed_date')
            );

         $data1 = $this->setting_model->update($data,'threewheel_bike_reservation','threewheel_bike_reservation_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-success">Update date Successful!</div>');

        redirect('Bike/viewReservation');
    }

    
    
    
    
	
}