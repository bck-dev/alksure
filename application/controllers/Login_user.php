<?php
/**
 * Created by PhpStorm.
 * User: yaish
 * Date: 10/23/2018
 * Time: 11:42 AM
 */

class Login_user extends CI_Controller{
    public function index($msg = NULL) {
        // Load our view to be displayed
        // to the user
        $data['msg'] = $msg;
        $this->load->view('admin/login',$data);
    }

    public function process() {

        if ($_POST) {

            $data = array(
                'email' => $this->input->post('email'),
                'password' => $this->input->post('password')
            );

            $result = $this->user_login_model->is_user_exist($data);
            // var_dump($result);
            if (!empty($result)) {


                $verification = $result->verification_status;

                if($verification==1){

                    $data = array(
                        'id' => $result->user_id,
                        'username' => $result->user_name,
                        'usertype' => $result->user_type, // 1-admin, 2-editor, 3-feader
                        'email'=>$result->email,
                        'contactnumber'=>$result->contact_number,
                        'address'=>$result->address,
                        'validated' => true
                    );

                    $this->session->set_userdata($data);
		$this->session->set_flashdata('success', 'Login Successful.');
                    redirect('Home');
                }

		else {

                    $this->session->set_flashdata('error', 'Your username & password is not verified...Please check your email for verification mail');
                    redirect('Home');
                }
            }else {

                $this->session->set_flashdata('error', 'Username or password is incorrect.');
                redirect('Home');
            }
        }

    }

    public function logout() {

        $data = ['id', 'username', 'usertype', 'validated'];
        $this->session->unset_userdata($data);

        redirect('home');
    }

   

    public function error(){
        echo '<div class="success" style="padding: 2px 2px;">Password Fields Mismatch</div>';
    }
    
    public function register(){

        $firstname = $this->input->post('fname');
        $lastname = $this->input->post('lname');

        $full_name= $firstname.' '.$lastname ;
        $password = $this->input->post('password');
        $confirm_password = $this->input->post('confirm_password');
        if($password!=$confirm_password){
            echo '<div class="error" style="padding: 20px 20px;">Password Fields Mismatch</div>';

        }else{
        
         $customerData = array(
                'customer_name' => $full_name,
                'phone_number' => $this->input->post('number'),
                'email' => $this->input->post('email'),
                'address_line_1' => $this->input->post('address'),
                'password' => md5($this->input->post('password')),

            );
            $this->setting_model->insert($customerData, 'selling_customer');
        
        $Data = array(

            'user_name' => $full_name,
            'nic_no' => $this->input->post('nic'),
            'contact_number' => $this->input->post('number'),
            'email' => $this->input->post('email'),
            'address' => $this->input->post('address'),
            'password' => md5($this->input->post('password')),
            'verification_status' => 0


        );}


     $id =$this->setting_model->insert($Data,'login');

     
       redirect('login_user/senduserverification/'.$id.'/'.$password);
         
    }
    private function _response(array $data = array(), $code = 200) {
 $this->output
 ->set_status_header($code)
 ->set_content_type('application/json')
 ->set_output(json_encode($data));
}

public function senduserverification(){
		$id = $this->uri->segment(3);
		$flashdata = $this->uri->segment(5);
		$useradd=$this->setting_model->Get_Single('login','user_id',$id);
		foreach ($useradd as $key ) {
			$user_name = $key['user_name'];
			$user_mail = $key['email'];
			$user_password = $this->uri->segment(4);
			//$user_type = $key['user_type'];
  		}
		$admin_name = $this->session->userdata('username');
		$address = $this->config->base_url();
		$link = '<a href ='.$address.'login_user/verifynewuser/?email='.$user_mail.'>here</a>'; //link for user verification

       $this->_send_contact_email( $id, $user_name,$user_password, $user_mail, $link);
         $this->_response( array( "status" => "true", "message" => "Thank you for contacting us." ) );
         $this->session->set_flashdata('success', 'registration successfull.. check your email verification.');
          
		}


private function _send_contact_email($id , $user_name, $user_password, $user_mail,$link){


    $log_path =  base_url('/assets/brand-logo.png');
    
    
    
    $message = "";
    
    $message .= "<p style=\"font-size: 1.2em; padding: 10px; margin-bottom: 0;\">* This is an auto genarated e-mail for a request of a live web use.</p>";
    $message .= "<table style=\"width: 60%; min-width: 500px;\">";
    $message .= "<tbody style=\"border: 1px solid #a8a8a8;\">";
    $message .= "<tr style=\"background-color: white;\">";
    $message .= "<td style=\"padding: 15px; text-align: center;\" colspan=2>";
    $message .= "<img style=\"padding: 10px; height: auto; width: 30%;\" src=\"$log_path\"/>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">User Name:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$user_name</p>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">User Password:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$user_password</p>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">User email:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$user_mail</p>";
    $message .= "</td>";
    $message .= "</tr>";   
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Verificatrion Link:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$link</p>";
    $message .= "</td>";
    $message .= "</tr>"; 
    
   
    
    

    

    
    
    
    
    $message .= "</tbody>";
    $message .= "</table>";
    
    
    
    $emailReceivers = array(
        $user_mail
    );
    
    foreach( $emailReceivers as $emailReceiver) {
        
        $from = 'Autosure';
        $to = $emailReceiver;
        $subject = "Verification Mail ";
        
        $config = array();
        $config["protocol"] = 'smpt';
        $config['smtp_host'] = 'bckonnect.com';
        $config["smtp_port"] = 465;
        $config["smtp_user"] = 'publicuser@bckonnect.com';
        $config["smtp_pass"] = 'PaSS@bckonnect.com';
        $config['mailtype'] = 'html';
        $this->email->initialize($config);

        $this->email->from($from, 'Verification Email');
        $this->email->to($to);              
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->send();                    
    }            
    
    return true;
    redirect('home');
    
}
		






    
	
	 public function verifynewuser(){
		$email = $this->input->get('email');
		$user_detail = $this->setting_model->Get_Single('login','email',$email);
		
		if(!empty($user_detail)){
			
			foreach($user_detail as $data){
				$status = $data['verification_status'];
				$name = $data['user_name'];
			}
			
			if($status==0){
			
				$Data = array(
					'verification_status'=>1
				);
				$this->setting_model->update($Data,'login','email',$email);
				$message = '<div class="alert alert-success">Hi '.$name.',<br/> Your user account is successfully verified !</div>';
				$this->session->set_flashdata('msg',$message);
				$this->load->view('verification');
			}
			
			else{
				$message = '<div class="alert alert-danger">Hi '.$name.',<br/> Your user account is already verified !</div>';
				$this->session->set_flashdata('msg',$message);
				$this->load->view('verification');
			
			}
		}
		
		else{
			$this->session->set_flashdata('msg','<div class="alert alert-danger">Something wrong! Please try again</div>');
			$this->load->view('verification');
		}
	}


}