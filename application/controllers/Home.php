<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function template($headerData, $page, $mainData, $footerData) {
		$headerData = array(
			'type' => $this->setting_model->Get_All('vehicle_type'),
            'make' => $this->setting_model->Get_All('vehicle_brand'),
            'model' => $this->setting_model->Get_All('vehicle_model'),
        );

		$mainData['ad'] = $this->setting_model->Get_All('advertisement');

		$this->load->view('components/header', $headerData);
		$this->load->view($page, $mainData);
		$this->load->view('components/footer', $footerData);
//		$this->load->view('components/bottom-script-links', $footerlink);
	}

	public function index() {
	    $this->output->delete_cache();
        $id1 = $this->uri->segment(3);
        $headerlink = null;
        $headerData = null;
        $page = 'index';

        $popularvehicles = [];
        $i=0;

        $date = new DateTime(date('Y-m-d'));
        $week = $date->format("W"); 
        
        $popularThisWeek = $this->setting_model->popular_vehicles_home($week);
        
        foreach ($popularThisWeek as $popular){
                       
            $popularvehicles[$i]= $this->setting_model->vehiclejoin1($popular['vehicle_id'])[0];
            $i++;
        }
        
        $mainData = array(
            'slider' => $this->setting_model->Get_All('slider'),
            'grid' => $this->setting_model->Get_All('grid'),
            'insideimage' => $this->setting_model->Get_All('inside_images'),
            'outside' => $this->setting_model->Get_All('out_side_images'),
            'vehicle' => $this->setting_model->frontjoin($id1),
            'popularvehicles' => $popularvehicles,
            'vehiclebrand' => $this->setting_model->Get_All('vehicle_brand'),
            'vehiclemodel' => $this->setting_model->Get_All('vehicle_model'),
            'vehicletype' => $this->setting_model->Get_All('vehicle_type'),
            'front' => $this->setting_model->Get_All('vehicle_front_image'),
        );

        $footerData = null;
        $footerlink= null;

        $this->template($headerData, $page, $mainData, $footerData);
    }
    
    public function import() {
        $headerlink = null;
        $headerData = null;
        $page = 'import';
        $mainData = array(
            'type' => $this->setting_model->Get_All('import_vehical_type'),
            'brand' => $this->setting_model->Get_All('import_vehicle_brand'),
            'model' => $this->setting_model->Get_All('import_vehicle_model'),
            'vehicle' => $this->setting_model->Get_All('import_vehicle'),
            'Front_image' => $this->setting_model->Get_All('import_front_image')
        );

        $footerData = null;
        $footerlink= null;

        $this->template($headerData, $page, $mainData, $footerData);
    }

    public function importVehicle() {
        $vehi_id = $this->uri->segment(3);

        $headerlink = null;
        $headerData = null;
        $page = 'import_inner';
        $mainData = array(
            'type' => $this->setting_model->Get_All('import_vehical_type'),
            'brand' => $this->setting_model->Get_All('import_vehicle_brand'),
            'model' => $this->setting_model->Get_All('import_vehicle_model'),
            'vehicle' => $this->setting_model->Get_All_Where('import_vehicle','vehicle_id',$vehi_id),
            'Front_image' => $this->setting_model->Get_All('import_front_image'),
            'outside_image' => $this->setting_model->Get_All('import_outside_images'),
        );

        $footerData = null;
        $footerlink= null;

        $this->template($headerData, $page, $mainData, $footerData);
    }

    public function importtypeSearch(){

        $type = $this->input->post('vehi_type');
        // print_r($type);

        $headerData = null;
        $page = 'importVehiType';

        $mainData = array(
            'import' => $this->setting_model->importviewvehiclejoin1($type),
            'vehiclebrand' => $this->setting_model->Get_All('vehicle_brand'),
            'vehiclemodel' => $this->setting_model->Get_All('vehicle_model'),
            'vehicletype' => $this->setting_model->Get_All('vehicle_type'),
            'Front_image' => $this->setting_model->Get_All('import_front_image'),

        );

        $footerData = null;
        // $this->template($headerData, $page, $mainData, $footerData);
        $this->load->view('importVehiType',$mainData);
    }

    public function importbrandSearch(){

        $type = $this->input->post('vehi_type');
        $brand = $this->input->post('vehi_brand');
        // print_r($type);

        $headerData = null;
        $page = 'importVehiBrand';

        $mainData = array(
            'import' => $this->setting_model->importviewvehiclejoin2($type,$brand),
            'vehiclebrand' => $this->setting_model->Get_All('vehicle_brand'),
            'vehiclemodel' => $this->setting_model->Get_All('vehicle_model'),
            'vehicletype' => $this->setting_model->Get_All('vehicle_type'),
            'Front_image' => $this->setting_model->Get_All('import_front_image'),

        );

        $footerData = null;
        // $this->template($headerData, $page, $mainData, $footerData);
        $this->load->view('importVehiBrand',$mainData);
    }

    public function importmodelSearch(){

        $type = $this->input->post('vehi_type');
        $brand = $this->input->post('vehi_brand');
        $model = $this->input->post('vehi_model');
        // print_r($type);

        $headerData = null;
        $page = 'importVehiModel';

        $mainData = array(
            'import' => $this->setting_model->importviewvehiclejoin3($type,$brand,$model),
            'vehiclebrand' => $this->setting_model->Get_All('vehicle_brand'),
            'vehiclemodel' => $this->setting_model->Get_All('vehicle_model'),
            'vehicletype' => $this->setting_model->Get_All('vehicle_type'),
            'Front_image' => $this->setting_model->Get_All('import_front_image'),

        );

        $footerData = null;
        // $this->template($headerData, $page, $mainData, $footerData);
        $this->load->view('importVehiModel',$mainData);
    }

    public function importinner() {
        $headerlink = null;
        $headerData = null;
        $page = 'vehicle-import-inner';
        $mainData = array(

        );

        $footerData = null;
        $footerlink= null;

        $this->template($headerData, $page, $mainData, $footerData);
    }
    
    // import inquiry process --
    
public function importInquiry() {
    $id = $this->input->post('im_vehicle_id');
    $bir_date = date("d/m/Y");

    $data = array(
        'customer_name'=>$this->input->post('name'),
        'email_address'=>$this->input->post('address'),
        'phone_number'=>$this->input->post('phone'),
        'inquiry_date'=>$bir_date ,
        'comment'=>$this->input->post('comments'),
        'import_vehical_id'=>$id,   
    );
    print_r($data);

    $data1 = $this->setting_model->insert($data, 'import_inquiry');

  $this->send_contact_email_import($id);
}
//end import inquiry

    public function commingsoon() {
      $headerlink = null;
      $headerData = null;
      $page = 'commingsoon';
      $mainData = array(
         
      );

      $footerData = null;
      $footerlink= null;

      $this->template($headerData, $page, $mainData, $footerData);
  }
  
  
   public function services() {
        $headerlink = null;
        $headerData = null;
        $page = 'services';
        $mainData = array(
            'vehicle' => $this->setting_model->Get_All('service_vehical_type'),
            'service' => $this->setting_model->Get_All('service_type'),
        );

        $footerData = null;
        $footerlink= null;

        $this->template($headerData, $page, $mainData, $footerData);
    }

    public function insertservice(){

        $serviceData = array(
            'vehical_vehical_type_id' =>$this->input->post('vehicle-type'),
            'model' =>$this->input->post('vehicle-model'),
            'regstration_number' =>$this->input->post('vehicle-number'),
            'booking_date' =>$this->input->post('booking-date'),
            'customer_name' =>$this->input->post('customer-name'),
            'NIC_number' =>$this->input->post('customer-nic'),
            'mobile_number' =>$this->input->post('mobile-number'),
            'email' =>$this->input->post('email'),
            'service_service_type_id' =>$this->input->post('available-services'),
            'description' => $this->input->post('comments'),
            'customer_id' => $this->input->post('customer_id')

        );

        $insert_id = $this->setting_model->insert($serviceData, 'service_inquiry');


        $this->session->set_flashdata('msg','<div class="alert alert-success">New Vehicle Added Successfully!</div>');
        $this->send_service_email($insert_id);
    }
    
private function _response(array $data = array(), $code = 200) {
 $this->output
 ->set_status_header($code)
 ->set_content_type('application/json')
 ->set_output(json_encode($data));
}

public function send_service_email($insert_id){
   
    $service = $this->setting_model->Get_Single('service_inquiry','service_id',$insert_id);
    foreach($service as $ser){
        $vehicle_id = $ser['vehical_vehical_type_id'];
        $sevice_type = $ser['service_service_type_id'];
        $model = $ser['model'];
        $reg_number = $ser['regstration_number'];
        $booking_date = $ser['booking_date'];
        $customer_name = $ser['customer_name'];
        $nic_number = $ser['NIC_number'];
        $mobile = $ser['mobile_number'];
        $email = $ser['email'];
        $description = $ser['description'];
    }
    $vehicle_type = $this->setting_model->Get_All('service_vehical_type');
    foreach($vehicle_type as $vtype){
        if($vtype['service_vehical_type_id']==$vehicle_id){
            $vehi_type_name = $vtype['vehical_type_name'];
        }
    }
    
    $service_type = $this->setting_model->Get_All('service_type');
    foreach($service_type as $Stype){
        if($Stype['service_type_id']==$sevice_type){
            $sevice_type_name = $Stype['service_type_name'];
        }
    }

    $this->_send_contact_email_service($vehi_type_name,$sevice_type_name,$model,$reg_number,$booking_date,$customer_name,$nic_number,$mobile,$email,$description);
    
    
    $this->_response( array( "status" => "true", "message" => "Thank you for contacting us." ) );
     redirect('home/services');
}

private function _send_contact_email_service($vehi_type_name,$sevice_type_name,$model,$reg_number,$booking_date,$customer_name,$nic_number,$mobile,$email,$description){


    $log_path =  base_url('/assets/brand-logo.png');
    
    $message = "";
    
    $message .= "<p style=\"font-size: 1.2em; padding: 10px; margin-bottom: 0;\">* This is an auto genarated e-mail for a request of a live web use.</p>";
    $message .= "<table style=\"width: 60%; min-width: 500px;\">";
    $message .= "<tbody style=\"border: 1px solid #a8a8a8;\">";
    $message .= "<tr style=\"background-color: white;\">";
    $message .= "<td style=\"padding: 15px; text-align: center;\" colspan=2>";
    $message .= "<img style=\"padding: 10px; height: auto; width: 30%;\" src=\"$log_path\"/>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Vehicle Type:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$vehi_type_name</p>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">vehicle Model:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$model</p>";
    $message .= "</td>";
    $message .= "</tr>";
    
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Registration Number:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$reg_number</p>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Booking date:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$booking_date</p>";
    $message .= "</td>";
    $message .= "</tr>";   
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Service Type:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$sevice_type_name</p>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Customer Name:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$customer_name</p>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Customer NIC Number:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$nic_number</p>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Customer phone number:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$mobile</p>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Customer Email:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$email</p>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Description:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$description</p>";
    $message .= "</td>";
    $message .= "</tr>";
    
 
    $message .= "</tbody>";
    $message .= "</table>";
    
    
    
    $emailReceivers = array(
         'dilruk@alliancefinance.lk','gihankaveendra94@gmail.com'
    );
    
    foreach( $emailReceivers as $emailReceiver) {
        
        $from = 'Client';
        $to = $emailReceiver;
        $subject = "service Inquiry ";
        
        $config = array();
        $config["protocol"] = 'smpt';
        $config['smtp_host'] = 'bckonnect.com';
        $config["smtp_port"] = 465;
        $config["smtp_user"] = 'publicuser@bckonnect.com';
        $config["smtp_pass"] = 'PaSS@bckonnect.com';
        $config['mailtype'] = 'html';
        $this->email->initialize($config);

        $this->email->from($from, 'Service Inquiry');
        $this->email->to($to);              
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->send();                    
    }            
    
    return true;
    redirect('home/services');
    
}

  
  
   public function contactus() {
      $headerlink = null;
      $headerData = null;
      $page = 'contactus';
      $mainData = array(
         
      );

      $footerData = null;
      $footerlink= null;

      $this->template($headerData, $page, $mainData, $footerData);
  }
   public function termscondition() {
      $headerlink = null;
      $headerData = null;
      $page = 'terms_condition';
      $mainData = array(
         
      );

      $footerData = null;
      $footerlink= null;

      $this->template($headerData, $page, $mainData, $footerData);
  }


  /* OUR ROOM AND SUITE MAIN PAGE*/
  public function rent() {
        $headerlink = null;
        $headerData = null;
        $page = 'rent';
        $mainData = array(
        	
        	'rentingVehi' => $this->setting_model->rentvehiclejoin1(),
			'types' => $this->setting_model->Get_All('renting_vehical_type'),

        	'vehicles' => $this->setting_model->Get_All('renting_vehical'),
        	// 'rentingtype' => $this->setting_model->Get_All('renting_vehical_type'),
            'rentingbrand' => $this->setting_model->Get_All('renting_vehical_brand'),
           	'rentingmodal' => $this->setting_model->Get_All('renting_vehical_model'),
           	'images' => $this->setting_model->Get_All('renting_outside_images'),
            'grid' => $this->setting_model->Get_All('grid'),
            'slider' => $this->setting_model->Get_All('slider'),

        );

        $footerData = null;
        $footerlink= null;

        $this->template($headerData, $page, $mainData, $footerData);
    }
    
     public function totalval() {
        $vale = $this->input->post('t_id');
        $headerData = null;
        $sidebarData = null;
        $page = '';

        $mainData = array(

            'monthly' => $vale,

        );


        $footerData = null;

//        $this->template( $page, $mainData, $footerData);
        $this->load->view('totalval',$mainData);
    }

public function viewvehicle() {

    $id = $this->uri->segment(3);

    $headerData = null;
    $page = 'view-vehicle';
    $mainData = array(
        'vehicle1' => $this->setting_model->Get_All('vehicle'),
        'insideimage' => $this->setting_model->Get_All('inside_images'),
        'outside' => $this->setting_model->Get_All('out_side_images'),
        'vehicle' => $this->setting_model->vehiclejoin1($id),
        'view_vehicle' => $this->setting_model->frontviewvehiclejoin($id),
        
    );

    //add to vehicle view counter
    $this->vehicleViewCounter($id);
    
    $footerData = null;
    $this->template($headerData,$page, $mainData,$footerData);
}
public function privacy() {
      $headerlink = null;
      $headerData = null;
      $page = 'privacy_policy';
      $mainData = array(
         
      );

      $footerData = null;
      $footerlink= null;

      $this->template($headerData, $page, $mainData, $footerData);
  }
  
public function sitemap() {
      $headerlink = null;
      $headerData = null;
      $page = 'sitemap';
      $mainData = array(
         
      );

      $footerData = null;
      $footerlink= null;

      $this->template($headerData, $page, $mainData, $footerData);
  }

public function insertiquery() {
    $id = $this->input->post('vehicle_id');
    $bir_date = date("d/m/Y");

    $data = array(
        'customer_name'=>$this->input->post('fullname'),
        'email_address'=>$this->input->post('address'),
        'phone_number'=>$this->input->post('phone'),
        'inquiry_date'=>$bir_date ,
        'comments'=>$this->input->post('comments'),
        'vehical_vehical_id'=>$id,
        
        
    );

    $data1 = $this->setting_model->insert($data, 'test_buying_inquiry');

    $this->send_inquiry_email($id);
}

public function insertoffer() {
    $id = $this->input->post('vehicle_id');
    $bir_date = date("Y-m-d");

    $data = array(
        'customer_name'=>$this->input->post('fullname'),
        'email_address'=>$this->input->post('address'),
        'phone_number'=>$this->input->post('phone'),
        'price'=>$this->input->post('price'),
        'offer_date'=>$bir_date ,
        'comments'=>$this->input->post('comments'),
        'vehical_id'=>$id,
    );

    $this->setting_model->insert($data, 'customer_offer');

    $this->send_offer_email($id);
}

public function send_inquiry_email(){
    $id = $this->input->post('vehicle_id');
    $fullname= $this->input->post('fullname');
    $address= $this->input->post('address');
    $phone= $this->input->post('phone');
    $comments= $this->input->post('comments');

    $this->_send_inquiry_email2($fullname, $address, $phone, $comments,$id);
    
    
    $this->_response( array( "status" => "true", "message" => "Thank you for contacting us." ) );
    redirect('home/results');
    // print_r($fullname);
    // print_r($address);
    // print_r($phone);
    // print_r($comments);
}

public function send_offer_email(){
    $id = $this->input->post('vehicle_id');
    $fullname= $this->input->post('fullname');
    $address= $this->input->post('address');
    $phone= $this->input->post('phone');
    $comments= $this->input->post('comments');

    $this->_send_offer_email($fullname, $address, $phone, $comments,$id);
    
    $this->_response( array( "status" => "true", "message" => "Thank you for Making an Offer." ) );
    redirect('home/viewvehicle/'.$id);
    // print_r($fullname);
    // print_r($address);
    // print_r($phone);
    // print_r($comments);
}



private function _send_inquiry_email2($fullname, $address, $phone, $comments,$id){


    $log_path =  base_url('/assets/brand-logo.png');
    
    
    
    $message = "";
    
    $message .= "<p style=\"font-size: 1.2em; padding: 10px; margin-bottom: 0;\">* This is an auto genarated e-mail for a request of a live web use.</p>";
    $message .= "<table style=\"width: 60%; min-width: 500px;\">";
    $message .= "<tbody style=\"border: 1px solid #a8a8a8;\">";
    $message .= "<tr style=\"background-color: white;\">";
    $message .= "<td style=\"padding: 15px; text-align: center;\" colspan=2>";
    $message .= "<img style=\"padding: 10px; height: auto; width: 30%;\" src=\"$log_path\"/>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Clinet Name:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$fullname</p>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Email:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$address</p>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Phone Number:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$phone</p>";
    $message .= "</td>";
    $message .= "</tr>";   
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Comments:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$comments</p>";
    $message .= "</td>";
    $message .= "</tr>"; 
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Veicle id:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">http://autosure.lk/home/viewvehicle/$id</p>";
    $message .= "</td>";
    $message .= "</tr>"; 

    $message .= "</tbody>";
    $message .= "</table>";
    
    
    
    $emailReceivers = array(
         'dilruk@alliancefinance.lk'
    );
    
    foreach( $emailReceivers as $emailReceiver) {
        
        $from = 'Client';
        $to = $emailReceiver;
        $subject = "Inquiry ";
        
        $config = array();
        $config["protocol"] = 'smpt';
        $config['smtp_host'] = 'bckonnect.com';
        $config["smtp_port"] = 465;
        $config["smtp_user"] = 'publicuser@bckonnect.com';
        $config["smtp_pass"] = 'PaSS@bckonnect.com';
        $config['mailtype'] = 'html';
        $this->email->initialize($config);

        $this->email->from($from, 'Enquire Form');
        $this->email->to($to);              
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->send();                    
    }            
    
    return true;
    redirect('home/results');
    
}

private function _send_offer_email($fullname, $address, $phone, $comments,$id){

    $log_path =  base_url('/assets/brand-logo.png');
      
    $message = "";
    
    $message .= "<p style=\"font-size: 1.2em; padding: 10px; margin-bottom: 0;\">* This is an auto genarated e-mail for a request of a live web use.</p>";
    $message .= "<table style=\"width: 60%; min-width: 500px;\">";
    $message .= "<tbody style=\"border: 1px solid #a8a8a8;\">";
    $message .= "<tr style=\"background-color: white;\">";
    $message .= "<td style=\"padding: 15px; text-align: center;\" colspan=2>";
    $message .= "<img style=\"padding: 10px; height: auto; width: 30%;\" src=\"$log_path\"/>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Clinet Name:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$fullname</p>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Email:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$address</p>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Phone Number:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$phone</p>";
    $message .= "</td>";
    $message .= "</tr>";   
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Comments:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$comments</p>";
    $message .= "</td>";
    $message .= "</tr>"; 
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Veicle id:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">http://autosure.lk/home/viewvehicle/$id</p>";
    $message .= "</td>";
    $message .= "</tr>"; 

    $message .= "</tbody>";
    $message .= "</table>";
    
    
    
    $emailReceivers = array(
         'anojan.t@bckonnect.com'
    );
    
    foreach( $emailReceivers as $emailReceiver) {
        
        $from = 'Client';
        $to = $emailReceiver;
        $subject = "Customer Offer";
        
        $config = array();
        $config["protocol"] = 'smpt';
        $config['smtp_host'] = 'bckonnect.com';
        $config["smtp_port"] = 465;
        $config["smtp_user"] = 'publicuser@bckonnect.com';
        $config["smtp_pass"] = 'PaSS@bckonnect.com';
        $config['mailtype'] = 'html';
        $this->email->initialize($config);

        $this->email->from($from, 'Make Offer Form');
        $this->email->to($to);              
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->send();                    
    }            
    
    return true;
    
}


public function insertiquery1() {
    $id = $this->input->post('vehicle_id');
    $bir_date = date("d/m/Y");

    $data = array(
        'customer_name'=>$this->input->post('fullname'),
        'email_address'=>$this->input->post('address'),
        'phone_number'=>$this->input->post('phone'),
        'inquiry_date'=>$bir_date ,
        'comments'=>$this->input->post('comments'),
        'vehical_vehical_id'=>$id,
         
    );

    $data1 = $this->setting_model->insert($data, 'test_buying_inquiry');

    $this->send_contact_email1($id);
}


public function send_contact_email1(){
    $id = $this->input->post('vehicle_id');
    $fullname= $this->input->post('fullname');
    $address= $this->input->post('address');
    $phone= $this->input->post('phone');
    $comments= $this->input->post('comments');


    $this->_send_contact_email1( $fullname, $address, $phone, $comments,$id);
    
    
    $this->_response( array( "status" => "true", "message" => "Thank you for contacting us." ) );
    redirect('home/viewvehicle/'.$id);
}



private function _send_contact_email1($fullname, $address, $phone, $comments, $id){


    $log_path =  base_url('/assets/brand-logo.png');
    
    
    
    $message = "";
    
    $message .= "<p style=\"font-size: 1.2em; padding: 10px; margin-bottom: 0;\">* This is an auto genarated e-mail for a request of a live web use.</p>";
    $message .= "<table style=\"width: 60%; min-width: 500px;\">";
    $message .= "<tbody style=\"border: 1px solid #a8a8a8;\">";
    $message .= "<tr style=\"background-color: white;\">";
    $message .= "<td style=\"padding: 15px; text-align: center;\" colspan=2>";
    $message .= "<img style=\"padding: 10px; height: auto; width: 30%;\" src=\"$log_path\"/>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Clinet Name:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$fullname</p>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Email:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$address</p>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Phone Number:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$phone</p>";
    $message .= "</td>";
    $message .= "</tr>";   
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Comments:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$comments</p>";
    $message .= "</td>";
    $message .= "</tr>"; 
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Veicle id:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">http://autosure.lk/home/viewvehicle/$id</p>";
    $message .= "</td>";
    $message .= "</tr>"; 

    $message .= "</tbody>";
    $message .= "</table>";
    
    //$emailReceivers = array(
    //     'buddhisenadeera485@gmail.com'
    //);
    
    $emailReceivers = array(
         'dilruk@alliancefinance.lk'
    );
    
    foreach( $emailReceivers as $emailReceiver) {
        
        $from = 'Client';
        $to = $emailReceiver;
        $subject = "Inquiry ";
        
        $config = array();
        $config["protocol"] = 'smpt';
        $config['smtp_host'] = 'bckonnect.com';
        $config["smtp_port"] = 465;
        $config["smtp_user"] = 'publicuser@bckonnect.com';
        $config["smtp_pass"] = 'PaSS@bckonnect.com';
        $config['mailtype'] = 'html';
        $this->email->initialize($config);

        $this->email->from($from, 'Enquire Form');
        $this->email->to($to);              
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->send();                    
    }            
    
    return true;
    redirect('home/viewvehicle/'.$id);
    
}

 public function results()
    {
        $headerData = null;
        $page = 'results';
        $result = '';
        $searchinsert = $this->input->post('searchbarinput');
        

        $vehicle_brand = $this->input->post('vehicle_brand');
        $vehicle_type = $this->input->post('vehicle_type');
        $vehicle_model = $this->input->post('vehicle_model');        
        $price_range = $this->input->post('price_range');
        $autosure_certified = $this->input->post('autosure_certified');

        $name_string = $vehicle_brand.' '.$vehicle_type.' '.$vehicle_model;

        $liker= [$searchinsert, $vehicle_type, $vehicle_brand, $vehicle_model];
        $result = $this->setting_model->multiple_search($liker, $price_range, $autosure_certified);

        $mainData = array(
//            'results'=>$this->setting_model->multiple_search($searchinsert),

            'insideimage' => $this->setting_model->Get_All('inside_images'),

            'results' => $result,
            'vehiclebrand' => $this->setting_model->Get_All('vehicle_brand'),
            'vehiclemodel' => $this->setting_model->Get_All('vehicle_model'),
            'vehicletype' => $this->setting_model->Get_All('vehicle_type'),
            'front' => $this->setting_model->Get_All('vehicle_front_image'),
            'advertisement' => $this->setting_model->Get_All('advertisement'),
            

        );


        $footerData = null;
        $this->template($headerData, $page, $mainData, $footerData);

    }
    public function typeSearch(){
        $headerData = null;
        $page = 'resultType';
        $result = '';

        $vehicle_type = $this->input->post('vehi_type');

        // if ($vehicle_type != null) {
        //     $liker = explode(" ", $vehicle_type);
        //     $result = $this->setting_model->multiple_search($liker);
        // } 

        $mainData = array(

            'insideimage' => $this->setting_model->Get_All('inside_images'),
            'results' => $this->setting_model->frontviewvehiclejoin1($vehicle_type),
            'vehiclebrand' => $this->setting_model->Get_All('vehicle_brand'),
            'vehiclemodel' => $this->setting_model->Get_All('vehicle_model'),
            'vehicletype' => $this->setting_model->Get_All('vehicle_type'),
            'front' => $this->setting_model->Get_All('vehicle_front_image'),

        );


        $footerData = null;
        // $this->template($headerData, $page, $mainData, $footerData);
        $this->load->view('resultType',$mainData);
    }

    public function brandSearch(){
        $headerData = null;
        $page = 'resultType';
        $result = '';

        $vehicle_brand = $this->input->post('vehi_brand');
        $vehicle_type = $this->input->post('vehi_type');

        // if ($vehicle_type != null) {
        //     $liker = explode(" ", $vehicle_type);
        //     $result = $this->setting_model->multiple_search($liker);
        // } 

        $mainData = array(

            'insideimage' => $this->setting_model->Get_All('inside_images'),
            'results' => $this->setting_model->frontviewvehiclejoin2($vehicle_type,$vehicle_brand),
            'vehiclebrand' => $this->setting_model->Get_All('vehicle_brand'),
            'vehiclemodel' => $this->setting_model->Get_All('vehicle_model'),
            'vehicletype' => $this->setting_model->Get_All('vehicle_type'),
            'front' => $this->setting_model->Get_All('vehicle_front_image'),

        );


        $footerData = null;
        // $this->template($headerData, $page, $mainData, $footerData);
        $this->load->view('resultType',$mainData);
    }

    public function modelSearch(){
        $headerData = null;
        $page = 'resultType';
        $result = '';

        $vehicle_brand = $this->input->post('vehi_brand');
        $vehicle_type = $this->input->post('vehi_type');
        $vehicle_model = $this->input->post('vehi_model');

        // if ($vehicle_type != null) {
        //     $liker = explode(" ", $vehicle_type);
        //     $result = $this->setting_model->multiple_search($liker);
        // } 

        $mainData = array(

            'insideimage' => $this->setting_model->Get_All('inside_images'),
            'results' => $this->setting_model->frontviewvehiclejoin3($vehicle_type,$vehicle_brand,$vehicle_model),
            'vehiclebrand' => $this->setting_model->Get_All('vehicle_brand'),
            'vehiclemodel' => $this->setting_model->Get_All('vehicle_model'),
            'vehicletype' => $this->setting_model->Get_All('vehicle_type'),
            'front' => $this->setting_model->Get_All('vehicle_front_image'),

        );


        $footerData = null;
        // $this->template($headerData, $page, $mainData, $footerData);
        $this->load->view('resultType',$mainData);
    }
    
    public function rentingResults()
    {

        $from_date = $this->input->post('datepicker1');
        $to_date = $this->input->post('datepicker2');

        $diff = abs(strtotime($to_date) - strtotime($from_date));
        $days = $diff/86400;

        $headerData = null;
        $page = 'rentingResults';
        $result = '';
        
        $type = $this->input->post('selecttype');
        $mainData = array(

        	'Rvehicle' => $this->setting_model->Get_All_Where('renting_vehical','vehical_vehical_type_id',$type),
        	'rentingtype' => $this->setting_model->Get_All('renting_vehical_type'),
            'rentingbrand' => $this->setting_model->Get_All('renting_vehical_brand'),
           	'rentingmodal' => $this->setting_model->Get_All('renting_vehical_model'),
           	'images' => $this->setting_model->Get_All('renting_outside_images'),
           	
            'trans' => $this->setting_model->Get_All('transmission'), 
            'difference' => $days
          
        );

        $footerData = null;
        $this->template($headerData, $page, $mainData, $footerData);

    }
    
     public function rentingType1(){
        $headerData = null;
        $page = 'rentingTypes';
        $result = '';

        
        $v_id = $this->uri->segment(3);
        $dayss = $this->uri->segment(4);
    
        $vehicle_type = $this->input->post('renting_type');
        // echo $vehicle_type;
    
    
        $mainData = array(
    
            'Rvehicle' => $this->setting_model->Get_All_Where('renting_vehical','vehical_vehical_type_id',$vehicle_type),
            'rentingtype' => $this->setting_model->Get_All('renting_vehical_type'),
            'rentingbrand' => $this->setting_model->Get_All('renting_vehical_brand'),
            'rentingmodal' => $this->setting_model->Get_All('renting_vehical_model'),
            'images' => $this->setting_model->Get_All('renting_outside_images'),
            'difference' => $dayss
    
        );
    
    
        $footerData = null;
        // $this->template($headerData, $page, $mainData, $footerData);
        $this->load->view('rentingTypes',$mainData);
    }

    public function rentingTransmi(){
        $headerData = null;
        $page = 'rentingTypes';
        $result = '';

        
        $v_id = $this->uri->segment(3);
        $dayss = $this->uri->segment(4);
    
        $vehicle_type = $this->input->post('renting_type');
        $vehicle_trans = $this->input->post('transs');
        // echo $vehicle_type;
    
    
        $mainData = array(
    
            'Rvehicle' => $this->setting_model->renting($vehicle_type,$vehicle_trans),
            'rentingtype' => $this->setting_model->Get_All('renting_vehical_type'),
            'rentingbrand' => $this->setting_model->Get_All('renting_vehical_brand'),
            'rentingmodal' => $this->setting_model->Get_All('renting_vehical_model'),
            'images' => $this->setting_model->Get_All('renting_outside_images'),
            'difference' => $dayss
    
        );
    
    
        $footerData = null;
        // $this->template($headerData, $page, $mainData, $footerData);
        $this->load->view('rentingTypes',$mainData);
    }

    
    public function insertrentiquery1() {
    $id = $this->input->post('vehicle_id');
    $bir_date = date("d/m/Y");

    $data = array(
        'customer_name'=>$this->input->post('fullname'),
        'email_address'=>$this->input->post('address'),
        'phone_number'=>$this->input->post('phone'),
        'inquiry_date'=>$bir_date ,
        'comments'=>$this->input->post('comments'),
        'vehical_vehical_id'=>$id,
        
        
    );

    $data1 = $this->setting_model->insert($data, 'test_buying_inquiry');

    $this->send_contact_email2($id);
}


public function send_contact_email2(){
    $id = $this->input->post('vehicle_id');
    $fullname= $this->input->post('fullname');
    $address= $this->input->post('address');
    $phone= $this->input->post('phone');
    $comments= $this->input->post('comments');


    $this->_send_contact_email2( $fullname, $address, $phone, $comments,$id);
    
    
    $this->_response( array( "status" => "true", "message" => "Thank you for contacting us." ) );
    redirect('home/rentingResults');
}



private function _send_contact_email2($fullname, $address, $phone, $comments, $id){


    $log_path =  base_url('/assets/brand-logo.png');
    
    
    
    $message = "";
    
    $message .= "<p style=\"font-size: 1.2em; padding: 10px; margin-bottom: 0;\">* This is an auto genarated e-mail for a request of a live web use.</p>";
    $message .= "<table style=\"width: 60%; min-width: 500px;\">";
    $message .= "<tbody style=\"border: 1px solid #a8a8a8;\">";
    $message .= "<tr style=\"background-color: white;\">";
    $message .= "<td style=\"padding: 15px; text-align: center;\" colspan=2>";
    $message .= "<img style=\"padding: 10px; height: auto; width: 30%;\" src=\"$log_path\"/>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Clinet Name:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$fullname</p>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Email:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$address</p>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Phone Number:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$phone</p>";
    $message .= "</td>";
    $message .= "</tr>";   
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Comments:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$comments</p>";
    $message .= "</td>";
    $message .= "</tr>"; 
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Veicle id:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">http://autosure.lk/home/viewvehicle/$id</p>";
    $message .= "</td>";
    $message .= "</tr>"; 
    
    
    

    

    
    
    
    
    $message .= "</tbody>";
    $message .= "</table>";
    
    
    
    $emailReceivers = array(
        'dilruk@alliancefinance.lk','gihankaveendra94@gmail.com'
    );
    
    foreach( $emailReceivers as $emailReceiver) {
        
        $from = 'Client';
        $to = $emailReceiver;
        $subject = "Rent Enquire ";
        
        $config = array();
        $config["protocol"] = 'smpt';
        $config['smtp_host'] = 'bckonnect.com';
        $config["smtp_port"] = 465;
        $config["smtp_user"] = 'publicuser@bckonnect.com';
        $config["smtp_pass"] = 'PaSS@bckonnect.com';
        $config['mailtype'] = 'html';
        $this->email->initialize($config);

        $this->email->from($from, 'Enquire Form');
        $this->email->to($to);              
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->send();                    
    }            
    
    return true;
    redirect('home/rentingResults');
    
}
 public function rentingVehicle()
    {   
        $v_id = $this->uri->segment(3);
        $dayss = $this->uri->segment(4);
        // $from_date = $this->input->post('from_date1');
        // $to_date = $this->input->post('to_date1');

        // $diff = abs(strtotime($to_date) - strtotime($from_date));
        // $days = $diff/86400;

        $headerData = null;
        $page = 'rentview-vehicle';
        $result = '';
        
        $mainData = array(

            'Rvehicle' => $this->setting_model->Get_All_Where('renting_vehical','renting_vehical_id',$v_id),
            'rentingtype' => $this->setting_model->Get_All('renting_vehical_type'),
            'rentingbrand' => $this->setting_model->Get_All('renting_vehical_brand'),
            'rentingmodal' => $this->setting_model->Get_All('renting_vehical_model'),
            'images' => $this->setting_model->Get_All('renting_outside_images'),
            'difference' => $dayss
        );

        $footerData = null;
        $this->template($headerData, $page, $mainData, $footerData);

    }
    
    public function ViewrentingVehicle()
    {   
        $v_id = $this->uri->segment(3);

        $headerData = null;
        $page = 'view-rentingvehicle';
        $result = '';
        
        $mainData = array(

            'Rvehicle' => $this->setting_model->Get_All_Where('renting_vehical','renting_vehical_id',$v_id),
            'rentingtype' => $this->setting_model->Get_All('renting_vehical_type'),
            'rentingbrand' => $this->setting_model->Get_All('renting_vehical_brand'),
            'rentingmodal' => $this->setting_model->Get_All('renting_vehical_model'),
            'images' => $this->setting_model->Get_All('renting_outside_images'),
        );

        $footerData = null;
        $this->template($headerData, $page, $mainData, $footerData);
        
        }
        
        public function addinquiry(){
        $v_id = $this->input->post('vehicle_id');
        $fname = $this->input->post('fullname');
        $email = $this->input->post('address');
        $phone = $this->input->post('phone');
        $comment1 = $this->input->post('comments');

        $data = array(
            'renting_renting_vehical_id' => $v_id,
            'customer_name' => $fname,
            'email_address' => $email,
            'phone_number' => $phone,
            'comments' => $comment1
        );

        $data1 = $this->setting_model->insert($data,'renting_inquiry');
             
    	redirect('home/rentingResults');
    }
    
    
    public function about_us() {
        $headerlink = null;
        $headerData = null;
        $page = 'about_us';
        $mainData = array(


        );

        $footerData = null;
        $footerlink= null;

        $this->template($headerData, $page, $mainData, $footerData);
    }
    
    //send contact
   public function insertcontact() {
   
    $data = array(
        'customer_name'=>$this->input->post('name'),
        'email_address'=>$this->input->post('phone'),
        'phone_number'=>$this->input->post('email'),
        'comments'=>$this->input->post('message'),
        
        
        
    );

    $data1 = $this->setting_model->insert($data, 'contact_us');

    $this->send_autosurecontact_email();
}


public function send_autosurecontact_email(){

        $fullname=$this->input->post('name');
        $phone=$this->input->post('phone');
        $email=$this->input->post('email');
        $Yourmessage =$this->input->post('message');
       // $Yourmessage =$this->input->post('message');


    $this->_send_contact_email4( $fullname, $phone, $email, $Yourmessage);
    
    
      $this->session->set_flashdata('success', 'EmailSent Successful.');
     
    redirect('home/contactus');
     $this->_response( array( "status" => "true", "message" => "Thank you for contacting us." ) );
}



private function _send_contact_email4($fullname, $phone, $email, $Yourmessage){


    $log_path =  base_url('/assets/brand-logo.png');
    
    
    
    $message = "";
    
    $message .= "<p style=\"font-size: 1.2em; padding: 10px; margin-bottom: 0;\">* This is an auto genarated e-mail for a request of a live web use.</p>";
    $message .= "<table style=\"width: 60%; min-width: 500px;\">";
    $message .= "<tbody style=\"border: 1px solid #a8a8a8;\">";
    $message .= "<tr style=\"background-color: white;\">";
    $message .= "<td style=\"padding: 15px; text-align: center;\" colspan=2>";
    $message .= "<img style=\"padding: 10px; height: auto; width: 30%;\" src=\"$log_path\"/>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Full Name:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$fullname</p>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Phone Number:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$phone</p>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Email:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$email</p>";
    $message .= "</td>";
    $message .= "</tr>";   
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Yourmessage:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$Yourmessage</p>";
    $message .= "</td>";
    $message .= "</tr>"; 
    
    
    
    

    

    
    
    
    
    $message .= "</tbody>";
    $message .= "</table>";
    
    
    
    $emailReceivers = array(
        'dilruk@alliancefinance.lk','achinid@alliancefinance.lk','rohanad@alliancefinance.lk'
    );
    
 foreach( $emailReceivers as $emailReceiver) {
        
        $from = 'Autosure';
        $to = $emailReceiver;
        $subject = "Contact Us  ";
        
        $config = array();
        $config["protocol"] = 'smpt';
        $config['smtp_host'] = 'bckonnect.com';
        $config["smtp_port"] = 465;
        $config["smtp_user"] = 'publicuser@bckonnect.com';
        $config["smtp_pass"] = 'PaSS@bckonnect.com';
        $config['mailtype'] = 'html';
        $this->email->initialize($config);

        $this->email->from($from, 'Contact Us');
        $this->email->to($to);              
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->send();                    
    }           
    
    return true;
      $this->session->set_flashdata('success', 'EmailSent Successful.');
    redirect('home/contactus');
    
     $this->_response( array( "status" => "true", "message" => "Thank you for contacting us." ) );
    
}
    public function insertsignup() {

        $id =$this->input->post('uriid');
        $base_url = 'http://localhost/autosure/home/'.$id;
        $data = array(
            'name'=>$this->input->post('name'),
            'email'=>$this->input->post('email'),



        );


        $data1 = $this->setting_model->insert($data, 'signup');

//if($base_url=='http://autosure.lk/home'){
        //redirect('home');
        //}else{
        redirect($base_url);
        // }
    }
    public function comparison(){

        $headerData = null;
        $page = 'comparison';
        $result = '';

        $hotelName = $this->input->post('x');
        $countx = $this->input->post('i');
        //$classes=explode(",",$hotelName);

        //$_SESSION["com"] =  $classes;
        //$_SESSION['yasas']= $classes;
        $data = array(
            'comparison1'=> $hotelName,
            'yasas' =>'yasas',
            'i'=>'i'

        );
        // print_r($data);
        $this->session->set_userdata($data);

        // if ($vehicle_type != null) {
        //     $liker = explode(" ", $vehicle_type);
        //     $result = $this->setting_model->multiple_search($liker);
        // } 

        $mainData = array(
            'vehicle' => $this->setting_model->vehiclejoinX(),







        );


        $footerData = null;
        $this->template($headerData, $page, $mainData, $footerData);

        //$this->load->view('comparison',$data);
    }

    public function comparisonoperating(){

        $headerData = null;
        $page = 'comparisonoperating';
        $result = '';

        $hotelName = $this->input->post('x');
        $countx = $this->input->post('i');
        //$classes=explode(",",$hotelName);

        //$_SESSION["com"] =  $classes;
        //$_SESSION['yasas']= $classes;
        $data = array(
            'comparison1'=> $hotelName,
            'yasas' =>'yasas',
            'i'=>'i'

        );
        // print_r($data);
        $this->session->set_userdata($data);

        // if ($vehicle_type != null) {
        //     $liker = explode(" ", $vehicle_type);
        //     $result = $this->setting_model->multiple_search($liker);
        // }

        $mainData = array(
            'vehicle' => $this->setting_model->vehiclejoinX(),







        );


        $footerData = null;
        $this->template($headerData, $page, $mainData, $footerData);

        //$this->load->view('comparison',$data);
    }

    public function divresult(){
        $headerData = null;
        $page = 'divreload';
        $result = '';

        $countcookies = $this->input->post('i');

        // if ($vehicle_type != null) {
        //     $liker = explode(" ", $vehicle_type);
        //     $result = $this->setting_model->multiple_search($liker);
        // } 

        $mainData = array(

            'countcookies'=>$countcookies

        );


        $footerData = null;
        // $this->template($headerData, $page, $mainData, $footerData);
        $this->load->view('divreload',$mainData);
    }

    public function divresultimport(){
        $headerData = null;
        $page = 'divereloadimport';
        $result = '';

        $countcookies = $this->input->post('i');

        // if ($vehicle_type != null) {
        //     $liker = explode(" ", $vehicle_type);
        //     $result = $this->setting_model->multiple_search($liker);
        // } 

        $mainData = array(

            'countcookies'=>$countcookies

        );


        $footerData = null;
        // $this->template($headerData, $page, $mainData, $footerData);
        $this->load->view('divereloadimport',$mainData);
    }
    public function divresultoperating(){
        $headerData = null;
        $page = 'divresultoperating';
        $result = '';

        $countcookies = $this->input->post('i');

        // if ($vehicle_type != null) {
        //     $liker = explode(" ", $vehicle_type);
        //     $result = $this->setting_model->multiple_search($liker);
        // } 

        $mainData = array(

            'countcookies'=>$countcookies

        );


        $footerData = null;
        // $this->template($headerData, $page, $mainData, $footerData);
        $this->load->view('divereloadoperating',$mainData);
    }
    
	public function operating() {  

        $headerlink = null;        
        $headerData = null;        
        $page = 'operating-lease';        
        $mainData = array(        
            //'vehiclejoin' => $this->setting_model->Get_All('vehicle'),
            'images' => $this->setting_model->Get_All('inside_images'),
            'vehicle' => $this->setting_model->vehiclejoin(),
            'front' => $this->setting_model->Get_All('vehicle_front_image'),
            'vehicletype' => $this->setting_model->Get_All('vehicle_type'),
            'vehiclebrand' => $this->setting_model->Get_All('vehicle_brand'),
            'vehiclemodel' => $this->setting_model->Get_All('vehicle_model'),

        );        
        $footerData = null;        
        $footerlink= null;        
        $this->template($headerData, $page, $mainData, $footerData);    
    }    

    public function operating_result() { 

        $id = $this->uri->segment(3);

        $headerlink = null;       
        $headerData = null;        
        $page = 'operating-lease-inner';        
        $mainData = array(        
            'images' => $this->setting_model->Get_All('inside_images'),
            'vehicle' => $this->setting_model->vehiclejoin1($id),
            'front' => $this->setting_model->Get_All('vehicle_front_image')
        );        

        //add to vehicle view counter
        $this->vehicleViewCounter($id);
        
        $footerData = null;        
        $footerlink= null;        
        $this->template($headerData, $page, $mainData, $footerData);    
    }
    
    //Start function ajax search --------------------------------------------------------------
    public function ajaxsearchduration(){
        $duration = $this->input->post('duration');
        $downpayment = $this->input->post('downpayment');
        $tot_Value = $downpayment * 2;
        $type = $this->input->post('type');
        $make = $this->input->post('make');
        $model = $this->input->post('model');
        $data = array(
            'images' => $this->setting_model->Get_All('inside_images'),
            'vehicle' => $this->setting_model->vehiclejoin_where('vehicle_total_value',$tot_Value,$type,$make,$model),
            'front' => $this->setting_model->Get_All('vehicle_front_image'),
            'downpaymentRange' => $downpayment,
            'durationRange' => $duration * 12
        );
        $this->load->view('operatingleaseAjaxSearchDuration',$data);
    }
//End function ajax search ----------------------------------------------------------------
public function comparisonimport(){

        $headerData = null;
        $page = 'comparisonimport';
        $result = '';

        $hotelName = $this->input->post('x');
        //$classes=explode(",",$hotelName);
      
      //$_SESSION["com"] =  $classes;
     //$_SESSION['yasas']= $classes;
           
   // print_r($data);
     //$this->session->set_userdata($data);

        $mainData = array(
            'vehicle' => $this->setting_model->importcomparison(),
            
        );

        $footerData = null;
        $this->template($headerData, $page, $mainData, $footerData);       
        //$this->load->view('comparison',$data);
    }
    
    public function opreatingType(){
     	$headerData = null;
        $page = 'operatingType';
        $result = '';

        $vehicle_type = $this->input->post('vehi_type');
        //print_r($vehicle_type);
 

        $mainData = array(

            'insideimage' => $this->setting_model->Get_All('inside_images'),
            'vehicle' => $this->setting_model->vehiclejoinOPTY($vehicle_type),
            'vehiclebrand' => $this->setting_model->Get_All('vehicle_brand'),
            'vehiclemodel' => $this->setting_model->Get_All('vehicle_model'),
            'vehicletype' => $this->setting_model->Get_All('vehicle_type'),
            'front' => $this->setting_model->Get_All('vehicle_front_image'),

        );


        $footerData = null;
        $this->load->view('operatingType',$mainData);
    
    }
    public function operatingBrand(){
     	$headerData = null;
        $page = 'operatingType';
        $result = '';

        $vehicle_type = $this->input->post('vehi_type');
        //print_r($vehicle_type);
 	$vehicle_brand = $this->input->post('vehi_brand');

        $mainData = array(

            'insideimage' => $this->setting_model->Get_All('inside_images'),
            'vehicle' => $this->setting_model->vehiclejoinOPBR($vehicle_type,$vehicle_brand),
            'vehiclebrand' => $this->setting_model->Get_All('vehicle_brand'),
            'vehiclemodel' => $this->setting_model->Get_All('vehicle_model'),
            'vehicletype' => $this->setting_model->Get_All('vehicle_type'),
            'front' => $this->setting_model->Get_All('vehicle_front_image'),

        );


        $footerData = null;
        $this->load->view('operatingType',$mainData);
    
    }
    
    public function opertingModel(){
     	$headerData = null;
        $page = 'operatingType';
        $result = '';

        $vehicle_type = $this->input->post('vehi_type');
        //print_r($vehicle_type);
 	$vehicle_brand = $this->input->post('vehi_brand');
 	$vehicle_model = $this->input->post('vehi_model');

        $mainData = array(

            'insideimage' => $this->setting_model->Get_All('inside_images'),
            'vehicle' => $this->setting_model->vehiclejoinOPMOD($vehicle_type,$vehicle_brand,$vehicle_model),
            'vehiclebrand' => $this->setting_model->Get_All('vehicle_brand'),
            'vehiclemodel' => $this->setting_model->Get_All('vehicle_model'),
            'vehicletype' => $this->setting_model->Get_All('vehicle_type'),
            'front' => $this->setting_model->Get_All('vehicle_front_image'),

        );


        $footerData = null;
        $this->load->view('operatingType',$mainData);
    
    }
    
   
    
    //====================================================== import email ===============================================
    public function send_contact_email_import(){
    $id = $this->input->post('im_vehicle_id');
    $fullname= $this->input->post('name');
    $address= $this->input->post('address');
    $phone= $this->input->post('phone');
    $comments= $this->input->post('comments');


    $this->_send_contact_email_import( $fullname, $address, $phone, $comments,$id);
    
    
    $this->_response( array( "status" => "true", "message" => "Thank you for contacting us." ) );
    redirect('home/import');
}



private function _send_contact_email_import($fullname, $address, $phone, $comments, $id){


    $log_path =  base_url('/assets/brand-logo.png');
    
    
    
    $message = "";
    
    $message .= "<p style=\"font-size: 1.2em; padding: 10px; margin-bottom: 0;\">* This is an auto genarated e-mail for a request of a live web use.</p>";
    $message .= "<table style=\"width: 60%; min-width: 500px;\">";
    $message .= "<tbody style=\"border: 1px solid #a8a8a8;\">";
    $message .= "<tr style=\"background-color: white;\">";
    $message .= "<td style=\"padding: 15px; text-align: center;\" colspan=2>";
    $message .= "<img style=\"padding: 10px; height: auto; width: 30%;\" src=\"$log_path\"/>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Clinet Name:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$fullname</p>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Email:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$address</p>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Phone Number:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$phone</p>";
    $message .= "</td>";
    $message .= "</tr>";   
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Comments:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$comments</p>";
    $message .= "</td>";
    $message .= "</tr>"; 
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Veicle id:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">http://autosure.lk/home/importVehicle/$id</p>";
    $message .= "</td>";
    $message .= "</tr>"; 
    $message .= "</tbody>";
    $message .= "</table>";
    
    
    
    $emailReceivers = array(
         'dilruk@alliancefinance.lk','gihankaveendra94@gmail.com'
    );
    
    foreach( $emailReceivers as $emailReceiver) {
        
        $from = 'Client';
        $to = $emailReceiver;
        $subject = "Enquire ";
        
        $config = array();
        $config["protocol"] = 'smpt';
        $config['smtp_host'] = 'bckonnect.com';
        $config["smtp_port"] = 465;
        $config["smtp_user"] = 'publicuser@bckonnect.com';
        $config["smtp_pass"] = 'PaSS@bckonnect.com';
        $config['mailtype'] = 'html';
        $this->email->initialize($config);

        $this->email->from($from, 'Import inquiry Form');
        $this->email->to($to);              
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->send();                    
    }            
    
    return true;
    redirect('home/import');
    
}
//==========================================end import email====================================

public function operatingInquiry() {
    $id = $this->input->post('vehicle_id');
    $bir_date = date("d/m/Y");

    $data = array(
        'customer_name'=>$this->input->post('fulname'),
        'email_address'=>$this->input->post('email'),
        'phone_number'=>$this->input->post('phone'),
        'inquiry_date'=>$bir_date ,
        'comments'=>$this->input->post('comments'),
        'leasing_vehical_id'=>$id,
         
    );

    $data1 = $this->setting_model->insert($data, 'oprating_leasing_inquiry');

    $this->send_contact_email_operating($id);
}

public function send_contact_email_operating(){
    $id = $this->input->post('vehicle_id');
    $fullname= $this->input->post('fulname');
    $address= $this->input->post('email');
    $phone= $this->input->post('phone');
    $comments= $this->input->post('comments');


    $this->_send_contact_email_operating( $fullname, $address, $phone, $comments,$id);
    
    
    $this->_response( array( "status" => "true", "message" => "Thank you for contacting us." ) );
    redirect('home/operating');
}



private function _send_contact_email_operating($fullname, $address, $phone, $comments, $id){


    $log_path =  base_url('/assets/brand-logo.png');
    
    
    
    $message = "";
    
    $message .= "<p style=\"font-size: 1.2em; padding: 10px; margin-bottom: 0;\">* This is an auto genarated e-mail for a request of a live web use.</p>";
    $message .= "<table style=\"width: 60%; min-width: 500px;\">";
    $message .= "<tbody style=\"border: 1px solid #a8a8a8;\">";
    $message .= "<tr style=\"background-color: white;\">";
    $message .= "<td style=\"padding: 15px; text-align: center;\" colspan=2>";
    $message .= "<img style=\"padding: 10px; height: auto; width: 30%;\" src=\"$log_path\"/>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Clinet Name:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$fullname</p>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Email:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$address</p>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Phone Number:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$phone</p>";
    $message .= "</td>";
    $message .= "</tr>";   
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Comments:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$comments</p>";
    $message .= "</td>";
    $message .= "</tr>"; 
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Veicle id:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">http://autosure.lk/home/operating_result/$id</p>";
    $message .= "</td>";
    $message .= "</tr>"; 
    $message .= "</tbody>";
    $message .= "</table>";
    
    
    
    $emailReceivers = array(
         'dilruk@alliancefinance.lk','gihankaveendra94@gmail.com'
    );
    
    foreach( $emailReceivers as $emailReceiver) {
        
        $from = 'Client';
        $to = $emailReceiver;
        $subject = "Enquire ";
        
        $config = array();
        $config["protocol"] = 'smpt';
        $config['smtp_host'] = 'bckonnect.com';
        $config["smtp_port"] = 465;
        $config["smtp_user"] = 'publicuser@bckonnect.com';
        $config["smtp_pass"] = 'PaSS@bckonnect.com';
        $config['mailtype'] = 'html';
        $this->email->initialize($config);

        $this->email->from($from, 'Operating inquiry Form');
        $this->email->to($to);              
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->send();                    
    }            
    
    return true;
    redirect('home/operating');
    
}
//======================================================================= renting inquiry process ===================================================

public function rentingInquiry() {
    $id = $this->input->post('vehicle_id');
    $bir_date = date("d/m/Y");

    $data = array(
        'customer_name'=>$this->input->post('fullname'),
        'email_address'=>$this->input->post('address'),
        'phone_number'=>$this->input->post('phone'),
        'inquiry_date'=>$bir_date ,
        'comments'=>$this->input->post('comments'),
        'renting_renting_vehical_id'=>$id,
         
    );

    $data1 = $this->setting_model->insert($data, 'renting_inquiry');

    $this->send_contact_email_renting($id);
}

public function send_contact_email_renting(){
    $id = $this->input->post('vehicle_id');
    $fullname= $this->input->post('fullname');
    $address= $this->input->post('address');
    $phone= $this->input->post('phone');
    $comments= $this->input->post('comments');


    $this->_send_contact_email_renting( $fullname, $address, $phone, $comments,$id);
    
    
    $this->_response( array( "status" => "true", "message" => "Thank you for contacting us." ) );
    redirect('home/rentingResults');
}



private function _send_contact_email_renting($fullname, $address, $phone, $comments, $id){


    $log_path =  base_url('/assets/brand-logo.png');
    
    
    
    $message = "";
    
    $message .= "<p style=\"font-size: 1.2em; padding: 10px; margin-bottom: 0;\">* This is an auto genarated e-mail for a request of a live web use.</p>";
    $message .= "<table style=\"width: 60%; min-width: 500px;\">";
    $message .= "<tbody style=\"border: 1px solid #a8a8a8;\">";
    $message .= "<tr style=\"background-color: white;\">";
    $message .= "<td style=\"padding: 15px; text-align: center;\" colspan=2>";
    $message .= "<img style=\"padding: 10px; height: auto; width: 30%;\" src=\"$log_path\"/>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Clinet Name:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$fullname</p>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Email:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$address</p>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Phone Number:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$phone</p>";
    $message .= "</td>";
    $message .= "</tr>";   
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Comments:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$comments</p>";
    $message .= "</td>";
    $message .= "</tr>"; 
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Veicle id:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">http://autosure.lk/home/rentingVehicle/$id</p>";
    $message .= "</td>";
    $message .= "</tr>"; 
    $message .= "</tbody>";
    $message .= "</table>";
    
    
    
    $emailReceivers = array(
         'dilruk@alliancefinance.lk','gihankaveendra94@gmail.com'
    );
    
    foreach( $emailReceivers as $emailReceiver) {
        
        $from = 'Client';
        $to = $emailReceiver;
        $subject = "Enquire ";
        
        $config = array();
        $config["protocol"] = 'smpt';
        $config['smtp_host'] = 'bckonnect.com';
        $config["smtp_port"] = 465;
        $config["smtp_user"] = 'publicuser@bckonnect.com';
        $config["smtp_pass"] = 'PaSS@bckonnect.com';
        $config['mailtype'] = 'html';
        $this->email->initialize($config);

        $this->email->from($from, 'Renting inquiry Form');
        $this->email->to($to);              
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->send();                    
    }            
    
    return true;
    redirect('home/rentingResults');
    
}
//threewheell index
	public function threewheelindex() {
		$headerlink = null;
		$headerData = null;
		$page = 'threewheelindex';
		$mainData = array(
			'threewheel_bike_image' => $this->setting_model->Get_All('threewheel_bike_image'),
            'bike' => $this->setting_model->biksjoin(),
            'grid' => $this->setting_model->Get_All('grid'),
            'slider' => $this->setting_model->Get_All('slider'),

			
		);

		$footerData = null;
		$footerlink= null;

		$this->template($headerData, $page, $mainData, $footerData);
	}
	
	public function bikeResults()
    {
        
        $headerData = null;
        $page = 'bike';
        
        
        $mainData = array(
            
            'threewheel_bike_brand' => $this->setting_model->Get_All('threewheel_bike_brand'),
            'threewheel_bike_type' => $this->setting_model->Get_All('threewheel_bike_type'),
            'threewheel_bike_model' => $this->setting_model->Get_All('threewheel_bike_model'),
            'image' => $this->setting_model->Get_All('threewheel_bike_image'),
            'bike' => $this->setting_model->biksjoin(),
            
          
        );

        $footerData = null;
        $this->template($headerData, $page, $mainData, $footerData);

    }
    
    public function bikeType(){
        $headerData = null;
        $page = 'biketype';
        $result = '';

        $vehicle_type = $this->input->post('vehi_type'); 

        $mainData = array(

            'image' => $this->setting_model->Get_All('threewheel_bike_image'),
            'results' => $this->setting_model->biksjointype($vehicle_type),
            'vehiclebrand' => $this->setting_model->Get_All('threewheel_bike_brand'),
            'vehiclemodel' => $this->setting_model->Get_All('threewheel_bike_model'),
            'vehicletype' => $this->setting_model->Get_All('threewheel_bike_type'),
            //'front' => $this->setting_model->Get_All('vehicle_front_image'),

        );

        $footerData = null;
        $this->load->view('biketype',$mainData);
    }

    public function bikeModel(){
        $headerData = null;
        $page = 'biketype';
        $result = '';

        $vehicle_type = $this->input->post('vehi_type');
        $vehicle_brand = $this->input->post('vehi_brand');  
        $vehicle_model = $this->input->post('vehi_model');  

        $mainData = array(

            'image' => $this->setting_model->Get_All('threewheel_bike_image'),
            'results' => $this->setting_model->biksjoinmodel($vehicle_type,$vehicle_brand,$vehicle_model),
            'vehiclebrand' => $this->setting_model->Get_All('threewheel_bike_brand'),
            'vehiclemodel' => $this->setting_model->Get_All('threewheel_bike_model'),
            'vehicletype' => $this->setting_model->Get_All('threewheel_bike_type'),
            //'front' => $this->setting_model->Get_All('vehicle_front_image'),

        );

        $footerData = null;
        $this->load->view('biketype',$mainData);
    }

    public function bikeBrand(){
        $headerData = null;
        $page = 'biketype';
        $result = '';

        $vehicle_type = $this->input->post('vehi_type');
        $vehicle_brand = $this->input->post('vehi_brand');  

        $mainData = array(

            'image' => $this->setting_model->Get_All('threewheel_bike_image'),
            'results' => $this->setting_model->biksjoinbrand($vehicle_type,$vehicle_brand),
            'vehiclebrand' => $this->setting_model->Get_All('threewheel_bike_brand'),
            'vehiclemodel' => $this->setting_model->Get_All('threewheel_bike_model'),
            'vehicletype' => $this->setting_model->Get_All('threewheel_bike_type'),
            //'front' => $this->setting_model->Get_All('vehicle_front_image'),

        );

        $footerData = null;
        $this->load->view('biketype',$mainData);
    }

    public function bikeview()
    {

        $id = $this->uri->segment(3);
        $headerData = null;
        $page = 'bikeview';
        
        
        $mainData = array(
            
            'threewheel_bike_brand' => $this->setting_model->Get_All('threewheel_bike_brand'),
            'threewheel_bike_type' => $this->setting_model->Get_All('threewheel_bike_type'),
            'threewheel_bike_model' => $this->setting_model->Get_All('threewheel_bike_model'),
            'threewheel_bike_model' => $this->setting_model->Get_All('threewheel_bike_model'),
            'image' => $this->setting_model->Get_All('threewheel_bike_image'),
            'bike' => $this->setting_model->biksjoin1($id),
            
          
        );

        $footerData = null;
        $this->template($headerData, $page, $mainData, $footerData);

    }
    
    //====================================threwheel inquiry=======================
    public function inserttrewheeliquery() {
    $id = $this->input->post('vehicle_id');
    $bir_date = date("d/m/Y");

    $data = array(
        'customer_name'=>$this->input->post('fulname'),
        'email_address'=>$this->input->post('email'),
        'phone_number'=>$this->input->post('phone'),
        'inquiry_date'=>$bir_date ,
        'comment'=>$this->input->post('comments'),
        'threewheel_bike_id'=>$id,
         
    );

    $data1 = $this->setting_model->insert($data, 'threewheel_bike_inquery');

    $this->send_contact_email_bike($id);
}

public function send_contact_email_bike(){
    $id = $this->input->post('vehicle_id');
    $fullname= $this->input->post('fulname');
    $address= $this->input->post('email');
    $phone= $this->input->post('phone');
    $comments= $this->input->post('comments');


    $this->_send_contact_email_bike( $fullname, $address, $phone, $comments,$id);
    
    
    $this->_response( array( "status" => "true", "message" => "Thank you for contacting us." ) );
    redirect('home/bikeResults');
}



private function _send_contact_email_bike($fullname, $address, $phone, $comments, $id){


    $log_path =  base_url('/assets/brand-logo.png');
    
    
    
    $message = "";
    
    $message .= "<p style=\"font-size: 1.2em; padding: 10px; margin-bottom: 0;\">* This is an auto genarated e-mail for a request of a live web use.</p>";
    $message .= "<table style=\"width: 60%; min-width: 500px;\">";
    $message .= "<tbody style=\"border: 1px solid #a8a8a8;\">";
    $message .= "<tr style=\"background-color: white;\">";
    $message .= "<td style=\"padding: 15px; text-align: center;\" colspan=2>";
    $message .= "<img style=\"padding: 10px; height: auto; width: 30%;\" src=\"$log_path\"/>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Clinet Name:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$fullname</p>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Email:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$address</p>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Phone Number:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$phone</p>";
    $message .= "</td>";
    $message .= "</tr>";   
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Comments:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$comments</p>";
    $message .= "</td>";
    $message .= "</tr>"; 
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Veicle id:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">http://autosure.lk/home/bikeview/$id</p>";
    $message .= "</td>";
    $message .= "</tr>"; 
    $message .= "</tbody>";
    $message .= "</table>";
    
    
    
    $emailReceivers = array(
         'dilruk@alliancefinance.lk','gihankaveendra94@gmail.com'
    );
    
    foreach( $emailReceivers as $emailReceiver) {
        
        $from = 'Client';
        $to = $emailReceiver;
        $subject = "Enquire ";
        
        $config = array();
        $config["protocol"] = 'smpt';
        $config['smtp_host'] = 'bckonnect.com';
        $config["smtp_port"] = 465;
        $config["smtp_user"] = 'publicuser@bckonnect.com';
        $config["smtp_pass"] = 'PaSS@bckonnect.com';
        $config['mailtype'] = 'html';
        $this->email->initialize($config);

        $this->email->from($from, 'Three Wheel And Bike inquiry Form');
        $this->email->to($to);              
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->send();                    
    }            
    
    return true;
    redirect('home/bikeResults');
    
}


public function vehicleViewCounter($vehicle_id){
    $year = date('y');
    $date = new DateTime(date('Y-m-d'));
    $week = $date->format("W");

    $counter = $this->vehicle_view_counter->get_counter($vehicle_id);
    if($counter){
        $newCount = $counter->count+1;
    }
    else{
        $newCount = 1;
    }
    
    $newCounterData = array (
        'week' => $week,
        'count' => $newCount,
        'vehicle_id' => $vehicle_id
    );

    if($counter){
        $update = $this->setting_model->update($newCounterData,'vehicle_view_counter','vehicle_id',$vehicle_id);
    }
    else{
        $this->setting_model->insert($newCounterData, 'vehicle_view_counter');
    }
}

public function popular(){
        $headerData = null;
        $page = 'popular';
        $result = [];
        $i=0;

        $date = new DateTime(date('Y-m-d'));
        $week = $date->format("W"); 
        
        $popularThisWeek = $this->setting_model->popular_vehicles($week);
        
        foreach ($popularThisWeek as $popular){
                       
            $result[$i]= $this->setting_model->vehiclejoin1($popular['vehicle_id'])[0];
            $i++;
        }

        $mainData = array(

            'insideimage' => $this->setting_model->Get_All('inside_images'),

            'results' => $result,
            'vehiclebrand' => $this->setting_model->Get_All('vehicle_brand'),
            'vehiclemodel' => $this->setting_model->Get_All('vehicle_model'),
            'vehicletype' => $this->setting_model->Get_All('vehicle_type'),
            'front' => $this->setting_model->Get_All('vehicle_front_image'),
            'advertisement' => $this->setting_model->Get_All('advertisement'),
            

        );

        $footerData = null;
        $this->template($headerData, $page, $mainData, $footerData);
    }

}