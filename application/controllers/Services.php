<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->check_isvalidated();
    }

    public function template($headerData, $sidebarData, $page, $mainData, $footerData)
    {
        $this->load->view('admin/components/header', $headerData);
        $this->load->view('admin/components/sidebar', $sidebarData);
        $this->load->view($page, $mainData);
        $this->load->view('admin/components/footer', $footerData);
    }

    //validate session start
    private function check_isvalidated()
    {
        if (!$this->session->userdata('validated')) {
            redirect('login');
        }
    }

    //add vehicle type function start -- ishara sewwandi
    public function addtype() {
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/services/addtype';
        $mainData = array(
            'pagetitle' => 'Add New Type',

        );

        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
    //add vehicle type function end


    //insert type to database for 'Vehicle'
    public function inserttype() {
        $data = array(
            'vehical_type_name' =>$this->input->post('vehicle_type_name'),
        );

        $insert_id = $this->setting_model->insert($data, 'service_vehical_type');



        $this->session->set_flashdata('msg','<div class="alert alert-success">New type Added Successfully!</div>');

        redirect('services/addtype');
    }
    //upload images and details to database for 'vehicle' end

    //View type function start--  ishara sewwandi
    public function viewtype() {

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/services/viewtype';
        $mainData = array(
            'vehicle_type' => $this->setting_model->Get_All('service_vehical_type'),
        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
    //View type function end
    //edit type -- ishara sewwandi

    public function edittype(){
        $id = $this->uri->segment(3);
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/services/edittype';
        $mainData = array(
            'vehicle_type' => $this->setting_model->Get_Single('service_vehical_type','service_vehical_type_id',$id),

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);

    }
    //update type -- ishara sewwandi

    public function updatetype(){
        $id = $this->uri->segment(3);
        $data = array(
            'vehical_type_name' =>$this->input->post('vehicle_type_name'),
        );

        $this->setting_model->update($data,'service_vehical_type','service_vehical_type_id',$id);



        $this->session->set_flashdata('msg','<div class="alert alert-success">Vehicle type update Successfully!</div>');

        redirect('services/edittype/'.$id);
    }

    //add  service type function start -- ishara sewwandi
    public function addservicesname() {
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/services/addservicesname';
        $mainData = array(
            'pagetitle' => 'Add New Type',

        );

        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
    //add vehicle type function end

    //insert service type to database for ''
    public function insertservicesname() {
        $data = array(
            'service_type_name' =>$this->input->post('service_type_name'),
        );

        $insert_id = $this->setting_model->insert($data, 'service_type');



        $this->session->set_flashdata('msg','<div class="alert alert-success">New service type Successfully!</div>');

        redirect('services/addservicesname');
    }
    //upload images and details to database for  end

    //View type function start--  ishara sewwandi
    public function viewservicesname() {

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/services/viewservicesname';
        $mainData = array(
            'service_type' => $this->setting_model->Get_All('service_type'),

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
    //View services function end

    //edit type -- ishara sewwandi

    public function editservicesname(){
        $id = $this->uri->segment(3);
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/services/editservicesname';
        $mainData = array(
            'service_type' => $this->setting_model->Get_Single('service_type','service_type_id',$id),

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);

    }
    //update service type -- ishara sewwandi

    public function updateservicesname(){
        $id = $this->uri->segment(3);
        $data = array(
            'service_type_name' =>$this->input->post('service_type_name'),
        );

        $this->setting_model->update($data,'service_type','service_type_id',$id);



        $this->session->set_flashdata('msg','<div class="alert alert-success">Vehicle type update Successfully!</div>');

        redirect('services/editservicesname/'.$id);
    }

     //View type function start--  ishara sewwandi
     public function viewenquiry() {

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/services/viewenquiry';
        $mainData = array(
            'service_inquiry' => $this->setting_model->Get_All('service_inquiry'),
            'vehicle_type' => $this->setting_model->Get_All('service_vehical_type'),
            'service_type' => $this->setting_model->Get_All('service_type'),
            'login' => $this->setting_model->Get_All('login'),

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
      //View type function start--  ishara sewwandi
      public function viewinqueryconfirm() {

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/services/viewinqueryconfirm';
        $mainData = array(
            'service_inquiry' => $this->setting_model->Get_All('service_inquiry'),
            'vehicle_type' => $this->setting_model->Get_All('service_vehical_type'),
            'service_type' => $this->setting_model->Get_All('service_type'),
            'login' => $this->setting_model->Get_All('login'),

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }



    // start function for confirm service inquiry ------------------ coded by yasas
    public function confirm(){

        $id = $this->uri->segment(3);
        $date = date('Y-m-d');
        $data = array(
            'status' => 2,
            'status_changed_date' => $date,
        );

        $data1 = $this->setting_model->update($data,'service_inquiry','service_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-success">Inquiry Confirm Successful!</div>');

        redirect('services/viewenquiry');
    }
    public function confirmreject(){

        $id = $this->uri->segment(3);
        $date = date('Y-m-d');
        $data = array(
            'status' => 2,
            'status_changed_date' => $date,
        );

        $data1 = $this->setting_model->update($data,'service_inquiry','service_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-success">Inquiry Confirm Successful!</div>');

        redirect('services/viewinqueryrejct');
    }
// end function ------------------------------------------------------------------
// start function for confirm service inquiry ------------------ coded by yasas
public function reject(){

    $id = $this->uri->segment(3);
    $date = date('Y-m-d');
    $data = array(
        'status' => 3,
        'status_changed_date' => $date,
    );

    $data1 = $this->setting_model->update($data,'service_inquiry','service_id',$id);

    $this->session->set_flashdata('msg','<div class="alert alert-success">Inquiry Confirm Successful!</div>');

    redirect('services/viewenquiry');
}
public function rejectconfirm(){

    $id = $this->uri->segment(3);
    $date = date('Y-m-d');
    $data = array(
        'status' => 3,
        'status_changed_date' => $date,
    );

    $data1 = $this->setting_model->update($data,'service_inquiry','service_id',$id);

    $this->session->set_flashdata('msg','<div class="alert alert-success">Inquiry Confirm Successful!</div>');

    redirect('services/viewinqueryconfirm');
}
// end function ------------------------------------------------------------------
 //View type function start--  ishara sewwandi
 public function viewinqueryrejct() {

    $headerData = null;
    $sidebarData = null;
    $page = 'admin/services/viewinqueryrejct';
    $mainData = array(
        'service_inquiry' => $this->setting_model->Get_All('service_inquiry'),
        'vehicle_type' => $this->setting_model->Get_All('vehicle_type'),
        'service_type' => $this->setting_model->Get_All('service_type'),
        'login' => $this->setting_model->Get_All('login'),

    );
    $footerData = null;

    $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
}
}