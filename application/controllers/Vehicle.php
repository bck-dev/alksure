<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vehicle extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->check_isvalidated();
        $this->CI = & get_instance();
    }

    public function template($headerData, $sidebarData, $page, $mainData, $footerData) {
        $this->load->view('admin/components/header', $headerData);
        $this->load->view('admin/components/sidebar', $sidebarData);
        $this->load->view($page, $mainData);
        $this->load->view('admin/components/footer', $footerData);
    }

    //validate session start
    private function check_isvalidated(){
        if(!$this->session->userdata('validated')){
            redirect('login');
        }
    }
    //validate session end



    //View event function start --ishara sewwandi
    public function viewvehicle() {
	$id1=$this->uri->segment(4);
        $ageid = $this->input->post('c_id');
        //print_r($ageid);
        $vehiid = $this->input->post('v_id');

        $data = array(
//                'vehicle_id' => $this->input->post('vehicle_id'),
            'inspection_status' =>$ageid,

        );
//            print_r($vehi_id);
        $this->setting_model->update($data,'vehicle','vehicle_id',$vehiid);


        $headerData = null;
        $sidebarData = null;
        $page = 'admin/vehicle/viewvehicle';


        $mainData = array(

            'insideimage' => $this->setting_model->Get_All('inside_images'),
            'outside' => $this->setting_model->Get_All('out_side_images'),
            'agent' => $this->setting_model->Get_All('agents'),
            'vehicle' => $this->setting_model->vehiclejoin(),
            'front' => $this->setting_model->Get_All('vehicle_front_image'),




        );


        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
    //View event function end --ishara sewwandi
    
    //View event function start --ishara sewwandi
    public function viewvehicles() {
        $id1=$this->uri->segment(4);
        $ageid = $this->input->post('c_id');
        //print_r($ageid);
        $vehiid = $this->input->post('v_id');

        $data = array(
//                'vehicle_id' => $this->input->post('vehicle_id'),
            'inspection_status' =>$ageid,

        );
//            print_r($vehi_id);
        $this->setting_model->update($data,'vehicle','vehicle_id',$vehiid);


        $headerData = null;
        $sidebarData = null;
        $page = 'admin/vehicle/viewvehicles';


        $mainData = array(
//            'vehicle' => $this->setting_model->Get_All('vehicle'),
            'insideimage' => $this->setting_model->Get_All('inside_images'),
            'outside' => $this->setting_model->Get_All('out_side_images'),
            'agent' => $this->setting_model->Get_All('agents'),
            'vehicle' => $this->setting_model->vehiclejoin(),

//            'agents' => $this->setting_model->Get_All_Where('agents','agent_id',$agentid),
            //'vehicleview' => $this->setting_model->vehicleviewjoin(),
//            print_r($agentid),
            print_r($ageid),


        );


        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
    
     public function editvehicle1() {
        $id = $this->uri->segment(3);

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/vehicle/editvehicle';

        $mainData = array(
            'vehicle1' => $this->setting_model->Get_Single('vehicle','vehicle_id',$id),
            'vehicle_images' => $this->multiple_image_uplaod->edit_data_image_vehicle1($id),
            'types'=> $this->setting_model->Get_All('vehicle_type'),
            'brands'=> $this->setting_model->Get_All('vehicle_brand'),
            'modals'=> $this->setting_model->Get_All('vehicle_model'),
            'front' => $this->setting_model->Get_All('vehicle_front_image'),
        );

        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);

    }

    public function sold() {
        $id = $this->uri->segment(3);

        $data = array(
            'vehicle_status' => 8
        );

        $data1 = $this->setting_model->update($data,'vehicle','vehicle_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-success">Update Successfully!</div>');

        redirect('vehicle/viewvehicle/'.$id);

    }
    
     public function viewvehiclecustomer() {
        $vehiid=$this->uri->segment(3);

        //print_r($ageid);




        $headerData = null;
        $sidebarData = null;
        $page = 'admin/vehicle/vehicle_view';


        $mainData = array(
//            'vehicle' => $this->setting_model->Get_All('vehicle'),
            'insideimage' => $this->setting_model->Get_All('inside_images'),
            'outside' => $this->setting_model->Get_All('out_side_images'),
            'agent' => $this->setting_model->Get_All('agents'),
            'vehicle' => $this->setting_model->vehiclejoincus($vehiid),




        );




        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
    public function editvehiclecus() {
        $id = $this->uri->segment(3);

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/vehicle/editvehicle_customer';

        $mainData = array(
            'vehicle1' => $this->setting_model->Get_Single('vehicle','vehicle_id',$id),
            'vehicle_images' => $this->multiple_image_uplaod->edit_data_image_vehicle1($id),
            'types'=> $this->setting_model->Get_All('vehicle_type'),
            'brands'=> $this->setting_model->Get_All('vehicle_brand'),
            'modals'=> $this->setting_model->Get_All('vehicle_model'),
        );

        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);

    }

    public function updatevehiclecus(){
        $id = $this->uri->segment(3);
         $id = $this->uri->segment(3);
        
        if(isset($_FILES['fileinput']) && empty($_FILES['fileinput']['tmp_name'])) {
            $image = $this->input->post('new_front_image');
        } else {
            $image = $this->setting_model->upload('fileinput', 'upload/selling/Front/');
        }
        
        $data = array(
            'inside_360_images' => $image,
            'vehicle_type' =>$this->input->post('vehicle_type'),
            'vehicle_brand' =>$this->input->post('vehicle_brand'),
            'vehicle_model' =>$this->input->post('vehicle_model'),
            'vehicle_transmission' =>$this->input->post('vehicle_transmission'),
            'vehicle_engine_capacity' =>$this->input->post('vehicle_engine_capacity'),
            'vehicle_seating_capacity' =>$this->input->post('vehicle_seating_capacity'),
            'year_of_manufacture' =>$this->input->post('year_of_manufacture'),
            'year_of_registration' =>$this->input->post('year_of_registration'),
            'average_of_fuel_consuption' =>$this->input->post('average_of_fuel_consuption'),
            'fuel_type' =>$this->input->post('fuel_type'),
            'vehicle_features' =>$this->input->post('vehicle_features'),
            'vehicle_mileage_range' =>$this->input->post('vehicle_mileage_range'),
            'number_of_owners' =>$this->input->post('owners'),
            'vehicle_minimum_downpayment' =>$this->input->post('downpayment'),
            'vehicle_total_value' =>$this->input->post('total'),
            'sellers_description' => $this->input->post('sellers_description'),
            'vehicle_number' => $this->input->post('vehi_number'),

        );

        $data1 = $this->setting_model->update($data,'vehicle','vehicle_id',$id);

        //edit multiple image upload
        $files = $_FILES;
        $path = "selling";
        $fileName = $this->multiple_image_uplaod->multi_image_upload($files,$path);
        $this->multiple_image_uplaod->edit_upload_image_vehicle1($fileName,$id);
        //end multiple image upload edit

        $this->session->set_flashdata('msg','<div class="alert alert-success">Update Successfully!</div>');

        redirect('vehicle/viewvehiclecustomer/'.$this->session->userdata('id'));
    }
    
    public function updatevehicle(){
        $id = $this->uri->segment(3);
        
        if(isset($_FILES['fileinput']) && empty($_FILES['fileinput']['tmp_name'])) {
            $image = $this->input->post('new_front_image');
        } else {
            $image = $this->setting_model->upload('fileinput', 'upload/selling/Front/');
        }

        $data = array(
          
            'inside_360_images' => $image,
            'vehicle_type' =>$this->input->post('vehicle_type'),
            'vehicle_brand' =>$this->input->post('vehicle_brand'),
            'vehicle_model' =>$this->input->post('vehicle_model'),
            'vehicle_transmission' =>$this->input->post('vehicle_transmission'),
            'vehicle_engine_capacity' =>$this->input->post('vehicle_engine_capacity'),
            'engine_capacity_unit' => $this->input->post('engine_cap'),
            'vehicle_seating_capacity' =>$this->input->post('vehicle_seating_capacity'),
            'year_of_manufacture' =>$this->input->post('year_of_manufacture'),
            'year_of_registration' =>$this->input->post('year_of_registration'),
            'average_of_fuel_consuption' =>$this->input->post('average_of_fuel_consuption'),
            'avarage_fuel_consumption_unit' => $this->input->post('fuel_con'),
            'fuel_type' =>$this->input->post('fuel_type'),
            'vehicle_features'=>implode(',', $this->input->post('vehicle_features')),
            'additional_features' =>$this->input->post('additional_features'),
            'vehicle_mileage_range' =>$this->input->post('vehicle_mileage_range'),
            'millage_range_unit' => $this->input->post('vehicle_range'),
            'number_of_owners' =>$this->input->post('owners'),
            'vehicle_minimum_downpayment' =>$this->input->post('downpayment'),
            'vehicle_total_value' =>$this->input->post('total'),
            'sellers_description' => $this->input->post('sellers_description'),
            'vehicle_number' => $this->input->post('number'),
            'warranty' => $this->input->post('warranty'),
            'autosure_certified' => $this->input->post('autosure_certified')
            
        );

        $data1 = $this->setting_model->update($data,'vehicle','vehicle_id',$id);

        //edit multiple image upload
        if(count($_FILES['userfile']['name'])>1){
        $files = $_FILES;
        $path = "selling";
        $fileName = $this->multiple_image_uplaod->multi_image_upload($files,$path);
        $this->multiple_image_uplaod->edit_upload_image_vehicle1($fileName,$id);
        //end multiple image upload edit
        }
        
        // if($this->input->post('fileinput')!=NULL){
        // $data22 = array(
        //     'front_image' => $this->setting_model->upload('fileinput', 'upload/selling/Front'),
        //     'vehicle_id' => $id
        // );
        
        // $dataX = $this->setting_model->update($data22,'vehicle_front_image','vehicle_id',$id);
        // }
        $this->session->set_flashdata('msg','<div class="alert alert-success">Update Successfully!</div>');

        redirect('vehicle/viewvehicle/'.$id);
    }
    
    public function deleteimageselling(){
        $deleteid  = $this->input->post('id');
        $this->db->delete('inside_images', array('inside_images_id' => $deleteid));
        $verify = $this->db->affected_rows();
        echo $verify;
    }
    
    public function deleteimagefront(){
        $deleteid  = $this->input->post('id');
        $this->db->delete('vehicle_front_image', array('front_image_id' => $deleteid));
        $verify = $this->db->affected_rows();
        echo $verify;
    }

    
     public function deletesellvehicle(){
        //$service_id = $this->uri->segment(3);
        $sel_id = $this->input->post('id');
        $limit_vehicle=count($this->setting_model->Get_Single('vehicle','vehicle_id',$sel_id));
        $this->setting_model->delete_data('vehicle','vehicle_id',$sel_id,$limit_vehicle);
        $this->setting_model->delete_data('inside_images','vehicle_vehicle_id',$sel_id,$limit_vehicle);
        redirect('vehicle/viewvehicle');
    }

//View vehicle datails function start --ishara sewwandi
    public function details() {

        $vehi = $this->uri->segment(3);

        $agentid = $this->input->post('a_id');

        $config = array(
            'wheres' => array(
                'vehicle' => array('vehicle_id', $vehi)
            ),
            'from' => array('vehicle'),
            'joins' => array(
                'inside_images' => array('inside_images.vehicle_vehicle_id = vehicle.vehicle_id', 'left'),
                'out_side_images' => array('out_side_images.vehicle_vehicle_id = vehicle.vehicle_id', 'left'),
                'vehicle_brand' => array('vehicle_brand.vehicle_brand_id = vehicle.vehicle_brand', 'left'),
                'vehicle_type' => array('vehicle_type.vehicle_type_id = vehicle.vehicle_type', 'left'),
                'vehicle_model' => array('vehicle_model.vehicle_model_id = vehicle.vehicle_model', 'left'),
                'selling_customer' => array('selling_customer.selling_customer_id = vehicle.selling_customer_id', 'left'),
                'agents' => array('agents.agent_id = vehicle.agent_id', 'left'),
            )
        );

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/vehicle/details';

        $mainData = array(

            'insideimage' => $this->setting_model->Get_All('inside_images'),
            'outside' => $this->setting_model->Get_All('out_side_images'),
            'agent' => $this->setting_model->Get_All('agents'),
            'vehicle' => $this->setting_model->vehicledetail($vehi),
            'agents' => $this->setting_model->Get_All_Where('agents','agent_id',$agentid),
            'front' => $this->setting_model->Get_All('vehicle_front_image'),
            'agentid' => $this->setting_model->Get_All_Where('agents','agent_id',$agentid),


        );



        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
    //View vehicle datails function end --ishara sewwandi

    //View agent datails function start --ishara sewwandi

    public function agentdetails() {

        $vehi = $this->uri->segment(3);

        $agentid = $this->input->post('a_id');

        $config = array(
            'wheres' => array(
                'vehicle' => array('vehicle_id', $vehi)
            ),
            'from' => array('vehicle'),
            'joins' => array(
                'inside_images' => array('inside_images.vehicle_vehicle_id = vehicle.vehicle_id', 'left'),
                'out_side_images' => array('out_side_images.vehicle_vehicle_id = vehicle.vehicle_id', 'left'),
                'vehicle_brand' => array('vehicle_brand.vehicle_brand_id = vehicle.vehicle_brand', 'left'),
                'vehicle_type' => array('vehicle_type.vehicle_type_id = vehicle.vehicle_type', 'left'),
                'vehicle_model' => array('vehicle_model.vehicle_model_id = vehicle.vehicle_model', 'left'),
                'selling_customer' => array('selling_customer.selling_customer_id = vehicle.selling_customer_id', 'left'),
                'agents' => array('agents.agent_id = vehicle.agent_id', 'left')
            )
        );

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/vehicle/agentdetails';

        $mainData = array(
//            'vehicle' => $this->setting_model->Get_All('vehicle'),
            'insideimage' => $this->setting_model->Get_All('inside_images'),
            'outside' => $this->setting_model->Get_All('out_side_images'),
            'agent' => $this->setting_model->Get_All('agents'),
            'type' => $this->setting_model->Get_All('vehicle_type'),
            'model' => $this->setting_model->Get_All('vehicle_model'),
            'brand' => $this->setting_model->Get_All('vehicle_brand'),
	'agentid' => $this->setting_model->Get_All_Where('agents','agent_id',$agentid),
            'agents' => $this->setting_model->Get_All_Where('vehicle','agent_id',$agentid),





        );



        $footerData = null;

//        $this->template( $page, $mainData, $footerData);
        $this->load->view('admin/vehicle/agentdetails',$mainData);
    }

//View agent datails function end --ishara sewwandi

//insert agent datails function start --ishara sewwandi
    public function insertagent()
    {
        $vehi_id = $this->uri->segment(3);


            $data = array(
//                'vehicle_id' => $this->input->post('vehicle_id'),
                'agent_id' => $this->input->post('agent_id'),
                'status' => 1,
            );
//            print_r($vehi_id);
            $this->setting_model->update($data,'vehicle','vehicle_id',$vehi_id);

            $this->send_contact_email();
            $this->send_email_admin();

//                redirect('vehicle/viewvehicle');


    }
//insert agent datails function end --ishara sewwandi


    private function _response(array $data = array(), $code = 200) {
        $this->output
            ->set_status_header($code)
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

//sent email for agent--ishara sewwandi
    public function send_contact_email()
    {
//        $client_name = $this->input->post('vehicle_id');
        $vehicle_number = $this->input->post('vehicle_number');
        $vehicle_type_name = $this->input->post('vehicle_type_name');
        $vehicle_brand_name = $this->input->post('vehicle_brand_name');
        $vehicle_transmission = $this->input->post('vehicle_transmission');
        $vehicle_engine_capacity = $this->input->post('vehicle_engine_capacity');
        $vehicle_seating_capacity= $this->input->post('vehicle_seating_capacity');
        $agent_email= $this->input->post('agent_email');

        $this->_send_contact_email( $vehicle_number, $vehicle_type_name, $vehicle_brand_name, $vehicle_transmission, $vehicle_engine_capacity, $vehicle_seating_capacity,$agent_email);


        $this->_response( array( "status" => "true", "message" => "Thank you for contacting us." ) );
        
    }

    private function _send_contact_email($vehicle_number, $vehicle_type_name, $vehicle_brand_name, $vehicle_transmission, $vehicle_engine_capacity, $vehicle_seating_capacity,$agent_email){


        $log_path =  base_url('/assets/logo.png');
//        $client_communication_star = $this->get_star($communication);
//        $client_quality_of_deliverible = $this->get_star($quality_of_deliverible);
//        $client_proffesionalism = $this->get_star($proffesionalism);
//        $client_effectiveness = $this->get_star($effectiveness);


        $message = "";

        $message .= "<p style=\"font-size: 1.2em; padding: 10px; margin-bottom: 0;\">* This is an auto genarated e-mail for a request of a live web use.</p>";
        $message .= "<table style=\"width: 60%; min-width: 500px;\">";
        $message .= "<tbody style=\"border: 1px solid #a8a8a8;\">";
        $message .= "<tr style=\"background-color: white;\">";
        $message .= "<td style=\"padding: 15px; text-align: center;\" colspan=2>";
        $message .= "<img style=\"padding: 10px; height: auto; width: 30%;\" src=\"$log_path\"/>";
        $message .= "</td>";
        $message .= "</tr>";

        $message .= "<tr style=\"\">";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Vehicle number:</p>";
        $message .= "</td>";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$vehicle_number</p>";
        $message .= "</td>";
        $message .= "</tr>";

        $message .= "<tr style=\"\">";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Vehicle type name:</p>";
        $message .= "</td>";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$vehicle_type_name</p>";
        $message .= "</td>";
        $message .= "</tr>";

        $message .= "<tr style=\"\">";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Vehicle brand name:</p>";
        $message .= "</td>";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$vehicle_brand_name</p>";
        $message .= "</td>";
        $message .= "</tr>";

        $message .= "<tr style=\"\">";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Vehicle transmission:</p>";
        $message .= "</td>";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$vehicle_transmission</p>";
        $message .= "</td>";
        $message .= "</tr>";


        $message .= "<tr style=\"\">";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Vehicle engine capacity:</p>";
        $message .= "</td>";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$vehicle_engine_capacity</p>";
        $message .= "</td>";
        $message .= "</tr>";

        $message .= "<tr style=\"\">";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Vehicle engine capacity:</p>";
        $message .= "</td>";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$vehicle_engine_capacity</p>";
        $message .= "</td>";
        $message .= "</tr>";

        $message .= "<tr style=\"\">";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Vehicle seating capacity:</p>";
        $message .= "</td>";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$vehicle_seating_capacity</p>";
        $message .= "</td>";
        $message .= "</tr>";




        $message .= "</tbody>";
        $message .= "</table>";



        $emailReceivers = array(
        	$agent_email
            
        );

        foreach( $emailReceivers as $emailReceiver) {

           $from = 'Admin';
            $to = $emailReceiver;
            $subject = "Admin Form";

            $config = array();
            $config["protocol"] = 'smpt';
            $config['smtp_host'] = 'bckonnect.com';
            $config["smtp_port"] = 465;
            $config["smtp_user"] = 'publicuser@bckonnect.com';
            $config["smtp_pass"] = 'PaSS@bckonnect.com';
            $config['mailtype'] = 'html';
            $this->email->initialize($config);

            $this->email->from($from, 'Vehicle Details');
            $this->email->to($to);
            $this->email->subject($subject);
            $this->email->message($message);
            $this->email->send();
        }

        return true;
          redirect('vehicle/viewvehicles');

    }

    //send email for admin --ishara sewwandi

    public function send_email_admin(){
        $agent_id = $this->input->post('agent_id');
        $agent_name = $this->input->post('agent_name');
        $vehicle_id = $this->input->post('vehicle_id');
        $vehicle_number = $this->input->post('vehicle_number');
        $assign_date = date('Y-m-d H:i:s');



        $this->_send_email_admin( $agent_id, $agent_name, $vehicle_id, $vehicle_number, $assign_date);



        $this->_response( array( "status" => "true", "message" => "Thank you for contacting us." ) );

	redirect('vehicle/viewvehicles');
        
    }



    private function _send_email_admin($agent_id, $agent_name, $vehicle_id, $vehicle_number, $assign_date){
        $log_path =  base_url('/assets/logo.png');

        $message = "";

        $message .= "<p style=\"font-size: 1.2em; padding: 10px; margin-bottom: 0;\">* This is an auto genarated e-mail for a request of a live web use.</p>";
        $message .= "<table style=\"width: 60%; min-width: 500px;\">";
        $message .= "<tbody style=\"border: 1px solid #a8a8a8;\">";
        $message .= "<tr style=\"background-color: white;\">";
        $message .= "<td style=\"padding: 15px; text-align: center;\" colspan=2>";
        $message .= "<img style=\"padding: 10px; height: auto; width: 30%;\" src=\"$log_path\"/>";
        $message .= "</td>";
        $message .= "</tr>";
        
        $message .= "<tr style=\"\">";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Agent id:</p>";
        $message .= "</td>";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$agent_id</p>";
        $message .= "</td>";
        $message .= "</tr>";

        $message .= "<tr style=\"\">";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Agent Name:</p>";
        $message .= "</td>";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$agent_name</p>";
        $message .= "</td>";
        $message .= "</tr>";

        $message .= "<tr style=\"\">";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Vehicle ID:</p>";
        $message .= "</td>";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$vehicle_id</p>";
        $message .= "</td>";
        $message .= "</tr>";

        $message .= "<tr style=\"\">";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Vehicle Number:</p>";
        $message .= "</td>";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$vehicle_number</p>";
        $message .= "</td>";
        $message .= "</tr>";


        $message .= "<tr style=\"\">";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Assign Date/Time:</p>";
        $message .= "</td>";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$assign_date</p>";
        $message .= "</td>";
        $message .= "</tr>";


        $message .= "</tbody>";
        $message .= "</table>";



        $emailReceivers = array(
            'dilruk@alliancefinance.lk'
        );

        foreach( $emailReceivers as $emailReceiver) {

            $from = 'Admin';
            $to = $emailReceiver;
            $subject = "Admin Form";

            $config = array();
            $config["protocol"] = 'smpt';
            $config['smtp_host'] = 'bckonnect.com';
            $config["smtp_port"] = 465;
            $config["smtp_user"] = 'publicuser@bckonnect.com';
            $config["smtp_pass"] = 'PaSS@bckonnect.com';
            $config['mailtype'] = 'html';
            $this->email->initialize($config);

            $this->email->from($from, 'Agent Details');
            $this->email->to($to);
            $this->email->subject($subject);
            $this->email->message($message);
            $this->email->send();
        }

        return true;
        

    }






//insert rejected vehicle function start --ishara sewwandi

        public function insertreject(){
//        $vehi_id = $this->uri->segment(3);
        $vehicleid = $this->input->post('vehicle_id');
        $rejecttdate = date('Y-m-d H:i:s');

        $data = array(
//                'vehicle_id' => $this->input->post('vehicle_id'),
            'vehicle_id' => $this->input->post('vehicle_id'),
            'customer_customer_id' => $this->input->post('selling_customer_id'),
            'reason' => $this->input->post('reject_reason'),
            'rejected_date' => $rejecttdate,
        );
//            print_r($vehi_id);
        $this->setting_model->insert($data, 'reject');


        $data = array(

            'status' => 0,
            'reject_status' => 1,
            'inspection_status' => 0,
        );
//            print_r($vehi_id);
        $this->setting_model->update($data,'vehicle','vehicle_id',$vehicleid);

        redirect('vehicle/viewvehicles');
    }

    //insert rejected vehicle function end --ishara sewwandi


    //view rejected vehicle function start --ishara sewwandi
    public function viewrejected(){

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/vehicle/viewrejected';

        $config = array(
            'wheres' => '',
            'from' => array('reject'),
            'joins' => array(
                'selling_customer' => array('selling_customer.selling_customer_id = reject.customer_customer_id', 'left'),
                'vehicle' => array('vehicle.vehicle_id = reject.vehicle_id', 'left'),
            )
        );

        $mainData = array(
            'sellingreject' => $this->setting_model->getJoin($config),


        );


        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
//view rejected vehicle function end --ishara sewwandi

//update rejected status function start --ishara sewwandi
    public function changeactionreject(){

        $id = $this->input->post('id');
        $action = $this->input->post('action');
        $vehicle_id = $this->input->post('vehicleId');
        print_r($action);
        $data = array(
            'reject_status'=> $action,

        );

        $this->setting_model->update($data,'vehicle','vehicle_id',$vehicle_id);

        $this->setting_model->delete('reject','reject_id',$id);


    }
//update rejected status function end --ishara sewwandi

    //vehicle inspection view -- ishara sewwandi

    public function viewinspection() {


        $vehi = $this->uri->segment(3);

        $config = array(
            'wheres' => array(
                'vehicle' => array('vehicle_id', $vehi)
            ),
            'from' => array('vehicle'),
            'joins' => array(
                'inside_images' => array('inside_images.vehicle_vehicle_id = vehicle.vehicle_id', 'left'),
                'out_side_images' => array('out_side_images.vehicle_vehicle_id = vehicle.vehicle_id', 'left'),
                'vehicle_brand' => array('vehicle_brand.vehicle_brand_id = vehicle.vehicle_brand', 'left'),
                'vehicle_type' => array('vehicle_type.vehicle_type_id = vehicle.vehicle_type', 'left'),
                'vehicle_model' => array('vehicle_model.vehicle_model_id = vehicle.vehicle_model', 'left'),
//                'selling_customer' => array('selling_customer.selling_customer_id = vehicle.selling_customer_id', 'left'),
                'agents' => array('agents.agent_id = vehicle.agent_id', 'left')
            )
        );

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/vehicle/viewinspection';




        $mainData = array(
//            'vehicle' => $this->setting_model->Get_All('vehicle'),
            'insideimage' => $this->setting_model->Get_All('inside_images'),
            'outside' => $this->setting_model->Get_All('out_side_images'),
            'agent' => $this->setting_model->Get_All('agents'),
            'vehicle' => $this->setting_model->vehicleviewinspection($vehi),


        );


        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }

    //vehicle inspection view end --ishara sewwandi


    //insert rating  function start --ishara sewwandi

    public function insertrating() {
        $vehi = $this->input->post('vehicle_id');
        $data = array(

            'vehical_vehical_id' =>$this->input->post('vehicle_id'),
            'customer_customer_id' =>$this->input->post('selling_customer_id'),
            'extirier_condition' =>$this->input->post('rating1'),
            'interior_condition' =>$this->input->post('rating2'),
            'engine_condition' =>$this->input->post('rating3'),
            'image' =>$this->setting_model->upload('image', 'upload/publish'),
            'details' =>$this->input->post('details'),

            // 'main_banner_description' =>$this->input->post('main_banner_description'),
            // 'main_banner_link' =>$this->input->post('main_banner_link'),
        );

        $this->setting_model->insert($data, 'rating');

        $data = array(
            'publish_status' => 1,
        );
        $this->setting_model->update($data,'vehicle','vehicle_id',$vehi);


        redirect('vehicle/viewvehicle');
    }
//insert rating  function end --ishara sewwandi

// Star ratings
    public function get_star($count){
        $string = '';
        for($i=1;$i<=$count;$i++){
            $string .= '★';
        }
        return $string;
    }

    // End star ratings

    //view publish vehicle function start --ishara sewwandi

    public function viewpublised(){


//        $vehicall = $this->input->post('vehical_id');


        $pubid = $this->input->post('c_id');
//        print_r($pubid);
        $vehiid = $this->input->post('pu_id');

        $data = array(
//                'vehicle_id' => $this->input->post('vehicle_id'),
            'publish_status' =>$pubid,

        );
//            print_r($vehi_id);
        $this->setting_model->update($data,'vehicle','vehicle_id',$vehiid);

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/vehicle/viewpublised';

        $config = array(
            'wheres' => "",
            'from' => array('rating'),
            'joins' => array(
                'vehicle' => array('vehicle.vehicle_id = rating.vehical_vehical_id', 'left'),
                'selling_customer' => array('selling_customer.selling_customer_id  = rating.customer_customer_id', 'left'),
//                'inside_images' => array('inside_images.vehicle_vehicle_id = vehicle.vehicle_id', 'left'),
//                'out_side_images' => array('out_side_images.vehicle_vehicle_id = vehicle.vehicle_id', 'left'),
//                'vehicle_brand' => array('vehicle_brand.vehicle_brand_id = vehicle.vehicle_brand', 'left'),
//                'vehicle_type' => array('vehicle_type.vehicle_type_id = vehicle.vehicle_type', 'left'),
//                'vehicle_model' => array('vehicle_model.vehicle_brand_id = vehicle.vehicle_model', 'left'),
//                'agents' => array('agents.agent_id = vehicle.agent_id', 'left')


            )
        );

        $config1 = array(
            'wheres' => "",
            'from' => array('vehicle'),
            'joins' => array(
                'inside_images' => array('inside_images.vehicle_vehicle_id = vehicle.vehicle_id', 'left'),
                'out_side_images' => array('out_side_images.vehicle_vehicle_id = vehicle.vehicle_id', 'left'),
                'vehicle_brand' => array('vehicle_brand.vehicle_brand_id = vehicle.vehicle_brand', 'left'),
                'vehicle_type' => array('vehicle_type.vehicle_type_id = vehicle.vehicle_type', 'left'),
                'vehicle_model' => array('vehicle_model.vehicle_model_id = vehicle.vehicle_model', 'left'),
                'selling_customer' => array('selling_customer.selling_customer_id = vehicle.selling_customer_id', 'left'),
                'agents' => array('agents.agent_id = vehicle.agent_id', 'left')
            )
        );

        $pub = $this->setting_model->getJoin($config);
//        foreach ($pub as $pu);{
////            if($pu->vahical_vehical_id == ) {
////                $idd = $pu->vahical_vehical_id ;
//                $aa = $pu->extirier_condition;
//                $bb = $pu->interior_condition;
//                $cc = $pu->engine_condition;
////            }
//        }
//
//        $extirier_condition = $this->get_star($aa);
//        $interior_condition = $this->get_star($bb);
//        $engine_condition = $this->get_star($cc);

      

        $mainData = array(
            'rats' => $this->setting_model->getJoin($config),
            'rating' => $this->setting_model->getJoin($config1),
            'insideimage' => $this->setting_model->Get_All('inside_images'),
            'outside' => $this->setting_model->Get_All('out_side_images'),
//            'extirier' => $extirier_condition,
//            'interior' => $interior_condition,
//            'engine' => $engine_condition,
           'rates1'=>$this->setting_model->Get_All('rating'),
//            'id' => $idd,



//            'vehicle_brand' => $this->setting_model->Get_All('vehicle_brand')

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);


    }

    //view publish vehicle function end --ishara sewwandi

    //delete entire publish
    public function deletepublish(){
        //$service_id = $this->uri->segment(3);
        $vehicle_publish_id = $this->input->post('id');
        $limit_vehicle = count($this->setting_model->Get_Single('vehicle','vehicle_id',$vehicle_publish_id));
        $this->setting_model->delete_data('vehicle','vehicle_id',$vehicle_publish_id,$limit_vehicle);
        $this->setting_model->delete_data('inside_images','vehicle_vehicle_id',$vehicle_publish_id,$limit_vehicle);
        $this->setting_model->delete_data('vehicle_front_image','vehicle_id',$vehicle_publish_id ,$limit_vehicle);
        $this->setting_model->delete_data('rating','vehical_vehical_id',$vehicle_publish_id,$limit_vehicle);

        redirect('vehicle/viewpublised');
    }


    //insert model to database for '' --- ishara sewwandi

    public function insertmodel() {
        $data = array(
            'vehicle_brand_id' => $this->input->post('vehicle_brand'),
            'vehicle_model_name' => $this->input->post('vehicle_model_name'),
            'vehicle_type_id' => $this->input->post('vehicle_type')
        );

        $insert_id = $this->setting_model->insert($data, 'vehicle_model');



        $this->session->set_flashdata('msg','<div class="alert alert-success">New model Added Successfully!</div>');

        redirect('vehicle/addmodel');
    }


    //add vehicle model function start --
    public function addmodel() {
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/vehicle/addmodel';
        $mainData = array(
            'pagetitle' => 'Add New Vehice Model',
            
            'vehicletype' => $this->setting_model->Get_All('vehicle_type'),
            'vehiclebrand' => $this->setting_model->Get_All('vehicle_brand')
        );

        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }

    //View model function start-- Ishara sewwandi
    public function viewmodel() {

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/vehicle/viewmodel';
        $mainData = array(
            'vehicle_model' => $this->setting_model->Get_All('vehicle_model'),
            'vehicle_brand' => $this->setting_model->Get_All('vehicle_brand'),
            'vehicle_type' => $this->setting_model->Get_All('vehicle_type')

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }


    //edit model function start-- ishara sewwandi
    public function editmodel() {
        $id = $this->uri->segment(3);
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/vehicle/editmodel';
        $mainData = array(
            'vehicle_model' => $this->setting_model->Get_Single('vehicle_model','vehicle_model_id',$id),
            'vehiclebrand' => $this->setting_model->Get_All('vehicle_brand'),
            'vehicle_type' => $this->setting_model->Get_All('vehicle_type')

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }

    public function updatemodel() {
        $id = $this->uri->segment(3);
        $data = array(
            'vehicle_brand_id' => $this->input->post('vehicle_brand'),
            'vehicle_model_name' => $this->input->post('vehicle_model_name'),
            'vehicle_type_id' => $this->input->post('vehicle_type')
        );

        $this->setting_model->update($data, 'vehicle_model','vehicle_model_id',$id);



        $this->session->set_flashdata('msg','<div class="alert alert-success">Vehicle model Update Successfully!</div>');

        redirect('vehicle/viewmodel/');
    }



    //add vehicle type function start
    public function addtype() {
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/vehicle/addtype';
        $mainData = array(
            'pagetitle' => 'Add New Type',

        );

        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
    //add vehicle type function end


    //insert type to database for 'Vehicle'
    public function inserttype() {
        $data = array(
            'vehicle_type_name' =>$this->input->post('vehicle_type_name'),
        );

        $insert_id = $this->setting_model->insert($data, 'vehicle_type');



        $this->session->set_flashdata('msg','<div class="alert alert-success">New Type Added Successfully!</div>');

        redirect('vehicle/addtype');
    }
    //upload images and details to database for 'vehicle' end

    //View type function start--  ishara sewwandi
    public function viewtype() {

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/vehicle/viewtype';
        $mainData = array(
            'vehicle_type' => $this->setting_model->Get_All('vehicle_type'),

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
    //View type function end
    //edit type -- ishara sewwandi

    public function edittype(){
        $id = $this->uri->segment(3);
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/vehicle/edittype';
        $mainData = array(
            'vehicle_type' => $this->setting_model->Get_Single('vehicle_type','vehicle_type_id',$id),

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);

    }
    //update type -- ishara sewwandi

    public function updatetype(){
        $id = $this->uri->segment(3);
        $data = array(
            'vehicle_type_name' =>$this->input->post('vehicle_type_name'),
        );

         $this->setting_model->update($data,'vehicle_type','vehicle_type_id',$id);



        $this->session->set_flashdata('msg','<div class="alert alert-success">Vehicle type update Successfully!</div>');

        redirect('vehicle/edittype/'.$id);
    }

    //delete entire type
    public function deletetype(){
        //$service_id = $this->uri->segment(3);
        $agent_id = $this->input->post('id');
        $this->setting_model->delete('agent','agent_id',$agent_id);
        redirect('vehicle/viewtype');
    }

    //add vehicle type function start -- ishara sewwandi
    public function addbrand() {
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/vehicle/addbrand';
        $mainData = array(
            'pagetitle' => 'Add New Brand',
            
            'vehicletype' => $this->setting_model->Get_All('vehicle_type')

        );

        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
    //add vehicle type function end




    //insert type to database for 'Vehicle' -- ishara sewwandi
    public function insertbrand() {
        $data = array(
            'vehicle_brand_name' =>$this->input->post('vehicle_brand_name'),
            'vehicle_type_id' =>$this->input->post('vehicle_type'),
        );

        $insert_id = $this->setting_model->insert($data, 'vehicle_brand');



        $this->session->set_flashdata('msg','<div class="alert alert-success">New Brand Added Successfully!</div>');

        redirect('vehicle/addbrand');
    }
    //upload images and details to database for 'vehicle' end

    //View type function start -- ishara sewwandi
    public function viewbrand() {

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/vehicle/viewbrand';
        $mainData = array(
            'Brand' => $this->setting_model->Get_All('vehicle_brand'),
            'vehicle_type' => $this->setting_model->Get_All('vehicle_type'),

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
    //View type function end

    //edit vehicle type function start -- ishara sewwandi
    public function editbrand() {
        $id = $this->uri->segment(3);
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/vehicle/editbrand';
        $mainData = array(
            'Brand' => $this->setting_model->Get_Single('vehicle_brand','vehicle_brand_id',$id),
            'vehicletype' => $this->setting_model->Get_All('vehicle_type')

        );

        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
    //edit vehicle type function end

    //insert type to database for 'Vehicle' -- ishara sewwandi
    public function updatebrand() {
        $id = $this->uri->segment(3);
        $data = array(
            'vehicle_brand_name' =>$this->input->post('vehicle_brand_name'),
            'vehicle_type_id' =>$this->input->post('vehicle_type'),
        );

         $this->setting_model->update($data, 'vehicle_brand','vehicle_brand_id',$id);



        $this->session->set_flashdata('msg','<div class="alert alert-success">Vehicle Brand update Successfully!</div>');

        redirect('vehicle/editbrand/'.$id);
    }
    //upload images and details to database for 'vehicle' end

    //delete entire type -- ishara sewwandi
    public function deletebrand(){
        //$service_id = $this->uri->segment(3);
        $vehicle_brand_id = $this->input->post('id');
        $limit_vehicle=count($this->setting_model->Get_Single('vehicle_brand','vehicle_brand_id',$vehicle_brand_id));
        $this->setting_model->delete_data('vehicle_brand','vehicle_brand_id',$vehicle_brand_id,$limit_vehicle);

        redirect('vehicle/viewbrand');
    }

    //add vehicle status function start -- ishara sewwandi
    public function addstatus() {
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/vehicle/addstatus';
        $mainData = array(
            'pagetitle' => 'Add New Vehice Status',

        );

        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
    //add vehicle status function end

    //insert status to database for 'Vehicle' - ishara sewwandi
    public function insertstatus() {
        $data = array(
            'vehicle_status_name' =>$this->input->post('vehicle_status_name'),
        );

        $insert_id = $this->setting_model->insert($data, 'vehicle_status');



        $this->session->set_flashdata('msg','<div class="alert alert-success">New Status Added Successfully!</div>');

        redirect('vehicle/addstatus');
    }
    //End insert status to database for 'Vehicle'

    //View status function start -- ishara sewwandi
    public function viewstatus() {

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/vehicle/viewstatus';
        $mainData = array(
            'vehicle_status' => $this->setting_model->Get_All('vehicle_status'),

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
    //View status function end

    // edit status -- Ishara sewwandi
    public function editstatus(){
        $id = $this->uri->segment(3);
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/vehicle/editstatus';
        $mainData = array(
            'vehicle_status' => $this->setting_model->Get_Single('vehicle_status','vehicle_status_id',$id),

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
    // end edit

    //update status -- ishara sewwandi

    public function updatestatus(){
        $id = $this->uri->segment(3);
        $data = array(
            'vehicle_status_name' =>$this->input->post('vehicle_status_name'),
        );

       $this->setting_model->update($data, 'vehicle_status','vehicle_status_id',$id);



        $this->session->set_flashdata('msg','<div class="alert alert-success">Vehicle Status update Successfully!</div>');

        redirect('vehicle/editstatus/'.$id);
    }
    //end update

    //delete entire type
    public function deletestatus(){
        //$service_id = $this->uri->segment(3);
        $vehicle_status_id = $this->input->post('id');
        $limit_vehicle=count($this->setting_model->Get_Single('vehicle_status','vehicle_status_id',$vehicle_status_id));
        $this->setting_model->delete_data('vehicle_status','vehicle_status_id',$vehicle_status_id,$limit_vehicle);

        redirect('vehicle/viewstatus');
    }


}