<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Import extends CI_Controller {

	public function template($headerData, $sidebarData, $page, $mainData, $footerData) {
		$this->load->view('admin/components/header', $headerData);
		$this->load->view('admin/components/sidebar', $sidebarData);
		$this->load->view($page, $mainData);
		$this->load->view('admin/components/footer', $footerData);
	}

//start addtype function ------------------------- coded by Gihan 
	public function addtype() {
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/import/addvehicletype';
		$mainData = array(
			'pagetitle' => 'Add New Vehicle Type',
		);

		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}
// end addtype function --------------------------


   

// start inserttype function -------------------------- coded by Gihan
	public function inserttype(){
		$data = array(
			'vehical_type_name' =>$this->input->post('vehicle_type_name'),
			);

		$data1 = $this->setting_model->insert($data,'import_vehical_type');
		
		$this->session->set_flashdata('msg','<div class="alert alert-success">New Type Added Successfully!</div>');

		redirect('import/addtype');
	}
//end inserttype function

//start viewtype function ------------------------------- coded by Gihan
	public function viewtype(){

		$headerData = null;
		$sidebarData = null;
		$page = 'admin/import/viewtype';
		$mainData = array(
			'types' => $this->setting_model->Get_All('import_vehical_type'),
		);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}
// end viewtype function

	public function editimporttype(){

	    $id = $this->uri->segment(3);
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/import/edittype';
        $mainData = array(
            'types' => $this->setting_model->Get_Single('import_vehical_type','vehical_type_id',$id),
        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }

    //update vehicle type------------------------------ coded by Gihan

    public function updatetype(){
        $id = $this->uri->segment(3);
        $data = array(
            'vehical_type_name' =>$this->input->post('type_name'),
        );

        $data1 = $this->setting_model->update($data,'import_vehical_type','vehical_type_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-success">Vehicle Type Update Successfully!</div>');

        redirect('import/viewtype');
    }

// start addbrand function ---------------------------------- coded by Gihan
	public function addbrand() {
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/import/addvehiclebrand';
		$mainData = array(
			'pagetitle' => 'Add New Vehicle Brand',
			'Types' => $this->setting_model->Get_All('import_vehical_type')
		);

		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}
// end addbrand function

// start insertbrand function -------------------------------- coded by Gihan
	public function insertbrand(){
		$data = array(
			'vehicle_brand_name' =>$this->input->post('vehicle_brand_name'),
			'vehicle_type_id' => $this->input->post('vehicle_type_im')
			);

		$data1 = $this->setting_model->insert($data,'import_vehicle_brand');
		
		$this->session->set_flashdata('msg','<div class="alert alert-success">New Brand Added Successfully!</div>');

		redirect('import/addbrand');
	}
// end insertbrand function

	// start viewbrand function --------------------------------- coded by Gihan
	public function viewbrand(){

		$headerData = null;
		$sidebarData = null;
		$page = 'admin/import/viewbrand';
		$mainData = array(
			'Brands' => $this->setting_model->Get_All('import_vehicle_brand'),
			'Types' => $this->setting_model->Get_All('import_vehical_type')
		);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}
// end viewbrand function 

	public function editbrand(){

	    $id = $this->uri->segment(3);
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/import/editbrand';
        $mainData = array(
            'Brands' => $this->setting_model->Get_Single('import_vehicle_brand','vehicle_brand_id',$id),
			'Types' => $this->setting_model->Get_All('import_vehical_type')
        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }

    //end edit

    //start update brand
    public function updatebrand(){
        $id = $this->uri->segment(3);
        $data = array(
            'vehicle_brand_name' =>$this->input->post('brand_name'),
            'vehicle_type_id' => $this->input->post('vehicle_type_edit_b')
        );

        $data1 = $this->setting_model->update($data,'import_vehicle_brand','vehicle_brand_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-success">Update Brand Successfully!</div>');

        redirect('import/viewbrand');
    }





// start function addmodal ------------------------------------ coded by Gihan
	public function addmodal() {
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/import/addvehicalmodal';
		$mainData = array(
			'pagetitle' => 'Add New Vehicle Brand',
			'Brands' => $this->setting_model->Get_All('import_vehicle_brand'),
			'Types' => $this->setting_model->Get_All('import_vehical_type')
		);

		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}
// end function addmodal


// start function insertmodal ----------------------------------- coded by Gihan
	public function insertmodal(){
		$data = array(
			'vehicle_brand_id' => $this->input->post('vehicle_brand'),
			'vehicle_model_name' => $this->input->post('vehicle_modal_name'),
			'vehicle_type_id' => $this->input->post('vehicle_type_im')
			);

		$data1 = $this->setting_model->insert($data,'import_vehicle_model');
		
		$this->session->set_flashdata('msg','<div class="alert alert-success">New Modal Added Successfully!</div>');

		redirect('import/addmodal');
	}
// end function insertmodal


// start function viewmodal --------------------------------------- coded by Gihan
	public function viewmodal(){

		$headerData = null;
		$sidebarData = null;
		$page = 'admin/import/viewmodal';
		$mainData = array(
			'brands' => $this->setting_model->Get_All('import_vehicle_brand'),
			'modals' => $this->setting_model->Get_All('import_vehicle_model'),
			'Types' => $this->setting_model->Get_All('import_vehical_type')
		);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}
// end viewmodal function

 public function editmodel(){

	    $id = $this->uri->segment(3);
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/import/editmodal';
        $mainData = array(
            'brands' => $this->setting_model->Get_All('import_vehicle_brand'),
            'modals' => $this->setting_model->Get_Single('import_vehicle_model','vehicle_model_id',$id),
			'Types' => $this->setting_model->Get_All('import_vehical_type')
        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }

    //end edit

    //start vehicle modal update

    public function updatemodal(){
        $id = $this->uri->segment(3);
        $data = array(
            'vehicle_brand_id' => $this->input->post('vehicle_brand'),
            'vehicle_model_name' => $this->input->post('modal_name'),
            'vehicle_type_id' => $this->input->post('vehicle_type_edit')
        );

        $data1 = $this->setting_model->update($data,'import_vehicle_model','vehicle_model_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-success">Update Modal Successfully!</div>');

        redirect('import/viewmodal');
    }


// start addnewvehicle function ---------------------------------- coded by Gihan
	public function addnewvehicle() {
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/import/addnewvehicle';
		$mainData = array(
			'pagetitle' => 'Add New Vehicle Brand',
			'types' => $this->setting_model->Get_All('import_vehical_type'),
			'Brands' => $this->setting_model->Get_All('import_vehicle_brand'),
			'modals' => $this->setting_model->Get_All('import_vehicle_model'),
		);

		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}
// end addnewvehicle function

	public function insertvehicle(){
		$millage_num = $this->input->post('mileage_range');
        $millage_unit = $this->input->post('vehicle_range');

        $fuel_cons = $this->input->post('fuel_consuption');
        $fuel_con_unit = $this->input->post('letersPer');

        $vehi_cap = $this->input->post('engine_capacity');
        $vehi_cap_unit = $this->input->post('capacity');

		$data = array(
			'import_vehicle_type' =>$this->input->post('vehicle_type'),
			'import_brand_id' =>$this->input->post('vehicle_brand'),
			'import_model_id' =>$this->input->post('vehicle_model'),
			'vehicle_transmission' =>$this->input->post('transmiss'),
			'vehicle_engine_capacity' =>$vehi_cap,
			'engine_capacity_unit' => $vehi_cap_unit,
			'vehicle_seating_capacity' =>$this->input->post('seating_capacity'),
			'year_of_manufacture' =>$this->input->post('manufacture'),
			'year_of_registration' =>$this->input->post('registration'),
			'vehical_number' =>$this->input->post('number'),
			'average_of_fuel_consuption' => $fuel_cons,
			'fuel_consumption_unit' => $fuel_con_unit,
			'availability' => $this->input->post('availabil'),
			'vehicle_total_value' =>$this->input->post('vehicle_total'),
			'vehicle_features'=>implode(',', $this->input->post('vehicle_features')),
			'additional_features' =>$this->input->post('additional_features'),
			'vehicle_minimum_downpayment' =>$this->input->post('minimum_downpayment'),
			'fuel_type' =>$this->input->post('fuel_ty'),
			'vehicle_mileage_range' =>$millage_num,
			'millage_range_unit' => $millage_unit,
			'sellers_description' => $this->input->post('sellers_description'),
			'number_of_owners' => $this->input->post('num_of_owners'),
			'import_inside_image' => $this->setting_model->upload('front_image','upload/import/inside')

			);
		// print_r($data);
		
		$insert_id = $this->setting_model->insert($data,'import_vehicle');
		// print_r($insert_id);
		$files = $_FILES;
		$path = "import/outside_multi";
		$fileName = $this->multiple_image_uplaod->multi_image_upload($files,$path);
		$this->multiple_image_uplaod->upload_image_vehicle($fileName, $insert_id);
		// print_r($files);

// 		$data22 = array(
//             'image' => $this->setting_model->upload('front_image','upload/import/inside'),
//             'vehicle_id' => $insert_id
//         );
//         $dataX = $this->setting_model->insert($data22,'import_front_image');
			
// 		print_r($data22);
		$this->session->set_flashdata('msg','<div class="alert alert-success">New Vehicle Added Successfully!</div>');

		redirect('import/addnewvehicle');

	}

	public function viewvehicle(){

		$headerData = null;
		$sidebarData = null;
		$page = 'admin/import/viewimportvehicle';
		$mainData = array(
			'brands' => $this->setting_model->Get_All('import_vehicle_brand'),
			'modals' => $this->setting_model->Get_All('import_vehicle_model'),
			'types' => $this->setting_model->Get_All('import_vehical_type'),
			'vehicles' => $this->setting_model->Get_All('import_vehicle'),
			'outside_images'=> $this->setting_model->Get_All('import_outside_images'),
			'front_image' => $this->setting_model->Get_All('import_front_image'),
		);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}

	public function editvehicle() {
        $id = $this->uri->segment(3);

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/import/editimportvehicle';

        $mainData = array(
            'vehicle' => $this->setting_model->Get_Single('import_vehicle','vehicle_id',$id),
            'vehicle_images' => $this->multiple_image_uplaod->edit_data_image_importvehicle($id),
            'types'=> $this->setting_model->Get_All('import_vehical_type'),
            'brands'=> $this->setting_model->Get_All('import_vehicle_brand'),
            'modals'=> $this->setting_model->Get_All('import_vehicle_model'),
            'front' => $this->setting_model->Get_All('import_front_image'),
        );

        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);

    }
    public function deleteimageimport(){
        $deleteid  = $this->input->post('id');
        $this->db->delete('import_front_image', array('id' => $deleteid));
        $verify = $this->db->affected_rows();
        echo $verify;
    }

    public function deleteimageimportMul(){
        $deleteid  = $this->input->post('id');
        $this->db->delete('import_outside_images', array('out_side_image_id' => $deleteid));
        $verify = $this->db->affected_rows();
        echo $verify;
    }
    public function deleteimport(){
    	
        $import_id = $this->input->post('id');
        $limit_vehicle=count($this->setting_model->Get_Single('import_vehicle','vehicle_id',$import_id));
        $this->setting_model->delete_data('import_vehicle','vehicle_id',$import_id,$limit_vehicle);
        $this->setting_model->delete_data('import_front_image','vehicle_id',$import_id,$limit_vehicle);
        $this->setting_model->delete_data('import_outside_images','vehicle_vehicle_id',$import_id,$limit_vehicle);
        redirect('import/viewvehicle');
    }

    public function updatevehicle() {

        $id = $this->uri->segment(3);
        
        if(isset($_FILES['fileinput']) && empty($_FILES['fileinput']['tmp_name'])) {
            $image = $this->input->post('new_front_image');
        } else {
            $image = $this->setting_model->upload('fileinput', 'upload/import/inside');
        }

        $data = array(
            'import_inside_image' => $image,
    		'import_vehicle_type' =>$this->input->post('vehicle_type'),
		'import_brand_id' =>$this->input->post('vehicle_brand'),
		'import_model_id' =>$this->input->post('vehicle_model'),
		'vehicle_transmission' =>$this->input->post('vehicle_transmission'),
		'vehicle_engine_capacity' =>$this->input->post('vehicle_engine_capacity'),
		'engine_capacity_unit' => $this->input->post('engine_cap'),
		'vehicle_seating_capacity' =>$this->input->post('vehicle_seating_capacity'),
		'year_of_manufacture' =>$this->input->post('year_of_manufacture'),
		'year_of_registration' =>$this->input->post('year_of_registration'),
		'vehical_number' =>$this->input->post('vehical_number'),
		'average_of_fuel_consuption' =>$this->input->post('average_of_fuel_consuption'),
		'fuel_consumption_unit' => $this->input->post('fuel_con'),
		'availability' => $this->input->post('availability'),
		'vehicle_total_value' =>$this->input->post('total'),
		'vehicle_features'=>implode(',', $this->input->post('vehicle_features')),
            'additional_features' =>$this->input->post('additional_features'),
		'vehicle_minimum_downpayment' =>$this->input->post('down'),
		'fuel_type' =>$this->input->post('fuel_type'),
		'vehicle_mileage_range' =>$this->input->post('vehicle_mileage_range'),
		'millage_range_unit' => $this->input->post('vehicle_range'),
		'sellers_description' => $this->input->post('seller_description_edit'),
		'number_of_owners' => $this->input->post('number_of_owners'),
        );

        $data1 = $this->setting_model->update($data,'import_vehicle','vehicle_id',$id);
 	if(count($_FILES['userfile']['name'])>1){
        //edit multiple image upload
        $files = $_FILES;
        $path = "import/outside_multi";
        $fileName = $this->multiple_image_uplaod->multi_image_upload($files,$path);
        $this->multiple_image_uplaod->edit_upload_image_vehicle2($fileName,$id);
        //end multiple image upload edit
	 }
        $this->session->set_flashdata('msg','<div class="alert alert-success">New Car Added Successfully!</div>');

        redirect('import/viewvehicle');
    }

    public function viewInquiry(){

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/import/viewinquiry';
        $mainData = array(

            'vehicles' => $this->setting_model->Get_All('import_vehicle'),
            'customer' => $this->setting_model->Get_All('customer'),
            'types' => $this->setting_model->Get_All('import_vehical_type'),
            'brands' => $this->setting_model->Get_All('import_vehicle_brand'),
            'model' => $this->setting_model->Get_All('import_vehicle_model'),
            // 'inside_images' => $this->setting_model->Get_All('inside_images'),
            'outside_images' => $this->setting_model->Get_All('import_outside_images'),
            'inquiry1' => $this->setting_model->Get_All('import_inquiry')
            

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }

    public function viewfollowup(){

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/import/viewfollowups';
        $mainData = array(

            'vehicles' => $this->setting_model->Get_All('import_vehicle'),
            'customer' => $this->setting_model->Get_All('customer'),
            'types' => $this->setting_model->Get_All('import_vehical_type'),
            'brands' => $this->setting_model->Get_All('import_vehicle_brand'),
            'model' => $this->setting_model->Get_All('import_vehicle_model'),
            // 'inside_images' => $this->setting_model->Get_All('inside_images'),
            'outside_images' => $this->setting_model->Get_All('import_outside_images'),
            'inquiry1' => $this->setting_model->Get_All('import_inquiry')
            

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }

    public function viewConfirm(){

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/import/viewConfirm';
        $mainData = array(

            'vehicles' => $this->setting_model->Get_All('import_vehicle'),
            'customer' => $this->setting_model->Get_All('customer'),
            'types' => $this->setting_model->Get_All('import_vehical_type'),
            'brands' => $this->setting_model->Get_All('import_vehicle_brand'),
            'model' => $this->setting_model->Get_All('import_vehicle_model'),
            'outside_images' => $this->setting_model->Get_All('import_outside_images'),
            'inquiry1' => $this->setting_model->Get_All('import_inquiry')

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }

    public function viewRejected(){

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/import/viewRejected';
        $mainData = array(

           'vehicles' => $this->setting_model->Get_All('import_vehicle'),
            'customer' => $this->setting_model->Get_All('customer'),
            'types' => $this->setting_model->Get_All('import_vehical_type'),
            'brands' => $this->setting_model->Get_All('import_vehicle_brand'),
            'model' => $this->setting_model->Get_All('import_vehicle_model'),
            'outside_images' => $this->setting_model->Get_All('import_outside_images'),
            'inquiry1' => $this->setting_model->Get_All('import_inquiry')

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
// ---------------------------------------------------------------------------


// start function for followup import inquiry --------------------- coded by Gihan

    public function followUp(){

        $id = $this->input->post("inq_id");
        // print_r($id);
        $date = date('Y-m-d');
        $data = array(
            'status' => 1,
            'status_change_date' => $date,
            'followUp_description' =>$this->input->post('Follow_up_description'),
            'next_followup_date' =>$this->input->post('next_date'),
        );

        $data1 = $this->setting_model->update($data,'import_inquiry','import_inquiry_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-warning">Follow Up Successfully!</div>');

        redirect('import/viewinquiry');
    }
// end function --------------------------------------------------------------------
    public function followUp1(){

        $id = $this->input->post("inq_id");
        // print_r($id);
        $date = date('Y-m-d');
        $data = array(
            'status' => 1,
            'status_change_date' => $date,
            'followUp_description' =>$this->input->post('Follow_up_description'),
            'next_followup_date' =>$this->input->post('next_date'),
        );

        $data1 = $this->setting_model->update($data,'import_inquiry','import_inquiry_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-warning">Follow Up Successfully!</div>');

        redirect('import/viewfollowup');
    }

// start function for confirm import inquiry ------------------ coded by Gihan
    public function confirm(){

        $id = $this->uri->segment(3);
        $date = date('Y-m-d');
        $data = array(
            'status' => 2,
            'status_change_date' => $date,
        );

        $data1 = $this->setting_model->update($data,'import_inquiry','import_inquiry_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-success">Inquiry Confirm Successful!</div>');

        redirect('import/viewinquiry');
    }
// end function ------------------------------------------------------------------
    public function confirm11(){

        $id = $this->uri->segment(3);
        $date = date('Y-m-d');
        $data = array(
            'status' => 2,
            'status_change_date' => $date,
        );

        $data1 = $this->setting_model->update($data,'import_inquiry','import_inquiry_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-success">Inquiry Confirm Successful!</div>');

        redirect('import/viewfollowup');
    }

// start function for reject import inquiry -------------------- coded by Gihan

    public function reject(){

        $id = $this->input->post('inq_id');
        $date = date('Y-m-d');
        $data = array(
            'status' => 3,
            'reject_reason' => $this->input->post('reject_reason'),
            'status_change_date' => $date,
        );

        $data1 = $this->setting_model->update($data,'import_inquiry','import_inquiry_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-danger"> Reject Successful!</div>');

        redirect('import/viewinquiry');
    }

    public function reject1(){

        $id = $this->input->post('inq_id');
        $date = date('Y-m-d');
        $data = array(
            'status' => 3,
            'reject_reason' => $this->input->post('reject_reason'),
            'status_change_date' => $date,
        );

        $data1 = $this->setting_model->update($data,'import_inquiry','import_inquiry_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-danger"> Reject Successful!</div>');

        redirect('import/viewfollowup');
    }

     public function updateDate(){
        // print_r($inq_id);
        $id = $this->uri->segment(3);
        $date = $this->input->post('changed_date');
        $data = array(
            'next_followup_date' => $this->input->post('changed_date')
        );

        $data1 = $this->setting_model->update($data,'import_inquiry','import_inquiry_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-success">Update date Successful!</div>');

        redirect('import/viewfollowup');
    }

    public function followUp2(){
        $id = $this->input->post("inq_id");
        $date = date('Y-m-d');
        $data = array(
            'status' => 1,
            'status_change_date' => $date,
            'followUp_description' =>$this->input->post('Follow_up_description'),
            'next_followup_date' =>$this->input->post('next_date'),
        );

        $data1 = $this->setting_model->update($data,'import_inquiry','import_inquiry_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-warning">Follow up Successful!</div>');

        redirect('import/viewRejected');
    }

    public function confirm1(){

        $id = $this->uri->segment(3);
        $date = date('Y-m-d');
        $data = array(
            'status' => 2,
            'status_change_date' => $date,
        );

        $data1 = $this->setting_model->update($data,'import_inquiry','import_inquiry_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-success">Inquiry Confirm Successful!</div>');

        redirect('import/viewRejected');
    }

    public function paymentStatus(){

    	$id = $this->uri->segment(3);
    	$date = date('Y-m-d');

    	$data = array(
    		'payment_status' => 1,
    		'payment_collected_date' => $date,
    	);

    	$data1 = $this->setting_model->update($data,'import_inquiry','import_inquiry_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-success">Payment Collected Successful!</div>');

        redirect('import/viewConfirm');
    }

}
?>