<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Selling extends CI_Controller {

	public function template($headerData, $sidebarData, $page, $mainData, $footerData) {
		$this->load->view('admin/components/header', $headerData);
		$this->load->view('admin/components/sidebar', $sidebarData);
		$this->load->view($page, $mainData);
		$this->load->view('admin/components/footer', $footerData);
	}

	//add vehicle page
	public function addvehicle(){
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/vehicle/addvehicle';
		$mainData = array(
			'pagetitle' => 'Add New Vehicle',
			'vehicletype' => $this->setting_model->Get_All('vehicle_type'),
			'vehiclebrand' => $this->setting_model->Get_All('vehicle_brand'),
			'vehiclemodel' => $this->setting_model->Get_All('vehicle_model'),
			'vehiclestatus' => $this->setting_model->Get_All('vehicle_status'),
		);

		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}


	//insert vehicle
	public function insertvehicle(){
	
		$user_password = 'auto@'.rand(0,100000);
        $radio = $this->input->post('rdoName');
        $radiotwo = $this->input->post('rdoNameee');



        if($radio == 1) {

            if ($_POST) {

                $data = array(
                    'email' => $this->input->post('email'),
                    'password' => $this->input->post('passwordreg')
                );

                $result = $this->user_login_model->is_user_exist($data);
                $result_sell = $this->user_login_model->is_sell_user_exist($data);
                // var_dump($result);
                if (!empty($result && $result_sell)) {


                    $verification = $result->verification_status;
                    $customer_id = $result_sell->selling_customer_id;

                    if ($verification == 1) {

                        $data = array(
                            'id' => $result->user_id,
                            'username' => $result->user_name,
                            'usertype' => $result->user_type, // 1-admin, 2-editor, 3-feader
                            'email' => $result->email,
                            'contactnumber' => $result->contact_number,
                            'address' => $result->address,
                            'validated' => true
                        );


                        $this->session->set_userdata($data);
			            $user_iid = $this->session->userdata('id');


                        $vehicleData = array(
                            'vehicle_type' =>$this->input->post('vehicle_type'),
                            'vehicle_brand' =>$this->input->post('vehicle_brand'),
                            'vehicle_model' =>$this->input->post('vehicle_model'),
                            'vehicle_transmission' =>$this->input->post('vehicle_transmission'),
                            'fuel_type' =>$this->input->post('fuel_type'),
                            'year_of_manufacture' =>$this->input->post('year_of_manufacture'),
                            'year_of_registration' =>$this->input->post('year_of_registration'),
                            'vehicle_number' =>$this->input->post('vehicle_number'),
                            'number_of_owners' =>$this->input->post('number_of_owners'),
                            'vehicle_engine_capacity' => $this->input->post('vehicle_engine_capacity'),
                            'vehicle_mileage_range' =>$this->input->post('vehicle_mileage_range'),
                            'vehicle_total_value' =>$this->input->post('vehicle_total_value'),
                            'selling_customer_id' =>$customer_id,
                            'user_id' => $user_iid,
                            'inside_360_images' => $this->setting_model->upload('front_image', 'upload/selling/Front/')

                        );

                        $insert_id = $this->setting_model->insert($vehicleData, 'vehicle');



                        $files = $_FILES;
                        $path = "selling";
                        $fileName = $this->multiple_image_uplaod->multi_image_upload($files,$path);
                        $this->multiple_image_uplaod->upload_image_selling_vehicle($fileName,$insert_id);

                        $this->send_customer_email();
                        $this->send_email_admin_vehicle();
                        $this->session->set_flashdata('success', 'Login Successful... Upload successful');
                        redirect('admin');
                    } else  {

                        $this->session->set_flashdata('error', 'Your username & password is not verified...Please check your email for verification mail');
                        redirect('Home');
                    }


                } else if (!empty($result)) {
                    if(empty($result_sell)){
                    $verification = $result->verification_status;

                    if ($verification == 1) {

                        $data = array(
                            'id' => $result->user_id,
                            'username' => $result->user_name,
                            'usertype' => $result->user_type, // 1-admin, 2-editor, 3-feader
                            'email' => $result->email,
                            'contactnumber' => $result->contact_number,
                            'address' => $result->address,
                            'validated' => true
                        );


                        $this->session->set_userdata($data);

                        $customerData = array(
                            'customer_name' => $this->input->post('customer_name'),
                            'phone_number' => $this->input->post('phone_number'),
                            'email' => $this->input->post('email'),
                            'address_line_1' => $this->input->post('address_line_1'),
                            'address_line_2' => $this->input->post('address_line_2'),
                            'address_city' => $this->input->post('address_city'),
                            'password' => md5($this->input->post('passwordreg')),
                        );


                        $user_id = $this->setting_model->insert($customerData, 'selling_customer');
			            $user_iid = $this->session->userdata('id');
                        $vehicleData = array(
                            'vehicle_type' => $this->input->post('vehicle_type'),
                            'vehicle_brand' => $this->input->post('vehicle_brand'),
                            'vehicle_model' => $this->input->post('vehicle_model'),
                            'vehicle_transmission' => $this->input->post('vehicle_transmission'),
                            'fuel_type' => $this->input->post('fuel_type'),
                            'year_of_manufacture' => $this->input->post('year_of_manufacture'),
                            'year_of_registration' => $this->input->post('year_of_registration'),
                            'vehicle_number' => $this->input->post('vehicle_number'),
                            'number_of_owners' => $this->input->post('number_of_owners'),
                            'vehicle_engine_capacity' => $this->input->post('vehicle_engine_capacity'),
                            'vehicle_mileage_range' => $this->input->post('vehicle_mileage_range'),
                            'vehicle_total_value' => $this->input->post('vehicle_total_value'),
                            'selling_customer_id' => (int)$user_id,
                            'user_id' => $user_iid,
                            'inside_360_images' => $this->setting_model->upload('front_image', 'upload/selling/Front/')


                        );

                        $insert_id = $this->setting_model->insert($vehicleData, 'vehicle');



                        $files = $_FILES;
                        $path = "selling";
                        $fileName = $this->multiple_image_uplaod->multi_image_upload($files, $path);
                        $this->multiple_image_uplaod->upload_image_selling_vehicle($fileName, $insert_id);

                        $this->send_customer_email();
                        $this->send_email_admin_vehicle();
                        $this->session->set_flashdata('success', 'Login Successful... upload successful..');
                        redirect('admin');
                    }
                    } else  {

                        $this->session->set_flashdata('error', 'Your username & password is not verified...Please check your email for verification mail');
                        redirect('Home');
                    }
                }
                else{

                    $this->session->set_flashdata('error', 'Username or password is incorrect... Please try again');
                    redirect('Home');
                }


            }






        }else{

		if ($_POST) {

                $data = array(
                    'email' => $this->input->post('email'),

                );




                $result = $this->user_login_model->is_user_exist_checkemail($data);
                if (empty($result)) {

            $customerData = array(
                'customer_name' => $this->input->post('customer_name'),
                'phone_number' => $this->input->post('phone_number'),
                'email' => $this->input->post('email'),
                'address_line_1' => $this->input->post('address_line_1'),
                'address_line_2' => $this->input->post('address_line_2'),
                'address_city' => $this->input->post('address_city'),
                'password' => md5($this->input->post('password')),

            );


            $user_id = $this->setting_model->insert($customerData, 'selling_customer');
            $user_iid = $this->session->userdata('id');
            $vehicleData = array(
                'vehicle_type' => $this->input->post('vehicle_type'),
                'vehicle_brand' => $this->input->post('vehicle_brand'),
                'vehicle_model' => $this->input->post('vehicle_model'),
                'vehicle_transmission' => $this->input->post('vehicle_transmission'),
                'fuel_type' => $this->input->post('fuel_type'),
                'year_of_manufacture' => $this->input->post('year_of_manufacture'),
                'year_of_registration' => $this->input->post('year_of_registration'),
                'vehicle_number' => $this->input->post('vehicle_number'),
                'number_of_owners' => $this->input->post('number_of_owners'),
                'vehicle_engine_capacity' => $this->input->post('vehicle_engine_capacity'),
                'vehicle_mileage_range' => $this->input->post('vehicle_mileage_range'),
                'vehicle_total_value' => $this->input->post('vehicle_total_value'),
                'selling_customer_id' => (int)$user_id,
                'user_id' => $user_iid,
                'inside_360_images' => $this->setting_model->upload('front_image', 'upload/selling/Front/')


            );

            $insert_id = $this->setting_model->insert($vehicleData, 'vehicle');



            $files = $_FILES;
            $path = "selling";
            $fileName = $this->multiple_image_uplaod->multi_image_upload($files, $path);
            $this->multiple_image_uplaod->upload_image_selling_vehicle($fileName, $insert_id);

	        $password = $this->input->post('password');

            $data = array(

                    'user_name' => $this->input->post('customer_name'),
                    'contact_number' => $this->input->post('phone_number'),
                    'email' => $this->input->post('email'),
                    'address' => $this->input->post('address_line_1'),
                    'password' => md5($this->input->post('password')),
                    'verification_status' => 0,


                );

            if($id=$this->setting_model->insert($data, 'login')) {
             $data1 = array(
                            'user_id' =>$id,
                        );
                        $this->setting_model->update($data1,'vehicle','vehicle_id',$insert_id);
                $this->session->set_flashdata('success', 'registered successfully.. and Upload successfully');
               redirect('selling/senduserverification/'.$id.'/'.$password);
            }else{
                $this->session->set_flashdata('error', 'Oops! Something went wrong! Try again.');
                redirect('Home');

            }
            }else{
                    $result_sell = $this->user_login_model->is_sell_user_exist_mail($data);
                        $customer_id = $result_sell->selling_customer_id;
//                        echo $customer_id;
                        $user_iid = $this->session->userdata('id');

                        $vehicleData = array(
                            'vehicle_type' => $this->input->post('vehicle_type'),
                            'vehicle_brand' => $this->input->post('vehicle_brand'),
                            'vehicle_model' => $this->input->post('vehicle_model'),
                            'vehicle_transmission' => $this->input->post('vehicle_transmission'),
                            'fuel_type' => $this->input->post('fuel_type'),
                            'year_of_manufacture' => $this->input->post('year_of_manufacture'),
                            'year_of_registration' => $this->input->post('year_of_registration'),
                            'vehicle_number' => $this->input->post('vehicle_number'),
                            'number_of_owners' => $this->input->post('number_of_owners'),
                            'vehicle_engine_capacity' => $this->input->post('vehicle_engine_capacity'),
                            'vehicle_mileage_range' => $this->input->post('vehicle_mileage_range'),
                            'vehicle_total_value' => $this->input->post('vehicle_total_value'),
                            'selling_customer_id' => $customer_id,
                            'user_id' => $user_iid,
                            'inside_360_images' => $this->setting_model->upload('front_image', 'upload/selling/Front/')


                        );


                        $files = $_FILES;
                        $path = "selling";
                        $fileName = $this->multiple_image_uplaod->multi_image_upload($files, $path);
                        $this->multiple_image_uplaod->upload_image_selling_vehicle($fileName, $insert_id);

                        $this->send_customer_email();
                        $this->send_email_admin_vehicle();
                        $this->session->set_flashdata('success', ' upload successful..');
                        redirect('admin');
                    }


            }
        }

	}
	
	
	private function _responses(array $data = array(), $code = 200) {
        $this->output
            ->set_status_header($code)
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

//sent email for agent--ishara sewwandi
    public function send_customer_email()
    {
//        $client_name = $this->input->post('vehicle_id');
        $custom_email = $this->input->post('email');


        $this->_send_customer_email( $custom_email );


        $this->_responses( array( "status" => "true", "message" => "Thank you for contacting us." ) );

    }

    private function _send_customer_email($custom_email){


        $log_path =  base_url('/assets/logo.png');
//        $client_communication_star = $this->get_star($communication);
//        $client_quality_of_deliverible = $this->get_star($quality_of_deliverible);
//        $client_proffesionalism = $this->get_star($proffesionalism);
//        $client_effectiveness = $this->get_star($effectiveness);


        $message = "";

        $message .= "<p style=\"font-size: 1.2em; padding: 10px; margin-bottom: 0;\">* This is an auto genarated e-mail for a request of a live web use.</p>";
        $message .= "<table style=\"width: 60%; min-width: 500px;\">";
        $message .= "<tbody style=\"border: 1px solid #a8a8a8;\">";
        $message .= "<tr style=\"background-color: white;\">";
        $message .= "<td style=\"padding: 15px; text-align: center;\" colspan=2>";
        $message .= "<img style=\"padding: 10px; height: auto; width: 30%;\" src=\"$log_path\"/>";
        $message .= "</td>";
        $message .= "</tr>";

        $message .= "<table style=\"width: 60%; min-width: 500px;\">";
        $message .= "<tbody style=\"border: 1px solid #a8a8a8;\">";
        $message .= "<tr style=\"background-color: white;\">";
        $message .= "<td style=\"padding: 15px; text-align: center;\" colspan=2>";
        $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Thank you for join with us..</p>";
        $message .= "</td>";
        $message .= "</tr>";





        $message .= "</tbody>";
        $message .= "</table>";



        $emailReceivers = array(
            $custom_email

        );

        foreach( $emailReceivers as $emailReceiver) {

            $from = 'Administer';
            $to = $emailReceiver;
            $subject = "Comfirm Form";

            $config = array();
            $config["protocol"] = 'smpt';
            $config['smtp_host'] = 'bckonnect.com';
            $config["smtp_port"] = 465;
            $config["smtp_user"] = 'publicuser@bckonnect.com';
            $config["smtp_pass"] = 'PaSS@bckonnect.com';
            $config['mailtype'] = 'html';
            $this->email->initialize($config);

            $this->email->from($from, 'Comfirm');
            $this->email->to($to);
            $this->email->subject($subject);
            $this->email->message($message);
            $this->email->send();
        }

        return true;
        $this->session->set_flashdata('success', ' upload successful..');
        redirect('admin');

    }
    
     //send email for admin --ishara sewwandi

    public function send_email_admin_vehicle(){
        $name = $this->input->post('customer_name');
        $number = $this->input->post('phone_number');
        $email = $this->input->post('email');
        $address = $this->input->post('address_line_1');
        $assign_date = date('Y-m-d H:i:s');



        $this->_send_email_admin_vehicle($name, $number, $email, $address, $assign_date);



        $this->_responses( array( "status" => "true", "message" => "Thank you for contacting us." ) );

        $this->session->set_flashdata('success', ' upload successful..');
        redirect('admin');

    }



    private function _send_email_admin_vehicle($name, $number, $email, $address, $assign_date){
        $log_path =  base_url('/assets/logo.png');

        $message = "";

        $message .= "<p style=\"font-size: 1.2em; padding: 10px; margin-bottom: 0;\">* This is an auto genarated e-mail for a request of a live web use.</p>";
        $message .= "<table style=\"width: 60%; min-width: 500px;\">";
        $message .= "<tbody style=\"border: 1px solid #a8a8a8;\">";
        $message .= "<tr style=\"background-color: white;\">";
        $message .= "<td style=\"padding: 15px; text-align: center;\" colspan=2>";
        $message .= "<img style=\"padding: 10px; height: auto; width: 30%;\" src=\"$log_path\"/>";
        $message .= "</td>";
        $message .= "</tr>";

        $message .= "<tr style=\"\">";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Agent id:</p>";
        $message .= "</td>";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$name</p>";
        $message .= "</td>";
        $message .= "</tr>";

        $message .= "<tr style=\"\">";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Agent Name:</p>";
        $message .= "</td>";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$number</p>";
        $message .= "</td>";
        $message .= "</tr>";

        $message .= "<tr style=\"\">";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Vehicle ID:</p>";
        $message .= "</td>";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$email</p>";
        $message .= "</td>";
        $message .= "</tr>";

        $message .= "<tr style=\"\">";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Vehicle Number:</p>";
        $message .= "</td>";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$address</p>";
        $message .= "</td>";
        $message .= "</tr>";


        $message .= "<tr style=\"\">";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Assign Date/Time:</p>";
        $message .= "</td>";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$assign_date</p>";
        $message .= "</td>";
        $message .= "</tr>";


        $message .= "</tbody>";
        $message .= "</table>";



        $emailReceivers = array(
            'dilruk@alliancefinance.lk'
        );

        foreach( $emailReceivers as $emailReceiver) {

            $from = 'Admin';
            $to = $emailReceiver;
            $subject = "Admin Form";

            $config = array();
            $config["protocol"] = 'smpt';
            $config['smtp_host'] = 'bckonnect.com';
            $config["smtp_port"] = 465;
            $config["smtp_user"] = 'publicuser@bckonnect.com';
            $config["smtp_pass"] = 'PaSS@bckonnect.com';
            $config['mailtype'] = 'html';
            $this->email->initialize($config);

            $this->email->from($from, 'Customer Details');
            $this->email->to($to);
            $this->email->subject($subject);
            $this->email->message($message);
            $this->email->send();
        }

        return true;


    }
	
	//insert vehicle from admin pannel
	public function insertvehicleadmin(){
	
		    $millage_num = $this->input->post('vehicle_mileage_range');
	        $millage_unit = $this->input->post('vehicle_range');
	
	        $fuel_cons = $this->input->post('average_of_fuel_consuption');
	        $fuel_con_unit = $this->input->post('letersPer');
	
	        $vehi_cap = $this->input->post('vehicle_engine_capacity');
	        $vehi_cap_unit = $this->input->post('capacity');

		    $vehicleData = array(
                'vehicle_type' =>$this->input->post('vehicle_type'),
                'vehicle_brand' =>$this->input->post('vehicle_brand'),
                'vehicle_model' =>$this->input->post('vehicle_model'),
                'vehicle_transmission' =>$this->input->post('vehicle_transmission'),
                'fuel_type' =>$this->input->post('fuel_type'),
                'year_of_manufacture' =>$this->input->post('year_of_manufacture'),
                'year_of_registration' =>$this->input->post('year_of_registration'),
                'vehicle_number' =>$this->input->post('vehicle_number'),
                'number_of_owners' =>$this->input->post('number_of_owners'),
                'vehicle_engine_capacity' => $vehi_cap ,
            	'engine_capacity_unit' => $vehi_cap_unit,
			    'vehicle_mileage_range' =>$millage_num,
            	'millage_range_unit' => $millage_unit,
                'vehicle_total_value' =>$this->input->post('vehicle_total_value'),
                'vehicle_seating_capacity' =>$this->input->post('vehicle_seating_capacity'),
            	'average_of_fuel_consuption' =>  $fuel_cons,
            	'avarage_fuel_consumption_unit' => $fuel_con_unit,
                'vehicle_features'=>implode(',', $this->input->post('vehicle_features')),
                'additional_features' =>$this->input->post('additional_features'),
                'vehicle_status' =>$this->input->post('vehicle_status'),
                'vehicle_minimum_downpayment' =>$this->input->post('vehicle_minimum_downpayment'),
                'publish_status' => 1,
                'admin_customer' => 1,
                'sellers_description' => $this->input->post('sellers_description'),
                'inside_360_images' => $this->setting_model->upload('front_image', 'upload/selling/Front/'),
                'warranty' => $this->input->post('warranty'),
                'autosure_certified' => $this->input->post('autosure_certified')
			);

		$insert_id = $this->setting_model->insert($vehicleData, 'vehicle');

		$files = $_FILES;
		$path = "selling";
		$fileName = $this->multiple_image_uplaod->multi_image_upload($files,$path);
		$this->multiple_image_uplaod->upload_image_selling_vehicle($fileName,$insert_id);
		//print_r($fileName);
		// $this->multiple_image_uplaod->upload_image_outside($this->input->post(),$fileName);
			
		$this->session->set_flashdata('success', ' New Vehicle Added Successfully!');	
            redirect('selling/addvehicle');
	}
	
	 private function _response(array $data = array(), $code = 200) {
 $this->output
 ->set_status_header($code)
 ->set_content_type('application/json')
 ->set_output(json_encode($data));
}

 public function senduserverification(){
		$id = $this->uri->segment(3);
		$flashdata = $this->uri->segment(5);
		$useradd=$this->setting_model->Get_Single('login','user_id',$id);
		foreach ($useradd as $key ) {
			$user_name = $key['user_name'];
			$user_mail = $key['email'];
			$user_password = $this->uri->segment(4);
			//$user_type = $key['user_type'];
  		}
		$admin_name = $this->session->userdata('username');
		$address = $this->config->base_url();
		$link = '<a href ='.$address.'login_user/verifynewuser/?email='.$user_mail.'>here</a>'; //link for user verification

       $this->_send_contact_email( $id, $user_name,$user_password, $user_mail, $link);
        
         $this->session->set_flashdata('success', 'registration successfull.. check your email verification.');
         redirect('home');
          
		}




private function _send_contact_email($id , $user_name, $user_password, $user_mail,$link){


    $log_path =  base_url('/assets/brand-logo.png');
    
    
    
    $message = "";
    
    $message .= "<p style=\"font-size: 1.2em; padding: 10px; margin-bottom: 0;\">* This is an auto genarated e-mail for a request of a live web use.</p>";
    $message .= "<table style=\"width: 60%; min-width: 500px;\">";
    $message .= "<tbody style=\"border: 1px solid #a8a8a8;\">";
    $message .= "<tr style=\"background-color: white;\">";
    $message .= "<td style=\"padding: 15px; text-align: center;\" colspan=2>";
    $message .= "<img style=\"padding: 10px; height: auto; width: 30%;\" src=\"$log_path\"/>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">User Name:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$user_name</p>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">User Password:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$user_password</p>";
    $message .= "</td>";
    $message .= "</tr>";
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">User email:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$user_mail</p>";
    $message .= "</td>";
    $message .= "</tr>";   
    
    $message .= "<tr style=\"\">";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Verificatrion Link:</p>";
    $message .= "</td>";
    $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
    $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$link</p>";
    $message .= "</td>";
    $message .= "</tr>"; 
    
   
    
    

    

    
    
    
    
    $message .= "</tbody>";
    $message .= "</table>";
    
    
    
    $emailReceivers = array(
        $user_mail
    );
    
    foreach( $emailReceivers as $emailReceiver) {
        
        $from = 'Autosure';
        $to = $emailReceiver;
        $subject = "Verification Mail ";
        
        $config = array();
        $config["protocol"] = 'smpt';
        $config['smtp_host'] = 'bckonnect.com';
        $config["smtp_port"] = 465;
        $config["smtp_user"] = 'publicuser@bckonnect.com';
        $config["smtp_pass"] = 'PaSS@bckonnect.com';
        $config['mailtype'] = 'html';
        $this->email->initialize($config);

        $this->email->from($from, 'Verification Email');
        $this->email->to($to);              
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->send();                    
    }            
    
    return true;
    redirect('home');
    
}
		






    
	
	 public function verifynewuser(){
		$email = $this->input->get('email');
		$user_detail = $this->setting_model->Get_Single('login','email',$email);
		
		if(!empty($user_detail)){
			
			foreach($user_detail as $data){
				$status = $data['verification_status'];
				$name = $data['user_name'];
				
			}
			
			if($status==0){
			
			
				$Data = array(
					'verification_status'=>1
				);
				$this->setting_model->update($Data,'login','email',$email);
				$message = '<div class="alert alert-success">Hi '.$name.',<br/> Your user account is successfully verified !</div>';
				$this->session->set_flashdata('msg',$message);
				$this->load->view('verification');
				
			}
			
			else{
				$message = '<div class="alert alert-danger">Hi '.$name.',<br/> Your user account is already verified !</div>';
				$this->session->set_flashdata('msg',$message);
				$this->load->view('verification');
			
			}
		}
		
		else{
			$this->session->set_flashdata('msg','<div class="alert alert-danger">Something wrong! Please try again</div>');
			$this->load->view('verification');
		}
	}


}	