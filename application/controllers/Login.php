<?php

class Login extends CI_Controller {

    public function index($msg = NULL) {
        // Load our view to be displayed
        // to the user
        $data['msg'] = $msg;
        $this->load->view('admin/login',$data);
    }
  //yasas vidanage
    public function process() {

        if ($_POST) {

            $data = array(
            'email' => $this->input->post('email'),
            'password' => $this->input->post('password')
            );

            $result = $this->login_model->is_user_exist($data);
           // var_dump($result);
            if (!empty($result)) {


                $verification = $result->verification_status;

                if($verification==1){

                    $data = array(
                        'id' => $result->user_id,
                        'username' => $result->user_name,
                        'usertype' => $result->user_type, // 1-admin, 2-editor, 3-feader
                        'usernic' => $result->nic_no,
                        'usermobile' => $result->contact_number,
                        'email'=>$result->email,
                        'validated' => true
                    );

                    $this->session->set_userdata($data);

                    redirect('Admin');
                }

                else{
                   $this->session->set_flashdata('msg','<div class="alert alert-danger">Your username & password is not verified<br/>Please check your email for verification mail</div>');
                    $this->load->view('admin/login'); 
                }
            } 

            else {
                $this->session->set_flashdata('msg','<div class="alert alert-danger">Username or password is incorrect!</div>');
                $this->load->view('admin/login');

            }
        }
    
    }

    public function logout() {

        $data = ['id', 'username', 'usertype', 'validated'];
        $this->session->unset_userdata($data);

        redirect('admin/login');
    }

}
