<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Renting extends CI_Controller {

	public function template($headerData, $sidebarData, $page, $mainData, $footerData) {
		$this->load->view('admin/components/header', $headerData);
		$this->load->view('admin/components/sidebar', $sidebarData);
		$this->load->view($page, $mainData);
		$this->load->view('admin/components/footer', $footerData);
	}

//start addtype function ------------------------- coded by Gihan 
	public function addtype() {
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/renting/addvehicletype';
		$mainData = array(
			'pagetitle' => 'Add New Vehicle Type',
		);

		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}
// end addtype function --------------------------

// start inserttype function -------------------------- coded by Gihan
	public function inserttype(){
		$data = array(
			'vehical_type_name' =>$this->input->post('vehicle_type_name'),
			);

		$data1 = $this->setting_model->insert($data,'renting_vehical_type');
		
		$this->session->set_flashdata('msg','<div class="alert alert-success">New Type Added Successfully!</div>');

		redirect('renting/addtype');
	}
//end inserttype function

//start viewtype function ------------------------------- coded by Gihan
	public function viewtype(){

		$headerData = null;
		$sidebarData = null;
		$page = 'admin/renting/viewtype';
		$mainData = array(
			'types' => $this->setting_model->Get_All('renting_vehical_type'),
		);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}
// end viewtype function

// start edittype function -- Ishara sewwandi

    public function editvehicletype(){

	    $id = $this->uri->segment(3);
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/renting/editvehicletype';
        $mainData = array(
            'types' => $this->setting_model->Get_Single('renting_vehical_type','vehical_type_id',$id),
        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }

    //update vehicle type-- Ishara sewwandi

    public function updatetype(){
        $id = $this->uri->segment(3);
        $data = array(
            'vehical_type_name' =>$this->input->post('vehicle_type_name'),
        );

        $data1 = $this->setting_model->update($data,'renting_vehical_type','vehical_type_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-success">Vehicle Type Update Successfully!</div>');

        redirect('renting/editvehicletype/'.$id);
    }
    //end update

// start addbrand function ---------------------------------- coded by Gihan
	public function addbrand() {
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/renting/addvehiclebrand';
		$mainData = array(
			'pagetitle' => 'Add New Vehicle Brand',
			'type' => $this->setting_model->Get_All('renting_vehical_type')
		);

		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}
// end addbrand function

// start insertbrand function -------------------------------- coded by Gihan
	public function insertbrand(){
		$data = array(
			'vehical_brand_name' =>$this->input->post('vehicle_brand_name'),
			'vehical_type_id' => $this->input->post('vehicle_type')
			);

		$data1 = $this->setting_model->insert($data,'renting_vehical_brand');
		
		$this->session->set_flashdata('msg','<div class="alert alert-success">New Brand Added Successfully!</div>');

		redirect('renting/addbrand');
	}
// end insertbrand function

// start viewbrand function --------------------------------- coded by Gihan
	public function viewbrand(){

		$headerData = null;
		$sidebarData = null;
		$page = 'admin/renting/viewbrand';
		$mainData = array(
		    'Types' => $this->setting_model->Get_All('renting_vehical_type'),
			'Brands' => $this->setting_model->Get_All('renting_vehical_brand')
		);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}
// end viewbrand function

//edit vehicle brand -- ishara sewwandi

    public function editvehiclebrand(){

	    $id = $this->uri->segment(3);
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/renting/editvehiclebrand';
        $mainData = array(
            'Brands' => $this->setting_model->Get_Single('renting_vehical_brand','vehical_brand_id',$id),
		    'Types' => $this->setting_model->Get_All('renting_vehical_type'),
        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }

    //end edit

    //start update brand
    public function updatebrand(){
        $id = $this->uri->segment(3);
        $data = array(
            'vehical_brand_name' =>$this->input->post('vehicle_brand_name'),
            'vehical_type_id' => $this->input->post('vehicle_type_edit')
        );

        $data1 = $this->setting_model->update($data,'renting_vehical_brand','vehical_brand_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-success">Update Brand Successfully!</div>');

        redirect('renting/editvehiclebrand/'.$id);
    }

// start function addmodal ------------------------------------ coded by Gihan
	public function addmodal() {
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/renting/addvehicalmodal';
		$mainData = array(
			'pagetitle' => 'Add New Vehicle Brand',
			'Brands' => $this->setting_model->Get_All('renting_vehical_brand'),
		    'Types' => $this->setting_model->Get_All('renting_vehical_type')
		);

		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}
// end function addmodal


// start function insertmodal ----------------------------------- coded by Gihan
	public function insertmodal(){
		$data = array(
			'vehical_brand_id' => $this->input->post('vehicle_rent_brand'),
			'vehical_model_name' => $this->input->post('vehicle_modal_name'),
			'vehical_type_id' => $this->input->post('vehicle_rent_type')
			);

		$data1 = $this->setting_model->insert($data,'renting_vehical_model');
		
		$this->session->set_flashdata('msg','<div class="alert alert-success">New Modal Added Successfully!</div>');

		redirect('renting/addmodal');
	}
// end function insertmodal


// start function viewmodal --------------------------------------- coded by Gihan
	public function viewmodal(){

		$headerData = null;
		$sidebarData = null;
		$page = 'admin/renting/viewmodal';
		$mainData = array(
			'brands' => $this->setting_model->Get_All('renting_vehical_brand'),
			'modals' => $this->setting_model->Get_All('renting_vehical_model'),
		    'Types' => $this->setting_model->Get_All('renting_vehical_type')
		);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}
// end viewmodal function

//edit vehicle model -- ishara sewwandi
    public function editvehiclemodel(){

	    $id = $this->uri->segment(3);
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/renting/editvehiclemodel';
        $mainData = array(
            
		    'Types' => $this->setting_model->Get_All('renting_vehical_type'),
            'brands' => $this->setting_model->Get_All('renting_vehical_brand'),
            'modals' => $this->setting_model->Get_Single('renting_vehical_model','vehical_model_id',$id),
        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }

    //end edit

    //start vehicle modal update

    public function updatemodal(){
        $id = $this->uri->segment(3);
        $data = array(
            'vehical_type_id' => $this->input->post('vehicle_type_rent_edit'),
            'vehical_model_name' => $this->input->post('vehicle_modal_name'),
            'vehical_brand_id' => $this->input->post('vehicle_brand_rent_edit')
        );

        $data1 = $this->setting_model->update($data,'renting_vehical_model','vehical_model_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-success">Update Modal Successfully!</div>');

        redirect('renting/editvehiclemodel/'.$id);
    }

//end update

//delete entire model -----------------------ishara sewwandi

    public function deletemodel(){
        //$service_id = $this->uri->segment(3);
        $model_id = $this->input->post('id');
        $this->setting_model->delete('renting_vehical_model','vehical_model_id',$model_id);
        redirect('renting/viewmodal');
    }


// start addnewvehicle function ---------------------------------- coded by Gihan
	public function addnewvehicle() {
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/renting/addnewvehicle';
		$mainData = array(
			'pagetitle' => 'Add New Vehicle Brand',
			'types' => $this->setting_model->Get_All('renting_vehical_type'),
			'Brands' => $this->setting_model->Get_All('renting_vehical_brand'),
			'modals' => $this->setting_model->Get_All('renting_vehical_model'),
		);

		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}
// end addnewvehicle function


// start insert vehicle function --------------------------- coded by Gihan
	public function insertvehicle(){
		$millage_num = $this->input->post('vehicle_mileage_range');
	        $millage_unit = $this->input->post('vehicle_range');
	
	        $fuel_cons = $this->input->post('average_of_fuel_consuption');
	        $fuel_con_unit = $this->input->post('letersPer');
	
	        $vehi_cap = $this->input->post('vehicle_engine_capacity');
	        $vehi_cap_unit = $this->input->post('capacity');

		$data = array(
			'vehical_vehical_type_id' =>$this->input->post('vehicle_type'),
			'vehical_vehical_brand_id' =>$this->input->post('vehical_brand'),
			'vehical_vehical_model_id' =>$this->input->post('vehicle_model'),
			'transmission' =>$this->input->post('vehicle_transmission'),
			'vehical_number' =>$this->input->post('vehical_number'),
			'engine_capacity' =>$this->input->post('vehicle_engine_capacity'),
			'engine_capacity_unit' => $vehi_cap_unit,
			'seating_capacity' =>$this->input->post('vehicle_seating_capacity'),
			'year_of_manufactured' =>$this->input->post('year_of_manufacture'),
			'year_of_registration' =>$this->input->post('year_of_registration'),
			'avarage_fuel_consumption' =>$this->input->post('average_of_fuel_consuption'),
			'avarage_fuel_consumption_unit' => $fuel_con_unit,
			'fuel_type' =>$this->input->post('fuel_type'),
			'vehicle_features'=>implode(',', $this->input->post('vehicle_features')),
            		'additional_features' =>$this->input->post('additional_features'),
			'millage_range' =>$this->input->post('vehicle_mileage_range'),
			'millage_range_unit' => $millage_unit,
			'renting_vehical_inside_image' =>$this->setting_model->upload('inside_images', 'upload/renting/inside'),
			'discount' =>$this->input->post('discount'),
			'per_day_price' =>$this->input->post('per_day_price'),
			'owned_by' =>$this->input->post('checkbox'),
			'name' =>$this->input->post('name'),
			'address' =>$this->input->post('adress'),
			'phone_number' =>$this->input->post('phone_number'),
			'email' =>$this->input->post('Email'),
			);
		
		$insert_id = $this->setting_model->insert($data, 'renting_vehical');
		$files = $_FILES;
		$path = "renting/outside";
		$fileName = $this->multiple_image_uplaod->multi_image_upload($files,$path);
		$this->multiple_image_uplaod->upload_renting_vehicle_image($fileName,$insert_id);
		// print_r($fileName);
		// $this->multiple_image_uplaod->upload_image_outside($this->input->post(),$fileName);
			$this->session->set_flashdata('msg','<div class="alert alert-success">New Vehicle Added Successfully!</div>');
			redirect('renting/addnewvehicle');
	}

	//start view rentigvehicle -- Ishara Sewwandi
	 public function viewvehicle(){

	 	$headerData = null;
	 	$sidebarData = null;
	 	$page = 'admin/renting/viewvehicle';
	 	$mainData = array(
//            'vehicle' => $this->setting_model->Get_All('vehicle'),
            'insideimage' => $this->setting_model->Get_All('inside_images'),
            'outside' => $this->setting_model->Get_All('renting_outside_images'),
            'vehicle' => $this->setting_model->rentvehiclejoin1(),
	 		// 'outside_images'=> $this->setting_model->Get_All('import_outside_images'),
	 	);
	 	$footerData = null;

	 	$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	 }

	 // end view rentingvehicle

    //=======edit vehicle=== Ishara Sewwandi
    public function editvehicle() {
        $id = $this->uri->segment(3);

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/renting/editvehicle';

        $config = array(
            'wheres' => array(
                'renting_vehical' => array('renting_vehical_id', $id)
            ),
            'from' => array('renting_vehical'),
            'joins' => array(
                'renting_vehical_brand' => array('renting_vehical_brand.vehical_brand_id = renting_vehical.vehical_vehical_brand_id', 'left'),
                'renting_vehical_type' => array('renting_vehical_type.vehical_type_id = renting_vehical.vehical_vehical_type_id', 'left'),
                'renting_vehical_model' => array('renting_vehical_model.vehical_model_id = renting_vehical.vehical_vehical_model_id', 'left')
            )
        );

        $mainData = array(
            'renting'=> $this->setting_model->rentvehiclejoin($id),
            'vehicle' => $this->setting_model->Get_Single('renting_vehical','renting_vehical_id',$id),
            'vehicle_images' => $this->multiple_image_uplaod->edit_data_image_vehicle($id),
            'types'=> $this->setting_model->Get_All('renting_vehical_type'),
            'brands'=> $this->setting_model->Get_All('renting_vehical_brand'),
            'modals'=> $this->setting_model->Get_All('renting_vehical_model'),
        );

        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);

    }
//edit dining end

//================update vehicle function start---------------- ishara sewwandi
    public function updatevehicle() {

        $id = $this->uri->segment(3);

        //============inside image upload start===
        if(isset($_FILES['fileinput']) && empty($_FILES['fileinput']['tmp_name'])) {
            $image = $this->input->post('inside_images');
        } else {
            $image = $this->setting_model->upload('fileinput', 'upload/renting/inside');
        }
        //image upload end


        $owned_by = $this->input->post('checkbox');


            if($owned_by == 0){
            $owned_by = 0;
            $name = null;
            $address = null;
            $phone_number = null;
            $email = null;

            }else{
                $owned_by = $this->input->post('checkbox');
                $name = $this->input->post('name');
                $address = $this->input->post('adress');
                $phone_number = $this->input->post('phone_number');
                $email = $this->input->post('Email');
            };


        $data = array(
            'renting_vehical_inside_image' =>$image,
            'vehical_vehical_type_id' =>$this->input->post('vehicle_type'),
            'vehical_vehical_brand_id' =>$this->input->post('vehicle_brand'),
            'vehical_vehical_model_id' =>$this->input->post('vehicle_model'),
            'transmission' =>$this->input->post('vehicle_transmission'),
            'vehical_number' =>$this->input->post('vehical_number'),
            'engine_capacity' =>$this->input->post('vehicle_engine_capacity'),
            'seating_capacity' =>$this->input->post('vehicle_seating_capacity'),
            'year_of_manufactured' =>$this->input->post('year_of_manufacture'),
            'year_of_registration' =>$this->input->post('year_of_registration'),
            'avarage_fuel_consumption' =>$this->input->post('average_of_fuel_consuption'),
            'fuel_type' =>$this->input->post('fuel_type'),
            'vehicle_features'=>implode(',', $this->input->post('vehicle_features')),
	'additional_features' =>$this->input->post('additional_features'),
            'millage_range' =>$this->input->post('vehicle_mileage_range'),
            'discount' =>$this->input->post('discount'),
            'per_day_price' =>$this->input->post('per_day_price'),
            'owned_by' =>$owned_by,
            'name' =>$name,
            'address' =>$address,
            'phone_number' =>$phone_number,
            'email' =>$email,
        );

        $data1 = $this->setting_model->update($data,'renting_vehical','renting_vehical_id',$id);

        //edit multiple image upload
         if(count($_FILES['userfile']['name'])>1){
        $files = $_FILES;
        $path = "renting/outside";
        $fileName = $this->multiple_image_uplaod->multi_image_upload($files,$path);
        $this->multiple_image_uplaod->edit_upload_image_vehicle($fileName,$id);
        //end multiple image upload edit
}
        $this->session->set_flashdata('msg','<div class="alert alert-success">New Car Added Successfully!</div>');

        redirect('renting/viewvehicle/'.$id);
    }
    //update vehicle function end

    //delete edi trent image vehicle -- Ishara sewwandi
    public function deleteimagerent(){
        $deleteid  = $this->input->post('id');
        $this->db->delete('renting_outside_images', array('renting_image_id' => $deleteid));
        $verify = $this->db->affected_rows();
        echo $verify;
    }

    public function deleterent(){
        //$service_id = $this->uri->segment(3);
        $rent_id = $this->input->post('id');
        $limit_vehicle=count($this->setting_model->Get_Single('renting_vehical','renting_vehical_id',$rent_id));
        $this->setting_model->delete_data('renting_vehical','renting_vehical_id',$rent_id,$limit_vehicle);
        $this->setting_model->delete_data('renting_outside_images','renting_vehical_id',$rent_id,$limit_vehicle);
        redirect('renting/viewvehicle');
    }
    //end delete

// start function for view renting inqiury ------------------------ coded by Gihan
    public function viewInquiry(){

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/renting/viewinquiry';
        $mainData = array(

            'vehicles' => $this->setting_model->Get_All('renting_vehical'),
            'customer' => $this->setting_model->Get_All('customer'),
            'types' => $this->setting_model->Get_All('renting_vehical_type'),
            'brands' => $this->setting_model->Get_All('renting_vehical_brand'),
            'model' => $this->setting_model->Get_All('renting_vehical_model'),
            // 'inside_images' => $this->setting_model->Get_All('inside_images'),
            'outside_images' => $this->setting_model->Get_All('renting_outside_images'),
            'inquiry' => $this->setting_model->Get_All('renting_inquiry'),
            // 'inquiry_join' => $this->setting_model->inquiryjoin()

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
// ---------------------------------------------------------------------------


// start function for followup renting inquiry --------------------- coded by Gihan

    public function followUp(){

        $id = $this->input->post("inq_id");
        // print_r($id);
        $date = date('Y-m-d');
        $data = array(
            'status' => 1,
            'status_change_date' => $date,
            'followUp_description' =>$this->input->post('Follow_up_description'),
            'next_followup_date' =>$this->input->post('next_date'),
        );

        $data1 = $this->setting_model->update($data,'renting_inquiry','renting_inquiry_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-warning">Follow Up Successfully!</div>');

        redirect('renting/viewinquiry');
    }
// end function --------------------------------------------------------------------


// start function for confirm renting inquiry ------------------ coded by Gihan
    public function confirm(){

        $id = $this->uri->segment(3);
        $date = date('Y-m-d');
        $data = array(
            'status' => 2,
            'status_change_date' => $date,
        );

        $data1 = $this->setting_model->update($data,'renting_inquiry','renting_inquiry_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-success">Inquiry Confirm Successful!</div>');

        redirect('renting/viewinquiry');
    }
// end function ------------------------------------------------------------------


// start function for reject renting inquiry -------------------- coded by Gihan

    public function reject(){

        $id = $this->input->post('inq_id');
        $date = date('Y-m-d');
        $data = array(
            'status' => 3,
            'reject_reason' => $this->input->post('reject_reason'),
            'status_change_date' => $date,
        );

        $data1 = $this->setting_model->update($data,'renting_inquiry','renting_inquiry_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-danger"> Reject Successful!</div>');

        redirect('renting/viewinquiry');
    }
// end function---------------------------------------------------------------------


// start function for view confirmed inquiries ---------------------- coded by Gihan

    public function viewConfirm(){

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/renting/viewConfirm';
        $mainData = array(

            'vehicles' => $this->setting_model->Get_All('renting_vehical'),
            'customer' => $this->setting_model->Get_All('customer'),
            'types' => $this->setting_model->Get_All('renting_vehical_type'),
            'brands' => $this->setting_model->Get_All('renting_vehical_brand'),
            'model' => $this->setting_model->Get_All('renting_vehical_model'),
            'outside_images' => $this->setting_model->Get_All('renting_outside_images'),
            'inquiry' => $this->setting_model->Get_All('renting_inquiry'),

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
// end function -----------------------------------------------------------------------


// start function for view rejected inquiries ------------------------- coded by Gihan

    public function viewRejected(){

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/renting/viewRejected';
        $mainData = array(

            'vehicles' => $this->setting_model->Get_All('renting_vehical'),
            'customer' => $this->setting_model->Get_All('customer'),
            'types' => $this->setting_model->Get_All('renting_vehical_type'),
            'brands' => $this->setting_model->Get_All('renting_vehical_brand'),
            'model' => $this->setting_model->Get_All('renting_vehical_model'),
            'outside_images' => $this->setting_model->Get_All('renting_outside_images'),
            'inquiry' => $this->setting_model->Get_All('renting_inquiry'),

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
// end function -----------------------------------------------------------------------


// start function for cofirm rejected inquiries ------------------- coded by Gihan

    public function confirm1(){

        $id = $this->uri->segment(3);
        $date = date('Y-m-d');
        $data = array(
            'status' => 2,
            'status_change_date' => $date,
        );

        $data1 = $this->setting_model->update($data,'renting_inquiry','renting_inquiry_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-success">Confirm Successful!</div>');

        redirect('renting/viewRejected');
    }
// end function ----------------------------------------------------------------------


// start function for followup rejected inqiuries ----------------- coded by Gihan

    public function followUp2(){
        $id = $this->input->post("inq_id");
        $date = date('Y-m-d');
        $data = array(
            'status' => 1,
            'status_change_date' => $date,
            'followUp_description' =>$this->input->post('Follow_up_description'),
            'next_followup_date' =>$this->input->post('next_date'),
        );

        $data1 = $this->setting_model->update($data,'renting_inquiry','renting_inquiry_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-warning">Follow up Successful!</div>');

        redirect('renting/viewRejected');
    }
// end function ----------------------------------------------------------------------


// start function for update follow up date -------------------- coded by Gihan

    public function updateDate(){
        // print_r($inq_id);
        $id = $this->uri->segment(3);
        $date = $this->input->post('changed_date');
        $data = array(
            'next_followup_date' => $this->input->post('changed_date')
        );

        $data1 = $this->setting_model->update($data,'renting_inquiry','renting_inquiry_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-success">Update date Successful!</div>');

        redirect('renting/viewinquiry');
    }
// end function ------------------------------------------------------------------------


// start function for book a vehicle process -------------------- coded by Gihan

    public function bookvehicle(){

        $id = $this->input->post('inq_id');

        $date = date('Y-m-d');

        $inq_table = $this->setting_model->Get_All_Where('renting_inquiry','renting_inquiry_id',$id);

        foreach($inq_table as $data){
            $v_id = $data['renting_renting_vehical_id'];
            $cus_name = $data['customer_name'];
            $cus_email = $data['email_address'];
            $phone = $data['phone_number'];
            $nic_number = $data['NIC_number'];
        }

            $data5 = array(
                'customer_name' => $cus_name,
                'email' => $cus_email,
                'phone_number' => $phone,
                'NIC_number' => $nic_number
            );
            
            $insert_id = $this->setting_model->insert($data5,'customer');

        $data1 = array(

            'renting_renting_vehical_id' => $v_id,
            'customer_customer_id' => $insert_id,
            'status_changed_date' => $date,
            'from_date' => $this->input->post('from_date'),
            'to_date' => $this->input->post('to_date'),
            'per_day_pric' => $this->input->post('view_perday_price'),
            'total' => $this->input->post('vehicle_total'),
            'deposit' => $this->input->post('diposit_amount'),
            'duration' => $this->input->post('duration'),

        );

        $data11 = array(
            'status' => 1
        );

        $data0 = $this->setting_model->update($data11,'renting_vehical','renting_vehical_id',$v_id);

        $data2 = $this->setting_model->insert($data1,'renting_booking');

        $data3 = $this->setting_model->delete('renting_inquiry','renting_inquiry_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-success">Booking Successful!</div>');

        redirect('renting/viewConfirm');

    }
// end function--------------------------------------------------------------------
// ===================================================================================

// start function for view bookings ----------------- coded by Gihan

    public function viewBooking(){

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/renting/viewbooking';
        $mainData = array(

            'vehicles' => $this->setting_model->Get_All('renting_vehical'),
            'customer' => $this->setting_model->Get_All('customer'),
            'types' => $this->setting_model->Get_All('renting_vehical_type'),
            'brands' => $this->setting_model->Get_All('renting_vehical_brand'),
            'model' => $this->setting_model->Get_All('renting_vehical_model'),
            // 'inside_images' => $this->setting_model->Get_All('inside_images'),
            'outside_images' => $this->setting_model->Get_All('renting_outside_images'),
            'booking' => $this->setting_model->Get_All('renting_booking'),

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
// end function -------------------------------------------------------------------

// Start function for booking Follow up ------------------- coded by Gihan

    public function BookingfollowUp(){
        // print_r($inq_id);
        $id = $this->input->post("book_id");
        // print_r($id);
        $date = date('Y-m-d');
        $data = array(
            'status' => 1,
            'status_changed_date' => $date,
            'followUp_description' =>$this->input->post('Follow_up_description'),
            'next_followup_date' =>$this->input->post('next_date'),
        );

        $data1 = $this->setting_model->update($data,'renting_booking','booking_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-warning">Follow up Successfully!</div>');

        redirect('renting/viewBooking');
    }
// End function ---------------------------------------------------------------------

// Start function for payment collected bookings ------------------- coded by Gihan

    public function Bookingconfirm(){

        $id = $this->uri->segment(3);
        $date = date('Y-m-d');
        $data = array(
            'status' => 2,
            'status_changed_date' => $date,
        );

        $data1 = $this->setting_model->update($data,'renting_booking','booking_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-success">Confirm Successfully!</div>');

        redirect('renting/viewBooking');
    }
// End function ----------------------------------------------------------------------

// start function for view payment collected bookings ------------ coded by Gihan

    public function viewConfirmBookings(){

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/renting/viewconfirmedBookings';
        $mainData = array(

            'vehicles' => $this->setting_model->Get_All('renting_vehical'),
            'customer' => $this->setting_model->Get_All('customer'),
            'types' => $this->setting_model->Get_All('renting_vehical_type'),
            'brands' => $this->setting_model->Get_All('renting_vehical_brand'),
            'model' => $this->setting_model->Get_All('renting_vehical_model'),
            // 'inside_images' => $this->setting_model->Get_All('inside_images'),
            'outside_images' => $this->setting_model->Get_All('renting_outside_images'),
            'booking' => $this->setting_model->Get_All('renting_booking')

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
// End function --------------------------------------------------------------------

// Start Function for reject bookings ----------------------------- coded by Gihan

    public function rejectBooking(){
        // print_r($inq_id);
        $id = $this->input->post('rej_id');
        print_r($id);
        $date = date('Y-m-d');
        $data = array(
            'status' => 3,
            'reject_reason' => $this->input->post('reject_reason1'),
            'status_changed_date' => $date,
        );

        $data1 = $this->setting_model->update($data,'renting_booking','booking_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-danger">Reject Successful!</div>');

        redirect('renting/viewBooking');
    }
// End Function ------------------------------------------------------------------------

// Start function for view rejected bookings --------------------- coded by Gihan

    public function viewrejectBookings(){

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/renting/viewrejectedbooking';
        $mainData = array(

            'vehicles' => $this->setting_model->Get_All('renting_vehical'),
            'customer' => $this->setting_model->Get_All('customer'),
            'types' => $this->setting_model->Get_All('renting_vehical_type'),
            'brands' => $this->setting_model->Get_All('renting_vehical_brand'),
            'model' => $this->setting_model->Get_All('renting_vehical_model'),
            'outside_images' => $this->setting_model->Get_All('renting_outside_images'),
            'booking' => $this->setting_model->Get_All('renting_booking'),

        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }

// End function --------------------------------------------------------------------


// Start function for payment collected on rejected bookings ------------- coded by Gihan

    public function paymentcollected1(){

        $id = $this->uri->segment(3);
        $date = date('Y-m-d');
        $data = array(
            'status' => 2,
            'status_changed_date' => $date,
        );

        $data1 = $this->setting_model->update($data,'renting_booking','booking_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-success">Confirm Successful!</div>');

        redirect('renting/viewrejectBookings');
    }

// End function ------------------------------------------------------------------------


// start function for delete rejected bookings ---------------------- coded by Gihan
    public function deleteRejected(){
        $id = $this->uri->segment(3);

        $data3 = $this->setting_model->delete('renting_booking','booking_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-danger">Record Deleted!</div>');

        redirect('renting/viewrejectBookings');
    }
// end function ------------------------------------------------------------------------

// start function for update follow up date -------------------- coded by Gihan

    public function updateDate1(){
        // print_r($inq_id);
        $id = $this->uri->segment(3);
        $date = $this->input->post('changed_date');
        $data = array(
            'next_followup_date' => $this->input->post('changed_date')
        );

        $data1 = $this->setting_model->update($data,'renting_booking','booking_id',$id);

        $this->session->set_flashdata('msg','<div class="alert alert-success">Update date Successful!</div>');

        redirect('renting/viewBooking');
    }
// end function ------------------------------------------------------------------------

}
?>