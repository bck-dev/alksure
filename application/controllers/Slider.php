<?php
/**
 * Created by PhpStorm.
 * User: yaish
 * Date: 10/12/2018
 * Time: 9:45 AM
 */

class Slider extends CI_Controller
{
    function __construct(){
        parent::__construct();
        $this->check_isvalidated();

    }

    public function template($headerData, $sidebarData, $page, $mainData, $footerData) {
        $this->load->view('admin/components/header', $headerData);
        $this->load->view('admin/components/sidebar', $sidebarData);
        $this->load->view($page, $mainData);
        $this->load->view('admin/components/footer', $footerData);
    }

    //validate session start
    private function check_isvalidated(){
        if(!$this->session->userdata('validated')){
            redirect('login');
        }
    }
    //validate session end

    //add slider function start --ishara sewwandi
    public function addslider()
    {
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/slider/addslider';
        $mainData = array(
            'pagetitle' => 'Add New Slider Images',

        );

        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }

    //insert slider to database for 'slider' ---ishara sewwandi
    public function insertslider()
    {
        $data = array(
            'image' =>$this->setting_model->upload('slider_image', 'upload/slider'),

        );

        $insert_id = $this->setting_model->insert($data, 'slider');


        $this->session->set_flashdata('msg', '<div class="alert alert-success">New Slider Added Successfully!</div>');

        redirect('slider/addslider');
    }

    //View slider function start-- ishara sewwandi
    public function viewslider()
    {

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/slider/viewslider';
        $mainData = array(
            'slider' => $this->setting_model->Get_All('slider'),


        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }

    //edit agent -- Ishara sewwandi

    public function editslider()
    {

        $id = $this->uri->segment(3);
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/slider/editslider';
        $mainData = array(
            'slider' => $this->setting_model->Get_Single('slider','slider_id',$id),


        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }

    public function updateslider()
    {

        $id = $this->uri->segment(3);

        if(isset($_FILES['fileinput']) && empty($_FILES['fileinput']['tmp_name'])) {
            $image = $this->input->post('slider_image');
        } else {
            $image = $this->setting_model->upload('fileinput', 'upload/slider');
        }
        $data = array(
            'image' =>$image,

        );

        $insert_id = $this->setting_model->update($data, 'slider','slider_id',$id);


        $this->session->set_flashdata('msg', '<div class="alert alert-success">Update Slider Successfully!</div>');

        redirect('Slider/editslider/'.$id);
    }

}