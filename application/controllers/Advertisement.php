<?php

class Advertisement extends CI_Controller
{
    function __construct(){
        parent::__construct();
        $this->check_isvalidated();

    }

    public function template($headerData, $sidebarData, $page, $mainData, $footerData) {
        $this->load->view('admin/components/header', $headerData);
        $this->load->view('admin/components/sidebar', $sidebarData);
        $this->load->view($page, $mainData);
        $this->load->view('admin/components/footer', $footerData);
    }

    //validate session start
    private function check_isvalidated(){
        if(!$this->session->userdata('validated')){
            redirect('login');
        }
    }
    //validate session end


    //View advertisement function start-- charith
    public function viewadvertisement()
    {

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/advertisement/viewadvertisement';
        $mainData = array(
            'advertisement' => $this->setting_model->Get_All('advertisement'),


        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }

    //edit advertisement -- charith

    public function editadvertisement()
    {

        $id = $this->uri->segment(3);
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/advertisement/editadvertisement';
        $mainData = array(
            'advertisement' => $this->setting_model->Get_Single('advertisement','ad_id',$id),


        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }

    public function updateadvertisement()
    {

        $id = $this->uri->segment(3);

        if(isset($_FILES['fileinput']) && empty($_FILES['fileinput']['tmp_name'])) {
            $image = $this->input->post('ad_image_name');
        } else {
            $image = $this->setting_model->upload('fileinput', 'upload/advertisement');
        }
        $data = array(
            'ad_image_name' =>$image,
            'link' => $this->input->post('link')
        );

        $insert_id = $this->setting_model->update($data, 'advertisement','ad_id',$id);


        $this->session->set_flashdata('msg', '<div class="alert alert-success">Update Slider Successfully!</div>');

        redirect('Advertisement/editadvertisement/'.$id);
    }

}