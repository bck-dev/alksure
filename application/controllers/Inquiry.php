<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inquiry extends CI_Controller {

	public function template($headerData, $sidebarData, $page, $mainData, $footerData) {
		$this->load->view('admin/components/header', $headerData);
		$this->load->view('admin/components/sidebar', $sidebarData);
		$this->load->view($page, $mainData);
		$this->load->view('admin/components/footer', $footerData);
	}

// start function for viewinquiry page ------- coded by Gihan
	public function viewInquiry(){

		$headerData = null;
		$sidebarData = null;
		$page = 'admin/inquiry/viewinquiry';
		$mainData = array(
			
			'vehicles' => $this->setting_model->Get_All('vehicle'),
			'customer' => $this->setting_model->Get_All('customer'),
			'types' => $this->setting_model->Get_All('vehicle_type'),
			'brands' => $this->setting_model->Get_All('vehicle_brand'),
			'model' => $this->setting_model->Get_All('vehicle_model'),
			'inside_images' => $this->setting_model->Get_All('inside_images'),
			'outside_images' => $this->setting_model->Get_All('out_side_images'),
			'inquiry' => $this->setting_model->Get_All('test_buying_inquiry'),
			'inquiry_join' => $this->setting_model->inquiryjoin()
			
		);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}
// End function for viewinquiry page

// start function for viewOffer page 
public function viewOffer(){

	$headerData = null;
	$sidebarData = null;
	$page = 'admin/offer/viewoffer';
	$mainData = array(
		
		'vehicles' => $this->setting_model->Get_All('vehicle'),
		'customer' => $this->setting_model->Get_All('customer'),
		'types' => $this->setting_model->Get_All('vehicle_type'),
		'brands' => $this->setting_model->Get_All('vehicle_brand'),
		'model' => $this->setting_model->Get_All('vehicle_model'),
		'inside_images' => $this->setting_model->Get_All('inside_images'),
		'outside_images' => $this->setting_model->Get_All('out_side_images'),
		'offer' => $this->setting_model->Get_All_Desc('customer_offer', 'offer_id'),
		'offer_join' => $this->setting_model->offerjoin()
		
	);
	$footerData = null;

	$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
}
// End function for viewOffer page

// Start function for follow up inquiry --------------- coded by Gihan

	public function followUp(){

		$id = $this->input->post("inq_id");
		// print_r($id);
		$date = date('Y-m-d');
		$data = array(
			'status' => 1,
			'status_change_date' => $date,
			'followUp_description' =>$this->input->post('Follow_up_description'),
			'next_followup_date' =>$this->input->post('next_date'),
			);

		$data1 = $this->setting_model->update($data,'test_buying_inquiry','inquiry_id',$id);

		$this->session->set_flashdata('msg','<div class="alert alert-warning">Follow Up Successfully!</div>');

		redirect('inquiry/viewinquiry');
	}
// end function for followup inquiry


// Start function for confirm inquiry --------- coded by Gihan

	public function confirm(){
		
		$id = $this->uri->segment(3);
		$date = date('Y-m-d');
		$data = array(
			'status' => 2,
			'status_change_date' => $date,
			);

		$data1 = $this->setting_model->update($data,'test_buying_inquiry','inquiry_id',$id);

		$this->session->set_flashdata('msg','<div class="alert alert-success">Inquiry Confirm Successful!</div>');

		redirect('inquiry/viewinquiry');
	}
// end function for confirm inquiry


// Start function for reject inquiry --------------- coded by Gihan 

	public function reject(){

		$id = $this->input->post('inq_id');
		$date = date('Y-m-d');
		$data = array(
			'status' => 3,
			'reject_reason' => $this->input->post('reject_reason'),
			'status_change_date' => $date,
			);

		$data1 = $this->setting_model->update($data,'test_buying_inquiry','inquiry_id',$id);

		$this->session->set_flashdata('msg','<div class="alert alert-danger"> Reject Successful!</div>');

		redirect('inquiry/viewinquiry');
	}
// End function for reject inquiry


// Start function for load viewConfirm page ----------- coded by Gihan

	public function viewConfirm(){

		$headerData = null;
		$sidebarData = null;
		$page = 'admin/inquiry/viewConfirm';
		$mainData = array(
			
			'vehicles' => $this->setting_model->Get_All('vehicle'),
			'customer' => $this->setting_model->Get_All('customer'),
			'types' => $this->setting_model->Get_All('vehicle_type'),
			'brands' => $this->setting_model->Get_All('vehicle_brand'),
			'model' => $this->setting_model->Get_All('vehicle_model'),
			'inside_images' => $this->setting_model->Get_All('inside_images'),
			'outside_images' => $this->setting_model->Get_All('out_side_images'),
			'inquiry' => $this->setting_model->Get_All('test_buying_inquiry'),
			'inquiry_join' => $this->setting_model->inquiryjoin()
			
		);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}
// End function for load viewConfirm page


// Start function for load viewRejected page ---------- coded by Gihan

	public function viewRejected(){

		$headerData = null;
		$sidebarData = null;
		$page = 'admin/inquiry/viewRejected';
		$mainData = array(
			
			'vehicles' => $this->setting_model->Get_All('vehicle'),
			'customer' => $this->setting_model->Get_All('customer'),
			'types' => $this->setting_model->Get_All('vehicle_type'),
			'brands' => $this->setting_model->Get_All('vehicle_brand'),
			'model' => $this->setting_model->Get_All('vehicle_model'),
			'inside_images' => $this->setting_model->Get_All('inside_images'),
			'outside_images' => $this->setting_model->Get_All('out_side_images'),
			'inquiry' => $this->setting_model->Get_All('test_buying_inquiry'),
			'inquiry_join' => $this->setting_model->inquiryjoin()
			
		);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}
// End function for load viewRejected page


// Start function for followup rejected inquiries ------ coded by Gihan

	public function followUp2(){
		$id = $this->input->post("inq_id");
		$date = date('Y-m-d');
		$data = array(
			'status' => 1,
			'status_change_date' => $date,
			'followUp_description' =>$this->input->post('Follow_up_description'),
			'next_followup_date' =>$this->input->post('next_date'),
			);

		$data1 = $this->setting_model->update($data,'test_buying_inquiry','inquiry_id',$id);

		$this->session->set_flashdata('msg','<div class="alert alert-warning">Follow up Successful!</div>');

		redirect('inquiry/viewRejected');
	}
// End function for followup rejected inquiries

// Start function for confirm rejected inquiries --------- coded by Gihan

	public function confirm1(){
		
		$id = $this->uri->segment(3);
		$date = date('Y-m-d');
		$data = array(
			'status' => 2,
			'status_change_date' => $date,
			);

		$data1 = $this->setting_model->update($data,'test_buying_inquiry','inquiry_id',$id);

		$this->session->set_flashdata('msg','<div class="alert alert-success">Confirm Successful!</div>');

		redirect('inquiry/viewRejected');
	}
// End function for confirm rejected inquiries


// Start function pending ------------ coded by Gihan

	public function pending(){
		$id = $this->uri->segment(3);

		$date = date('Y-m-d');
		$data = array(
			'status' => NULL,
			'status_change_date' => NULL,
			'followUp_description' => NULL,
			'next_followup_date' => NULL,
			);

		$data1 = $this->setting_model->update($data,'test_buying_inquiry','inquiry_id',$id);

		$this->session->set_flashdata('msg','<div class="alert alert-success">Pending Successful!</div>');

		redirect('inquiry/viewRejected');
	}
// End function pending


// Start function payment collected -------------- coded by Gihan

	public function paymentCollected(){
		$id = $this->input->post('inq_id');

		$date = date('Y-m-d');

		$inq_table = $this->setting_model->Get_All_Where('test_buying_inquiry','inquiry_id',$id);
			foreach($inq_table as $data){
				$v_id = $data['vehical_vehical_id'];
				$cus_name = $data['customer_name'];
				$cus_email = $data['email_address'];
				$phone = $data['phone_number'];
				$nic_number = $data['NIC_number'];
			}

			$data5 = array(
				'customer_name' => $cus_name,
				'email' => $cus_email,
				'phone_number' => $phone,
				'NIC_number' => $nic_number
			);
			
			$insert_id = $this->setting_model->insert($data5,'customer');

			$data1 = array(

				'vehical_vehical_id' => $v_id,
				'customer_customer_id' => $insert_id,
				'buying_method' => $this->input->post('checkbox'),
				'price' => $this->input->post('price'),
				'downpayment' => $this->input->post('downpayment'),
				'monthly_installment' => $this->input->post('monthly_ins'),
				'duration' => $this->input->post('dura'),
				'status' => 1,
				'status_changed_date' => $date
			);


			$data2 = $this->setting_model->insert($data1,'reservation');


			$data3 = $this->setting_model->delete('test_buying_inquiry','inquiry_id',$id);

		$this->session->set_flashdata('msg','<div class="alert alert-success">Reservation Successful!</div>');

		redirect('inquiry/viewConfirm');

	}
// end function payment collected 


// start function for change followup date ------------- coded by Gihan

	public function updateDate(){
		// print_r($inq_id);
		$id = $this->uri->segment(3);
		$date = $this->input->post('changed_date');
		$data = array(
			'next_followup_date' => $this->input->post('changed_date')
			);

		$data1 = $this->setting_model->update($data,'test_buying_inquiry','inquiry_id',$id);

		$this->session->set_flashdata('msg','<div class="alert alert-success">Update date Successful!</div>');

		redirect('inquiry/viewinquiry');
	}
// end of the function

}
?>