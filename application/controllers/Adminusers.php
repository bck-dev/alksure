<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminusers extends CI_Controller {

	public function template($headerData, $sidebarData, $page, $mainData, $footerData) {
		$this->load->view('admin/components/header', $headerData);
		$this->load->view('admin/components/sidebar', $sidebarData);
		$this->load->view($page, $mainData);
		$this->load->view('admin/components/footer', $footerData);
	}

	public function addnewadmin() {
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/adminuseres/addnewadmin';
		$mainData = array(
			'pagetitle' => 'Add New Admin',
			'adminlogin' => $this->setting_model->Get_All('login'),
			'admintypes' => $this->setting_model->Get_All('admin_types'),
		);

		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}

	public function insertadmin(){
		$data = array(
			'user_name' =>$this->input->post('username1'),
			'user_type' =>$this->input->post('admin_type1'),
			'email' =>$this->input->post('email'),
			'password' =>md5($this->input->post('password')),
			'verification_status'=>1
			);

		$id = $this->setting_model->insert($data,'login');

		// $data1 = $this->setting_model->update($data,'renting_inquiry','renting_inquiry_id',$id) 
		
		$this->session->set_flashdata('msg','<div class="alert alert-success">New Admin Added Successfully!</div>');

		redirect('adminusers/addnewadmin');
	}

	public function viewadmins() {
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/adminuseres/viewadmin';
		$mainData = array(
			'pagetitle' => 'View Admin',
			'admin' => $this->setting_model->adminjoin(),
			'adminlogin' => $this->setting_model->Get_All('login'),
			'admintypes' => $this->setting_model->Get_All('admin_types'),
		);

		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}

	public function editadmin() {

		$id = $this->uri->segment(3);

		$headerData = null;
		$sidebarData = null;
		$page = 'admin/adminuseres/editadmins';
		$mainData = array(
			'pagetitle' => 'View Admin',
			'admin' => $this->setting_model->adminjoin1($id),
			'adminlogin' => $this->setting_model->Get_All('login'),
			'admintypes' => $this->setting_model->Get_All('admin_types'),
		);

		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}

	public function updateadmin(){

		$id = $this->uri->segment(3);
		print_r($id);
		$data = array(
			'user_name' => $this->input->post('admin_name1'),
			'user_type' => $this->input->post('admin_type2'),
			'email' => $this->input->post('email1'),
		);
		print_r($data);
		$data1 = $this->setting_model->update($data,'login','user_id',$id);

		$this->session->set_flashdata('msg','<div class="alert alert-success">Updated Successfully!</div>');

		redirect('adminusers/viewadmins');
	}
	
	//View Customer Yasas Vidanage......
	public function viewcostomer() {
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/adminuseres/viewcostomer';
		$mainData = array(
			'pagetitle' => 'View Customers',
			'admin' => $this->setting_model->Get_All('login'),
			
		);

		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}
}