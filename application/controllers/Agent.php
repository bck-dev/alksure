<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agent extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->check_isvalidated();
    }

    public function template($headerData, $sidebarData, $page, $mainData, $footerData)
    {
        $this->load->view('admin/components/header', $headerData);
        $this->load->view('admin/components/sidebar', $sidebarData);
        $this->load->view($page, $mainData);
        $this->load->view('admin/components/footer', $footerData);
    }

    //validate session start
    private function check_isvalidated()
    {
        if (!$this->session->userdata('validated')) {
            redirect('login');
        }
    }
    //validate session end


    //insert agent to database for 'agent' ---ishara sewwandi
    public function insertagent()
    {
        $data = array(
            'agent_name' => $this->input->post('agent_name'),
            'image' =>$this->setting_model->upload('agent_image', 'upload/agent'),
            'adress' => $this->input->post('agent_address'),
            'NIC_number' => $this->input->post('agent_nic_no'),
            'contact_number' => $this->input->post('agent_con_no'),
            'emp_id' => $this->input->post('emp_id')
        );

        $insert_id = $this->setting_model->insert($data, 'agents');


        $this->session->set_flashdata('msg', '<div class="alert alert-success">New model Added Successfully!</div>');

        redirect('Agent/addagent');
    }

    //add agent model function start --ishara sewwandi
    public function addagent()
    {
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/agent/addagent';
        $mainData = array(
            'pagetitle' => 'Add New Agent',

        );

        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }

    //View agent function start-- ishara sewwandi
    public function viewagent()
    {

        $headerData = null;
        $sidebarData = null;
        $page = 'admin/agent/viewagent';
        $mainData = array(
            'agent' => $this->setting_model->Get_All('agents'),


        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }



    //edit agent -- Ishara sewwandi

    public function editagent()
    {

        $id = $this->uri->segment(3);
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/agent/editagent';
        $mainData = array(
            'agent' => $this->setting_model->Get_Single('agents','agent_id',$id),


        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
//update agent --------------------ishara sewwandi
    public function updateagent()
    {

        $id = $this->uri->segment(3);

        if(isset($_FILES['fileinput']) && empty($_FILES['fileinput']['tmp_name'])) {
            $image = $this->input->post('agent_image');
        } else {
            $image = $this->setting_model->upload('fileinput', 'upload/agent');
        }
        $data = array(
            'agent_name' => $this->input->post('agent_name'),
            'image' =>$image,
            'adress' => $this->input->post('agent_address'),
            'NIC_number' => $this->input->post('agent_nic_no'),
            'contact_number' => $this->input->post('agent_con_no'),
            'emp_id' => $this->input->post('emp_id')
        );

        $insert_id = $this->setting_model->update($data, 'agents','agent_id',$id);


        $this->session->set_flashdata('msg', '<div class="alert alert-success">Update Agent Successfully!</div>');

        redirect('Agent/editagent/'.$id);
    }
}