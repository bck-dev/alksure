<?php
/**
 * Created by PhpStorm.
 * User: yaish
 * Date: 10/23/2018
 * Time: 11:50 AM
 */

class Vehicle_view_counter extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function update($Data, $table_name, $where, $id) {
        $this->db->where($where, $id);
        if($this->db->update($table_name, $Data)) {
            return true;
        }
    }

    public function get_counter($vehicle_id) {

        $this->db->from('vehicle_view_counter');
        $this->db->where('vehicle_id', $vehicle_id);
        return $this->db->get()->row();
    }

    public function update_counter($data) {

        $this->db->where('vehicle_id', $data['id']);
        $query = $this->db->get('login');
        $this->db->update('login', $data);
    }

    
}