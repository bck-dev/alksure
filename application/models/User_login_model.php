<?php
/**
 * Created by PhpStorm.
 * User: yaish
 * Date: 10/23/2018
 * Time: 11:50 AM
 */

class User_login_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function update($Data, $table_name, $where, $id) {
        $this->db->where($where, $id);
        if($this->db->update($table_name, $Data)) {
            return true;
        }
    }

    public function is_user_exist($data) {
        // echo "user";
        $this->db->from('login');
        $this->db->where('email', $data['email']);
        $this->db->where('password', md5($data['password']));
        $query = $this->db->get();
        // echo $this->db->last_query();
        $result = $query->num_rows();
        // var_dump($result);
        if ($result == 0) {

            return false;
        } else {
            return $query->row();
        }
    }

    public function reset_user_exist($email) {

        $this->db->from('login');
        $this->db->where('email', $email);
        $query = $this->db->get();
        $rest = $query->num_rows();
        if ($rest == 0) {
            return false;
        } else {
            return true;
        }
    }

    public function get_user($email) {

        $this->db->from('login');
        $this->db->where('email', $email);
        return $this->db->get()->row();
    }

    public function update_scode($data) {

        $id = $this->input->post('id');
        $this->db->where('user_id', $id);
        $query = $this->db->get('login');
        $this->db->update('login', $data);
    }

    public function is_verified($email, $scode) {
        $this->db->where('email', $email);
        $this->db->where('security_code', $scode);

        $query = $this->db->get('login');

        if ($query->num_rows() == 1) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
     public function is_sell_user_exist($data) {
        // echo "user";
        $this->db->from('selling_customer');
        $this->db->where('email', $data['email']);
        $this->db->where('password', md5($data['password']));
        $query = $this->db->get();
        // echo $this->db->last_query();
        $result = $query->num_rows();
        // var_dump($result);
        if ($result == 0) {

            return false;
        } else {
            return $query->row();
        }
    }

	public function is_user_exist_checkemail($data) {
        // echo "user";
        $this->db->from('login');
        $this->db->where('email', $data['email']);
        $query = $this->db->get();
        // echo $this->db->last_query();
        $result = $query->num_rows();
        // var_dump($result);
        if ($result == 0) {
            return false;
        } else {
            return $query->row();
        }
    }
    
    public function is_sell_user_exist_mail($data) {
        // echo "user";
        $this->db->from('selling_customer');
        $this->db->where('email', $data['email']);
        $query = $this->db->get();
        // echo $this->db->last_query();
        $result = $query->num_rows();
        // var_dump($result);
        if ($result == 0) {

            return false;
        } else {
            return $query->row();
        }
    }


}