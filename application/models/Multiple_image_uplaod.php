<?php

class Multiple_image_uplaod extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	 //multiple image upload (also using in edit too) --yasas vidanage
	public function multi_image_upload($files,$path){
    	//print_r($files);
		$count = count($_FILES['userfile']['name']);
		for($i=0; $i<$count; $i++)
		{
			$_FILES['userfile']['name']= time().$files['userfile']['name'][$i];
			$_FILES['userfile']['type']= $files['userfile']['type'][$i];
			$_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
			$_FILES['userfile']['error']= $files['userfile']['error'][$i];
			$_FILES['userfile']['size']= $files['userfile']['size'][$i];
			$config['upload_path'] = './upload/'.$path.'/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size'] ='' ;
			$config['remove_spaces'] = true;
			$config['overwrite'] = false;
			$config['max_width'] = '';
			$config['max_height'] = '';
			$this->upload->initialize($config);
			$this->upload->do_upload();
			$fileName = $_FILES['userfile']['name'];
			$images[] = $fileName;
		}
		$fileName = implode(',',$images);
		return $fileName;
	}
//======================================== Dining Images Upload ================================//
	//upload multiple images --yasas vidanage
	// public function upload_image_vehicle($inputdata,$filename)
	// {
	// 	$this->db->insert('import_vehicle', $inputdata); 
	// 	$insert_id = $this->db->insert_id();

	// 	if($filename!=''){
	// 		$filename1 = explode(',',$filename);
	// 		foreach($filename1 as $file){
	// 			$file_data = array(
	// 				'import_outside_images_id' => $insert_id
	// 			);
	// 			$this->db->insert('import_outside_images', $file_data);
	// 		}
	// 	}
	// }

	public function upload_image_selling_vehicle($filename,$dataid)
	{	
		if($filename!='' ){
			$filename1 = explode(',',$filename);
			foreach($filename1 as $file){
				$file_data = array(
					'inside_images_name' => $file,
					'vehicle_vehicle_id' => $dataid
				);
				$this->db->insert('inside_images', $file_data);
			}
		}
	}

	public function upload_image_vehicle($filename,$dataid)
	{	
		if($filename!='' ){
			$filename1 = explode(',',$filename);
			foreach($filename1 as $file){
				$file_data = array(
					'out_side_image_name' => $file,
					'vehicle_vehicle_id' => $dataid
				);
				$this->db->insert('import_outside_images', $file_data);
			}
		}
	}

	public function upload_renting_vehicle_image($filename,$dataid)
	{	
		if($filename!='' ){
			$filename1 = explode(',',$filename);
			foreach($filename1 as $file){
				$file_data = array(
					'renting_vehical_images' => $file,
					'renting_vehical_id' => $dataid
				);
				$this->db->insert('renting_outside_images', $file_data);
			}
		}
	}




    public function edit_data_image_vehicle($id){
        $query=$this->db->query("SELECT * FROM `renting_vehical` 
    RIGHT JOIN `renting_outside_images` ON `renting_outside_images`.`renting_vehical_id` = `renting_vehical`.`renting_vehical_id`
    WHERE `renting_vehical`.`renting_vehical_id` = '{$id}'");
        return $query->result_array();
    }

    //edit uploaded images for
    public function edit_upload_image_vehicle($filename,$dataid)
    {

        if($filename!='' ){
            $filename1 = explode(',',$filename);
            foreach($filename1 as $file){
                $file_data = array(
                    'renting_vehical_images' => $file,
                    'renting_vehical_id' => $dataid
                );
                $this->db->insert('renting_outside_images', $file_data);
            }
        }


    }
    
    public function edit_data_image_vehicle1($id){
        $query=$this->db->query("SELECT * FROM `vehicle` 
    RIGHT JOIN `inside_images` ON `inside_images`.`vehicle_vehicle_id` = `vehicle`.`vehicle_id`
    WHERE `vehicle`.`vehicle_id` = '{$id}'");
        return $query->result_array();
    }
    
     public function edit_upload_image_vehicle1($filename,$dataid)
    {

        if($filename!='' ){
            $filename1 = explode(',',$filename);
            foreach($filename1 as $file){
                $file_data = array(
                    'inside_images_name' => $file,
		    'vehicle_vehicle_id' => $dataid
                );
                $this->db->insert('inside_images', $file_data);
            }
        }


    }

	public function edit_data_image_importvehicle($id){
	
	        $query=$this->db->query("SELECT * FROM `import_vehicle` 
	    	RIGHT JOIN `import_outside_images` ON `import_outside_images`.`vehicle_vehicle_id` = `import_vehicle`.`vehicle_id`
	    	WHERE `import_vehicle`.`vehicle_id` = '{$id}'");
	        return $query->result_array();
	   }

	public function edit_upload_image_vehicle2($filename,$dataid){

        if($filename!='' ){
            $filename1 = explode(',',$filename);
            foreach($filename1 as $file){
                $file_data = array(
                    'out_side_image_name' => $file,
                    'vehicle_vehicle_id' => $dataid
                );
                $this->db->insert('import_outside_images', $file_data);
            }
        }
    }


public function upload_image_3wheel_vehicle($filename,$dataid)
    {
        if($filename!='' ){
            $filename1 = explode(',',$filename);
            foreach($filename1 as $file){
                $file_data = array(
                    'threewheel_bike_image_name' => $file,
                    'threewheel_bike_id' => $dataid
                );
                $this->db->insert('threewheel_bike_image', $file_data);
            }
        }
    }
    public function edit_image_3wheel_vehicle($id){
        $query=$this->db->query("SELECT * FROM `threewheel_bike` 
    RIGHT JOIN `threewheel_bike_image` ON `threewheel_bike_image`.`threewheel_bike_id` = `threewheel_bike`.`threewheel_bike_id`
    WHERE `threewheel_bike`.`threewheel_bike_id` = '{$id}'");
        return $query->result_array();
    }

    public function edit_upload_image_wheel($filename,$dataid)
    {

        if($filename!='' ){
            $filename1 = explode(',',$filename);
            foreach($filename1 as $file){
                $file_data = array(
                    'threewheel_bike_image_name' => $file,
                    'threewheel_bike_id' => $dataid
                );
                $this->db->insert('threewheel_bike_image', $file_data);
            }
        }


    }



	//edit dining images from database--yasas vidanage
	public function edit_data_image_dining($id){
		$query=$this->db->query("SELECT *
			FROM dining
			RIGHT JOIN dining_images 
			ON dining_id = dining_dining_id
			WHERE dining_id = $id");
		return $query->result_array();
	}

	//edit uploaded images for dining--yasas vidanage
	public function edit_upload_image_dining($user_id,$inputdata,$filename ='')
	{

		$data = array(
			'dining_title' => $inputdata['dining_title'],
			'dining_description'=> $inputdata['dining_description'],
			'dining_link'=>$inputdata['dining_link'],
			'cuisine_type'=>$inputdata['cuisine_type'],
			'dining_link'=>$inputdata['dining_link'],
			'attire'=>$inputdata['attire'],
			'seating_capacity'=>$inputdata['seating_capacity'],
			'location'=>$inputdata['location'],
			'opening_hours'=>$inputdata['opening_hours'],
			'telephone'=>$inputdata['telephone']);
		$this->db->where('dining_id', $user_id);
		$this->db->update('dining', $data);

		if($filename!='' ){
			$filename1 = explode(',',$filename);
			foreach($filename1 as $file){
				$file_data = array(
					'dining_images_name' => $file,
					'dining_dining_id' => $user_id
				);
				$this->db->insert('dining_images', $file_data);
			}
		}

		
	}
//=========================== End Dining Images Upload ========================================//

//=========================== Room Images Upload ===========================================//

//upload multiple images to room
	public function upload_image_room($inputdata,$filename)
	{
		$this->db->insert('rooms', $inputdata); 
		$insert_id = $this->db->insert_id();

		if($filename!='' ){
			$filename1 = explode(',',$filename);
			foreach($filename1 as $file){
				$file_data = array(
					'roomimages_name' => $file,
					'roomimages_room_id' => $insert_id
				);
				$this->db->insert('room_images', $file_data);
			}
		}
	}

//edit uploaded images for dining--yasas vidanage
	public function edit_upload_image_room($cover_image,$user_id,$inputdata,$filename ='')
	{
		$data = array(
			'room_type'=>$inputdata['room_type'],
			'room_name'=>$inputdata['room_name'],
			'room_category_one'=>$inputdata['room_category_one'],
			'room_category_two'=>$inputdata['room_category_two'],
			'room_category_one_link'=>$inputdata['room_category_one_link'],
			'room_category_two_link'=>$inputdata['room_category_two_link'],
			'room_description'=>$inputdata['room_description'],
			'room_size'=>$inputdata['room_size'],
			'room_main_page_points'=>$inputdata['room_main_page_points'],
			// 'room_bed_type'=>$inputdata['room_bed_type'],
			'room_fratures'=>implode(',', $inputdata['room_fratures']),
			'room_night_price'=>$inputdata['room_night_price'],
			'room_details'=>$inputdata['room_details'],
			'room_amenity_comfort'=>$inputdata['room_amenity_comfort'],
			'room_aminity_indulgence'=>$inputdata['room_aminity_indulgence'],
			'room_aminity_convenience'=>$inputdata['room_aminity_convenience'],
		);
		$this->db->where('room_id', $user_id);
		$this->db->update('rooms', $data);

		if($filename!='' ){
			$filename1 = explode(',',$filename);
			foreach($filename1 as $file){
				$file_data = array(
					'roomimages_name' => $file,
					'roomimages_room_id' => $user_id
				);
				$this->db->insert('room_images', $file_data);
			}
		}
		//update 360 image
		if($cover_image!='' ){
			$data_image = array('room_360_image'=>$cover_image);
			$this->db->update('rooms',$data_image,"room_id = $user_id");
		}

		
	}
//=========================== End Room Images Upload =======================================//

}