<?php

class Setting_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	function Get_language()
	{
		$this->db->select('*');
		$this->db->from('tbl_language');
		$query = $this->db->get();
		$result = $query->result_array();
		foreach( $result as $row)
		{
			$data =  $row;
		}
		return $data['language'];

	}
	function Get_Single_Row($table_name,$where,$id)
	{
		$this->db->where($where,$id);
		$query = $this->db->get($table_name);
		return $result = $query->row();

	}	

	function Get_All_joinAgent($table1,$table2,$where1,$where2,$where3,$orderby)
	{
		$this->db->select('*');
		$this->db->from($table1);
		$this->db->join($table2,$where1, 'inner');
		$this->db->where_in($where2,$where3);
		if($orderby != '')
		{
			$this->db->order_by($orderby, "desc"); 
		}

		$query = $this->db->get();
		return $query->result_array();
		
	}

	function Get_All($tablename)
	{
		$this->db->select('*');
		$this->db->from($tablename);
		$query = $this->db->get();
		return $query->result_array();
	}
	
	function Get_All_Desc($tablename, $feild)
	{
		$sql = "SELECT * FROM $tablename ORDER BY $feild DESC";
		$query = $this->db->query($sql);
		return $query->result_array();
		
	}

	function Get_All_Asc($tablename, $feild)
	{
		$sql = "SELECT * FROM $tablename ORDER BY $feild ASC";
		$query = $this->db->query($sql);
		return $query->result_array();
		
	}

	function Get_All_Where($tablename,$field,$id)
	{
		$this->db->select('*');
		$this->db->from($tablename);
		$this->db->where($field,$id);
		$query = $this->db->get();
		return $query->result_array();
	}

	function Get_Count($tablename)
	{
		$this->db->select('*');
		$this->db->from($tablename);
		$query = $this->db->get();
		return $query->num_rows();
	}


	function Get_Single($table_name,$where,$id)
	{
		$this->db->select('*');
		$this->db->from($table_name);
		$this->db->where($where,$id);
		$query = $this->db->get();
		return $query->result_array();

	}

	function update($Data,$table_name,$where,$id)
	{
		$this->db->where($where, $id);
		$this->db->update($table_name, $Data);
	}
	function insert($Data,$table_name)
	{
		$this->db->insert($table_name, $Data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
		
	}
	

	function delete($table_name,$where,$id )
	{
		$this->db->where($where, $id);
		$this->db->delete($table_name);

	}

	function Get_All_join($table1,$table2,$where1)
	{
		$this->db->select('*');
		$this->db->from($table1);
		$this->db->join($table2,$where1, 'inner');
		$query = $this->db->get();
		return $query->result_array();
		
	}

	function Get_Single_join($table1,$table2,$where1,$where2=NULL,$id=NULL)
	{
		$this->db->select('*');
		$this->db->from($table1);
		$this->db->join($table2,$where1, 'inner');
		//$this->db->where($where2, $id);

		$query = $this->db->get();
		return $query->result_array();
	}

	public function getJoin($config) {

		$single = false;

		if(!empty($config)) {

			foreach($config as $option => $values) {

				if($option == 'wheres') {
					if(is_array($values)) {

						$single = true;

						foreach($values as $value => $where) {
							$this->db->where($value.'.'.$where[0], $where[1]);
						}
					} else {
						$this->db->select('*');
					}
				}

				if($option == 'from') {
					if(is_array($values)) {
						foreach($values as $value) {
							$this->db->from($value);
						}
					}
				}

				if($option == 'joins') {
					if(is_array($values)) {
						foreach($values as $value => $join) {
							$this->db->join($value, $join[0], $join[1]);
						}
					}
				}

				if($option == 'groupby') {
					if(is_array($values)) {
						foreach($values as $value => $groupby) {
							$this->db->group_by($value.'.'.$groupby[0]);
						}
					}
				}

				if($option == 'orderby') {
					if(is_array($values)) {
						foreach($values as $value => $orderby) {
							$this->db->order_by($value.'.'.$orderby[0], $orderby[1]);
						}
					}
				}
			}

			$query = $this->db->get();

			if($single == true) {
				if($query->num_rows() == 1) {
					return $query->row();
				} else {
					return $query->result();
				}
			} else {
				return $query->result();
			}

		} else {
			return false;
		}
	}

	function Get_agent_booking($tablename)
	{
		
		$this->db->select('*');
		$this->db->from($tablename);
		$this->db->where('agent_id !=', 'admin');
		$query = $this->db->get();
		return $query->result_array();
		
	}

	function upload($inputname, $path) {
		$config['upload_path'] = "./".$path."/"; // path where image will be saved
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		//$config['max_size'] = 3072;
		$config['encrypt_name'] = TRUE;
		$this->upload->initialize($config);
		$this->upload->do_upload($inputname);
		$data_upload_files = $this->upload->data();

		return $image = $data_upload_files['file_name'];		
	}
	
	function deleteData($table, $field, $id) {
		$this->db->where($field, $id);
		$this->db->delete($table);
	}


	//upload multiple images to case study
	public function upload_image_services($inputdata,$filename,$covername)
	{
		$this->db->insert('services', $inputdata); 
		$insert_id = $this->db->insert_id();

		if($filename!='' ){
			$filename1 = explode(',',$filename);
			foreach($filename1 as $file){
				$file_data = array(
					'service_image_name' => $file,
					'service_service_id' => $insert_id
				);
				$this->db->insert('service_images', $file_data);
			}
		}

		if($covername!='' ){
			$data = array('cover_image' =>$covername);
			$this->db->update('services', $data, "service_id = $insert_id");
		}

	}

	//upload multiple images to events
	public function upload_image_events($inputdata,$filename)
	{
		$this->db->insert('news', $inputdata); 
		$insert_id = $this->db->insert_id();

		if($filename!='' ){
			$filename1 = explode(',',$filename);
			foreach($filename1 as $file){
				$file_data = array(
					'news_image' => $file,
					'news_news_id' => $insert_id
				);
				$this->db->insert('news_images', $file_data);
			}
		}
	}

	//edit uploaded images for casestudy
	public function edit_upload_image_services($covername,$user_id,$inputdata,$filename ='')
	{

		$data = array(
			'service_description' => $inputdata['service_description'],
			'service_title'=> $inputdata['service_title'],
			'category_fixed'=>$inputdata['category_fixed']);
		$this->db->where('service_id', $user_id);
		$this->db->update('services', $data);

		if($filename!='' ){
			$filename1 = explode(',',$filename);
			foreach($filename1 as $file){
				$file_data = array(
					'service_image_name' => $file,
					'service_service_id' => $user_id
				);
				$this->db->insert('service_images', $file_data);
			}
		}

		if($covername!='' ){
			$data = array('cover_image' =>$covername);
			$this->db->update('services', $data, "service_id = $user_id");
		}
	}

	//edit uploaded images for news
	public function edit_upload_image_news($user_id,$inputdata,$filename ='')
	{

		$data = array('news_description' => $inputdata['news_description'],
			'news_title'=> $inputdata['news_title']);
		$this->db->where('news_id', $user_id);
		$this->db->update('news', $data);

		if($filename!='' ){
			$filename1 = explode(',',$filename);
			foreach($filename1 as $file){
				$file_data = array(
					'news_image' => $file,
					'news_news_id' => $user_id
				);
				$this->db->insert('news_images', $file_data);
			}
		}
	}

	//delete data from table
	public function delete_data($tablename,$where,$user_id,$limt){
		$this->db->select('*');
		$this->db->from($tablename);
		$this->db->where($where,$user_id);
		$this->db->limit($limit);
		$this->db->delete();

	}

	//edit case study images from database
	public function edit_data_image_services($id){
		$query=$this->db->query("SELECT *
			FROM services
			RIGHT JOIN service_images 
			ON service_id = service_service_id
			WHERE service_id = $id");
		return $query->result_array();
	}

	//edit news images from database
	public function edit_data_image_news($id){
		$query=$this->db->query("SELECT *
			FROM news
			RIGHT JOIN news_images 
			ON news_id = news_news_id
			WHERE news_id = $id");
		return $query->result_array();
	}

    //multiple image upload (also using in edit too)
	public function multi_image_upload($files,$path){
    	//print_r($files);
		$count = count($_FILES['userfile']['name']);
		for($i=0; $i<$count; $i++)
		{
			$_FILES['userfile']['name']= time().$files['userfile']['name'][$i];
			$_FILES['userfile']['type']= $files['userfile']['type'][$i];
			$_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
			$_FILES['userfile']['error']= $files['userfile']['error'][$i];
			$_FILES['userfile']['size']= $files['userfile']['size'][$i];
			$config['upload_path'] = './upload/'.$path.'/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size'] = 3072;
			$config['remove_spaces'] = true;
			$config['overwrite'] = false;
			$config['max_width'] = '';
			$config['max_height'] = '';
			$this->upload->initialize($config);
			$this->upload->do_upload();
			$fileName = $_FILES['userfile']['name'];
			$images[] = $fileName;
		}
		$fileName = implode(',',$images);
		return $fileName;
	}

	public function vacancycategory() {
		$sql = "SELECT * FROM `vacancy_category` WHERE `vacancy_category_status`='1'";

		$query = $this->db->query($sql);

		return $query->result_array();
	}

	public function award() {
		$sql = "SELECT * FROM `news` INNER JOIN `news_images` ON `news`.`news_id` = `news_images`.`news_news_id` WHERE `news`.`news_category` = 'award'";

		$query = $this->db->query($sql);

		return $query->result_array();
	}


	function Get_All_by_id($table, $field, $id) {
		$this->db->where($field, $id);
		$query = $this->db->get($table);
		return $query->result_array();
	}

 //feature products
	public function newarivals() {
		$sql = "SELECT * FROM `products` inner JOIN `product_category` ON `products`.`category_category_id` = `product_category`.`category_id` WHERE `product_category`.`category_id` = '6'";

		$query = $this->db->query($sql);

		return $query->result_array();
	}
	public function clinical() {
		$sql = "SELECT * FROM `products` inner JOIN `product_category` ON `products`.`category_category_id` = `product_category`.`category_id` WHERE `product_category`.`category_id` = '9'";

		$query = $this->db->query($sql);

		return $query->result_array();
	}

	public function homecare() {
		$sql = "SELECT * FROM `products` inner JOIN `product_category` ON `products`.`category_category_id` = `product_category`.`category_id` WHERE `product_category`.`category_id` = '10'";

		$query = $this->db->query($sql);

		return $query->result_array();
	}

	//add feature products

	public function addnewarival() {
		$query = $this->db->query("SELECT * FROM `feature_product` inner JOIN `products` ON `feature_product`.`product_id`=`products`.`product_id` WHERE `feature_product`.`category_id` = '6' ");

		return $query->result_array();
	}

	public function addclinical() {
		$query = $this->db->query("SELECT * FROM `feature_product` inner JOIN `products` ON `feature_product`.`product_id`=`products`.`product_id` WHERE `feature_product`.`category_id` = '9' ");

		return $query->result_array();
	}

	public function addhomecare() {
		$query = $this->db->query("SELECT * FROM `feature_product` inner JOIN `products` ON `feature_product`.`product_id`=`products`.`product_id` WHERE `feature_product`.`category_id` = '10' ");

		return $query->result_array();
	}

	public function inquiryjoin() {
		$sql = "SELECT * FROM `buying_inquiry` 
		LEFT JOIN `vehicle` ON `buying_inquiry`.`vehical_vehical_id` = `vehicle`.`vehicle_id`
		LEFT JOIN `customer` ON `buying_inquiry`.`customer_customer_id` = `customer`.`customer_id`";
				// LEFT JOIN `vehicle` ON `vehicle_brand`.`vehicle_brand_id` = `vehicle`.`vehicle_brand`
				// LEFT JOIN `vehicle` ON `vehicle_model`.`vehicle_model_id` = `vehicle`.`vehicle_model`
	}

	public function offerjoin() {
		$sql = "SELECT * FROM `customer_offer` 
		LEFT JOIN `vehicle` ON `customer_offer`.`vehical_id` = `vehicle`.`vehicle_id`";
		// LEFT JOIN `customer` ON `buying_inquiry`.`customer_customer_id` = `customer`.`customer_id`
				// LEFT JOIN `vehicle` ON `vehicle_brand`.`vehicle_brand_id` = `vehicle`.`vehicle_brand`
				// LEFT JOIN `vehicle` ON `vehicle_model`.`vehicle_model_id` = `vehicle`.`vehicle_model`
	}



	public function vehiclejoin() {
		$sql = "SELECT * FROM `vehicle` 
		LEFT JOIN `inside_images` ON `vehicle`.`vehicle_id` = `inside_images`.`vehicle_vehicle_id`
		LEFT JOIN `out_side_images` ON `vehicle`.`vehicle_id` = `out_side_images`.`vehicle_vehicle_id`
		LEFT JOIN `vehicle_brand` ON `vehicle`.`vehicle_brand` = `vehicle_brand`.`vehicle_brand_id`
		LEFT JOIN `vehicle_type` ON `vehicle`.`vehicle_type` = `vehicle_type`.`vehicle_type_id`
		LEFT JOIN `vehicle_model` ON `vehicle`.`vehicle_model` = `vehicle_model`.`vehicle_model_id`
		LEFT JOIN `selling_customer` ON `vehicle`.`selling_customer_id` = `selling_customer`.`selling_customer_id`
		LEFT JOIN `agents` ON `vehicle`.`agent_id` = `agents`.`agent_id`
		GROUP BY `vehicle_id`
		DESC";

		$query = $this->db->query($sql);

		return $query->result_array();
	}
	
	public function vehiclejoinXX() {
		$sql = "SELECT * FROM `vehicle` 
		LEFT JOIN `inside_images` ON `vehicle`.`vehicle_id` = `inside_images`.`vehicle_vehicle_id`
		LEFT JOIN `vehicle_brand` ON `vehicle`.`vehicle_brand` = `vehicle_brand`.`vehicle_brand_id`
		LEFT JOIN `vehicle_type` ON `vehicle`.`vehicle_type` = `vehicle_type`.`vehicle_type_id`
		LEFT JOIN `vehicle_model` ON `vehicle`.`vehicle_model` = `vehicle_model`.`vehicle_model_id`
		GROUP BY `vehicle_id`
		DESC";

		$query = $this->db->query($sql);

		return $query->result_array();
	}
	
	public function vehiclejoinOPTY($vehicle_type) {
		$sql = "SELECT * FROM `vehicle` 
				LEFT JOIN `inside_images` ON `vehicle`.`vehicle_id` = `inside_images`.`vehicle_vehicle_id`
				LEFT JOIN `out_side_images` ON `vehicle`.`vehicle_id` = `out_side_images`.`vehicle_vehicle_id`
				LEFT JOIN `vehicle_brand` ON `vehicle`.`vehicle_brand` = `vehicle_brand`.`vehicle_brand_id`
				LEFT JOIN `vehicle_type` ON `vehicle`.`vehicle_type` = `vehicle_type`.`vehicle_type_id`
				LEFT JOIN `vehicle_model` ON `vehicle`.`vehicle_model` = `vehicle_model`.`vehicle_model_id`
				WHERE `vehicle`.`vehicle_type` = {$vehicle_type}
				GROUP BY `vehicle`.`vehicle_id` DESC";

		$query = $this->db->query($sql);

		return $query->result_array();
	}
	
	public function vehiclejoinOPBR($vehicle_type,$vehicle_brand) {
		$sql = "SELECT * FROM `vehicle` 
				LEFT JOIN `inside_images` ON `vehicle`.`vehicle_id` = `inside_images`.`vehicle_vehicle_id`
				LEFT JOIN `out_side_images` ON `vehicle`.`vehicle_id` = `out_side_images`.`vehicle_vehicle_id`
				LEFT JOIN `vehicle_brand` ON `vehicle`.`vehicle_brand` = `vehicle_brand`.`vehicle_brand_id`
				LEFT JOIN `vehicle_type` ON `vehicle`.`vehicle_type` = `vehicle_type`.`vehicle_type_id`
				LEFT JOIN `vehicle_model` ON `vehicle`.`vehicle_model` = `vehicle_model`.`vehicle_model_id`
				WHERE `vehicle`.`vehicle_type` = {$vehicle_type} AND `vehicle`.`vehicle_brand` = {$vehicle_brand}
				GROUP BY `vehicle`.`vehicle_id`
				DESC";

		$query = $this->db->query($sql);

		return $query->result_array();
	}
	
	public function vehiclejoinOPMOD($vehicle_type,$vehicle_brand,$vehicle_model) {
		$sql = "SELECT * FROM `vehicle` 
				LEFT JOIN `inside_images` ON `vehicle`.`vehicle_id` = `inside_images`.`vehicle_vehicle_id`
				LEFT JOIN `out_side_images` ON `vehicle`.`vehicle_id` = `out_side_images`.`vehicle_vehicle_id`
				LEFT JOIN `vehicle_brand` ON `vehicle`.`vehicle_brand` = `vehicle_brand`.`vehicle_brand_id`
				LEFT JOIN `vehicle_type` ON `vehicle`.`vehicle_type` = `vehicle_type`.`vehicle_type_id`
				LEFT JOIN `vehicle_model` ON `vehicle`.`vehicle_model` = `vehicle_model`.`vehicle_model_id`
				WHERE `vehicle`.`vehicle_type` = {$vehicle_type} AND `vehicle`.`vehicle_brand` = {$vehicle_brand} AND `vehicle`.`vehicle_model` = {$vehicle_model}
				GROUP BY `vehicle`.`vehicle_id`
				DESC";

		$query = $this->db->query($sql);

		return $query->result_array();
	}
	
	public function vehiclejoin_where($field,$value,$value1,$value2,$value3) {
	if($value1==0 && $value2==0 && $value3==0){
        $sql = "SELECT * FROM `vehicle` 
				LEFT JOIN `inside_images` ON `vehicle`.`vehicle_id` = `inside_images`.`vehicle_vehicle_id`
				LEFT JOIN `out_side_images` ON `vehicle`.`vehicle_id` = `out_side_images`.`vehicle_vehicle_id`
				LEFT JOIN `vehicle_brand` ON `vehicle`.`vehicle_brand` = `vehicle_brand`.`vehicle_brand_id`
				LEFT JOIN `vehicle_type` ON `vehicle`.`vehicle_type` = `vehicle_type`.`vehicle_type_id`
				LEFT JOIN `vehicle_model` ON `vehicle`.`vehicle_model` = `vehicle_model`.`vehicle_model_id`
				LEFT JOIN `selling_customer` ON `vehicle`.`selling_customer_id` = `selling_customer`.`selling_customer_id`
				LEFT JOIN `agents` ON `vehicle`.`agent_id` = `agents`.`agent_id`
				WHERE `vehicle`.$field <='{$value}'
				GROUP BY `vehicle_id`
				ORDER BY `vehicle_minimum_downpayment` ASC ";
	}
	elseif($value1>0 && $value2==0 && $value3==0){
	$sql = "SELECT * FROM `vehicle` 
				LEFT JOIN `inside_images` ON `vehicle`.`vehicle_id` = `inside_images`.`vehicle_vehicle_id`
				LEFT JOIN `out_side_images` ON `vehicle`.`vehicle_id` = `out_side_images`.`vehicle_vehicle_id`
				LEFT JOIN `vehicle_brand` ON `vehicle`.`vehicle_brand` = `vehicle_brand`.`vehicle_brand_id`
				LEFT JOIN `vehicle_type` ON `vehicle`.`vehicle_type` = `vehicle_type`.`vehicle_type_id`
				LEFT JOIN `vehicle_model` ON `vehicle`.`vehicle_model` = `vehicle_model`.`vehicle_model_id`
				LEFT JOIN `selling_customer` ON `vehicle`.`selling_customer_id` = `selling_customer`.`selling_customer_id`
				LEFT JOIN `agents` ON `vehicle`.`agent_id` = `agents`.`agent_id`
				WHERE `vehicle`.$field <='{$value}' && `vehicle`.`vehicle_type` = '{$value1}'
				GROUP BY `vehicle_id`
				ORDER BY `vehicle_minimum_downpayment` ASC ";
	}
	elseif($value1>0 && $value2>0 && $value3==0){
	$sql = "SELECT * FROM `vehicle` 
				LEFT JOIN `inside_images` ON `vehicle`.`vehicle_id` = `inside_images`.`vehicle_vehicle_id`
				LEFT JOIN `out_side_images` ON `vehicle`.`vehicle_id` = `out_side_images`.`vehicle_vehicle_id`
				LEFT JOIN `vehicle_brand` ON `vehicle`.`vehicle_brand` = `vehicle_brand`.`vehicle_brand_id`
				LEFT JOIN `vehicle_type` ON `vehicle`.`vehicle_type` = `vehicle_type`.`vehicle_type_id`
				LEFT JOIN `vehicle_model` ON `vehicle`.`vehicle_model` = `vehicle_model`.`vehicle_model_id`
				LEFT JOIN `selling_customer` ON `vehicle`.`selling_customer_id` = `selling_customer`.`selling_customer_id`
				LEFT JOIN `agents` ON `vehicle`.`agent_id` = `agents`.`agent_id`
				WHERE `vehicle`.$field <='{$value}' && `vehicle`.`vehicle_type` = '{$value1}' && `vehicle`.`vehicle_brand` = '{$value2}'
				GROUP BY `vehicle_id`
				ORDER BY `vehicle_minimum_downpayment` ASC ";
	}
	elseif($value1>0 && $value2>0 && $value3>0){
	$sql = "SELECT * FROM `vehicle` 
				LEFT JOIN `inside_images` ON `vehicle`.`vehicle_id` = `inside_images`.`vehicle_vehicle_id`
				LEFT JOIN `out_side_images` ON `vehicle`.`vehicle_id` = `out_side_images`.`vehicle_vehicle_id`
				LEFT JOIN `vehicle_brand` ON `vehicle`.`vehicle_brand` = `vehicle_brand`.`vehicle_brand_id`
				LEFT JOIN `vehicle_type` ON `vehicle`.`vehicle_type` = `vehicle_type`.`vehicle_type_id`
				LEFT JOIN `vehicle_model` ON `vehicle`.`vehicle_model` = `vehicle_model`.`vehicle_model_id`
				LEFT JOIN `selling_customer` ON `vehicle`.`selling_customer_id` = `selling_customer`.`selling_customer_id`
				LEFT JOIN `agents` ON `vehicle`.`agent_id` = `agents`.`agent_id`
				WHERE `vehicle`.$field <='{$value}' && `vehicle`.`vehicle_type` = '{$value1}' && `vehicle`.`vehicle_brand` = '{$value2}' && `vehicle`.`vehicle_model` = '{$value3}'
				GROUP BY `vehicle_id`
				ORDER BY `vehicle_minimum_downpayment` ASC ";
	}
        $query = $this->db->query($sql);

        return $query->result_array();
    }
	
	public function vehiclejoinX() {
		$sql = "SELECT * FROM `vehicle` 
		LEFT JOIN `out_side_images` ON `vehicle`.`vehicle_id` = `out_side_images`.`vehicle_vehicle_id`
		LEFT JOIN `vehicle_brand` ON `vehicle`.`vehicle_brand` = `vehicle_brand`.`vehicle_brand_id`
		LEFT JOIN `vehicle_type` ON `vehicle`.`vehicle_type` = `vehicle_type`.`vehicle_type_id`
		LEFT JOIN `vehicle_model` ON `vehicle`.`vehicle_model` = `vehicle_model`.`vehicle_model_id`
		LEFT JOIN `selling_customer` ON `vehicle`.`selling_customer_id` = `selling_customer`.`selling_customer_id`
		LEFT JOIN `agents` ON `vehicle`.`agent_id` = `agents`.`agent_id`
		";

		$query = $this->db->query($sql);

		return $query->result_array();
	}

	public function vehiclejoin1($id) {
	        $sql = "SELECT * FROM `vehicle` 
					LEFT JOIN `inside_images` ON `vehicle`.`vehicle_id` = `inside_images`.`vehicle_vehicle_id`
					LEFT JOIN `out_side_images` ON `vehicle`.`vehicle_id` = `out_side_images`.`vehicle_vehicle_id`
					LEFT JOIN `vehicle_brand` ON `vehicle`.`vehicle_brand` = `vehicle_brand`.`vehicle_brand_id`
					LEFT JOIN `vehicle_type` ON `vehicle`.`vehicle_type` = `vehicle_type`.`vehicle_type_id`
					LEFT JOIN `vehicle_model` ON `vehicle`.`vehicle_model` = `vehicle_model`.`vehicle_model_id`
					LEFT JOIN `selling_customer` ON `vehicle`.`selling_customer_id` = `selling_customer`.`selling_customer_id`
					LEFT JOIN `agents` ON `vehicle`.`agent_id` = `agents`.`agent_id`
					WHERE `vehicle`.`vehicle_id` ='{$id}'
					 ";
	
	        $query = $this->db->query($sql);
	
	        return $query->result_array();
	    }
	
	public function vehiclejoin2($agentid) {
		$sql = "SELECT * FROM `vehicle` 
		LEFT JOIN `inside_images` ON `vehicle`.`vehicle_id` = `inside_images`.`vehicle_vehicle_id`
		LEFT JOIN `out_side_images` ON `vehicle`.`vehicle_id` = `out_side_images`.`vehicle_vehicle_id`
		LEFT JOIN `vehicle_brand` ON `vehicle`.`vehicle_brand` = `vehicle_brand`.`vehicle_brand_id`
		LEFT JOIN `vehicle_type` ON `vehicle`.`vehicle_type` = `vehicle_type`.`vehicle_type_id`
		LEFT JOIN `vehicle_model` ON `vehicle`.`vehicle_model` = `vehicle_model`.`vehicle_model_id`
		LEFT JOIN `selling_customer` ON `vehicle`.`selling_customer_id` = `selling_customer`.`selling_customer_id`
		LEFT JOIN `agents` ON `vehicle`.`agent_id` = `agents`.`agent_id`
		WHERE `vehicle`.`agent_id` = '{$agentid}' ";

		$query = $this->db->query($sql);

		return $query->result_array();
	}
	
	 public function vehiclejoincus($vehiid)
    {
        $sql = "SELECT * FROM `vehicle` 
		LEFT JOIN `inside_images` ON `vehicle`.`vehicle_id` = `inside_images`.`vehicle_vehicle_id`
		LEFT JOIN `out_side_images` ON `vehicle`.`vehicle_id` = `out_side_images`.`vehicle_vehicle_id`
		LEFT JOIN `vehicle_brand` ON `vehicle`.`vehicle_brand` = `vehicle_brand`.`vehicle_brand_id`
		LEFT JOIN `vehicle_type` ON `vehicle`.`vehicle_type` = `vehicle_type`.`vehicle_type_id`
		LEFT JOIN `vehicle_model` ON `vehicle`.`vehicle_model` = `vehicle_model`.`vehicle_brand_id`
		WHERE `vehicle`.`user_id` = '{$vehiid}'
		GROUP BY `vehicle_id` ";

        $query = $this->db->query($sql);

        return $query->result_array();
    }


	public function rentvehiclejoin1() {
		$sql = "SELECT * FROM `renting_vehical` 
		LEFT JOIN `renting_vehical_brand` ON `renting_vehical`.`vehical_vehical_brand_id` = `renting_vehical_brand`.`vehical_brand_id`
		LEFT JOIN `renting_vehical_type` ON `renting_vehical`.`vehical_vehical_type_id` = `renting_vehical_type`.`vehical_type_id`
		LEFT JOIN `renting_vehical_model` ON `renting_vehical`.`vehical_vehical_model_id` = `renting_vehical_model`.`vehical_model_id`
		GROUP BY `renting_vehical_id`";

		$query = $this->db->query($sql);

		return $query->result_array();
	}

	public function rentvehiclejoin($id) {
		$sql = "SELECT * FROM `renting_vehical` 
		LEFT JOIN `renting_vehical_brand` ON `renting_vehical`.`vehical_vehical_brand_id` = `renting_vehical_brand`.`vehical_brand_id`
		LEFT JOIN `renting_vehical_type` ON `renting_vehical`.`vehical_vehical_type_id` = `renting_vehical_type`.`vehical_type_id`
		LEFT JOIN `renting_vehical_model` ON `renting_vehical`.`vehical_vehical_model_id` = `renting_vehical_model`.`vehical_model_id`
		WHERE `renting_vehical`.`renting_vehical_id` = '{$id}'";

		$query = $this->db->query($sql);

		return $query->result_array();
	}

	public function vehiclemodel($id) {
		$sql = "SELECT * FROM `vehicle_model` 
		LEFT JOIN `vehicle_brand` ON `vehicle_model`.`vehicle_brand_id` = `vehicle_brand`.`vehicle_brand_id`
		WHERE `vehicle_model`.`vehicle_model_id` = '{$id}'";

		$query = $this->db->query($sql);

		return $query->result_array();
	}


	public function frontjoin() {
		$sql = "SELECT * FROM `vehicle` 
		LEFT JOIN `inside_images` ON `vehicle`.`vehicle_id` = `inside_images`.`vehicle_vehicle_id`
		LEFT JOIN `out_side_images` ON `vehicle`.`vehicle_id` = `out_side_images`.`vehicle_vehicle_id`
		LEFT JOIN `vehicle_brand` ON `vehicle`.`vehicle_brand` = `vehicle_brand`.`vehicle_brand_id`
		LEFT JOIN `vehicle_type` ON `vehicle`.`vehicle_type` = `vehicle_type`.`vehicle_type_id`
		LEFT JOIN `vehicle_model` ON `vehicle`.`vehicle_model` = `vehicle_model`.`vehicle_model_id`
		GROUP BY `vehicle_id` DESC
		
		";

		$query = $this->db->query($sql);

		return $query->result_array();
	}

	public function multiple_search($liker, $price_range, $autosure_certified){

		if($liker[0]){
			$search_words=explode(" ", $liker[0]);

			foreach($search_words as $search_word){
						
				$query = $this->db->query(
					"SELECT * FROM `vehicle` 
					LEFT JOIN `inside_images` ON `vehicle`.`vehicle_id` = `inside_images`.`vehicle_vehicle_id`
					LEFT JOIN `out_side_images` ON `vehicle`.`vehicle_id` = `out_side_images`.`vehicle_vehicle_id`
					LEFT JOIN `vehicle_brand` ON `vehicle`.`vehicle_brand` = `vehicle_brand`.`vehicle_brand_id`
					LEFT JOIN `vehicle_type` ON `vehicle`.`vehicle_type` = `vehicle_type`.`vehicle_type_id`
					LEFT JOIN `vehicle_model` ON `vehicle`.`vehicle_model` = `vehicle_model`.`vehicle_model_id`
					WHERE vehicle_model.vehicle_model_name LIKE '%" . $search_word . "%' 
					OR vehicle.vehicle_transmission LIKE '%" . $search_word . "%' 
					OR vehicle.year_of_manufacture LIKE '%" . $search_word . "%' 
					OR vehicle.fuel_type LIKE '%" . $search_word . "%' 
					OR vehicle_brand.vehicle_brand_name LIKE '%" . $search_word . "%' 
					OR vehicle.vehicle_features LIKE '%". $search_word."%' 
					OR vehicle_type.vehicle_type_name LIKE '%" . $search_word . "%' 
					OR CONCAT(vehicle_model.vehicle_model_name, ' ', vehicle.year_of_manufacture, ' ',vehicle.fuel_type,'',vehicle.vehicle_transmission,'',vehicle_brand.vehicle_brand_name,'',vehicle_type.vehicle_type_name) LIKE '%" . $search_word . "%'
					GROUP BY `vehicle_id` ORDER BY `vehicle_id` DESC"
				);

			}

		}
		elseif($liker[1] || $liker[2] || $liker[3] || $price_range || $autosure_certified){

			$vehicle_type_query = $liker[1] ?	"vehicle_type.vehicle_type_name='$liker[1]'" : "";
			$vehicle_brand_query = ($liker[2]) ? "vehicle_brand.vehicle_brand_name ='$liker[2]'" : "";
			$vehicle_model_query = ($liker[3]) ? "vehicle_model.vehicle_model_name = '$liker[3]'" : "";
			$autosure_certified_query = $autosure_certified ?	"vehicle.autosure_certified='$autosure_certified'" : "";

			if ($price_range){
				$prices = explode("-", $price_range);
				$price_range_query="vehicle.vehicle_total_value>=$prices[0] AND vehicle.vehicle_total_value<=$prices[1]";
			}
			else{
				$price_range_query="";
			}

			if($vehicle_type_query && ($vehicle_brand_query || $vehicle_model_query || $autosure_certified_query || $price_range_query)){
				$t_and=" AND ";
			} else{
				$t_and="";
			}

			if($vehicle_brand_query && ($vehicle_model_query || $autosure_certified_query || $price_range_query)){
				$b_and=" AND ";
			} else{
				$b_and="";
			}

			if($vehicle_model_query && ($autosure_certified_query || $price_range_query)){
				$m_and=" AND ";
			} else{
				$m_and="";
			}

			if($autosure_certified_query && $price_range_query){
				$a_and=" AND ";
			} else{
				$a_and="";
			}

			$query = $this->db->query(
				"SELECT * FROM `vehicle` 
				LEFT JOIN `inside_images` ON `vehicle`.`vehicle_id` = `inside_images`.`vehicle_vehicle_id`
				LEFT JOIN `out_side_images` ON `vehicle`.`vehicle_id` = `out_side_images`.`vehicle_vehicle_id`
				LEFT JOIN `vehicle_brand` ON `vehicle`.`vehicle_brand` = `vehicle_brand`.`vehicle_brand_id`
				LEFT JOIN `vehicle_type` ON `vehicle`.`vehicle_type` = `vehicle_type`.`vehicle_type_id`
				LEFT JOIN `vehicle_model` ON `vehicle`.`vehicle_model` = `vehicle_model`.`vehicle_model_id`
				WHERE $vehicle_type_query $t_and $vehicle_brand_query $b_and $vehicle_model_query $m_and $autosure_certified_query $a_and $price_range_query GROUP BY `vehicle_id` ORDER BY `vehicle_id` DESC"
			);
		}
		else{
			$search_word='';
			$query = $this->db->query(
				"SELECT * FROM `vehicle` 
				LEFT JOIN `inside_images` ON `vehicle`.`vehicle_id` = `inside_images`.`vehicle_vehicle_id`
				LEFT JOIN `out_side_images` ON `vehicle`.`vehicle_id` = `out_side_images`.`vehicle_vehicle_id`
				LEFT JOIN `vehicle_brand` ON `vehicle`.`vehicle_brand` = `vehicle_brand`.`vehicle_brand_id`
				LEFT JOIN `vehicle_type` ON `vehicle`.`vehicle_type` = `vehicle_type`.`vehicle_type_id`
				LEFT JOIN `vehicle_model` ON `vehicle`.`vehicle_model` = `vehicle_model`.`vehicle_model_id`
				WHERE vehicle_model.vehicle_model_name LIKE '%" . $search_word . "%' 
				OR vehicle.vehicle_transmission LIKE '%" . $search_word . "%' 
				OR vehicle.year_of_manufacture LIKE '%" . $search_word . "%' 
				OR vehicle.fuel_type LIKE '%" . $search_word . "%' 
				OR vehicle_brand.vehicle_brand_name LIKE '%" . $search_word . "%' 
				OR vehicle.vehicle_features LIKE '%". $search_word."%' 
				OR vehicle_type.vehicle_type_name LIKE '%" . $search_word . "%' 
				OR CONCAT(vehicle_model.vehicle_model_name, ' ', vehicle.year_of_manufacture, ' ',vehicle.fuel_type,'',vehicle.vehicle_transmission,'',vehicle_brand.vehicle_brand_name,'',vehicle_type.vehicle_type_name) LIKE '%" . $search_word . "%'
				GROUP BY `vehicle_id` ORDER BY `vehicle_id` DESC"
			);


		}

		return $query->result_array();

	}
	public function frontviewvehiclejoin1($vehicle_type) {
		$sql = "SELECT * FROM `vehicle` 
				LEFT JOIN `inside_images` ON `vehicle`.`vehicle_id` = `inside_images`.`vehicle_vehicle_id`
				LEFT JOIN `out_side_images` ON `vehicle`.`vehicle_id` = `out_side_images`.`vehicle_vehicle_id`
				LEFT JOIN `vehicle_brand` ON `vehicle`.`vehicle_brand` = `vehicle_brand`.`vehicle_brand_id`
				LEFT JOIN `vehicle_type` ON `vehicle`.`vehicle_type` = `vehicle_type`.`vehicle_type_id`
				LEFT JOIN `vehicle_model` ON `vehicle`.`vehicle_model` = `vehicle_model`.`vehicle_model_id`
				WHERE `vehicle`.`vehicle_type` = {$vehicle_type}
				GROUP BY `vehicle`.`vehicle_id`
				DESC";

		$query = $this->db->query($sql);		

		return $query->result_array();
	}

	public function frontviewvehiclejoin2($vehicle_type,$vehicle_brand) {
		$sql = "SELECT * FROM `vehicle` 
				LEFT JOIN `inside_images` ON `vehicle`.`vehicle_id` = `inside_images`.`vehicle_vehicle_id`
				LEFT JOIN `out_side_images` ON `vehicle`.`vehicle_id` = `out_side_images`.`vehicle_vehicle_id`
				LEFT JOIN `vehicle_brand` ON `vehicle`.`vehicle_brand` = `vehicle_brand`.`vehicle_brand_id`
				LEFT JOIN `vehicle_type` ON `vehicle`.`vehicle_type` = `vehicle_type`.`vehicle_type_id`
				LEFT JOIN `vehicle_model` ON `vehicle`.`vehicle_model` = `vehicle_model`.`vehicle_model_id`
				WHERE `vehicle`.`vehicle_type` = {$vehicle_type} AND `vehicle`.`vehicle_brand` = {$vehicle_brand}
				GROUP BY `vehicle`.`vehicle_id`
				DESC";

		$query = $this->db->query($sql);		

		return $query->result_array();
	}

	public function frontviewvehiclejoin3($vehicle_type,$vehicle_brand,$vehicle_model) {
		$sql = "SELECT * FROM `vehicle` 
				LEFT JOIN `inside_images` ON `vehicle`.`vehicle_id` = `inside_images`.`vehicle_vehicle_id`
				LEFT JOIN `out_side_images` ON `vehicle`.`vehicle_id` = `out_side_images`.`vehicle_vehicle_id`
				LEFT JOIN `vehicle_brand` ON `vehicle`.`vehicle_brand` = `vehicle_brand`.`vehicle_brand_id`
				LEFT JOIN `vehicle_type` ON `vehicle`.`vehicle_type` = `vehicle_type`.`vehicle_type_id`
				LEFT JOIN `vehicle_model` ON `vehicle`.`vehicle_model` = `vehicle_model`.`vehicle_model_id`
				WHERE `vehicle`.`vehicle_type` = {$vehicle_type} AND `vehicle`.`vehicle_brand` = {$vehicle_brand} AND `vehicle`.`vehicle_model` = {$vehicle_model}
				GROUP BY `vehicle`.`vehicle_id`
				DESC";

		$query = $this->db->query($sql);		

		return $query->result_array();
	}
	
	public function frontviewvehiclejoin($id1) {
		$sql = "SELECT * FROM `vehicle` 
				LEFT JOIN `inside_images` ON `vehicle`.`vehicle_id` = `inside_images`.`vehicle_vehicle_id`
				LEFT JOIN `out_side_images` ON `vehicle`.`vehicle_id` = `out_side_images`.`vehicle_vehicle_id`
				LEFT JOIN `vehicle_brand` ON `vehicle`.`vehicle_brand` = `vehicle_brand`.`vehicle_brand_id`
				LEFT JOIN `vehicle_type` ON `vehicle`.`vehicle_type` = `vehicle_type`.`vehicle_type_id`
				LEFT JOIN `vehicle_model` ON `vehicle`.`vehicle_model` = `vehicle_model`.`vehicle_model_id`
				-- GROUP BY `vehicle_id`
				WHERE `vehicle`.`vehicle_id` = {$id1}
				";

		$query = $this->db->query($sql);		

		return $query->result_array();
	}
	 public function adminjoin() {
        $sql = "SELECT * FROM `login` 
    LEFT JOIN `admin_types` ON `login`.`user_type` = `admin_types`.`admin_type_id`
    ";

        $query = $this->db->query($sql);

        return $query->result_array();
    }
    public function adminjoin1($id) {
        $sql = "SELECT * FROM `login` 
    LEFT JOIN `admin_types` ON `login`.`user_type` = `admin_types`.`admin_type_id`
    WHERE `login`.`user_id` = '{$id}'";

        $query = $this->db->query($sql);

        return $query->result_array();
    }
    
      public function vehicledetail($vehi) {
        $sql = "SELECT * FROM `vehicle` 
				LEFT JOIN `inside_images` ON `vehicle`.`vehicle_id` = `inside_images`.`vehicle_vehicle_id`
				LEFT JOIN `out_side_images` ON `vehicle`.`vehicle_id` = `out_side_images`.`vehicle_vehicle_id`
				LEFT JOIN `vehicle_brand` ON `vehicle`.`vehicle_brand` = `vehicle_brand`.`vehicle_brand_id`
				LEFT JOIN `vehicle_type` ON `vehicle`.`vehicle_type` = `vehicle_type`.`vehicle_type_id`
				LEFT JOIN `vehicle_model` ON `vehicle`.`vehicle_model` = `vehicle_model`.`vehicle_model_id`
				LEFT JOIN `selling_customer` ON `vehicle`.`selling_customer_id` = `selling_customer`.`selling_customer_id`
				LEFT JOIN `agents` ON `vehicle`.`agent_id` = `agents`.`agent_id`
				WHERE `vehicle`.`vehicle_id` = '{$vehi}' ";

        $query = $this->db->query($sql);

        return $query->result_array();
    }
    
    public function renting($vehicle_type,$vehicle_trans){
		$sql = "SELECT * FROM `renting_vehical` WHERE `vehical_vehical_type_id`= {$vehicle_type} AND `transmission` = {$vehicle_trans} ";

		$query = $this->db->query($sql);		
		return $query->result_array();
	}

public function importviewvehiclejoin1($type) {
		$sql = "SELECT * FROM `import_vehicle` 
				LEFT JOIN `import_front_image` ON `import_vehicle`.`vehicle_id` = `import_front_image`.`vehicle_id`
				LEFT JOIN `import_vehicle_brand` ON `import_vehicle`.`import_brand_id` = `import_vehicle_brand`.`vehicle_brand_id`
				LEFT JOIN `import_vehical_type` ON `import_vehicle`.`import_vehicle_type` = `import_vehical_type`.`vehical_type_id`
				LEFT JOIN `import_vehicle_model` ON `import_vehicle`.`import_model_id` = `import_vehicle_model`.`vehicle_model_id`
				WHERE `import_vehicle`.`import_vehicle_type` = {$type}
				GROUP BY `import_vehicle`.`vehicle_id`
				DESC";

		$query = $this->db->query($sql);		

		return $query->result_array();
	}

	public function importviewvehiclejoin2($type,$brand) {
		$sql = "SELECT * FROM `import_vehicle` 
				LEFT JOIN `import_front_image` ON `import_vehicle`.`vehicle_id` = `import_front_image`.`vehicle_id`
				LEFT JOIN `import_vehicle_brand` ON `import_vehicle`.`import_brand_id` = `import_vehicle_brand`.`vehicle_brand_id`
				LEFT JOIN `import_vehical_type` ON `import_vehicle`.`import_vehicle_type` = `import_vehical_type`.`vehical_type_id`
				LEFT JOIN `import_vehicle_model` ON `import_vehicle`.`import_model_id` = `import_vehicle_model`.`vehicle_model_id`
				WHERE `import_vehicle`.`import_vehicle_type` = {$type} AND `import_vehicle`.`import_brand_id` = {$brand}
				GROUP BY `import_vehicle`.`vehicle_id`
				DESC";

		$query = $this->db->query($sql);		

		return $query->result_array();
	}

	public function importviewvehiclejoin3($type,$brand,$model) {
		$sql = "SELECT * FROM `import_vehicle` 
				LEFT JOIN `import_front_image` ON `import_vehicle`.`vehicle_id` = `import_front_image`.`vehicle_id`
				LEFT JOIN `import_vehicle_brand` ON `import_vehicle`.`import_brand_id` = `import_vehicle_brand`.`vehicle_brand_id`
				LEFT JOIN `import_vehical_type` ON `import_vehicle`.`import_vehicle_type` = `import_vehical_type`.`vehical_type_id`
				LEFT JOIN `import_vehicle_model` ON `import_vehicle`.`import_model_id` = `import_vehicle_model`.`vehicle_model_id`
				WHERE `import_vehicle`.`import_vehicle_type` = {$type} AND `import_vehicle`.`import_brand_id` = {$brand} AND `import_vehicle`.`import_model_id` = {$model}
				GROUP BY `import_vehicle`.`vehicle_id`
				DESC";

		$query = $this->db->query($sql);		

		return $query->result_array();
	}
	public function importcomparison() {
		$sql = "SELECT * FROM `import_vehicle` 
				LEFT JOIN `import_front_image` ON `import_vehicle`.`vehicle_id` = `import_front_image`.`vehicle_id`
				LEFT JOIN `import_vehicle_brand` ON `import_vehicle`.`import_brand_id` = `import_vehicle_brand`.`vehicle_brand_id`
				LEFT JOIN `import_vehical_type` ON `import_vehicle`.`import_vehicle_type` = `import_vehical_type`.`vehical_type_id`
				LEFT JOIN `import_vehicle_model` ON `import_vehicle`.`import_model_id` = `import_vehicle_model`.`vehicle_model_id`";

		$query = $this->db->query($sql);		

		return $query->result_array();
	}
	public function biksjoin()
    {
        $sql = "SELECT * FROM `threewheel_bike` 
  LEFT JOIN `threewheel_bike_brand` ON `threewheel_bike`.`threewheel_bike_brand_id` = `threewheel_bike_brand`.`threewheel_bike_brand_id`
  LEFT JOIN `threewheel_bike_type` ON `threewheel_bike`.`threewheel_bike_type_id` = `threewheel_bike_type`.`threewheel_bike_type_id`
  LEFT JOIN `threewheel_bike_model` ON `threewheel_bike`.`threewheel_bike_model_id` = `threewheel_bike_model`.`threewheel_bike_model_id`
  LEFT JOIN `threewheel_bike_customer` ON `threewheel_bike`.`threewheel_selling_customer_id` = `threewheel_bike_customer`.`threewheel_bike_customer_id`
  LEFT JOIN `agents` ON `threewheel_bike`.`agent_id` = `agents`.`agent_id`
  LEFT JOIN `threewheel_bike_image` ON `threewheel_bike`.`threewheel_bike_id` = `threewheel_bike_image`.`threewheel_bike_id`
  GROUP BY `threewheel_bike`.`threewheel_bike_id` DESC ";

        $query = $this->db->query($sql);

        return $query->result_array();
    }
    
    public function biksjointype($vehicle_type)
    {
    $sql = "SELECT * FROM `threewheel_bike` 
	LEFT JOIN `threewheel_bike_brand` ON `threewheel_bike`.`threewheel_bike_brand_id` = `threewheel_bike_brand`.`threewheel_bike_brand_id`
	LEFT JOIN `threewheel_bike_type` ON `threewheel_bike`.`threewheel_bike_type_id` = `threewheel_bike_type`.`threewheel_bike_type_id`
	LEFT JOIN `threewheel_bike_model` ON `threewheel_bike`.`threewheel_bike_model_id` = `threewheel_bike_model`.`threewheel_bike_model_id`
	LEFT JOIN `threewheel_bike_customer` ON `threewheel_bike`.`threewheel_selling_customer_id` = `threewheel_bike_customer`.`threewheel_bike_customer_id`
	LEFT JOIN `agents` ON `threewheel_bike`.`agent_id` = `agents`.`agent_id`
	LEFT JOIN `threewheel_bike_image` ON `threewheel_bike`.`threewheel_bike_id` = `threewheel_bike_image`.`threewheel_bike_id`
	WHERE `threewheel_bike`.`threewheel_bike_type_id` = {$vehicle_type}
	GROUP BY `threewheel_bike`.`threewheel_bike_id` DESC";

        $query = $this->db->query($sql);

        return $query->result_array();
	}
	
	public function biksjoinbrand($vehicle_type,$vehicle_brand)
    {
    $sql = "SELECT * FROM `threewheel_bike` 
	LEFT JOIN `threewheel_bike_brand` ON `threewheel_bike`.`threewheel_bike_brand_id` = `threewheel_bike_brand`.`threewheel_bike_brand_id`
	LEFT JOIN `threewheel_bike_type` ON `threewheel_bike`.`threewheel_bike_type_id` = `threewheel_bike_type`.`threewheel_bike_type_id`
	LEFT JOIN `threewheel_bike_model` ON `threewheel_bike`.`threewheel_bike_model_id` = `threewheel_bike_model`.`threewheel_bike_model_id`
	LEFT JOIN `threewheel_bike_customer` ON `threewheel_bike`.`threewheel_selling_customer_id` = `threewheel_bike_customer`.`threewheel_bike_customer_id`
	LEFT JOIN `agents` ON `threewheel_bike`.`agent_id` = `agents`.`agent_id`
	LEFT JOIN `threewheel_bike_image` ON `threewheel_bike`.`threewheel_bike_id` = `threewheel_bike_image`.`threewheel_bike_id`
	WHERE `threewheel_bike`.`threewheel_bike_type_id` = {$vehicle_type} AND `threewheel_bike`.`threewheel_bike_brand_id` = {$vehicle_brand}
	GROUP BY `threewheel_bike`.`threewheel_bike_id`;
	";

        $query = $this->db->query($sql);

        return $query->result_array();
	}
	
	public function biksjoinmodel($vehicle_type,$vehicle_brand,$vehicle_model)
    {
    $sql = "SELECT * FROM `threewheel_bike` 
	LEFT JOIN `threewheel_bike_brand` ON `threewheel_bike`.`threewheel_bike_brand_id` = `threewheel_bike_brand`.`threewheel_bike_brand_id`
	LEFT JOIN `threewheel_bike_type` ON `threewheel_bike`.`threewheel_bike_type_id` = `threewheel_bike_type`.`threewheel_bike_type_id`
	LEFT JOIN `threewheel_bike_model` ON `threewheel_bike`.`threewheel_bike_model_id` = `threewheel_bike_model`.`threewheel_bike_model_id`
	LEFT JOIN `threewheel_bike_customer` ON `threewheel_bike`.`threewheel_selling_customer_id` = `threewheel_bike_customer`.`threewheel_bike_customer_id`
	LEFT JOIN `agents` ON `threewheel_bike`.`agent_id` = `agents`.`agent_id`
	LEFT JOIN `threewheel_bike_image` ON `threewheel_bike`.`threewheel_bike_id` = `threewheel_bike_image`.`threewheel_bike_id`
	WHERE `threewheel_bike`.`threewheel_bike_type_id` = {$vehicle_type} AND `threewheel_bike`.`threewheel_bike_brand_id` = {$vehicle_brand} AND `threewheel_bike`.`threewheel_bike_model_id` = {$vehicle_model}
	GROUP BY `threewheel_bike`.`threewheel_bike_id`;
	";

        $query = $this->db->query($sql);

        return $query->result_array();
    }


    public function biksjoin1($id)
    {
        $sql = "SELECT * FROM `threewheel_bike` 
  LEFT JOIN `threewheel_bike_brand` ON `threewheel_bike`.`threewheel_bike_brand_id` = `threewheel_bike_brand`.`threewheel_bike_brand_id`
  LEFT JOIN `threewheel_bike_type` ON `threewheel_bike`.`threewheel_bike_type_id` = `threewheel_bike_type`.`threewheel_bike_type_id`
  LEFT JOIN `threewheel_bike_model` ON `threewheel_bike`.`threewheel_bike_model_id` = `threewheel_bike_model`.`threewheel_bike_model_id`
  LEFT JOIN `threewheel_bike_customer` ON `threewheel_bike`.`threewheel_selling_customer_id` = `threewheel_bike_customer`.`threewheel_bike_customer_id`
  LEFT JOIN `agents` ON `threewheel_bike`.`agent_id` = `agents`.`agent_id`
  LEFT JOIN `threewheel_bike_image` ON `threewheel_bike`.`threewheel_bike_id` = `threewheel_bike_image`.`threewheel_bike_id`
  WHERE `threewheel_bike`.`threewheel_bike_id` = {$id}

  ";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function wheeldetail($vehi)
    {
        $sql = "SELECT * FROM `threewheel_bike` 
    LEFT JOIN `threewheel_bike_image` ON `threewheel_bike`.`threewheel_bike_id` = `threewheel_bike_image`.`threewheel_bike_id`
  LEFT JOIN `threewheel_bike_brand` ON `threewheel_bike`.`threewheel_bike_brand_id` = `threewheel_bike_brand`.`threewheel_bike_brand_id`
  LEFT JOIN `threewheel_bike_type` ON `threewheel_bike`.`threewheel_bike_type_id` = `threewheel_bike_type`.`threewheel_bike_type_id`
  LEFT JOIN `threewheel_bike_model` ON `threewheel_bike`.`threewheel_bike_model_id` = `threewheel_bike_model`.`threewheel_bike_model_id`
  LEFT JOIN `threewheel_bike_customer` ON `threewheel_bike`.`threewheel_selling_customer_id` = `threewheel_bike_customer`.`threewheel_bike_customer_id`
  LEFT JOIN `agents` ON `threewheel_bike`.`agent_id` = `agents`.`agent_id`
  WHERE `threewheel_bike`.`threewheel_bike_id` = '{$vehi}' ";

        $query = $this->db->query($sql);

        return $query->result_array();
    }
    
     public function vehicleviewinspection($vehi)
    {
        $sql = "SELECT * FROM `vehicle` 
				LEFT JOIN `inside_images` ON `vehicle`.`vehicle_id` = `inside_images`.`vehicle_vehicle_id`
				LEFT JOIN `out_side_images` ON `vehicle`.`vehicle_id` = `out_side_images`.`vehicle_vehicle_id`
				LEFT JOIN `vehicle_brand` ON `vehicle`.`vehicle_brand` = `vehicle_brand`.`vehicle_brand_id`
				LEFT JOIN `vehicle_type` ON `vehicle`.`vehicle_type` = `vehicle_type`.`vehicle_type_id`
				LEFT JOIN `vehicle_model` ON `vehicle`.`vehicle_model` = `vehicle_model`.`vehicle_model_id`
				LEFT JOIN `agents` ON `vehicle`.`agent_id` = `agents`.`agent_id`
				WHERE `vehicle`.`vehicle_id` = '{$vehi}' ";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function wheelpublish($vehi)
    {
        $sql = "SELECT * FROM `threewheel_bike` 
    LEFT JOIN `threewheel_bike_image` ON `threewheel_bike`.`threewheel_bike_id` = `threewheel_bike_image`.`threewheel_bike_id`
  LEFT JOIN `threewheel_bike_brand` ON `threewheel_bike`.`threewheel_bike_brand_id` = `threewheel_bike_brand`.`threewheel_bike_brand_id`
  LEFT JOIN `threewheel_bike_type` ON `threewheel_bike`.`threewheel_bike_type_id` = `threewheel_bike_type`.`threewheel_bike_type_id`
  LEFT JOIN `threewheel_bike_model` ON `threewheel_bike`.`threewheel_bike_model_id` = `threewheel_bike_model`.`threewheel_bike_model_id`
  LEFT JOIN `threewheel_bike_customer` ON `threewheel_bike`.`threewheel_selling_customer_id` = `threewheel_bike_customer`.`threewheel_bike_customer_id`
  LEFT JOIN `agents` ON `threewheel_bike`.`agent_id` = `agents`.`agent_id`
  WHERE `threewheel_bike`.`threewheel_bike_id` = '{$vehi}' ";

        $query = $this->db->query($sql);

        return $query->result_array();
    }
     public function biksjoinnew() {
        $sql = "SELECT * FROM `threewheel_bike` 
		LEFT JOIN `threewheel_bike_brand` ON `threewheel_bike`.`threewheel_bike_brand_id` = `threewheel_bike_brand`.`threewheel_bike_brand_id`
		LEFT JOIN `threewheel_bike_type` ON `threewheel_bike`.`threewheel_bike_type_id` = `threewheel_bike_type`.`threewheel_bike_type_id`
		LEFT JOIN `threewheel_bike_model` ON `threewheel_bike`.`threewheel_bike_model_id` = `threewheel_bike_model`.`threewheel_bike_model_id`
		LEFT JOIN `threewheel_bike_customer` ON `threewheel_bike`.`threewheel_selling_customer_id` = `threewheel_bike_customer`.`threewheel_bike_customer_id`
		LEFT JOIN `agents` ON `threewheel_bike`.`agent_id` = `agents`.`agent_id`
		";

        $query = $this->db->query($sql);

        return $query->result_array();
    }


		public function popular_vehicles_home($week)
		{
			$sql = "SELECT * FROM vehicle_view_counter WHERE week='$week' ORDER BY count DESC LIMIT 6";
			$query = $this->db->query($sql);
			return $query->result_array();
			
		}

		public function popular_vehicles($week)
		{
			$sql = "SELECT * FROM vehicle_view_counter WHERE week='$week' ORDER BY count DESC";
			$query = $this->db->query($sql);
			return $query->result_array();
			
		}
}
	// function upload($inputname, $path) {
 //  		$config['upload_path'] = './upload/'.$path.'/'; // path where image will be saved
 //  		$config['allowed_types'] = 'gif|jpg|png|jpeg';
 //  		$config['max_size'] = 3000;
 //  		$this->upload->initialize($config);
 //  		$this->upload->do_upload($inputname);
 //  		$data_upload_files = $this->upload->data();

 //  		return $image = $data_upload_files['file_name'];  
 //  	}

 