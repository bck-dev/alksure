$(function() {

    vehiclemodelone();
    disableDropdownone();
    
    dropdownX();
    vehiclemodel1X();
    disableDropdownX();
    dropdown1X();
    vehicletypeoneX();
    vehiclemodeloneX();
    disableDropdownoneX();
    refreshAll();
	
    optypeSearch();
    opbrandSearch();
    opmodelSearch();
    dropdownOpeType();
    dropdownOpeBrand();
    disableDropdownOPTYPE();
    disableDropdownOPBRAND();
    refreshAllOL();
    
    wheeltypeSearch();
    wheelbrandSearch();
    wheelmodelSearch();
    dropdown3wheelType();
    dropdown3wheelBrand();
    disableDropdown3wheelTYPE();
    disableDropdown3wheelBRAND();
    refreshAll3wheel();
    
    //dropdownimportType();
    dropdownimportBrand();
    disableDropdownImportBrand();
    disableDropdownImportModel();
    //refreshAllimport();
    typeSearch();
    brandSearch();
    modelSearch();
    refreshAllImport();
   
dropdown();
// vehiclemodel21();
vehiclemodel1();
disableDropdown();

dropdown1();

    
    
    
    rentingVtype();
    rentingtrans();
    disableDropdownotrans();
    validateregisterform();
    change_forms();
    
    enablereguser();
    enablenotreguser();
    disablenotreguser();
    disablereguser();
    


});



function dropdownimportBrand(){
    $(document).on('change', '#importBrand', function(){
        //var baseurl = $_BASE_URL;
        // alert("yes");
        var x =$(this).find(':selected').attr('data-id');
        //alert(x);
        $("#importModel").children('option').hide();
        // alert()
        $("#importModel").children("option[class^=" + x + "]").show();

    });
}

//enable disabled brand dropdown
function disableDropdownImportBrand(){
 $(document).on('change', '#importType', function(){
    document.getElementById("importBrand").disabled = false;

 });

}

//enable disabled modeldropdown
function disableDropdownImportModel(){
 $(document).on('change', '#importBrand', function(){
    document.getElementById("importModel").disabled = false;

 });

}



function typeSearch(){

    $('#importType').on('change',function(){
        var type = ($(this).val());
        

        $.ajax({
            url:'http://autosure.lk/home/importtypeSearch',
            type:'post',
            data:{"vehi_type":type},

            success: function(results){
                $("#results-wrap").html(results)
            }
        });
    });
}

function brandSearch(){

    $('#importBrand').on('change',function(){
        var brand = ($(this).val());
        
        var type = $("#importType option:selected").val();
        

        $.ajax({
            url:'http://autosure.lk/home/importbrandSearch',
            type:'post',
            data:{"vehi_type":type, "vehi_brand":brand},

            success: function(results){
                $("#results-wrap").html(results)
            }
        });
    });
}

function modelSearch(){

    $('#importModel').on('change',function(){
        var model = ($(this).val());
        

        var type = $("#importType option:selected").val();
        

        var brand = $("#importBrand option:selected").val();
        

        $.ajax({
            url:'http://autosure.lk/home/importmodelSearch',
            type:'post',
            data:{"vehi_type":type, "vehi_brand":brand, "vehi_model":model},

            success: function(results){
                $("#results-wrap").html(results)
            }
        });
    });
}

function refreshAllImport(){
        $(document).on('change',function(){
        // var x =$(this).find(':selected').attr('data-id');

            var x = $("#importType").children("option").filter(":selected").data('id');
           // alert(x);
        if(x=='bb'){
            location.reload();
            //alert('All');
            // $('.result').show();
        }


        });

}


//================================================ operating leasing section ============================

//dependency type-brand
function dropdownOpeType(){
    $(document).on('change', '#opType', function(){
        //var baseurl = $_BASE_URL;
        // alert("yes");
        var x = "so"+$(this).find(':selected').attr('data-id')+"so";
        //alert(x);
        $("#opMake").children('option').hide();
        // alert()
        $("#opMake").children("option[class^=" + x + "]").show();

    });
}

//dependency brand-model
function dropdownOpeBrand(){
    $(document).on('change', '#opMake', function(){
        //var baseurl = $_BASE_URL;
        // alert("yes");
        var x = "so"+$(this).find(':selected').attr('data-id')+"so";
        //alert(x);
        $("#opModel").children('option').hide();
        // alert()
        $("#opModel").children("option[class^=" + x + "]").show();

    });
}

//enable disabled brand dropdown
function disableDropdownOPTYPE(){
 $(document).on('change', '#opType', function(){
    document.getElementById("opMake").disabled = false;

 });

}

//enable disabled model dropdown
function disableDropdownOPBRAND(){
 $(document).on('change', '#opMake', function(){
    document.getElementById("opModel").disabled = false;

 });

}

function refreshAllOL(){
        $(document).on('change',function(){
        // var x =$(this).find(':selected').attr('data-id');

            var x = $("#opType").children("option").filter(":selected").data('id');
           // alert(x);
        if(x=='bb'){
            location.reload();
            //alert('All');
            // $('.result').show();
        }


        });

}

function optypeSearch(){

    $('#opType').on('change',function(){
        var type = ($(this).val());
        //alert(type);

	$.ajax({
            url:'http://autosure.lk/home/opreatingType',
            type:'post',
            data:{"vehi_type":type},

            success: function(results){
                $("#results-wrap").html(results)
            }
        });
        
    });
}

function opbrandSearch(){

    $('#opMake').on('change',function(){
        var brand = ($(this).val());
        //alert(brand);
        var type = $("#opType option:selected").val();
        //alert(type);
        
        $.ajax({
            url:'http://autosure.lk/home/operatingBrand',
            type:'post',
            data:{"vehi_type":type, "vehi_brand":brand},

            success: function(results){
                $("#results-wrap").html(results)
            }
        });

        
    });
}

function opmodelSearch(){

    $('#opModel').on('change',function(){
        var model = ($(this).val());
        //alert(model);

        var type = $("#opType option:selected").val();
        //alert(type);

        var brand = $("#opMake option:selected").val();
        //alert(brand);

	$.ajax({
            url:'http://autosure.lk/home/opertingModel',
            type:'post',
            data:{"vehi_type":type, "vehi_brand":brand, "vehi_model":model},

            success: function(results){
                $("#results-wrap").html(results)
            }
        });
        
    });
}
//================================================

//===============================3wheel and bike section dropdown search =================

//dependency type-brand
function dropdown3wheelType(){
    $(document).on('change', '#vehicle_type3wheel', function(){
        //var baseurl = $_BASE_URL;
        // alert("yes");
        var x =$(this).find(':selected').attr('data-id');
        //alert(x);
        $("#vehicle_brand3wheel").children('option').hide();
        // alert()
        $("#vehicle_brand3wheel").children("option[class^=" + x + "]").show();

    });
}

//dependency brand-model
function dropdown3wheelBrand(){
    $(document).on('change', '#vehicle_brand3wheel', function(){
        //var baseurl = $_BASE_URL;
        // alert("yes");
        var x =$(this).find(':selected').attr('data-id');
        //alert(x);
        $("#vehicle_model3wheel").children('option').hide();
        // alert()
        $("#vehicle_model3wheel").children("option[class^=" + x + "]").show();

    });
}

//enable disabled brand dropdown
function disableDropdown3wheelTYPE(){
 $(document).on('change', '#vehicle_type3wheel', function(){
    document.getElementById("vehicle_brand3wheel").disabled = false;

 });

}

//enable disabled model dropdown
function disableDropdown3wheelBRAND(){
 $(document).on('change', '#vehicle_brand3wheel', function(){
    document.getElementById("vehicle_model3wheel").disabled = false;

 });

}

function refreshAll3wheel(){
        $(document).on('change',function(){
        // var x =$(this).find(':selected').attr('data-id');

            var x = $("#vehicle_type3wheel").children("option").filter(":selected").data('id');
        //    alert(x);
        if(x=='aa'){
            location.reload();
            //alert('All');
            // $('.result').show();
        }


        });

}

function wheeltypeSearch(){

    $('#vehicle_type3wheel').on('change',function(){
        var type = ($(this).val());
        //alert(type);

	$.ajax({
            url:'http://autosure.lk/home/bikeType',
            type:'post',
            data:{"vehi_type":type},

            success: function(results){
                $("#results-wrap").html(results)
            }
        });
        
    });
}

function wheelbrandSearch(){

    $('#vehicle_brand3wheel').on('change',function(){
        var brand = ($(this).val());
        //alert(brand);
        var type = $("#vehicle_type3wheel option:selected").val();
        //alert(type);
        
        $.ajax({
            url:'http://autosure.lk/home/bikeBrand',
            type:'post',
            data:{"vehi_type":type, "vehi_brand":brand},

            success: function(results){
                $("#results-wrap").html(results)
            }
        });

        
    });
}

function wheelmodelSearch(){

    $('#vehicle_model3wheel').on('change',function(){
        var model = ($(this).val());
        //alert(model);

        var type = $("#vehicle_type3wheel option:selected").val();
        //alert(type);

        var brand = $("#vehicle_brand3wheel option:selected").val();
        //alert(brand);

	$.ajax({
            url:'http://autosure.lk/home/bikeModel',
            type:'post',
            data:{"vehi_type":type, "vehi_brand":brand, "vehi_model":model},

            success: function(results){
                $("#results-wrap").html(results)
            }
        });
        
    });
}

//=============================end function ==============================================



function dropdown(){
    $(document).on('change', '#vehicle_brand', function(){
        //var baseurl = $_BASE_URL;
        var x = "so"+$(this).find(':selected').attr('data-id')+"so";
        // alert(x);
        $("#select2").children('option').hide();
        // alert()
        $("#select2").children("option[class^=" + x + "]").show();

    });
}

function vehiclemodel1(){
    $(document).on('change', '#select2', function(){
        //var baseurl = $_BASE_URL;
        var x =$(this).find(':selected').attr('data-id');
        $(".result").hide();
        $("." + x ).show();

    });
}

function disableDropdown(){
 $(document).on('change', '#vehicle_brand', function(){
    document.getElementById("select2").disabled = false;

 });

}


function dropdown1(){
    $(document).on('change', '#ddlContent', function(){
        //var baseurl = $_BASE_URL;
        // alert("yes");
        var x = "so"+$(this).find(':selected').attr('data-id')+"so";
        // alert(x);
        $("#vehicle_brand").children('option').hide();
        // alert()
        $("#vehicle_brand").children("option[class^=" + x + "]").show();

    });
}

// function dropdown1(){
// 	$(document).on('change', '#ddlContent', function(){
//         $('#vehicle_brand option:first').prop('selected', 'selected');
//         if ($(this).val().length > 0) {
//             $("#vehicle_brand").prop("disabled", false);
//         }
//         else{
//             $("#vehicle_brand").prop("disabled", true);
//         }
//     });
    
//      $(document).on('change', '#ddlContent', function(){
//         $('#vehicle_brand option:first').prop('selected', 'selected');
//         if ($(this).val().length > 0){
//             var b_name = "so" + $(this).val() + "so";
//             $("#vehicle_brand").prop("disabled", false);
//             $("#vehicle_brand").children('option').hide();
//             $("#vehicle_brand").children("option[class^=" + b_name + "]").show();
//         }
//         else{
//             $("#vehicle_brand").prop("disabled", true);
//         }
//     }); 

//      $(document).on('change', '#vehicle_brand', function(){
//         $('#select2 option:first').prop('selected', 'selected');
//         if ($(this).val().length > 0){
//             var b_name = "so" + $(this).val() + "so";
//             $("#select2").prop("disabled", false);
//             $("#select2").children('option').hide();
//             $("#select2").children("option[class^=" + b_name + "]").show();
//         }
//         else{
//             $("#select2").prop("disabled", true);
//         }
//     }); 
// }

// $(document).on('change', '#typeSelect', function(){
//         $('#makeSelect option:first').prop('selected', 'selected');
//         if ($(this).val().length > 0) {
//             $("#makeSelect").prop("disabled", false);
//         }
//         else{
//             $("#makeSelect").prop("disabled", true);
//         }
//     });

//     $(document).on('change', '#makeSelect', function(){
//         $('#modelSelect option:first').prop('selected', 'selected');
//         if ($(this).val().length > 0){
//             var b_name = "brand" + $(this).val() + "id";
//             $("#modelSelect").prop("disabled", false);
//             $("#modelSelect").children('option').hide();
//             $("#modelSelect").children("option[class^=" + b_name + "]").show();
//             //alert(b_name);
//         }
//         else{
//             $("#modelSelect").prop("disabled", true);
//         }
//     });

function vehiclemodelone(){
    $(document).on('change', '#vehicle_brand', function(){
        //var baseurl = $_BASE_URL;
        var x =$(this).find(':selected').attr('data-id');
        $(".result").hide();
        $("." + x ).show();

    });
}

function disableDropdownone(){
    $(document).on('change', '#ddlContent', function(){
        document.getElementById("vehicle_brand").disabled = false;

    });

}

//dependancy dropdown brand-model
function dropdownX(){
    $(document).on('change', '#vehicle_brand1', function(){
        //var baseurl = $_BASE_URL;
        // alert("yes");
        var x = "aa"+$(this).find(':selected').attr('data-id')+"aa";
        // alert(x);
        $("#select1").children('option').hide();
        // alert()
        $("#select1").children("option[class^=" + x + "]").show();

    });
}

function vehiclemodel1X(){
    $('#select1').on('change',function(){
        var model = ($(this).val());
        var type = $("#vehicle_type1 option:selected").val();

        var brand = $("#vehicle_brand1 option:selected").val();
        
        $.ajax({
            url:'http://autosure.lk/home/modelSearch',
            type:'post',
            data:{"vehi_brand":brand, "vehi_type":type, "vehi_model":model},

            success: function(results){
                $("#results-wrap").html(results)
            }
        });
    });
}

// enable model drop down
function disableDropdownX(){
    $(document).on('change', '#vehicle_brand1', function(){
        document.getElementById("select1").disabled = false;

    });

}

//denpendancy dropdown type-brand
function dropdown1X(){
    $(document).on('change', '#vehicle_type1', function(){
        //var baseurl = $_BASE_URL;

        var x = "aa"+$(this).find(':selected').attr('data-id')+"aa";
        // alert(x);
        $("#vehicle_brand1").children('option').hide();
        // alert()
        $("#vehicle_brand1").children("option[class^=" + x + "]").show();

    });
}

function vehicletypeoneX(){
    $('#vehicle_type1').on('change',function(){
        var type = ($(this).val());
        
        $.ajax({
            url:'http://autosure.lk/home/typeSearch',
            type:'post',
            data:{"vehi_type":type},

            success: function(results){
                $("#results-wrap").html(results)
            }
        });
    });
}

// brand filteration
function vehiclemodeloneX(){
    $('#vehicle_brand1').on('change',function(){
        var brand = ($(this).val());
        var type = $("#vehicle_type1 option:selected").val();
      
        $.ajax({
            url:'http://autosure.lk/home/brandSearch',
            type:'post',
            data:{"vehi_brand":brand, "vehi_type":type},

            success: function(results){
                $("#results-wrap").html(results)
            }
        });
    });
}

// enable brand dropdown
function disableDropdownoneX(){
    $(document).on('change', '#vehicle_type1', function(){
        document.getElementById("vehicle_brand1").disabled = false;

    });

}

function refreshAll(){
        $(document).on('change',function(){
        // var x =$(this).find(':selected').attr('data-id');

            var x = $("#vehicle_type1").children("option").filter(":selected").data('id');
           // alert(x);
        if(x=='aa'){
            location.reload();
            //alert('All');
            // $('.result').show();
        }


        });

}

function disableDropdownotrans(){
    $(document).on('change', '#vehicle_typeR', function(){
        document.getElementById("transmiss1").disabled = false;

    });

}

// renting vehicle type search
function rentingVtype(){
    $('#vehicle_typeR').on('change',function(){
        var Rtype = ($(this).val());
       // alert(Rtype);
        $.ajax({
            url:'http://autosure.lk/home/rentingType1',
            type:'post',
            data:{"renting_type":Rtype},

            success: function(results){
                $("#results-wrap").html(results)
            }
        });
    });
}

// renting transmision search
function rentingtrans(){
    $('#transmiss1').on('change',function(){
        var trans1 = ($(this).val());
        var Rtype = $("#vehicle_typeR option:selected").val();
        //alert(trans1);
        //alert(Rtype);
        $.ajax({
            url:'http://autosure.lk/home/rentingTransmi',
            type:'post',
            data:{"renting_type":Rtype, "transs":trans1},

            success: function(results){
                $("#results-wrap").html(results)
            }
        });
    });
}


function validateregisterform() {
    $('#register-form').validate({
        rules: {
            first_name: "required",
            last_name: "required",
            address: "required",
            regpassword: "required",
            cconfirm_password: "required",

            nic: "required",
            number: {
                required: true,
                number: true,
                maxlength: 10,
                minlength: 10
            },
            regemail: {
                required: true,
                email: true
            }


        },
        messages: {
            first_name: "Please enter the first name ",
            last_name: "Please enter the last name",
            address: "Please enter the address",
            regpassword: "Please enter the password",
            cconfirm_password: "Please enter the confirm password",
            nic: "Please enter the NIC no",
            number: {
                required: "Please enter the phone number",
                number: "Please enter the valide phone number",
                maxlength: "Phone shouldn't be more the 10 digits",
                minlength: "Phone shouldn't be less the 10 digits"
            },
            regemail: {
                required: "Please enter the email",
                email: "Please enter the vaild email"
            }

        },
        submitHandler: function() {
            registeruser();
            // alert('hi');
        }
    });

}




function registeruser() {

    var base_url = "http://autosure.lk/";

    var password1 =  $('#regpassword').val();
    var confirm_password = $('#cconfirm_password').val();
    if(password1 == confirm_password) {


        var data = {
            'fname': $('#first_name').val(),
            'lname': $('#last_name').val(),
            'address': $('#address').val(),
            'password': $('#regpassword').val(),
            'confirm_password': $('#cconfirm_password').val(),
            'nic': $('#nic').val(),
            'number': $('#number').val(),
            'email': $('#regemail').val()

        };





        $.ajax({
            type: "POST",
            url: base_url + "login_user/register",
            //dataType: "JSON",
            data: data,
            success: function(result) {
                $('#register-create-msg').html(result);
                document.getElementById("register-form").reset();
                location.reload();

                 

            }


        });

       



    }else{
        $.ajax({
            type: "POST",
            url: base_url + "login_user/error",
            //dataType: "JSON",
            data: data,
            success: function(result) {
                $('#password-match').html(result);
                
            }

        });
        // alert('password not matching')
    }
}

$("input.numberss").keypress(function(event) {
  return /\d/.test(String.fromCharCode(event.keyCode));
});

function change_forms(){
    $(document).ready(function(){
        $('.reggisterednot').on('click',function(){
            $('.selform1').hide();
            $('.selform2').hide();
        });

        $('.registeruser').on('click',function(){
            if ($('.registeruser').is(":checked")) {
                $('.selform1').show();
                $('.selform2').hide();
            }
            else {
                $(".selform1").hide();
                $(".selform2").hide();
            }
        });

        $('.notregisteruser').on('click',function(){
            if ($('.notregisteruser').is(":checked")) {
                $('.selform2').show();
                $('.selform1').hide();
            }
            else {
                $(".selform1").hide();
                $(".selform2").hide();
            }
        });
    });
}





function enablereguser() {

    $('.registeruser').click(function() {
        $("#passwordselluser").prop("disabled",true);
        if($(this).attr('id') == 'registeruser') {
            $("#passwordselluser").prop("disabled",false);
        }
    });
}

function disablenotreguser() {

    $('.notregisteruser').click(function() {
        $("#passwordselluser").prop("disabled",true);

    });
}


function enablenotreguser() {

    $('.notregisteruser').click(function() {
        $(".passwordsell").prop("disabled",true);
        if($(this).attr('id') == 'registeruser') {
            $(".passwordsell").prop("disabled",false);
        }
    });
}
function disablereguser() {

    $('.registeruser').click(function() {
        $(".passwordsell").prop("disabled",true);

    });
}

$('#password1, #confirm_password1').on('keyup', function () {
  if ($('#password1').val() == $('#confirm_password1').val()) {
    $('#message').html('Matching').css('color', 'green');
  } else 
    $('#message').html('Not Matching').css('color', 'red');
});