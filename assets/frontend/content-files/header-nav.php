<!--top header-->
                    <div class="da-da AppMainHeaderBox">
                        <div class="da-da wh100">
                            <div class="da-da wh100">
                            <!--top layer-->
                            <div class="da-da AppMainHeaderBox-top">
                                <div class="da-da wh100">
                                    <div class="da-da wh100">
                                    <!--container-->
                                    <div class="da-da container">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="da-da wh100">
                                                    <div class="da-da wh100">
                                                    <!--sign in and sign up options-->
                                                    <div class="da-da AppMainHeaderBox-top-sinout">
                                                        <div class="da-da wh100">
                                                            <div class="da-da wh100 displayFlex">
                                                                <ul class="da-da wh100 AppMainHeaderBox-top-sinoutUl">
                                                                    <li>
                                                                        <span>
                                                                        <a href="#">
                                                                        Register    
                                                                        </a>
                                                                        </span>
                                                                    </li>
                                                                    <li>
                                                                        <span>
                                                                        <a href="#" class="da-da IsSignin">
                                                                        Sign In
                                                                        </a>
                                                                        </span>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--sign in and sign up options-->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--container-->
                                    </div>
                                </div>
                            </div>
                            <!--top layer-->
                                
                            <!--bottom menu-->
                            <div class="da-da AppMainHeaderBox-bottom">
                                <div class="da-da wh100">
                                    <div class="da-da wh100">
                                    
                                    <!--container-->
                                    <div class="da-da container">
                                        <div class="da-da wh100">
                                            <div class="da-da wh100">
                                            
                                                <div class="row">
                                                    <!--left-->
                                                    <div class="da-da col-md-5">
                                                        <div class="da-da wh100">
                                                            <div class="da-da wh100">
                                                                <ul class="da-da wh100 mainMenuBarsApp_navigation">
                                                                    <li>
                                                                        <a href="#">
                                                                            <span>
                                                                            Trade-in
                                                                            </span>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">
                                                                            <span>
                                                                            Operating Lease
                                                                            </span>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">
                                                                            <span>
                                                                            Rent
                                                                            </span>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--left-->
                                                    
                                                    <!--center-->
                                                    <div class="da-da col-md-2">
                                                        <div class="da-da wh100">
                                                            <div class="da-da wh100">
                                                            <a href="<?php echo $defaultlink;?>">
                                                            <img src="<?php  echo $defaultlink; ?>assets/logo/brand-logo.png" class="da-da mainLogo">
                                                            </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--center-->
                                                    
                                                    <!--riht-->
                                                    <div class="da-da col-md-5">
                                                        <div class="da-da wh100">
                                                            <div class="da-da wh100">
                                                                <ul class="da-da wh100 mainMenuBarsApp_navigation">
                                                                    <li>
                                                                        <a href="#">
                                                                            <span>
                                                                            Importing
                                                                            </span>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">
                                                                            <span>
                                                                            Services
                                                                            </span>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">
                                                                            <span>
                                                                            Contact us
                                                                            </span>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--riht-->
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <!--container-->
                                    
                                    
                                    </div>
                                </div>
                            </div>
                            <!--bottom menu-->
                                
                                
                            <!--mobile menu-->
                            <div class="da-da AppMainHeaderBox-MobileNav">
                                <div class="da-da wh100 container">
                                    <div class="da-da wh100 displayFlex">
                                    <!--left logo-->
                                    <div class="da-da AppMainHeaderBox-MobileNavLogo" >
                                        <div class="da-da wh100">
                                            <div class="da-da wh100">
                                            <img src="assets/logo/brand-logo.png">
                                            </div>
                                        </div>
                                    </div>
                                    <!--left logo-->
                                    
                                    <!--right menu icon-->
                                    <div class="da-da AppMainHeaderBox-MobileNavIcon">
                                        <div class="da-da wh100"    >
                                            <div class="da-da wh100">
                                                <nav class="navbar navbar-expand-lg navbar-light bg-light">

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Dropdown
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      
    </ul>
    
  </div>
</nav>
                                            </div>
                                        </div>
                                    </div>
                                    <!--right menu icon-->
                                    </div>
                                </div>
                            </div>
                            <!--mobile menu-->
                            </div>
                        </div>
                    </div>
                    <!--top header-->   




<!--pop up one-->
<div class="da-da AppPopupDefault wh100" id="popup1selling">
    <div class="da-da wh100" style="
    margin-bottom: 69px;
    position: relative;
">
        <div class="da-da wh100" style="
    overflow: auto;
    position: absolute;
    margin-bottom: 90px;
">
            <!--container-->
            <div class="da-da container">
                <div class="da-da wh100">
                    <div class="da-da wh100">
                    
                        
                    <!--pop up starts-->
                    <div class="da-da ppupStarts-2">
                        <div class="da-da wh100">
                            <div class="da-da wh100">
                                
                                <!--close-->
                                <span class="closepupI1" id="popup1sellingClose">x</span>
                                <!--close-->
                                
                                
                                <!--body starts-->
                                <div class="da-da ppupStarts-2BodyStarts">
                                    <div class="da-da wh100">
                                        <div class="da-da wh100 ppupStarts-2BodyStartshl displayFlex">
                                        <!--left-->
                                        <div class="da-da ppupStarts-2BodyStartsLeft">
                                            <div class="da-da wh100"    >
                                                <div class="da-da wh100">
                                                    
                                                    <!--title-->
                                                    <div class="da-da ppupStarts-2BodyStartsTitleMain">
                                                        <div class="da-da wh100">
                                                            <div class="da-da wh100">
                                                                <h3>
                                                                Vehicle Information
                                                                </h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--title-->
                                                    
                                                    <!--description-->
                                                    <div class="da-da ppupStarts-2BodyStartsDescription">
                                                        <div class="da-da wh100">
                                                            <div class="da-da wh100">
                                                            <p>
                                                                Please note that this listing will not be published until the information provided are verified by our experts in order to maintain the quality of our service.

                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--description-->
                                                    
                                                    <!--field one-->
                                                    <div class="da-da ppupStarts-2BodyStartsField">
                                                        <div class="da-da wh100">
                                                            <div class="da-da wh100 displayFlex">
                                                            <!--left-->
                                                            <div class="da-da w50px rm10">
                                                             <select class="da-da wh100 inputFieldIsonForm inputFieldIsonFormInPup">
                                                                            <option>Type</option>
                                                                        </select>       
                                                            </div>
                                                            <!--left-->
                                                                
                                                            <!--right-->
                                                            <div class="da-da w50px lm10">
                                                                    
                                                            </div>
                                                            <!--right-->
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--field one-->
                                                    
                                                    
                                                    <!--field one-->
                                                    <div class="da-da ppupStarts-2BodyStartsField">
                                                        <div class="da-da wh100">
                                                            <div class="da-da wh100 displayFlex">
                                                            <!--left-->
                                                            <div class="da-da w50px rm10">
                                                             <select class="da-da wh100 inputFieldIsonForm inputFieldIsonFormInPup">
                                                                            <option>Make</option>
                                                                        </select>       
                                                            </div>
                                                            <!--left-->
                                                                
                                                            <!--right-->
                                                            <div class="da-da w50px lm10">
                                                                 <select class="da-da wh100 inputFieldIsonForm inputFieldIsonFormInPup">
                                                                            <option>Model</option>
                                                                        </select>     
                                                            </div>
                                                            <!--right-->
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--field one-->
                                                    
                                                    <!--field one-->
                                                    <div class="da-da ppupStarts-2BodyStartsField">
                                                        <div class="da-da wh100">
                                                            <div class="da-da wh100 displayFlex">
                                                            <!--left-->
                                                            <div class="da-da w50px rm10">
                                                             <select class="da-da wh100 inputFieldIsonForm inputFieldIsonFormInPup">
                                                                            <option>Fuel Type</option>
                                                                        </select>       
                                                            </div>
                                                            <!--left-->
                                                                
                                                            <!--right-->
                                                            <div class="da-da w50px lm10">
                                                                    <select class="da-da wh100 inputFieldIsonForm inputFieldIsonFormInPup">
                                                                            <option>Transmission</option>
                                                                        </select>  
                                                            </div>
                                                            <!--right-->
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--field one-->
                                                    
                                                    <!--field one-->
                                                    <div class="da-da ppupStarts-2BodyStartsField">
                                                        <div class="da-da wh100">
                                                            <div class="da-da wh100 displayFlex">
                                                            <!--left-->
                                                            <div class="da-da w50px rm10">
                                                             <select class="da-da wh100 inputFieldIsonForm inputFieldIsonFormInPup">
                                                                            <option>Year of Make</option>
                                                                        </select>       
                                                            </div>
                                                            <!--left-->
                                                                
                                                            <!--right-->
                                                            <div class="da-da w50px lm10">
                                                                   <select class="da-da wh100 inputFieldIsonForm inputFieldIsonFormInPup">
                                                                            <option>Year of Register</option>
                                                                        </select>   
                                                            </div>
                                                            <!--right-->
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--field one-->
                                                    
                                                    <!--field one-->
                                                    <div class="da-da ppupStarts-2BodyStartsField">
                                                        <div class="da-da wh100">
                                                            <div class="da-da wh100 displayFlex">
                                                            <!--left-->
                                                            <div class="da-da w50px rm10">
                                                             <input class="da-da wh100 inputFieldIsonForm inputFieldIsonFormInPup" placeholder="Registration No.">
                                                                                
                                                            </div>
                                                            <!--left-->
                                                                
                                                            <!--right-->
                                                            <div class="da-da w50px lm10">
                                                                    <select class="da-da wh100 inputFieldIsonForm inputFieldIsonFormInPup">
                                                                            <option>Owners</option>
                                                                        </select>  
                                                            </div>
                                                            <!--right-->
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--field one-->
                                                    
                                                    <!--field one-->
                                                    <div class="da-da ppupStarts-2BodyStartsField">
                                                        <div class="da-da wh100">
                                                            <div class="da-da wh100 displayFlex">
                                                            <!--left-->
                                                            <div class="da-da w50px rm10">
                                                             <input class="da-da wh100 inputFieldIsonForm inputFieldIsonFormInPup" placeholder="Engine Capacity">       
                                                            </div>
                                                            <!--left-->
                                                                
                                                            <!--right-->
                                                            <div class="da-da w50px lm10">
                                                                   <input class="da-da wh100 inputFieldIsonForm inputFieldIsonFormInPup" placeholder="Miliage">    
                                                            </div>
                                                            <!--right-->
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--field one-->
                                                    
                                                    
                                                    <!--field one-->
                                                    <div class="da-da ppupStarts-2BodyStartsField">
                                                        <div class="da-da wh100">
                                                            <div class="da-da wh100 displayFlex">
                                                            <!--left-->
                                                            <div class="da-da w50px rm10">
                                                             <input class="da-da wh100 inputFieldIsonForm inputFieldIsonFormInPup" placeholder="Price">   
                                                            </div>
                                                            <!--left-->
                                                                
                                                            <!--right-->
                                                            <div class="da-da w50px lm10">
                                                                   
                                                            </div>
                                                            <!--right-->
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--field one-->
                                                    
                                                    
                                                    <!--field one-->
                                                    <div class="da-da ppupStarts-2BodyStartsField" style="
    padding-top: 25px;
    border-top: 1px solid #e3e3e3;
    margin-top: 35px;
    padding-bottom: 0px;
    border-bottom: 1px solid #e3e3e3;
    position: relative;
">
                                                        <div class="da-da wh100">
                                                            <div class="da-da wh100 displayFlex">
                                                            <!--left-->
                                                            <div class="da-da w50px rm10" style="
    width: 29%;
    white-space: nowrap;
    margin: 0;
">
                                                             <div class="da-da uiIsApplinfile" style="
    position: relative;
    min-height: 60px;
">
                                                                 <span class="uiIsApplinfileSpan">Add photos</span>
                                                                <input type="file" class="uiIsApplinfileActu">
                                                                </div>
                                                            </div>
                                                            <!--left-->
                                                                
                                                            <!--right-->
                                                            <div class="da-da w50px lm10" style="
    width: 71%;
    margin: 0;
">
                                                                <span class="classIsuldText">Upload upto 5 pictures</span>   
                                                            </div>
                                                            <!--right-->
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--field one-->
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <!--left-->
                                            
                                        <!--right-->
                                        <div class="da-da ppupStarts-2BodyStartsRight">
                                            <div class="da-da wh100"    >
                                                <div class="da-da wh100">
                                                    <!--title-->
                                                    <div class="da-da ppupStarts-2BodyStartsTitleMain">
                                                        <div class="da-da wh100">
                                                            <div class="da-da wh100">
                                                                <h3>
                                                                Vehicle Information
                                                                </h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--title-->
                                                    
                                                    <!--description-->
                                                    <div class="da-da ppupStarts-2BodyStartsDescription">
                                                        <div class="da-da wh100">
                                                            <div class="da-da wh100">
                                                            <p>
                                                               Below information will be only used by our experts to contact you and will not be published or shared with outside parties.

                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--description-->
                                                    
                                                    <!--field one-->
                                                    <div class="da-da ppupStarts-2BodyStartsField">
                                                        <div class="da-da wh100">
                                                            <div class="da-da wh100 displayFlex">
                                                            <!--left-->
                                                            <div class="da-da w100">
                                                             <input class="da-da wh100 inputFieldIsonForm inputFieldIsonFormInPup" placeholder="Full Name">   
                                                            </div>
                                                            <!--left-->
                                                                
                                                           
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--field one-->
                                                    
                                                    <!--field one-->
                                                    <div class="da-da ppupStarts-2BodyStartsField">
                                                        <div class="da-da wh100">
                                                            <div class="da-da wh100 displayFlex">
                                                            <!--left-->
                                                            <div class="da-da w100">
                                                             <input class="da-da wh100 inputFieldIsonForm inputFieldIsonFormInPup" placeholder="Telephone">   
                                                            </div>
                                                            <!--left-->
                                                                
                                                           
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--field one-->
                                                    
                                                    <!--field one-->
                                                    <div class="da-da ppupStarts-2BodyStartsField">
                                                        <div class="da-da wh100">
                                                            <div class="da-da wh100 displayFlex">
                                                            <!--left-->
                                                            <div class="da-da w100">
                                                             <input class="da-da wh100 inputFieldIsonForm inputFieldIsonFormInPup" placeholder="E-mail">   
                                                            </div>
                                                            <!--left-->
                                                                
                                                           
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--field one-->
                                                    
                                                    <!--field one-->
                                                    <div class="da-da ppupStarts-2BodyStartsField">
                                                        <div class="da-da wh100">
                                                            <div class="da-da wh100 displayFlex">
                                                            <!--left-->
                                                            <div class="da-da w100">
                                                             <input class="da-da wh100 inputFieldIsonForm inputFieldIsonFormInPup" placeholder="Address Line 1">   
                                                            </div>
                                                            <!--left-->
                                                                
                                                           
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--field one-->
                                                    
                                                    <!--field one-->
                                                    <div class="da-da ppupStarts-2BodyStartsField">
                                                        <div class="da-da wh100">
                                                            <div class="da-da wh100 displayFlex">
                                                            <!--left-->
                                                            <div class="da-da w100">
                                                             <input class="da-da wh100 inputFieldIsonForm inputFieldIsonFormInPup" placeholder="Address Line 2">   
                                                            </div>
                                                            <!--left-->
                                                                
                                                           
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--field one-->
                                                    
                                                    
                                                    
                                                    <!--field one-->
                                                    <div class="da-da ppupStarts-2BodyStartsField">
                                                        <div class="da-da wh100">
                                                            <div class="da-da wh100 displayFlex">
                                                            <!--left-->
                                                            <div class="da-da w100">
                                                             <select class="da-da wh100 inputFieldIsonForm inputFieldIsonFormInPup">
                                                                            <option>City</option>
                                                                        </select>  
                                                            </div>
                                                            <!--left-->
                                                                
                                                           
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--field one-->
                                                    
                                                    
                                                    <!--field one-->
                                                    <div class="da-da ppupStarts-2BodyStartsField" >
                                                        <div class="da-da wh100">
                                                            <div class="da-da wh100 displayFlex">
                                                            <!--left-->
                                                            <div class="da-da w50px rm10" >
                                                             <div class="da-da uiIsApplinfile" >
                                                                 <input type="button" class="da-da InputFieldAppIndexBtn IssearchInputFieldAppIndexBtn w100 BtnIsSubmit" value="Submit">
                                                                </div>
                                                            </div>
                                                            <!--left-->
                                                                
                                                            <!--right-->
                                                            <div class="da-da w50px lm10" style="
    width: 71%;
    margin: 0;
">
                                                                
                                                            </div>
                                                            <!--right-->
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--field one-->
                                                    
                                                    
                                                    <!--description-->
                                                    <div class="da-da ppupStarts-2BodyStartsDescription desctinSmalPopfrmBtm">
                                                        <div class="da-da wh100">
                                                            <div class="da-da wh100">
                                                            <p>
                                                               One of our agents will contact you within 3-4 working days between 9.00am - 5.00pm.

                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--description-->
                                                    
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <!--right-->
                                        </div>
                                    </div>
                                </div>
                                <!--body starts-->
                            </div>
                        </div>
                    </div>
                    <!--pop up starts-->
                        
                        
                    </div>
                </div>
            </div>
            <!--container-->
        </div>
    </div>
</div>
<!--pop up one-->