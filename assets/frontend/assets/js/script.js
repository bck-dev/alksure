$(function() {
    //import lease vehicle slider
	importLeaseVehicleSlider();
	loadCss('.home-body', 'custom.css');
	//lease calculator
	leaseCalculator();
	//vehicleidcheck();
	//enable lease calculator on lease select
	enableCaculator();

	/* responsive comparison table config initialize */
	var TablesawConfig = {
		i18n: {
			swipePreviousColumn: "The column before",
			swipeNextColumn: "The column after"
		},
		swipe: {
			horizontalThreshold: 45,
			verticalThreshold: 45
		}
	};

	/* operating filter bars */
	operatingLeaseSearchBars();
	
	/* booking date */
	datePicker();
	
	
	//scroll fixed filter column
	if($('.floating-section .results-container').length > 0) {
		scrollFixedColumn();
	}
	
	//operating leasing slider 
	vehicleSliderDuration();

});

//Search products in product page
function vehicleSliderDuration(){
	var baseurl = 'http://autosure.lk/';
	$(document).on('change', '.filtrationSlider' ,function(){
		var type = $('#opType').val();
		var make = $('#opMake').val();
		var model = $('#opModel').val();
		var duration = $('#duration').val();
		var downpayment = $('#operateLease').val();
		//alert(type + '-' + make + '-' + model);
		$.ajax({
         url: baseurl + 'home/ajaxsearchduration', //http://localhost/autosurefull/oparating_leasing/ajaxsearch
         type: 'post',
         data: {'downpayment':downpayment, 'duration':duration, 'type':type, 'make':make, 'model':model},
         success: function(msg) {
         	$("#resultId").html(msg);
         	var numItems = $('.result').length;
         	$('#resultCount').html(numItems);
         	if(numItems == 1){
         		$('#vehicleNumber').html(' vehicle');
         	}
         	else{
         		$('#vehicleNumber').html(' vehicles');
         	}
             //$('#js-rangeslider-2').children('.rangeslider__fill').css('width', '10px');
             //$('#js-rangeslider-2').children('.rangeslider__handle').css('left', '0px');
         }
     });
	});
}
//End search products in product page

/* import lease vehicle slider */
function importLeaseVehicleSlider() {
	var owl = $('.import-lease-vehicle-slider');

	owl.owlCarousel({
		items: 1,
		margin: 20,
		autoplay: true,
		loop: false,
		animateIn: 'fadeIn',
		animateOut: 'fadeOut',
		thumbs: true,
		thumbsPrerendered: true,
		responsive: {
			0: {
				items: 1
			}
		}			
	});	
}

//price number format
function numberWithCommas(x) {
	var parts = x.toString().split(".");
	parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	return parts.join(".");
}

// lease amount bar
function leaseCalculator() {

	var lease, period;

	$('.lease-amount-bar, .duration-bar').rangeslider({
		polyfill: false,
		
		// Callback function
		onInit: function(value) {
			var value = $('.lease-amount-bar').val();
			var months = $('.duration-bar').val();

			lease = value;
			period = parseInt(months, 10);

			$('.lease-amount-bar').parent().find('.selected-value .amount').text(numberWithCommas(value));
			$('.duration-bar').parent().find('.selected-value .months').text(months);

			//onInit show the mothly installment
			calculator(lease, period);
		},

		// Callback function
		onSlide: function(position, value) {
			if(this.$element.hasClass('lease-amount-bar')) {
				$('.lease-amount-bar').parent().find('.selected-value .amount').text(numberWithCommas(value));

				lease = value;

			}

			if(this.$element.hasClass('duration-bar')) {
				$('.duration-bar').parent().find('.selected-value .months').text(value);

				period = value;

			}

			//onSlide update the monthly installment
			calculator(lease, period);			

		},

		// Callback function
		onSlideEnd: function(position, value) {

		}		
	});


	function calculator(leaseAmount, leaseTerm) {
		var interest = 10; //interest rate
		var leaseInterest = interest/1200;

		var monthlyAmount = (leaseAmount * leaseInterest / (1 - (Math.pow(1/(1 + leaseInterest), leaseTerm)))).toFixed(2);
		var totalAmount = (+monthlyAmount * +leaseTerm).toFixed(2);
		var totalInterest = (+totalAmount - +leaseAmount).toFixed(2);

		$('.monthly-instalment .instalment').text(numberWithCommas(monthlyAmount));
	}

}

//enable calculator
function enableCaculator() {
	$('input[name="total-value-input"]').change(function() {
		$(window).resize();

		if($(this).val() == 'lease') {
			$('.vehicle-import-lease-calculator').show();
		} else {
			$('.vehicle-import-lease-calculator').hide();
		}
	});
}

//scroll fixed column
function scrollFixedColumn() {
	var filterColumn = $('.floating-section .filter-column');
	var resultsColumn = $('.floating-section .results-column');
	var adsColumn = $('.floating-section .ads-column');

	$(window).scroll(function() {
		var wScroll = $(this).scrollTop();

		if($(window).width() > 992) {

			if(wScroll >= $('.results-container').offset().top) {
				filterColumn.addClass('fixed');
				resultsColumn.addClass('fixed');
				// adsColumn.addClass('fixed');
			} else {
				filterColumn.removeClass('fixed');
				resultsColumn.removeClass('fixed');
				// adsColumn.removeClass('fixed');
			}
		}

		if(($(window).width() > 992) && (1200 > $(window).width())) {
			if(wScroll >= $('.results-container').offset().top) {
				adsColumn.addClass('fixed');
			} else {
				adsColumn.removeClass('fixed');
			}
		}
		
	});
}

/* operating lease search bars */
function operatingLeaseSearchBars() {
	$('.operate-lease-bar').rangeslider({
		polyfill: false,
		
		// Callback function
		onInit: function(value) {
			var donwPayment = $('.search-down-payment-bar').val();
			var installment = $('.search-monthly-installment-bar').val();
			var months = $('.search-duration-bar').val();

			period = parseInt(months, 10);

			$('.search-down-payment-bar').parent().find('.selected-value .amount').text(numberWithCommas(donwPayment));
			$('.search-monthly-installment-bar').parent().find('.selected-value .amount').text(numberWithCommas(installment));
			$('.search-duration-bar').parent().find('.selected-value .months').text(months);

		},

		// Callback function
		onSlide: function(position, value) {
			if(this.$element.hasClass('search-down-payment-bar')) {
				$('.search-down-payment-bar').parent().find('.selected-value .amount').text(numberWithCommas(value));
			}

			if(this.$element.hasClass('search-monthly-installment-bar')) {
				$('.search-monthly-installment-bar').parent().find('.selected-value .amount').text(numberWithCommas(value));
			}

			if(this.$element.hasClass('search-duration-bar')) {
				$('.search-duration-bar').parent().find('.selected-value .months').text(value);
			}		

		},

		// Callback function
		onSlideEnd: function(position, value) {

		}		
	});
}

//datepicker
function datePicker() {
	//var currentYear = (new Date).getFullYear();
	var date = new Date();
	var dateFormat = "yy-mm-dd";

	$('.date-from-today').datepicker({
		inline: true,
		dateFormat: dateFormat,
		changeMonth: true,
		minDate: date
	});
}
function vehicleidcheck(){
	var baseurl = 'http://autosure.lk';
	$(document).on('click', '.checkresult', function(){

		 var id= $(this).data('id');
		
		
		var x = $('#resyltInqyry').val(id);
		
	});	
}
function loadCss($body, $stylesheet) {

	//change the path according to your project configuration, When upload to the live server, don't forget to change 'base url'.
	var path = 'http://autosure.lk/assets/frontend/assets/css/' + $stylesheet;

	if($($body).length > 0) {
		$('head').append('<link rel="stylesheet" href="'+ path +'" />');
	}
}



