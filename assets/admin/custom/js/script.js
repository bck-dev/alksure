$(function() {

    dropdown();
// vehiclemodel21();
    vehiclemodel1();
    disableDropdown();
    
    //drop down agent
    dropagent();

    rating();
    //snd back to vehicle list
    adminActionConfirm();

 wheeldropagent();
    adminActionrejectwheel();
    dropdownwheel();
    wheelmodel();
    disableDropdownwheel();
    dropdowntypewheel();
    wheelmodelone();
    disableDropdowwheel();

});

//vehicle brand dependancy drop down

function dropdown(){
    $(document).on('change', '#vehicle_brand', function(){
        //var baseurl = $_BASE_URL;
        var x =$(this).find(':selected').attr('data-id');
        // alert(x);
        $("#select2").children('option').hide();
        // alert()
        $("#select2").children("option[class^=" + x + "]").show();

    });
}

function vehiclemodel1(){
    $(document).on('change', '#select2', function(){
        //var baseurl = $_BASE_URL;
        var x =$(this).find(':selected').attr('data-id');
        $(".result").hide();
        $("." + x ).show();

    });
}

function disableDropdown(){
    $(document).on('change', '#vehicle_brand', function(){
        document.getElementById("select2").disabled = false;

    });

}



function dropagent() {
    $(".agentt").on('change',function() {
        var id = ($(this).val());

        // alert(id);
        $.ajax({
            url :  'http://autosure.lk/vehicle/agentdetails', // my controller :<?php echo base_url(); ?>admin/order/new_order
            method: "POST",
            data: {"a_id":id},
            success: function(response) {
                $('#age').html(response);
                
            }
        })
    });
}


$('.inspected').click(function() {
    var vehical_id =  $(this).data('vehi-id');
    // alert(vehical_id);
    var id = $(this).attr('id');//-->this will alert id of checked checkbox.
    // alert(id);
    if(this.checked){
        var idd = 1;
        $.ajax({
            url: 'http://autosure.lk/vehicle/viewvehicle',
            method: "POST",
            data: {'c_id':idd, 'v_id':vehical_id},
            success: function(response) {
                // handle
                location.reload();
                // alert(vehical_id);
                // alert(idd);
                // alert(response);

            }

        })
    }else{
        var id = 0;
        $.ajax({
            url: 'http://autosure.lk/vehicle/viewvehicle',
            method: "POST",
            data: {'c_id':id, 'v_id':vehical_id},
            success: function(response) {
                // handle
                location.reload();
                // alert(vehical_id);
                // alert(idd);
                // alert(response);

            }

        })
    }
});


$('.published').click(function() {
    var pub_id =  $(this).data('pub-id');
    // alert(pub_id);
    var id = $(this).attr('id');//-->this will alert id of checked checkbox.
    // alert(id);
    if(this.checked){
        var idd = 1;
        $.ajax({
            url: 'http://autosure.lk/vehicle/viewpublised',
            method: "POST",
            data: {'c_id':idd, 'pu_id':pub_id},
            success: function(response) {
                // handle
                location.reload();
                // alert(pub_id);
                // alert(idd);
                // alert(response);

            }

        })
    }else{
        var id = 0;
        $.ajax({
            url: 'http://autosure.lk/vehicle/viewpublised',
            method: "POST",
            data: {'c_id':id, 'pu_id':pub_id},
            success: function(response) {
                // handle
                location.reload();
                // alert(vehical_id);
                // alert(idd);
                // alert(response);

            }

        })
    }
});


function adminActionConfirm($className)
{
    // var baseurl = $_BASE_URL;

    $('.rejectButton').click(function(e) {
        e.preventDefault();
        var id = $(this).data('reject-id');
        var action = $(this).data('action-id');
        var vehicleId = $(this).data('vehicle-id');
        // alert(vehicleId);
        $.ajax({
            url: 'http://autosure.lk/vehicle/changeactionreject',
            type: 'post',
            data: {'id':id, 'action':action, 'vehicleId':vehicleId},
            success: function(msg) {
                location.reload();
            }
        });
    });
}


function rating(){
    var baseurl = $_BASE_URL;
    // rating for sections
    $("#rating_star1").spaceo_rating_widget({
        starLength: '5',
        initialValue: '',
        callbackFunctionName: 'processRating',
        imageDirectory: baseurl + 'assets/admin/images/',
        inputAttr: 'post_id'
    });

    $("#rating_star2").spaceo_rating_widget({
        starLength: '5',
        initialValue: '',
        callbackFunctionName: 'processRating',
        imageDirectory: baseurl + 'assets/admin/images/',
        inputAttr: 'post_id'
    });

    $("#rating_star3").spaceo_rating_widget({
        starLength: '5',
        initialValue: '',
        callbackFunctionName: 'processRating',
        imageDirectory: baseurl + 'assets/admin/images/',
        inputAttr: 'post_id'
    });

    //end rating for sections
}

function processRating(val, attrVal) {
    var star_val = val;
    var post_id = attrVal;
    $('#rating_star' + attrVal).val(star_val);
}

// function modelidpass(){
//     $(document).ready(function(){
//         $('.viewmodal').on('click',function(){
//             var mod_id = $(this).data('id');
//             alert(mod_id);
//             $.ajax({
//                 url: 'http://bckonnect.com/autosurenew/vehicle/viewpublised',
//                 method: "POST",
//                 data: {'vehical_id':mod_id},
//                 success: function(msg) {
//                     // handle
//                     // location.reload();
//                     alert(mod_id);
//                     // alert(idd);
//                     // alert(response);
//
//                 }
//
//             })
//         });
//     });
// }




function wheeldropagent() {
    $(".agenttwheel").on('change',function() {
        var id = ($(this).val());

        // alert(id);
        $.ajax({
            url :  'http://autosure.lk/bike/agentdetail', // my controller :<?php echo base_url(); ?>admin/order/new_order
            method: "POST",
            data: {"a_id":id},
            success: function(response) {
                $('#agen').html(response);
            }
        })
    });
}


function adminActionrejectwheel($className)
{
    // var baseurl = $_BASE_URL;

    $('.rejectwheelButton').click(function(e) {
        e.preventDefault();
        var id = $(this).data('reject-id');
        var action = $(this).data('action-id');
        var vehicleId = $(this).data('vehicle-id');
        // alert(vehicleId);
        $.ajax({
            url: 'http://autosure.lk/bike/changeactionreject',
            type: 'post',
            data: {'id':id, 'action':action, 'vehicleId':vehicleId},
            success: function(msg) {
                alert(action);
                location.reload();
            }
        });
    });
}


$('.inspectedwheel').click(function() {
    var vehical_id =  $(this).data('vehi-id');
    // alert(vehical_id);
    var id = $(this).attr('id');//-->this will alert id of checked checkbox.
    // alert(id);
    if(this.checked){
        var idd = 1;
        $.ajax({
            url: 'http://autosure.lk/bike/viewbikes',
            method: "POST",
            data: {'c_id':idd, 'v_id':vehical_id},
            success: function(response) {
                // handle
                location.reload();
                // alert(vehical_id);
                // alert(idd);
                // alert(response);

            }

        })
    }else{
        var id = 0;
        $.ajax({
            url: 'http://autosure.lk/bike/viewbikes',
            method: "POST",
            data: {'c_id':id, 'v_id':vehical_id},
            success: function(response) {
                // handle
                location.reload();
                // alert(vehical_id);
                // alert(idd);
                // alert(response);

            }

        })
    }
});


$('.wheelpublished').click(function() {
    var pub_id =  $(this).data('pub-id');
    // alert(pub_id);
    var id = $(this).attr('id');//-->this will alert id of checked checkbox.
    // alert(id);
    if(this.checked){
        var idd = 1;
        $.ajax({
            url: 'http://autosure.lk/bike/viewpublised',
            method: "POST",
            data: {'c_id':idd, 'pu_id':pub_id},
            success: function(response) {
                // handle
                location.reload();
                // alert(pub_id);
                // alert(idd);
                // alert(response);

            }

        })
    }else{
        var id = 0;
        $.ajax({
            url: 'http://autosure.lk/bike/viewpublised',
            method: "POST",
            data: {'c_id':id, 'pu_id':pub_id},
            success: function(response) {
                // handle
                location.reload();
                // alert(vehical_id);
                // alert(idd);
                // alert(response);

            }

        })
    }
});


function dropdownwheel(){
    $(document).on('change', '#threewheel_bike_brand_id', function(){
        //var baseurl = $_BASE_URL;
        //alert("yes");
        var x =$(this).find(':selected').attr('data-id');
        // alert(x);
        $("#threewheel_bike_model_id").children('option').hide();
        // alert()
        $("#threewheel_bike_model_id").children("option[class^=" + x + "]").show();

    });
}

function wheelmodel(){
    $(document).on('change', '#threewheel_bike_model_id', function(){
        //var baseurl = $_BASE_URL;
        var x =$(this).find(':selected').attr('data-id');
        $(".result").hide();
        $("." + x ).show();

    });
}

function disableDropdownwheel(){
    $(document).on('change', '#threewheel_bike_brand_id', function(){
        document.getElementById("threewheel_bike_model_id").disabled = false;

    });

}



function dropdowntypewheel(){
    $(document).on('change', '#threewheel_bike_type_id', function(){
        //var baseurl = $_BASE_URL;
        // alert("yes");
        var x =$(this).find(':selected').attr('data-id');
        // alert(x);
        $("#threewheel_bike_brand_id").children('option').hide();
        // alert()
        $("#threewheel_bike_brand_id").children("option[class^=" + x + "]").show();

    });
}

function wheelmodelone(){
    $(document).on('change', '#threewheel_bike_brand_id', function(){
        //var baseurl = $_BASE_URL;
        var x =$(this).find(':selected').attr('data-id');
        $(".result").hide();
        $("." + x ).show();

    });
}

function disableDropdowwheel(){
    $(document).on('change', '#threewheel_bike_type_id', function(){
        document.getElementById("threewheel_bike_brand_id").disabled = false;

    });

}


