$(function() {
    //import lease vehicle slider
	importLeaseVehicleSlider();
	
	//lease calculator
	leaseCalculator();

	//enable lease calculator on lease select
	enableCaculator();

	/* responsive comparison table config initialize */
	var TablesawConfig = {
		i18n: {
			swipePreviousColumn: "The column before",
			swipeNextColumn: "The column after"
		},
		swipe: {
			horizontalThreshold: 45,
			verticalThreshold: 45
		}
	};

	/* operating filter bars */
	operatingLeaseSearchBars();

	/* booking date */
	datePicker();
	
	//scroll fixed filter column
	if($('.floating-section .results-container').length > 0) {
		scrollFixedColumn();
	}

	//popup form validation
	popupValidation();

	//mobile toggle
	mobileToggle();

	//banner slider
	banerSlider();
	
	//wow animation 
	new WOW().init();

	//existing/new user radio button
	radionButton()
	
	// inputfile function
	fileInput();
});

/* import lease vehicle slider */
function importLeaseVehicleSlider() {
	var owl = $('.import-lease-vehicle-slider');

	owl.owlCarousel({
		items: 1,
		margin: 20,
		autoplay: true,
		loop: false,
		animateIn: 'fadeIn',
		animateOut: 'fadeOut',
		thumbs: true,
		thumbsPrerendered: true,
		responsive: {
			0: {
				items: 1
			}
		}			
	});	
}

//price number format
function numberWithCommas(x) {
	var parts = x.toString().split(".");
	parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	return parts.join(".");
}

// lease amount bar
function leaseCalculator() {

	var lease, period;

	$('.lease-amount-bar, .duration-bar').rangeslider({
		polyfill: false,
		
		// Callback function
		onInit: function(value) {
			var value = $('.lease-amount-bar').val();
			var months = $('.duration-bar').val();

			lease = value;
			period = parseInt(months, 10);

			$('.lease-amount-bar').parent().find('.selected-value .amount').text(numberWithCommas(value));
			$('.duration-bar').parent().find('.selected-value .months').text(months);

			//onInit show the mothly installment
			calculator(lease, period);
		},

		// Callback function
		onSlide: function(position, value) {
			if(this.$element.hasClass('lease-amount-bar')) {
				$('.lease-amount-bar').parent().find('.selected-value .amount').text(numberWithCommas(value));

				lease = value;

			}

			if(this.$element.hasClass('duration-bar')) {
				$('.duration-bar').parent().find('.selected-value .months').text(value);

				period = value;

			}

			//onSlide update the monthly installment
			calculator(lease, period);			

		},

		// Callback function
		onSlideEnd: function(position, value) {

		}		
	});


	function calculator(leaseAmount, leaseTerm) {
		var interest = 10; //interest rate
		var leaseInterest = interest/1200;

		var monthlyAmount = (leaseAmount * leaseInterest / (1 - (Math.pow(1/(1 + leaseInterest), leaseTerm)))).toFixed(2);
		var totalAmount = (+monthlyAmount * +leaseTerm).toFixed(2);
		var totalInterest = (+totalAmount - +leaseAmount).toFixed(2);

		$('.monthly-instalment .instalment').text(numberWithCommas(monthlyAmount));
	}

}

//enable calculator
function enableCaculator() {
	$('input[name="total-value-input"]').change(function() {
		$(window).resize();

		if($(this).val() == 'lease') {
			$('.vehicle-import-lease-calculator').show();
		} else {
			$('.vehicle-import-lease-calculator').hide();
		}
	});
}

//scroll fixed column
function scrollFixedColumn() {
	var filterColumn = $('.floating-section .filter-column');
	var resultsColumn = $('.floating-section .results-column');
	var adsColumn = $('.floating-section .ads-column');

	$(window).scroll(function() {
		var wScroll = $(this).scrollTop();
		//var bottom = $(window).height() - $('.results-container').offset().top - $('.results-container').height();

		var bottom = $('.results-container').position().top + $('.results-container').offset().top + $('.results-container').outerHeight(true);

		if($(window).width() > 992) {
			if(wScroll >= $('.results-container').offset().top) {
				filterColumn.addClass('fixed');
				resultsColumn.addClass('fixed');
				// adsColumn.addClass('fixed');
			} else {
				filterColumn.removeClass('fixed');
				filterColumn.css({
					'position': 'static'
				});
				resultsColumn.removeClass('fixed');
				// adsColumn.removeClass('fixed');
			}
		
			if(wScroll > ($('footer').offset().top - $(window).height())) {
				$('.floating-section .filter-column.fixed').css({
					'position': 'absolute',
					'top': $('footer').offset().top - $(window).height() - 500
				});
			} else {
				$('.floating-section .filter-column.fixed').css({
					'position': 'fixed',
					'top': 0
				});
			}
		}

		if(($(window).width() > 992) && (1200 > $(window).width())) {
			if(wScroll >= $('.results-container').offset().top) {
				adsColumn.addClass('fixed');
			} else {
				adsColumn.removeClass('fixed');
			}
		}
		
	});
}

/* operating lease search bars */
function operatingLeaseSearchBars() {
	$('.operate-lease-bar').rangeslider({
		polyfill: false,
		
		// Callback function
		onInit: function(value) {
			var donwPayment = $('.search-down-payment-bar').val();
			var installment = $('.search-monthly-installment-bar').val();
			var months = $('.search-duration-bar').val();

			period = parseInt(months, 10);

			$('.search-down-payment-bar').parent().find('.selected-value .amount').text(numberWithCommas(donwPayment));
			$('.search-monthly-installment-bar').parent().find('.selected-value .amount').text(numberWithCommas(installment));
			$('.search-duration-bar').parent().find('.selected-value .months').text(months);

		},

		// Callback function
		onSlide: function(position, value) {
			if(this.$element.hasClass('search-down-payment-bar')) {
				$('.search-down-payment-bar').parent().find('.selected-value .amount').text(numberWithCommas(value));
			}

			if(this.$element.hasClass('search-monthly-installment-bar')) {
				$('.search-monthly-installment-bar').parent().find('.selected-value .amount').text(numberWithCommas(value));
			}

			if(this.$element.hasClass('search-duration-bar')) {
				$('.search-duration-bar').parent().find('.selected-value .months').text(value);
			}		

		},

		// Callback function
		onSlideEnd: function(position, value) {

		}		
	});
}

//datepicker
function datePicker() {
	//var currentYear = (new Date).getFullYear();
	var date = new Date();
	var dateFormat = "yy-mm-dd";

	$('.date-from-today').datepicker({
		inline: true,
		dateFormat: dateFormat,
		changeMonth: true,
		minDate: date
	});

	from = $( "#from" ).datepicker({
		dateFormat: dateFormat,
		minDate: date,
		changeMonth: true
	}).on( "change", function() {
		to.datepicker( "option", "minDate", getDate( this ) );
	}),
	to = $( "#to" ).datepicker({
		dateFormat: dateFormat,
		minDate: date,
		changeMonth: true
	}).on( "change", function() {
		from.datepicker( "option", "maxDate", getDate( this ) );
	});
}

/* convert string to date */
function getDate( element ) {
	var dateFormat = "yy-mm-dd";
	var date;

	try {
		date = $.datepicker.parseDate( dateFormat, element.value );
	} catch( error ) {
		date = null;
	}
	return date;
}

//popup validation
jQuery.validator.addMethod("phone_number", function(phone_number, element) {
    phone_number = phone_number.replace(/\s+/g, "");
    return this.optional(element) || phone_number.length > 8 && 
    phone_number.match(/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/);
}, "Please specify a valid phone number");

function popupValidation() {
	$(".popup form").each(function() {
		$(this).validate({
			rules: {
				full_name: "required",
				email: {
					required: true,
					email: true
				},
				phone: {
					required: true,
					phone_number: true,
				},
				name: "required"
			},
			submitHandler: function(form) {
				form.submit();
			}
		});
	});
}

//mobile toggle 
function mobileToggle() {
	$('.mobile-toggle-button').click(function() {
		$(this).toggleClass('is-open');
		$('.navigation').slideToggle();
	});

	//active navigation highlight
	loc = location.pathname.split("/")[1];

	$(".navigation li a[href*='" + loc + "']").addClass('active');	
	
	if(loc == '') {
		$(".navigation .bottom-row li").first().find('a').addClass('active');
	}	
}

/* home banner slider */
function banerSlider() {
	var owl = $('.banner-slider');

	owl.owlCarousel({
		items: 1,
		margin: 0,
		smartSpeed: 500,
		autoplay: true,
		loop: true			
	});	
}

/* existing/new user radio button */
function radionButton() {
	$('input[name="user-type"]').on('change', function() {
		var type = $(this).attr('id');

		if(type == 'existing-user') {
			$('.existing-user-block').slideDown('fast');
			$('.new-user-block').slideUp('fast');
		} else {
			$('.new-user-block').slideDown('fast');
			$('.existing-user-block').slideUp('fast');
		}
	});
}

// file input function
function fileInput() {
	$('.inputfile').on('change', function(e) {
		var filename = '';

		if ( this.files && this.files.length > 1 ) {
			filename = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
		} else {
			filename = e.target.value.split('\\').pop();
		}

		if(filename) {
			$(this).parent('.upload-group').find('.input-file-name').text(filename);
		} else {
			$(this).parent('.upload-group').find('.input-file-name').text('no image');
		}
	});
}