$(function() {
	/* expand block initialize */
	//expandBlock();

	/* results filter responsive initialize */
	refineBy();

	/* single vehicle slider initilaize */
	singleVehicleSlider();

    /* popup initialize */
    popup('.make-offer', '#make-offer-popup');
	popup('.open-popup', '#enquire-popup');
	popup('.sign-up', '#signup-popup');
	popup('.sign-in', '#signin-popup');
	popup('.sell', '#seller-popup');
	popup('.register', '#register-popup');
	popup('.lease', '#lease-popup');

	 lease_forms();
   	 enableautolease();
    	disableoutolease();
    	enableotherlease();
    	disableotherlease();
	/* detect mac browsers */
	var mac = navigator.platform.match(/(Mac|iPhone|iPod|iPad)/i) ? true : false;
	if(mac) {
		$('body').addClass('mac-os');
	}

});

/* smooth scroll function */
function smoothScroll(duration) {
	$('a[href^="#"]').on('click', function(event) {
		var target = $( $(this).attr('href') );

		if(target.length) {
			event.preventDefault();
			$('html, body').animate({
				scrollTop: target.offset().top
			}, duration);
		}
	});
}


/* single vehicle slider */
function singleVehicleSlider() {
	var owl = $('.single-vehicle-slider');

	owl.owlCarousel({
		items: 1,
		margin: 20,
		autoplay: true,
		loop: true,
		animateIn: 'fadeIn',
		animateOut: 'fadeOut',
		thumbs: true,
		thumbsPrerendered: true,
		responsive: {
			0: {
				items: 1
			}
		}			
	});	
}


/* expand block function */
function expandBlock() {
	$('.expand-link').click(function(e) {
		e.preventDefault();

		$(this).parent().siblings('.collapse-block').slideToggle();
		$(this).next('.hidden-block').slideToggle();
		$('i', this).toggleClass('fa-angle-down fa-angle-up');
	});
}

/* results filter responsive initialize */
function refineBy() {
	$('.refine-by').click(function(e) {
		e.preventDefault();

		$(this).parent('.filter-column').toggleClass('is-open');
	});
}


/* enquiry popup function */
function popup($link, $popup) {

	$($link).click(function(e) {
		e.preventDefault();

		$($popup + ' form')[0].reset();

		//if navigation dropdown is opened
		if($('.mobile-toggle-button').hasClass('is-open')) {
			$('.mobile-toggle-button').removeClass('is-open');
			$('.navigation').slideUp();
		}

		// $($popup).addClass("wow pluse animated");
		// $($popup).attr("style","visibility: visible; animation-name: pluse;");

		$($popup).css({
			'display': 'flex'
		});

	});

	$($popup).on('click', function(e) {
		if (e.target !== this)
		  return;
		  
		  $(this).hide();
	  });	

	$('.popup-close').click(function() {
		$($popup).hide();
	});
}


function lease_forms(){
    $(document).ready(function(){
        $('.lesebtn').on('click',function(){
        
            $('.formlease').show();
            $('.formlease1').hide();
        });

        $('.autosurerad').on('click',function(){
            if ($('.autosurerad').is(":checked")) {
                $('.formlease').show();
                $('.formlease1').hide();
            }
            else {
                $(".formlease").hide();
                $(".formlease1").hide();
            }
        });

        $('.otherrad').on('click',function(){
            if ($('.otherrad').is(":checked")) {
                $('.formlease1').show();
                $('.formlease').hide();
            }
            else {
                $(".formlease1").hide();
                $(".formlease").hide();
            }
        });
    });
}

function enableautolease() {

    $('.autosurerad').click(function() {
        $(".autoauto").prop("disabled",true);
        if($(this).attr('id') == 'otherr') {
            $(".autoauto").prop("disabled",false);
        }
    });
}

function disableoutolease() {

    $('.otherrad').click(function() {
        $(".autoauto").prop("disabled",true);

    });
}

function enableotherlease() {

    $('.otherrad').click(function() {
        $(".otherother").prop("disabled",true);
        if($(this).attr('id') == 'other') {
            $(".otherother").prop("disabled",false);
        }
    });
}
function disableotherlease() {

    $('.autosurerad').click(function() {
        $(".otherother").prop("disabled",true);

    });
}


$('#submitlease').click(function(){
    var id = $("input[name='aradio']:checked").val();

    if(id=1){

        var down =  $("#downpayment").val();
        var dur =  $("#duration").val();
        var inter =  $("#interest").val();
       
        var step = (1.0+inter);
        
        var step2 = Math.pow(step, dur);

       
        var step3 = (1/(step2));
        var step4 = (1-(step3));
        var tep = (down*inter);
        var total = (tep/(step4));
       

        // alert(total);
        $.ajax({
            url :  'http://autosure.lk/home/totalval', // my controller :<?php echo base_url(); ?>admin/order/new_order
            method: "POST",
            data: {"t_id":total},
            success: function(response) {
                $('#instal').html(response);
                // alert(total);
            }
        })
    }



});

$('#submitleasecal').click(function(){
    var id = $("input[name='aradio']:checked").val();

   
    if(id=2){


        var downn =  $("#downpaymentone").val();
        var durr =  $("#durationone").val();
        var interr =  $("#interestone").val();
        var interest = (interr/1200);
        
        var powr =  (1.0+interest);
        var totall = (downn*interest)/(1 - (1/(Math.pow(powr,durr))));
        
        $.ajax({
            url :  'http://autosure.lk/home/totalval', // my controller :<?php echo base_url(); ?>admin/order/new_order
            method: "POST",
            data: {"t_id":totall},
            success: function(response) {
                $('#instal').html(response);
                
            }
        })
    }




});
